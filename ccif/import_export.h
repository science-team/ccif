/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* Using the standard extern declaration to share variables between modules
 * in libccif, will cause portability problems with VMS. This is because
 * the semantics of C scope/storage class declarations, while slightly 
 * better than Fortran, don't allow a distinction between an entity which 
 * is shared between several modules in a library, but not exported from the
 * library, and one which is global to the project as a whole. Also, for systems
 * which allow several processes to share a library, standard C cannot differentiate
 * between a variable which is shared between the processes, and one which is
 * unique to each process. Different systems (i.e. linkers/loaders) have different
 * defaults <sigh>.
 * 
 * !!!IMPORTANT BIT!!! To share variables between several modules within CCIF,
 * define the variable like:
 * 
 * __export__ __noshare__ int i;  -- For something which can be modified
 * __export__   const     int j;  -- For something which is constant
 * 
 * Then, to use the variable in other modules:
 * 
 * __import__ int i,j;
 * 
 * Using "int i;"/"extern int i;" will cause problems for some!
 * 
 * Also, make sure that each such variable has only one definition (good practice).
 * With DEC C under VMS, this is actually enforced by the pragma extern_model.
 */

#ifndef IMPORT_EXPORT_H
#define IMPORT_EXPORT_H

#if !defined (VMS) || defined (__DECC)

#ifdef VMS
#pragma extern_model strict_refdef   /* Implies the NOSHR attribute for PSECT $DATA$ */
#endif /* VMS */

/* All unix, and DEC C under VMS */
#define __export__
#define __import__  extern
#define __noshare__

#else

/* VAX C under VMS only */
#define __export__   globaldef
#define __import__   globalref
#define __noshare__  noshare

#endif


#endif /* IMPORT_EXPORT_H */

/* $Id$ */
/*
 * errsupport.c -- error support code for SORCERER output
 *
 * Define your own or compile and link this in.
 *
 * Terence Parr
 * U of MN, AHPCRC
 * February 1994
 */

/* Modified for CCIF (signal mechanism, basically)
 * Peter Keller */

/* Include this in every sorcerer grammar file */

#ifndef S_ERRSUPPORT_C
#define S_ERRSUPPORT_C

#include "condition.h"

static char sor_errpre[] = "Sorcerer error module";

void
#ifdef __USE_PROTOS
mismatched_range( STreeParser *_parser, int looking_for, int upper_token, SORAST *found )
#else
mismatched_range( _parser, looking_for, upper_token, found )
int looking_for;
int upper_token;
SORAST *found;
STreeParser *_parser;
#endif
{
   if ( found!=NULL ) 
     ccif_signal(SOR_NOTINRANGE, sor_errpre, NULL, NULL, 0, looking_for, upper_token, found->token);

   else 
     ccif_signal(SOR_NULL, sor_errpre, NULL, NULL, 0, looking_for, upper_token);

}

void
#ifdef __USE_PROTOS
missing_wildcard(STreeParser *_parser)
#else
missing_wildcard(_parser)
STreeParser *_parser;
#endif
{
	ccif_signal(SOR_NOTWILD, sor_errpre, NULL, NULL, 0);
}

void
#ifdef __USE_PROTOS
mismatched_token( STreeParser *_parser, int looking_for, SORAST *found )
#else
mismatched_token( _parser, looking_for, found )
int looking_for;
SORAST *found;
STreeParser *_parser;
#endif
{
	if ( found!=NULL ) 
	   ccif_signal(SOR_WRONGTOK, sor_errpre, NULL, NULL, 0,
				looking_for,
				found->token);
	else 
	   ccif_signal(SOR_NULL2, sor_errpre, NULL, NULL, 0,
				looking_for);
}

void
#ifdef __USE_PROTOS
no_viable_alt( STreeParser *_parser, char *rulename, SORAST *root )
#else
no_viable_alt( _parser, rulename, root )
char *rulename;
SORAST *root;
STreeParser *_parser;
#endif
{
   if ( root==NULL )
     ccif_signal(SOR_NOVIALT, sor_errpre, NULL, NULL, 0, rulename);
   else
     ccif_signal(SOR_NOVIALT, sor_errpre, NULL, NULL, 0, rulename);
}

void
#ifdef __USE_PROTOS
sorcerer_panic(char *err)
#else
sorcerer_panic(err)
char *err;
#endif
{
   ccif_signal(SOR_PANIC, sor_errpre, NULL, NULL, 0, err);
}

#endif


/*
 * S O R C E R E R  T r a n s l a t i o n  H e a d e r
 *
 * SORCERER Developed by Terence Parr, Aaron Sawdey, & Gary Funck
 * Parr Research Corporation, Intrepid Technology, University of Minnesota
 * 1992-1994
 * SORCERER Version 13322
 */
#define SORCERER_VERSION	13322
#define SORCERER_NONTRANSFORM
#include "pcctscfg.h"
#include <stdio.h>
#include <setjmp.h>
/* rename error routines; used in macros, must use /lib/cpp */
#define mismatched_token ccif_print_mismatched_token
#define mismatched_range ccif_print_mismatched_range
#define missing_wildcard ccif_print_missing_wildcard
#define no_viable_alt ccif_print_no_viable_alt
#define sorcerer_panic ccif_print_sorcerer_panic

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define tokens_h   /* Inhibit declaration of tokens from tokens.h */
#include "stdpccts.h"

#include "astlib.h"
#include "sorcerer.h"
#include "tokdefs.h"
extern void ccif_print_data(STreeParser *_parser, SORAST **_root);
extern void ccif_print_star_file(STreeParser *_parser, SORAST **_root,  const int cifunit );

#ifndef _MSC_VER
#ifndef __GNUC__
void
#ifdef __USE_PROTOS
_refvar_inits(STreeParser *p)
#else
_refvar_inits(p)
STreeParser *p;
#endif
{
}
#endif
#endif



#include "s_errsupport.c"

static FILE *cifout;
static char line[ZZLEXBUFSIZE+1], *start_line, *end_line, *line_cursor;
/* line_cursor points to the character after the last character written */

static int blank_lines_out = 0;
static int lines_out = 1;      /* Keep track of output file line number */
static Attrib *last_error;    /* Build linked list of attributes with errors */


static void make_gap(const int nlines);
static void flush_line(const Attrib a);
static void print_attrib(const AST * const node, const int col);
static void print_loop_row(const AST * const node);
static void prt_error_rept_callback(char callback_msg[], 
void ** ptr, int *count, int first );

/* Ensure that at least nlines blank lines have been output since 
the last bit of text.
Can use make_gap(0) to force a new line
*/
static void make_gap(const int nlines) {
	int i, blanks_so_far;
	flush_line(NULL);
	blanks_so_far = blank_lines_out;
	for ( i = 0; i < nlines - blanks_so_far; i++ ) {
		putc('\n',cifout);
		blank_lines_out++;
		lines_out++;
	}
}

/* Make sure that there is nothing in line[] waiting to be printed out. */
static void flush_line(const Attrib a) {
	int i;
	*( MAX( MIN( end_line, line_cursor ), start_line) ) = '\0';
	if ( line[0] ) {		       /* Don't print out a blank line */
		fprintf(cifout, "%s\n", start_line);
		lines_out++;
	}
	blank_lines_out = 0;
	
   memset (line, ' ', ZZLEXBUFSIZE);
	line[ZZLEXBUFSIZE] = '\0';
	start_line = line;
	line_cursor = line - 1;

	if ( a && a->line > 0 && a->prev && a->prev->line > 0 )
	make_gap( a->line - a->prev->line - 1 );
}

/* Print out text of attrib into buffer, ready for output.
* If col >= 0, overrides column stored in attrib. (col == 0 implies
* output as far to the left as possible. This applies only to the
* attrib of node, not to preceding comments */

static void print_attrib(const AST * const node, const int col) {
Attrib a = node->a;
char *text;
int col_out = col < 0 ? a->col : col;
int line_flushed;

if ( !a ) return;

   while ( a->prev && a->prev->text[0] == '#' )  /* Back up to first STAR comment associated */
a = a->prev; 		                 /* with this AST node  */

   do {                                       /* while ( a != node->a && (a = a->next) ) */

col_out = a->col;
if ( a == node->a && col >= 0 )
col_out =  col;

if ( a->status == error ) {
*last_error = a;
last_error = &(a->err);
a->output_line = lines_out;   /* Only need this for error reporting at the moment */
}

      if ( a->text[0] == ';' && a->col < 0 ) {  /* semi-colon bounded text string */
if ( line_cursor + 1 != line ) 
flush_line(a);

	 make_gap(1);
fputs(a->text, cifout);
make_gap(1);

	 blank_lines_out = 0;
lines_out += abs(a->col);
start_line = line + 1;
line_cursor = line + 1;  /* Allow for printing the final ';' */
continue;
}

      if ( col_out > 0 ) { 	       /* A column is specified */

/*	fprintf(stderr, "col_out %d; text: %s; line end offset: %d; hard end offset: %d", 
col_out, a->text, line_cursor  - line, end_line - line ); */
line_flushed = 0;

	if ( line + col_out - 2 < line_cursor ) { /* Already gone past specified position */
flush_line(a);
line_flushed = 1;
}

line_cursor = MAX( line_cursor + 1, line + col_out - 1 );

if ( line_cursor + a->length + 1 > end_line ) { /* No more room on line */
if ( !line_flushed ) 
flush_line(a);
line_cursor = line; /* Force start at beginning of line */
col_out = 1;
}      
/*	 fprintf(stderr," line end offset: %d\n", line_cursor  - line ); */
}
else {			       /* No column specified */
int length = strlen(a->text);
if ( line_cursor + length + 1 > end_line )   /* No more room on this line */
flush_line(a);
line_cursor = MAX( line_cursor + 1, line );
}
text = a->text;
if ( text[0] == ' ' ) text++;    /* May have been padded - get width right */
strncpy(line_cursor, text, strlen(text) );     /* Copy text to output buffer */

#if 0   /* Not sure about this - if text was output left-justified in a fixed-width,
* non-quoted, field, probably don't want to trample over right-hand spaces*/

/* Advance line_cursor, then backup to first space character (there may be trailing
spaces in text) */
for ( line_cursor += strlen(text); 
*(line_cursor-1) == ' ' || *(line_cursor-1) == '\t';
line_cursor-- ) ;
#else
/* Here, just allow two 'fields' to join together, 
* if the first ends in at least one space */
line_cursor += strlen(text);
if ( *(line_cursor-1) == ' ')
line_cursor--;
#endif /* 0 */

   } while ( a != node->a && (a = a->next) );
}


static void print_loop_row(const AST * const node) {
const AST * ptr;
flush_line(NULL);
for ( ptr = node->right; ptr; ptr = ptr->right )
print_attrib(ptr, -1);
}

static void print_error_report(CIF_FILE_LIST *entry) {

   Attrib err = entry->data_blocks->err, *err_ptr = &(entry->data_blocks->err);
static char *sfmt="Line %d: %s\n", *lfmt="Line %d: %.50s.....\n";

   if ( err ) 
ccif_signal(CCIF_BADOUTPUT, NULL, 
prt_error_rept_callback, (void *) err_ptr, 0, entry->filename);

#if 0
while (err) {
printf( (err->length > 50 ? lfmt : sfmt), err->output_line, err->text);
*err_ptr = NULL; 		       /* Cleans up list after itself, so that  */
err_ptr = &(err->err);	       /* attribs which are shared between nodes */
err = err->err;		       /* don't confuse the next lot of output. */
}
#endif
}

static void prt_error_rept_callback(char callback_msg[], 
void ** ptr, int *count, int first ) {

static const char *sfmt="Line %d: %s\n", *lfmt="Line %d: %.50s.....\n";
Attrib *err_ptr = *((Attrib **) ptr), err =  *err_ptr;

if ( first ) {
strcpy(callback_msg, "The affected tokens are:");
return;
}

if ( !err ) {
callback_msg[0] = '\0';
return;
}

sprintf(callback_msg, (err->length > 50 ? lfmt : sfmt), err->output_line, err->text);
*err_ptr = NULL; 		       /* Cleans up list after itself, so that  */
*ptr = (void *) &(err->err);	       /* attribs which are shared between nodes */
err = err->err;		       /* don't confuse the next lot of output. */

   return;
}


void ccif_print_data(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	SORAST *dn=NULL;
	SORAST *dv=NULL;
	SORAST *l=NULL;
	SORAST *dnr=NULL;
	SORAST *r=NULL;
	SORAST *dnv=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Data_item) ) {
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Data_item);
		dn=(SORAST *)_t;
		_DOWN;
		_MATCHRANGE(Non_quoted_text_string,Line_of_text);
		dv=(SORAST *)_t;
		_RIGHT;
		if ( !_parser->guessing ) {
		print_attrib(dn,1); print_attrib(dv,-1);   
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
	_GUESS;
	if ( !_gv && _t!=NULL && (_t->token==Loop) ) {
		Sym *item, *cat; char cat_fmt; int row_col, this_row_col;   
		{_SAVE; TREE_CONSTR_PTRS;
		if ( _t!=NULL && (_t->token==Loop) ) {
			{_SAVE; TREE_CONSTR_PTRS;
			_MATCH(Loop);
			_DOWN;
			_WILDCARD;
			_RESTORE;
			}
			_RIGHT;
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "data", _t);
		}
		_RESTORE;
		}
		_GUESS_DONE;
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Loop);
		l=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		make_gap(2); 
		print_attrib(l,1); 
		item = zzs_get(l->down->down->a->text);
		cat = item ? item->category.rec : NULL;
		cat_fmt = cat && cat->fmt ? cat->fmt[0] : '\0';
		if ( cat_fmt == '\n' ) {
			row_col = 1;      /* Force new line for packet */
			this_row_col = 0; /* Re-format rest of packet */
		}
		else if (cat_fmt == ' ') {
			row_col = 0;       /* Suppress new line for packet */
			this_row_col = 0;  /* Re-format rest of packet */
		}
		else {
			row_col = -1; /* Leave as in attrib */
			this_row_col = -1;
		}
		}
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Row);
		_DOWN;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Data_name) ) {
				_MATCH(Data_name);
				dnr=(SORAST *)_t;
				_RIGHT;
				if ( !_parser->guessing ) {
				print_attrib (dnr,1);   
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		make_gap(0);  
		}
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Row) ) {
				{_SAVE; TREE_CONSTR_PTRS;
				_MATCH(Row);
				r=(SORAST *)_t;
				_DOWN;
				if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
				_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
					_MATCHRANGE(Non_quoted_text_string,Line_of_text);
					dnv=(SORAST *)_t;
					_RIGHT;
					if ( !_parser->guessing ) {
					print_attrib (dnv, row_col);   
					}
				}
				else {
					if ( _parser->guessing ) _GUESS_FAIL;
					no_viable_alt(_parser, "data", _t);
				}
				{int _done=0;
				while ( !_done ) {
					if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
					_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
						_MATCHRANGE(Non_quoted_text_string,Line_of_text);
						dnv=(SORAST *)_t;
						_RIGHT;
						if ( !_parser->guessing ) {
						print_attrib (dnv, this_row_col);   
						}
					}
					else {
					if ( _t==NULL ) {
						_done = 1;
					}
					else {
						if ( _parser->guessing ) _GUESS_FAIL;
						no_viable_alt(_parser, "data", _t);
					}
					}
				}
				}
				_RESTORE;
				}
				_RIGHT;
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		make_gap(1);  
		}
	}
	else {
	if ( _parser->guessing ) _GUESS_DONE;
	if ( _t!=NULL && (_t->token==Loop) ) {
		_MATCH(Loop);
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "data", _t);
	}
	}
	}
	*_root = _t;
}

void ccif_print_star_file(STreeParser *_parser, SORAST **_root,  const int cifunit )
{
	SORAST *_t = *_root;
	SORAST *sf=NULL;
	SORAST *dh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Star_file) ) {
		char *filename, *iofilename ;
		CIF_FILE_LIST file_list_entry = ccif_file_list_entry(cifunit);
		filename = file_list_entry.filename;
#ifndef VMS
		iofilename = filename;
#else
		iofilename = file_list_entry.name;
		/* Make sure that a DECnet file has the correct record attributes, by
		deleting and re-recreating it (I have no documentation on QIO services
		available for the NET: device) */
		if (file_list_entry.decnet) {
			errno = 0;  /* EXIT_ON_SYS_ERROR uses this */
			if ( (unlink(iofilename) == -1) ) {
				sprintf(errmsg,"Cannot unlink %s!",filename);
				EXIT_ON_SYS_ERROR("ccif_print_star_file", errmsg)
			}
			cifout = fopen (iofilename,"w");
		}
		else
#endif
		cifout = fopen (iofilename, "r+" );
		
       if ( !cifout ) 
		ccif_signal(CCIF_FOPEN, "ccif_print_star_file", NULL, NULL, 0, filename, "writing");

		last_error = &(file_list_entry.data_blocks->err);
		start_line=line;
		end_line=line+file_list_entry.line_limit;
		line_cursor=line-1;
		/*       memset(line, ' ',end_line - line);
		line[end_line - line] = '\0'; */
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Star_file);
		sf=(SORAST *)_t;
		_DOWN;
		if ( _t!=NULL && (_t->token==Comment) ) {
			_MATCH(Comment);
			_RIGHT;
		}
		else {
		if ( _t==NULL || (_t->token==Data_heading) ) {
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "star_file", _t);
		}
		}
		{int _done=0;
		while ( !_done ) {
			if ( _t!=NULL && (_t->token==Data_heading) ) {
				{_SAVE; TREE_CONSTR_PTRS;
				_MATCH(Data_heading);
				dh=(SORAST *)_t;
				_DOWN;
				if ( !_parser->guessing ) {
				print_attrib(dh,1);   
				}
				{int _done=0;
				do {
					if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
						ccif_print_data(_parser, &_t);
					}
					else {
					if ( _t==NULL ) {
						_done = 1;
					}
					else {
						if ( _parser->guessing ) _GUESS_FAIL;
						no_viable_alt(_parser, "star_file", _t);
					}
					}
				} while ( !_done );
				}
				_RESTORE;
				}
				_RIGHT;
				if ( !_parser->guessing ) {
				flush_line(NULL);  make_gap(3);   
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "star_file", _t);
			}
			}
		}
		}
		if ( !_parser->guessing ) {
		if ( line_cursor + 1 != line ) 
		flush_line(NULL);
		fclose(cifout);
		print_error_report(&file_list_entry);
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "star_file", _t);
	}
	*_root = _t;
}

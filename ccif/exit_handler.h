/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#ifndef EXIT_HANDER_H
#define EXIT_HANDER_H

__import__ char errmsg[];	

void ccif_exit(int status, int line, const char *file);
#ifdef exit
#undef exit
#endif
#define exit(status) ccif_exit(status, __LINE__, __FILE__)


#ifdef VMS

/* N.B. (1) Don't try to inline the call to strerror under VMS, because the 'VMS form'
*  strerror(errno, vaxc$errno) won't compile with DEC C under /STANDARD=ANSI89 
*
*  N.B. (2) EXIT_ON_SYS_ERROR is a bit of a misnomer under VMS, since the strerror/perror
*  style stuff is only valid for errors returned from the VAX C or DEC C rtl.
*  See the ERR macro in CCIF_VMS.C, for the general method for handling errors
*  returned from system services, library services, RMS, etc. The ERR macro is VMS-specific,
*  so it should not be invoked from any file other than CCIF_VMS.C
*/

#define EXIT_ON_SYS_ERROR(pre, msg) ccif_exit_c_rtl_error(pre, msg);
extern void ccif_exit_c_rtl_error(char *, char *);

#else  /* VMS */

#define EXIT_ON_SYS_ERROR(pre,msg) ccif_signal(CCIF_SYSERR, pre, NULL, NULL, 0, msg, strerror(errno));

#endif /* VMS */


#endif /* EXIT_HANDLER_H */

/*
 * S O R C E R E R  T r a n s l a t i o n  H e a d e r
 *
 * SORCERER Developed by Terence Parr, Aaron Sawdey, & Gary Funck
 * Parr Research Corporation, Intrepid Technology, University of Minnesota
 * 1992-1994
 * SORCERER Version 13322
 */
#define SORCERER_VERSION	13322
#define SORCERER_NONTRANSFORM
#include "pcctscfg.h"
#include <stdio.h>
#include <setjmp.h>
/* rename error routines; used in macros, must use /lib/cpp */
#define mismatched_token ccif_dot_mismatched_token
#define mismatched_range ccif_dot_mismatched_range
#define missing_wildcard ccif_dot_missing_wildcard
#define no_viable_alt ccif_dot_no_viable_alt
#define sorcerer_panic ccif_dot_sorcerer_panic

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define tokens_h   /* Inhibit declaration of tokens from tokens.h */
#include "stdpccts.h"

#include "sorlist.h"

#include "sorcerer.h"
#include "tokdefs.h"
extern void ccif_dot_save_frame(STreeParser *_parser, SORAST **_root,  const char * const cell_name  );
extern void ccif_dot_data(STreeParser *_parser, SORAST **_root, int block_level, const char * const cell_name, const char * const edge_wt );
extern void ccif_dot_global_block(STreeParser *_parser, SORAST **_root);
extern void ccif_dot_data_block(STreeParser *_parser, SORAST **_root);
extern void ccif_dot_star_file(STreeParser *_parser, SORAST **_root,  FILE *file  );

#ifndef _MSC_VER
#ifndef __GNUC__
void
#ifdef __USE_PROTOS
_refvar_inits(STreeParser *p)
#else
_refvar_inits(p)
STreeParser *p;
#endif
{
}
#endif
#endif

#include "s_errsupport.c"


__import__ int ccif_number_of_blocks;

static int ndict_start, ndict_end;

static char last_default_value[MAXCLLEN+1];

static FILE *dot_file;
static AST *current_data_block, *current_save_frame;

#define DOT_OUT1(f,a)               fprintf(f,"%s", a);
#define DOT_OUT2(f,a,b)             fprintf(f,"%s%s", a, b);
#define DOT_OUT3(f,a,b,c)           fprintf(f, "%s%s%s", a, b, c);
#define DOT_OUT4(f,a,b,c,d)         fprintf(f, "%s%s%s%s", a, b, c, d);
#define DOT_OUT5(f,a,b,c,d,e)       fprintf(f, "%s%s%s%s%s", a, b, c, d, e);
#define DOT_OUT7(f,a,b,c,d,e,g,h)   fprintf(f, "%s%s%s%s%s%s%s", a, b, c, d, e, g, h);


static void gen_uniq_id(const char * const prefix, char * const id, const int maxlen) {
	static int s = 0;
	static char id_suffix[100];

	sprintf(id_suffix, "%d", s++);
	if (prefix && ( strlen(prefix)+strlen(id_suffix) < maxlen - 1 ) ) {
		sprintf(id, "%s_%s", prefix, id_suffix);
	}
	else {
		sprintf(id, "n%s", id_suffix);
	}
	return;
}


  

void ccif_dot_save_frame(STreeParser *_parser, SORAST **_root,  const char * const cell_name  )
{
	SORAST *_t = *_root;
	SORAST *sh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Save_heading) ) {
		char save_node_name[100], save_sub_name[100];   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Save_heading);
		sh=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		current_save_frame = sh; 
		gen_uniq_id(sh->a->text, save_node_name, 100);
		gen_uniq_id("cluster", save_sub_name, 100);
		DOT_OUT5(dot_file, "\t\"", cell_name, "\" -> \"", save_node_name, \
		"\" [ weight = 0 ]  ; \n");
		DOT_OUT3(dot_file, "\tsubgraph \"",save_sub_name, "\" {\n");
			DOT_OUT5(dot_file, "\t\"", save_node_name, "\"[label = \"", \
			sh->a->text,"\"] ; \n");
		}
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
				ccif_dot_data(_parser, &_t,  0, save_node_name, "2" );
			}
			else {
			if ( _t!=NULL && (_t->token==Save) ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "save_frame", _t);
			}
			}
		} while ( !_done );
		}
		_MATCH(Save);
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		DOT_OUT1(dot_file, "\t};\n");   
		}
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "save_frame", _t);
	}
	*_root = _t;
}

void ccif_dot_data(STreeParser *_parser, SORAST **_root, int block_level, const char * const cell_name, const char * const edge_wt )
{
	SORAST *_t = *_root;
	SORAST *dn=NULL;
	SORAST *dv=NULL;
	SORAST *l=NULL;
	SORAST *r1=NULL;
	SORAST *dnl=NULL;
	SORAST *dvl=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Data_item) ) {
		char data_value[31], dv_node_name[100], dn_node_name[100];   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Data_item);
		dn=(SORAST *)_t;
		_DOWN;
		_MATCHRANGE(Non_quoted_text_string,Line_of_text);
		dv=(SORAST *)_t;
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		gen_uniq_id(dn->a->text, dn_node_name, 100);
		gen_uniq_id("data_value", dv_node_name, 100);
		if ( dv->token == Line_of_text ) {
			strcpy(data_value, "Multi-line text");
		}
		else {
			strcpy( &(data_value[27]), "...");
			strncpy( data_value, dv->a->text, 27);
		}
		
                                         DOT_OUT7(dot_file, "\t\"", cell_name, "\" -> \"", dn_node_name, \
		"\" [ weight=", edge_wt, "]; \n");
		DOT_OUT5(dot_file, "\t  \"", dn_node_name, "\"[label = \"", dn->a->text, "\"] ; \n");
		DOT_OUT5(dot_file, "\t\"", dn_node_name, "\" -> \"", dv_node_name, "\"; \n");
		DOT_OUT5(dot_file, "\t  \"", dv_node_name, "\"[label = \"",data_value, "\"] ; \n");
		}
	}
	else {
	_GUESS;
	if ( !_gv && _t!=NULL && (_t->token==Loop) ) {
		int loop_offset=0; char loop_node_name[30], row_node_name[30], val_node_name[30], loop_value[30];    
		{_SAVE; TREE_CONSTR_PTRS;
		if ( _t!=NULL && (_t->token==Loop) ) {
			{_SAVE; TREE_CONSTR_PTRS;
			_MATCH(Loop);
			_DOWN;
			_WILDCARD;
			_RESTORE;
			}
			_RIGHT;
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "data", _t);
		}
		_RESTORE;
		}
		_GUESS_DONE;
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Loop);
		l=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		gen_uniq_id("loop_", loop_node_name, 30);
		DOT_OUT7(dot_file, "\t\"", cell_name, "\" -> \"", loop_node_name, \
		"\" [ weight=", edge_wt,  "]; \n");
		DOT_OUT3(dot_file, "\t  \"", loop_node_name, "\"[label = \"loop_\"] ; \n");
		}
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Row);
		r1=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		gen_uniq_id("row",row_node_name, 30);
		DOT_OUT5(dot_file, "\t\"", loop_node_name, "\" -> \"", row_node_name, "\" ; \n");
		DOT_OUT3(dot_file, "\t  \"", row_node_name, "\"[label = \"Row\"] ; \n");
		}
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Data_name) ) {
				_MATCH(Data_name);
				dnl=(SORAST *)_t;
				_RIGHT;
				if ( !_parser->guessing ) {
				gen_uniq_id(dnl->a->text, val_node_name, 30);
				DOT_OUT5(dot_file, "\t\"", row_node_name, "\" -> \"", val_node_name, "\"; \n");
				DOT_OUT5(dot_file, "\t  \"", val_node_name, "\"[label = \"", dnl->a->text, "\"] ; \n");
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Row) ) {
				{_SAVE; TREE_CONSTR_PTRS;
				_MATCH(Row);
				_DOWN;
				if ( !_parser->guessing ) {
				gen_uniq_id("row",row_node_name, 30);
				DOT_OUT5(dot_file, "\t\"", loop_node_name, "\" -> \"", row_node_name, "\" ; \n");
				DOT_OUT3(dot_file, "\t  \"", row_node_name, "\"[label = \"Row\"] ; \n");
				}
				{int _done=0;
				do {
					if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
					_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
						_MATCHRANGE(Non_quoted_text_string,Line_of_text);
						dvl=(SORAST *)_t;
						_RIGHT;
						if ( !_parser->guessing ) {
						gen_uniq_id(dvl->a->text, val_node_name, 30);
						DOT_OUT5(dot_file, "\t\"", row_node_name, "\" -> \"", val_node_name, "\"; \n");
						DOT_OUT5(dot_file, "\t  \"", val_node_name, "\"[label = \"", dvl->a->text, "\"] ; \n");
						}
					}
					else {
					if ( _t==NULL ) {
						_done = 1;
					}
					else {
						if ( _parser->guessing ) _GUESS_FAIL;
						no_viable_alt(_parser, "data", _t);
					}
					}
				} while ( !_done );
				}
				_RESTORE;
				}
				_RIGHT;
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
	if ( _parser->guessing ) _GUESS_DONE;
	if ( _t!=NULL && (_t->token==Loop) ) {
		_MATCH(Loop);
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "data", _t);
	}
	}
	}
	*_root = _t;
}

void ccif_dot_global_block(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	SORAST *gh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Global_heading) ) {
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Global_heading);
		gh=(SORAST *)_t;
		_DOWN;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
				ccif_dot_data(_parser, &_t,  2, "global_", "0" );
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "global_block", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "global_block", _t);
	}
	*_root = _t;
}

void ccif_dot_data_block(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	SORAST *dh=NULL;
	SORAST *sf=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Data_heading) ) {
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Data_heading);
		dh=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		current_data_block = dh;   
		}
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
				ccif_dot_data(_parser, &_t,  1, dh->a->text, "3" );
			}
			else {
			if ( _t!=NULL && (_t->token==Save_heading) ) {
				sf=(SORAST *)_t; ccif_dot_save_frame(_parser, &_t,   dh->a->text  );
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data_block", _t);
			}
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "data_block", _t);
	}
	*_root = _t;
}

void ccif_dot_star_file(STreeParser *_parser, SORAST **_root,  FILE *file  )
{
	SORAST *_t = *_root;
	SORAST *db=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Star_file) ) {
		dot_file = file;   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Star_file);
		_DOWN;
		if ( !_parser->guessing ) {
		DOT_OUT1(dot_file, 
		"digraph SF {\n  center=yes;\n  orientation=landscape;\n  node [ shape=plaintext ]\n");   
		}
		if ( _t!=NULL && (_t->token==Comment) ) {
			_MATCH(Comment);
			_RIGHT;
		}
		else {
		if ( _t==NULL || (_t->token==Data_heading||_t->token==Global_heading) ) {
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "star_file", _t);
		}
		}
		{int _done=0;
		while ( !_done ) {
			if ( _t!=NULL && (_t->token==Global_heading) ) {
				ccif_dot_global_block(_parser, &_t);
				if ( !_parser->guessing ) {
				DOT_OUT1(dot_file, "\tStar_file -> global_ ;\n");   
				}
			}
			else {
			if ( _t!=NULL && (_t->token==Data_heading) ) {
				db=(SORAST *)_t; ccif_dot_data_block(_parser, &_t);
				if ( !_parser->guessing ) {
				DOT_OUT3(dot_file, "\tStar_file -> \"",db->a->text, "\" ;\n");   
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "star_file", _t);
			}
			}
			}
		}
		}
		if ( !_parser->guessing ) {
		DOT_OUT1(dot_file, "}\n");   
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "star_file", _t);
	}
	*_root = _t;
}

/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define MAX_MAPPED_FILES 10

typedef struct _map_file_record {
  char *start;

#ifdef VMS
/* start and end are ajacent, so that they can be used as the
   2-element array required for mapping and unmapping services */

  char *end;
  unsigned short int channel;
  int mapped;   /* As opposed to fread-ed into malloc-ed memory */
#else
   FILE *fileptr;
   ino_t st_ino;
   dev_t st_dev;
#endif /* VMS */

#if defined (__convex__) || defined (__convexc__)
  unsigned length;
#else
  size_t length;
#endif


  int nnodes;
  char *filename;

} MAP_FILE_RECORD;


#ifdef VMS

#define _MAPPED_FILE_INFO    { int _m_info; \
  for (_m_info=0; _m_info < MAX_MAPPED_FILES; _m_info++) { \
    if ( ccif_map_file_list[_m_info] ) { \
      if ( ccif_map_file_list[_m_info]->mapped ) { \
         printf("Mapped file %d (name %s): starting address 0x%x, ending address 0x%x, channel %d, nodes %d\n", \
	     _m_info, ccif_map_file_list[_m_info]->filename, ccif_map_file_list[_m_info]->start, \
             ccif_map_file_list[_m_info]->end, ccif_map_file_list[_m_info]->channel, \
             ccif_map_file_list[_m_info]->nnodes); \
      } \
      else { \
         printf("Mapped/malloc-ed file %d (name %s): starting address 0x%x, length %d, nodes %d\n", \
	     _m_info, ccif_map_file_list[_m_info]->filename, ccif_map_file_list[_m_info]->start, \
             ccif_map_file_list[_m_info]->length, ccif_map_file_list[_m_info]->nnodes); \
      } \
    } \
  } \
} 

#else

#define _MAPPED_FILE_INFO    { int _m_info; \
  for (_m_info=0; _m_info < MAX_MAPPED_FILES; _m_info++) { \
    if ( ccif_map_file_list[_m_info] ) { \
      printf("Mapped file %d (name %s): starting address 0x%x, length %d, nodes %d\n", \
	     _m_info, ccif_map_file_list[_m_info]->filename, ccif_map_file_list[_m_info]->start, \
             ccif_map_file_list[_m_info]->length, ccif_map_file_list[_m_info]->nnodes); \
    }\
  } \
}

#endif

#ifdef VMS
extern int ccif_vms_munmap( char ** const addr,
                              const unsigned short int channel);
#endif


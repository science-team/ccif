/*
 * S O R C E R E R  T r a n s l a t i o n  H e a d e r
 *
 * SORCERER Developed by Terence Parr, Aaron Sawdey, & Gary Funck
 * Parr Research Corporation, Intrepid Technology, University of Minnesota
 * 1992-1994
 * SORCERER Version 13322
 */
#define SORCERER_VERSION	13322
#define SORCERER_NONTRANSFORM
#include "pcctscfg.h"
#include <stdio.h>
#include <setjmp.h>
/* rename error routines; used in macros, must use /lib/cpp */
#define mismatched_token sor_mismatched_token
#define mismatched_range sor_mismatched_range
#define missing_wildcard sor_missing_wildcard
#define no_viable_alt sor_no_viable_alt
#define sorcerer_panic sor_sorcerer_panic

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define tokens_h   /* Inhibit declaration of tokens from tokens.h */
#include "stdpccts.h"

#include "sorlist.h"

#include "sorcerer.h"
#include "tokdefs.h"
extern void sor_save_frame(STreeParser *_parser, SORAST **_root);
extern void sor_data(STreeParser *_parser, SORAST **_root, int block_level );
extern void sor_global_block(STreeParser *_parser, SORAST **_root);
extern void sor_data_block(STreeParser *_parser, SORAST **_root);
extern void sor_star_file(STreeParser *_parser, SORAST **_root);

#ifndef _MSC_VER
#ifndef __GNUC__
void
#ifdef __USE_PROTOS
_refvar_inits(STreeParser *p)
#else
_refvar_inits(p)
STreeParser *p;
#endif
{
}
#endif
#endif



#include "s_errsupport.c"


__import__ int ccif_number_of_blocks;

static int ndict_start, ndict_end;

static char last_default_value[MAXCLLEN+1];


/* These are global, so that zzs_dump can get at them */
__import__ SList *ccif_item_type_list[], *ccif_dictionary[];

static int pattern_line;  /* So that add_constr_to_type_list can share the line number of the pattern with end_type_entry,
for error reporting */

static Sym *related_name; /* Set by add_related_name, and used by add_function_code */
static Sym *dictionary_scope, *save_frame_scope;

static AST *current_save_frame, *current_data_block; /* Used for implicitly-determined data cell names */

/* static enum item_type_code type_code_to_enum ( AST * const node ) ; */
static void add_to_table(int * const i);
static void add_dict_info (int * const i, const AST * const block_heading, const int block_level);
static Sym * get_sym_record(const AST * const name);

__export__ int block_seq_no = 0;        /* Numerical ID of each current data cell (save frame, data block, global block) */
static int block_scope_no[3];       /* Current data cell at each scope level */


#include "cifdic_to_symtab_ddl.inc"
#include "cifdic_to_symtab.inc"
#include "cifdic_to_symtab_main.inc"

  

void sor_save_frame(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	SORAST *sh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Save_heading) ) {
		int i;  block_scope_no[0] = block_seq_no++;   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Save_heading);
		sh=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		current_save_frame = sh;   
		}
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
				sor_data(_parser, &_t,  0 );
			}
			else {
			if ( _t!=NULL && (_t->token==Save) ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "save_frame", _t);
			}
			}
		} while ( !_done );
		}
		_MATCH(Save);
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		/* Actions in here to store dictionary data in symbol table */

		if ( dictionary_scope ) { ;
			/* Whatever goes in here, has to go into data_block and global_block too */
			for (i=0;
			dictionary_items[i].name; ) {  /* i is updated in add_dict_info */
				add_dict_info(&i, sh, 0);
			}
			zzs_init_scope(&dictionary_scope, 0);
		}
		else if ( save_frame_scope ) {
#ifdef DEBUG
			ccif_signal(DICT_SAVEFRAME, NULL, NULL, NULL, 0, 
			sh->a->text, "tree-walk"); 
#endif
			for (i = 0; 
			save_frame_items[i].name ; )  {  /* i is updated in add_to_table. */
				add_to_table(&i);
			}
			zzs_init_scope(&save_frame_scope, 0);
		}
		
                                            current_save_frame = NULL;
		}
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "save_frame", _t);
	}
	*_root = _t;
}

void sor_data(STreeParser *_parser, SORAST **_root, int block_level )
{
	SORAST *_t = *_root;
	SORAST *dn=NULL;
	SORAST *dv=NULL;
	SORAST *l=NULL;
	SORAST *dnl=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Data_item) ) {
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Data_item);
		dn=(SORAST *)_t;
		_DOWN;
		_MATCHRANGE(Non_quoted_text_string,Line_of_text);
		dv=(SORAST *)_t;
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		ccif_add_value ( dn, dn, 0, block_level, 0);   
		}
	}
	else {
	_GUESS;
	if ( !_gv && _t!=NULL && (_t->token==Loop) ) {
		int loop_offset=0;   
		{_SAVE; TREE_CONSTR_PTRS;
		if ( _t!=NULL && (_t->token==Loop) ) {
			{_SAVE; TREE_CONSTR_PTRS;
			_MATCH(Loop);
			_DOWN;
			_WILDCARD;
			_RESTORE;
			}
			_RIGHT;
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "data", _t);
		}
		_RESTORE;
		}
		_GUESS_DONE;
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Loop);
		l=(SORAST *)_t;
		_DOWN;
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Row);
		_DOWN;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Data_name) ) {
				_MATCH(Data_name);
				dnl=(SORAST *)_t;
				_RIGHT;
				if ( !_parser->guessing ) {
				ccif_add_value(l, dnl, loop_offset++, block_level, 0);   
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Row) ) {
				{_SAVE; TREE_CONSTR_PTRS;
				_MATCH(Row);
				_DOWN;
				if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
				_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
					{int _done=0;
					do {
						if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
						_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
							_MATCHRANGE(Non_quoted_text_string,Line_of_text);
							_RIGHT;
						}
						else {
						if ( _t==NULL ) {
							_done = 1;
						}
						else {
							if ( _parser->guessing ) _GUESS_FAIL;
							no_viable_alt(_parser, "data", _t);
						}
						}
					} while ( !_done );
					}
				}
				else {
					if ( _parser->guessing ) _GUESS_FAIL;
					no_viable_alt(_parser, "data", _t);
				}
				_RESTORE;
				}
				_RIGHT;
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
	if ( _parser->guessing ) _GUESS_DONE;
	if ( _t!=NULL && (_t->token==Loop) ) {
		_MATCH(Loop);
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "data", _t);
	}
	}
	}
	*_root = _t;
}

void sor_global_block(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	SORAST *gh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Global_heading) ) {
		int i; block_scope_no[2] = block_seq_no++;   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Global_heading);
		gh=(SORAST *)_t;
		_DOWN;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
				sor_data(_parser, &_t,  2 );
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "global_block", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		
		if ( dictionary_scope ) { 
			for (i=0;
			dictionary_items[i].name; ) {  /* i is updated in add_dict_info */
				add_dict_info(&i, gh, 2);
			}
			zzs_init_scope(&dictionary_scope, 2);
		}
		/* global blocks have no names, so nothing to set to NULL here */
		}
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "global_block", _t);
	}
	*_root = _t;
}

void sor_data_block(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	SORAST *dh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Data_heading) ) {
		int i; block_scope_no[1] = block_seq_no;   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Data_heading);
		dh=(SORAST *)_t;
		_DOWN;
		if ( !_parser->guessing ) {
		current_data_block = dh; 
		block_scope_no[1] = search_for_data_block(dh->a->text); 
		if ( block_scope_no[1] == -1 ) block_scope_no[1] = block_seq_no++;   
		}
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
				sor_data(_parser, &_t,  1 );
			}
			else {
			if ( _t!=NULL && (_t->token==Save_heading) ) {
				sor_save_frame(_parser, &_t);
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data_block", _t);
			}
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		if ( !_parser->guessing ) {
		
		if ( dictionary_scope ) { 
			for (i=0;
			dictionary_items[i].name; ) {  /* i is updated in add_dict_info */
				add_dict_info(&i, dh, 1);
			}
			zzs_init_scope(&dictionary_scope, 1);
		}
		current_data_block = NULL; 
		}
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "data_block", _t);
	}
	*_root = _t;
}

void sor_star_file(STreeParser *_parser, SORAST **_root)
{
	SORAST *_t = *_root;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Star_file) ) {
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Star_file);
		_DOWN;
		if ( _t!=NULL && (_t->token==Comment) ) {
			_MATCH(Comment);
			_RIGHT;
		}
		else {
		if ( _t==NULL || (_t->token==Data_heading||_t->token==Global_heading) ) {
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "star_file", _t);
		}
		}
		{int _done=0;
		while ( !_done ) {
			if ( _t!=NULL && (_t->token==Global_heading) ) {
				sor_global_block(_parser, &_t);
			}
			else {
			if ( _t!=NULL && (_t->token==Data_heading) ) {
				sor_data_block(_parser, &_t);
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "star_file", _t);
			}
			}
			}
		}
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "star_file", _t);
	}
	*_root = _t;
}

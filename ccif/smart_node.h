/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/


/* Declarations for types required where
 *    node->ptr
 * points to some non-trivial data type. (Only loop nodes, at the moment.)
 * 
 * These declarations do not actually need AST's to be defined.
 */




typedef struct { 
				       /* Might be nice to have a pointer back to the
					  category record in the symbol table as well. */
  int nrows, ncols, 
    novf;   /* Number of overflow values (only non-0 for loops with incomplete loop packets) */
} ccif_loop_dim;

#define LOOP_DIMENSION(node, rows, cols, ovf)   if ( (node)->token == Loop ) { \
                                              rows = ( (ccif_loop_dim *) (node)->ptr)->nrows; \
                                              cols = ( (ccif_loop_dim *) (node)->ptr)->ncols; \
                                              ovf =  ( (ccif_loop_dim *) (node)->ptr)->novf;  \
                                           } \
                                           else { rows = (cols = 0) ; }
                                              
#define SET_LOOP_DIMENSION(node, rows, cols, ovf) if ( (node)->token == Loop ) { \
                                                ( (ccif_loop_dim *) (node)->ptr)->nrows = rows; \
                                                ( (ccif_loop_dim *) (node)->ptr)->ncols = cols; \
                                                ( (ccif_loop_dim *) (node)->ptr)->novf  = ovf; }

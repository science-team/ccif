                                      /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/


/* Avoid including stdpccts.h, to allow the most flexibility in supplying
 * a ccif_signal routine */

#include "library.h"
#include "import_export.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

/* The following conditional declaration has been duplicated here
   from ccif_defines.h. We do not want to include ccif_defines.h
   in this file
*/

/* Declare strdup, if X/open stuff has inhibited its declaration.
   N.B. This assumes that the linker will find it, even if its
   declaration has not been found in string.h
   See CCIF_STRDUP_DECL in aclocal.m4.
   Fixme! Eventually, must write our own version to be absolutely safe
*/

#ifndef STRDUP_DECL
extern char *strdup (const char *s1);
#endif


/* Declare this here, so that ccif_exit doesn't have problems */
void ccif_signal(const int code, const char * const pre, 
		 void (*callback)(), const void * ptr, int idx,
		 ...);

void ccif_exit(int status, int line, const char *file);

typedef struct _ccif_message {
   char *name, *fmt;
   int prtmsg;
} MESSAGE;

typedef struct _ccif_facility {
   char *name;
   struct _ccif_message *msg;
   int nmsgs;
} FACILITY;

/* Changed by environment in ccif_condition_init
   If unset for a particular severity, don't output a message 
   (unless we must exit)
*/
static int message_prt[5] = {-1,-1,-1,-1,-1};

/* This should really go in library.h..... */

/* Exit status for various systems: */
#ifdef VMS

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS SS$_NORMAL
#endif

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 0  /* for now */
#endif 

#else /* VMS */

/* I think that these should cover all Unix's which don't define them already */
#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

#endif /* VMS */

#define CCIF_EXIT_SUCCESS EXIT_SUCCESS
#define CCIF_EXIT_FAILURE EXIT_FAILURE


/* This file is generated automatically from condition.list,
 * by condition.awk */
#include "condition_list.h"

/* Descriptive words for severity codes */
static const char * const severity_names[5] = {
   "Warning",
   "Success",
   "Error",
   "Informational",
   "SEVERE ERROR/FATAL"
};

__export__ __noshare__ int ttyout, /* Set in ccif_init, if standard output is
				    * going to a terminal */
                           ttyerr; /* Same for standard error */


/* Final exit status for program. If an error signal is raised,
 * set this to failure, then struggle on. Program will be exited
 * with error status, even if it exits normally.
 */
__export__ __noshare__ int ccif_exit_status=CCIF_EXIT_SUCCESS;
__export__ __noshare__ char errmsg[512];

MESSAGE *ccif_get_msg (const char * const condition_name );

/* Set up condition codes: check for specific CCIF_ISNUM etc.
   environment variables, and set the prtmsg elements of the
   message structures appropriately

   Printing of a particular message can be turned off by setting an
   environment variable FACILITY_NAME=OFF
*/

void ccif_condition_init ( void ) {

  int nfacs = ( sizeof CCIF_facs ) / sizeof (FACILITY);
  int i,j;
  char fac[20], envvar[50], *status; 

  for ( i = 0; i < nfacs; i++ ) {
    strcpy (fac, CCIF_facs[i].name);
    strcat (fac, "_");
    for (j = 0; j < CCIF_facs[i].nmsgs; j++ ) {
      strcpy (envvar,fac);
      strcat (envvar, CCIF_facs[i].msg[j].name);
      status = getenv(envvar);
      if ( status && !zzs_keycmp(status, "off" ) )
	CCIF_facs[i].msg[j].prtmsg = 0;
      else if ( status && !zzs_keycmp(status, "on") )
	CCIF_facs[i].msg[j].prtmsg = 1;
	
    }
  }

  /* Set output status of messages on a per-severity basis, as specified by
     setting environment variables CCIF_WARN=OFF, etc.
     Ignored for severe and error, as we must always exit with these */
  for ( i = 0; i < ( sizeof message_prt ) / sizeof (int); i++ ) {
    strcpy (envvar, (message_control_vars[i] ) );
    status = getenv(envvar);
    if ( status && !zzs_keycmp(status, "off" ) )
      message_prt[i] = 0;
    else if ( status && !zzs_keycmp(status, "on") )
      message_prt[i] = 1;
  }

}

/* Function to retrive pointer to message structure based on
   condition name.
   Horrible kludge - should be done with some kind of hash table
*/

MESSAGE *ccif_get_msg (const char * const condition_name ) {
  char *fac, *v2, *name;
  int n, i, j;

  /* Get and check facility name */
  v2 = strdup(condition_name);
  fac = strtok(v2, "_");
  if ( ! fac ) return ( NULL );
  n = ( sizeof CCIF_facs ) / sizeof (FACILITY);
  for ( i = 0; i < n; i++ ) {
    
    if ( ! strcmp(fac, CCIF_facs[i].name) ) { /* Found facility name */
      
      /* Get and check message name */
      name = strtok(NULL, "_");
      if ( !name ) return ( NULL );
      for ( j = 0; j < CCIF_facs[i].nmsgs; j++ ) {
	if ( !strcmp(name, CCIF_facs[i].msg[j].name) ) { /* Found message name */
	  return &(CCIF_facs[i].msg[j]);
	}
      }
    }
  }
  return ( NULL );
}

/* Set/unset warning message status, returning previous value,
   or -2 if name not valid */

int ccif_warnmsg_switch ( const char * varname, const int on ) {

  int i, v,
    n = ( sizeof message_prt ) / sizeof (int) ;
  MESSAGE *msg;

  /* Check for one of the severity-based switches first */
  for ( i = 0; i < n; i++ ) {
    if ( ! strcmp(varname, message_control_vars[i] ) ) {
      v = message_prt[i];
      if ( on == -1 )
	message_prt[i] = -1;
      else
	message_prt[i] = on ? 1 : 0;
      return (v);
    }
  }
       
  /* Set output status based on message name */
  msg = ccif_get_msg(varname);
  if ( ! msg ) {
    return (-2);
  }
  v = msg->prtmsg;
  if ( on == -1 )
    msg->prtmsg = -1;
  else
    msg->prtmsg = on ? 1 : 0;
  return (v);
}

/* Procedure to ensure that any call to 'exit' actually exits
   via the condition handler */

void ccif_exit(int status, int line, const char *file) {
  
  if ( status == EXIT_SUCCESS )
    ccif_signal(EXIT_SUCCESS, NULL, NULL, NULL, 0);
  else
    ccif_signal(EXIT_FAIL, NULL, NULL, NULL, 0, line, file);
}

      

/* Code for CCIF condition handler */
/* How to use the condition handler:
 * 
 * Call with symbolic value of condition as first parameter. As with VMS,
 * the severity can be changed from the default, by setting the appropriate
 * bits. E.g. to force severity severe, do:
 * 
 *    ccif_signal ( ( CCIF_SOMESIG & ~CCIF_M_SEVERITY ) | CCIF_K_SEVERE, ... )
 * 
 * The second argument should be:
 *   (1) NULL - just text of message is printed
 *   (2) Name of calling routine or rule - a slightly more verbose header to the
 *       signal will be printed
 * 
 * After printing out the message, the callback function, if set, will be
 * called. It will be called as:
 * 
 *    (*callback)( char callback_msg[], void ** ptr, int *count, int first ),
 * where:
 *    callback is the third argument to ccif_signal
 *    callback_msg is a buffer of length CALLBACK_LEN (defined in condition.h)
 *    (*ptr) is a copy of the fourth argument to ccif_signal
 *    (*count) is a copy of the fifth argument to ccif_signal
 *    first will be 1 the first time the callback function is called by
 *         this invocation of the condition handler, 0 on subsequent
 *         calls.
 * 
 * The callback function may freely change ptr and count, since they are
 * auto variables in the signal handler - they will be preserved between
 * calls to callback, for a single invocation of the signal handler.
 * 
 * On return from callback, callback_msg should contain a message which will
 * be output, or callback_msg[0] should be set to '\0', in which case the
 * condition handler will return or exit.
 * 
 * If a callback is not required, the third, fourth and fifth arguments to
 * ccif_signal should be "NULL, NULL, 0".
 * 
 * The remaining arguments to ccif_signal should be the printf-style arguments
 * required by the format string associated with the condition. They will be
 * passed as-is to vprintf/vfprintf.
 * 
 * If the severity of the condition indicates that the program should exit, 
 * and standard output is not a terminal, but standard error is,  output 
 * will go to both stderr and stdout.
 * 
 * New conditions can be added to "condition.list", and the files "condition.h"
 * and "condition_list.h" re-generated with "condition.awk"
 */


/* Macro for condition to decide whether or not to print message. If condition evaluates
   to true, then print message */
#define PRINT_MSG(fac,msg,severity)  (message_prt[severity] == -1 && CCIF_facs[fac].msg[msg].prtmsg) \
                                  || (!message_prt[severity] && CCIF_facs[fac].msg[msg].prtmsg == 1) \
                                  || (message_prt[severity] == 1 && CCIF_facs[fac].msg[msg].prtmsg)

void ccif_signal(const int code, const char * const pre, 
		 void (*callback)(), const void * ptr, int idx,
		 ...) {

   va_list ap;
   int fac = ( code & CCIF_M_FAC_NO ) >> CCIF_V_FAC_NO,
       msg = ( code & CCIF_M_MSG_NO ) >> CCIF_V_MSG_NO,
       severity = ( code & CCIF_M_SEVERITY ),
       exit_prog = ( severity == CCIF_K_SEVERE );
                               /* If exiting, echo everything to stderr as well as stdout  */
   
   const void * c_ptr = ptr;
   
   static const char pre_fmt[] = ">>>> CCIF signal %s_%s (severity: %s) <<<<\n\t(Raised in %s)\n";

   /*   printf("****message_prt[severity]: %d; CCIF_facs[fac].msg[msg].prtmsg: %d; PRINT_MSG: %d\n", 
	  message_prt[severity], CCIF_facs[fac].msg[msg].prtmsg, PRINT_MSG(fac,msg,severity) );
   */

   if ( severity == CCIF_K_SEVERE || severity == CCIF_K_ERROR ) {
     ccif_exit_status = CCIF_EXIT_FAILURE;
   }
   /* else if ?: Always print a message if we are going to exit! */
   else if ( ! ( PRINT_MSG(fac,msg,severity) )
	     &&
	     /* Always print the CCIF_DICTLOADED message */
	     ( strcmp(CCIF_facs[fac].name, "CCIF") || strcmp(CCIF_facs[fac].msg[msg].name, "DICTLOADED") )
	     ) {
     return;
   }
   
   if (pre) {
      printf( pre_fmt,
	      CCIF_facs[fac].name,
	      CCIF_facs[fac].msg[msg].name,
	      severity_names[severity],
	      pre);
      
      if (exit_prog && !ttyout && ttyerr) 
	fprintf(stderr, pre_fmt,
		CCIF_facs[fac].name,
		CCIF_facs[fac].msg[msg].name,
		severity_names[severity],
		pre);
      
      
#ifdef DEBUG
   printf("ccif_signal: facility = %d, message = %d, severity = %d\n",fac, msg, severity);
   printf("Masks: facility = 0x%x, message 0x%x, severity = 0x%x\n",
	   CCIF_M_FAC_NO, CCIF_M_MSG_NO, CCIF_M_SEVERITY);
#endif
   
   }

   va_start(ap, idx);
 
   vprintf(CCIF_facs[fac].msg[msg].fmt,ap);
   putchar('\n');

   if ( exit_prog && !ttyout && ttyerr ) {
      vfprintf(stderr,CCIF_facs[fac].msg[msg].fmt,ap);
      fputs("\n\n",stderr);
   }
      
   va_end(ap);

   if ( callback ) {
      (*callback)(callback_msg, &c_ptr, &idx, 1);
      do {
	 puts(callback_msg);
	 if (exit_prog && !ttyout && ttyerr) {
	    fputs(callback_msg,stderr);
	    fputs("\n",stderr);
	 }
	 (*callback)(callback_msg, &c_ptr, &idx, 0);
      } while ( callback_msg[0] );
      putchar ('\n');
   }
   putchar('\n');

   /* Use the real exit here. This should be the end of file:
    * don't put anything else after the end of this procedure */
#ifdef exit
#undef exit
#endif	
	
   if ( exit_prog ) exit(ccif_exit_status);
}



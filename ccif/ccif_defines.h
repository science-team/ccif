                                                                          /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define MAX_CIF_FILES 10   /* Same as MAXFILES in library.c for now */

#ifndef MAXCLLEN
#define MAXCLLEN      80   /* Maximum length of lines in CIF's */
#endif

#ifndef MAX
#define MAX(i1,i2)  ( i1 > i2 ? i1 : i2 )
#endif

#ifndef MIN
#define MIN(i1,i2)  ( i1 < i2 ? i1 : i2 )
#endif

/* For MIPS systems (SGI, in practice) that need %d to convert size_t
 * and ptrdiff_t when compiled 32-bit, and %ld when compiled 64-bit,
 * we define two macros to make our life easier */

#ifdef _MIPS_SZLONG
#if _MIPS_SZLONG == 32
#define LONG_FMT(d) # d
#else
#define LONG_FMT(d) "l" # d
#endif
#else
#define LONG_FMT(d) # d
#endif

#ifdef _MIPS_SZPTR
#if _MIPS_SZPTR == 32
#define PTR_FMT(d) # d
#else
#define PTR_FMT(d) "l" # d
#endif
#else
#define PTR_FMT(d) # d
#endif


/* Declare strdup, if X/open stuff has inhibited its declaration.
   N.B. This assumes that the linker will find it, even if its
   declaration has not been found in string.h
   See CCIF_STRDUP_DECL in aclocal.m4.
   Fixme! Eventually, must write our own version to be absolutely safe
*/

#ifndef STRDUP_DECL
extern char *strdup (const char *s1);
#endif

/* To determine if we are writing to a terminal or not */
#ifdef VMS
extern int outtty(void);
extern int errtty(void);
#define isouttty devtty("SYS$OUTPUT")
#define iserrtty devtty("SYS$ERROR")
#else
/* Doesn't assume that we have unistd.h */
#define isouttty isatty(fileno(stdout))
#define iserrtty isatty(fileno(stderr))
#endif

/* Exit status for various systems: */
#ifdef VMS

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS SS$_NORMAL
#endif

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 0  /* for now */
#endif 

#else /* VMS */

/* I think that these should cover all Unix's which don't define them already */
#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

#endif /* VMS */

#define CCIF_EXIT_SUCCESS EXIT_SUCCESS
#define CCIF_EXIT_FAILURE EXIT_FAILURE


/* Set mandatory scope by specifying category name (s is char *) */
#define CAT_SCOPE(s) zzs_scope( &((zzs_get(s))->mandatory_scope) )

/* Set mandatory scope by specifying a pointer to a member item of the category (r is Sym *) */
#define CAT_SCOPE_ITEM(r) { char c[MAXCLLEN+1]; Sym *r1; r1 = r; if (r->true_name) r1 = r->true_name; \
                            strncpy( c, zzs_recname(r1), MAXCLLEN); *(strchr(c,'.')) = '\0'; \
                            CAT_SCOPE(c); }
                            
/* Set pointer to category record from name of member item. */
#define CAT_ITEM_NAME(r, name)  { Sym *s; s = zzs_get(name); r = s->category; }
                             
/* Set pointer to category record from item record of member item */  
#define CAT_ITEM(r, s)       r = s->category;

#if 0
				       /* Old, dumb, zzcr_ast */
#define zzcr_ast(ast, attr, tok, txt) (ast)->a = *(attr); \
                                      (ast)->token = tok; \
                                      (ast)->ptr = NULL; \
                                      (*(attr))->nnodes++;
#endif /* 0 */

#if _USE_MMAP == 1
/* Housekeeping macros for file mapping.
 * The functionality of DEL_FROM_MAP_REC is incorporated into zzd_ast, so it is only
 * necessary to invoke it directly when ast->a is re-assigned explicitly.
 * Similarly, ADD_TO_MAP_REC is included in zzmk_ast, so only use this macro
 * when zzmk_ast or ast_node is not invoked to make a node point at an Attrib.
 */

#define ADD_TO_MAP_REC(ast)    if ( ast->a && ast->a->map_file ) (ast->a->map_file->nnodes)++;
#define DEL_FROM_MAP_REC(ast)  if ( ast->a && ast->a->map_file ) (ast->a->map_file->nnodes)--;
#define _HOUSEKEEP_MAPPED_FILES housekeep_mapped_files();
#else
#define ADD_TO_MAP_REC(ast)
#define DEL_FROM_MAP_REC(ast)
#define _HOUSEKEEP_MAPPED_FILES
#endif

				       /* New, smart, zzcr_ast */
#define zzcr_ast(ast, attr, tok, txt) if ( (ast)->a ) { \
                                         zzd_ast(ast);\
                                      } \
                 		      if ( !(*(attr)) ) { \
					 zzcr_attr( (attr), tok, txt ); \
                                      } \
				      (ast)->a = *(attr); ADD_TO_MAP_REC(ast) \
				      ((*(attr))->nnodes)++; \
                                      (ast)->token = tok; \
                                      (ast)->ptr = NULL;

#define ast_node(t,a) zzmk_ast(zzastnew(), a, t)


/* Free attributes associated with a AST node. The node itself is freed by 
 * whatever invokes this macro (zztfree() or whatever). */

#if _USE_MMAP == 1

#define zzd_ast(t)   if ( t->a ) { \
                        if ( t->a->map_file ) (t->a->map_file->nnodes)--; \
                        if ( !( --(t->a->nnodes) ) ) { \
                           Attrib a = t->a, *ptr = &(t->a); \
                           do { \
		       	      a = a->prev; \
			      zzfr_attr( ptr ); \
			   } while ( a && a->text[0] == '#' && (ptr = &a) ); \
			} \
                     }

#else /* #if _USE_MMAP == 1 */

#define zzd_ast(t)   if ( t->a ) { \
                        if ( !( --(t->a->nnodes) ) ) { \
                           Attrib a = t->a, *ptr = &(t->a); \
                           do { \
		       	      a = a->prev; \
			      zzfr_attr( ptr ); \
			   } while ( a && a->text[0] == '#' && (ptr = &a) ); \
			} \
                     }
#endif /* #if _USE_MMAP == 1 */			     


/* This macro is 'DDL knowledge'  */
/* #define ITEM_TYPE(item)  zzs_get_item_type( item->parent.rec ? item->parent.rec : item ) */
#define ITEM_TYPE(item) zzs_get_item_type(item)

/*  number of subexpressions which regexec etc. should match. Values greater
* than 10 *might* cause problems with older (pre-posix) regex packages */
#define NMATCH 10

#ifndef ZZCOL
#define ZZCOL
#endif


#if _USE_MMAP == 1
static char *mmap_status = "Using memory mapping\n";
#include "map_file.h"
#endif   /* #if _USE_MMAP == 1 */

#if _USE_MMAP == 0
static char *mmap_status = "Not using memory mapping\n";
#endif

#if 0	 
/* IRIX fast malloc(3X) stuff. Needs '-lmalloc' argument to ld. Can only be used if the whole
 * CCP4 installation moves over to malloc(3X). */
#if defined(sgi) && defined(DEBUG)
#include <malloc.h>
#endif
#endif /* 0 */

/* Stuff for memory profiling. Needs Mark Moraes' debugging malloc from
  ftp://ftp.cs.toronto.edu/pub/moraes/malloc-n.nn.tar.gz  (n.nn is version number)
  I rename the file "malloc.h" which comes with this software to "prof_malloc.h", to
  avoid any possible confusion.
*/
#ifdef MALLOC_TRACE 
#include <prof_malloc.h>
#endif

/* Left this here as a reminder. Needs to be declared as a compiler switch,
   because pcctscfg.h is now included before any user-supplied code to the
   generators */
#ifndef USER_ZZMODE_STACK
#define USER_ZZMODE_STACK 1
#endif

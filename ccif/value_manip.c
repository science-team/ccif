                                                                              /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "stdpccts.h"
/* Routines to manipulate data values in a data item or loop structure */

#ifndef UNKNOWN_EQ
/* If set, this macro will revert the package to the older sorting,
   i.e. unknown/not present will compare equal to anything.
   Newer behaviour is:
        null < specified value < unknown/not present
*/

/* Macro to set mask for comparison */
#define SET_COMPARISON_MASK(length,val,mask) if ( length == 1 ) {      \
                                                switch (val) {         \
						  case ( '.' ) :       \
						    mask = 0x01;       \
						    break;             \
  					          case ( '?' ):        \
						    mask = 0x08;       \
						    break;             \
						}                      \
					     }

#endif   /* #ifndef UNKNOWN_EQ */

__import__ int ccif_number_of_blocks; 


/* Make symbol table entry point at this structure 
*  cif == 0  => processing dictionary: don't warn about data items which have no records in table
*  cif == 1  => processing CIF: warn about records which are not in symbol table
*/

Sym * ccif_add_value ( AST * const data_structure, AST * const data_name, 
                      const int loop_offset, const int current_block,
		      const int cif)
{
   Sym *item, *cat;
   char cat_name[MAXCLLEN+1], *ptr;
   int cat_len, i, new_blocks;
   
   if ( ! ( item = zzs_get(data_name->a->text) ) ) {

      if (cif) {

	/* Mark this category as not fully described by
	 * dictionary. Relies on the dataname containing a '.', and the
	 * part before being a known category. (Loops also have a
	 * second check, which is more robust: see
	 * ccif_setup_context). Only do this for first bad item.
	 */
	ptr = strchr(data_name->a->text,'.');
	if ( ptr ) {
	  cat_len = ptr - data_name->a->text - 1;
	  strncpy(cat_name, data_name->a->text + 1, cat_len); /* trimming off initial '_' */
	  cat_name[cat_len] = '\0';
	  cat = zzs_get(cat_name);

	  if ( cat ) {
	    zzs_expand_item_record(cat,  ccif_number_of_blocks);
	    if ( ! cat->loop_offset[current_block] )
	      cat->loop_offset[current_block] = zzs_tmpstrdup(data_name->a->text);
	  }
	}

	/* Emit warning and return */
	if (data_name->a->line > 0)
	  ccif_signal(CCIF_NOITEMIP, NULL, NULL, NULL, 0, data_name->a->line, data_name->a->text);
	else
	  ccif_signal(CCIF_NOITEM, NULL, NULL, NULL, 0, data_name->a->text);
      }
      return (NULL);

   }
   
   if ( item->true_name.rec ) item = item->true_name.rec;
   cat = item->category.rec;
   
   zzs_expand_item_record(item, ccif_number_of_blocks);
   zzs_expand_item_record(cat,  ccif_number_of_blocks);
   

   if ( item->node[current_block] ) {
     ccif_signal(CCIF_DNNOTUNIQ, "ccif_add_value", NULL, NULL, 0, 
		 data_name->a->line, data_name->a->text);
     return (item);
   }

   item->node[current_block] = data_structure;
   item->loop_offset[current_block] = loop_offset;
      
   cat->last[current_block] = data_structure;
   if ( ! cat->node[current_block] ) 
      cat->node[current_block] = data_structure;

   
/*     If items from the category occur in more than one data structure, 
 *     no more than one of the data structures may be a loop. */

   /* Allow for the case when some of the loop data names are not defined
      in the dictionary, including the first one in the loop.
      Complain if/when the application attempts to open a context on the category
   */

   if ( data_structure->token == Loop ) {
     if ( cat->l.loop[current_block]  && cat->l.loop[current_block] != data_structure )
       ccif_signal(CCIF_1LOOP, "ccif_add_value", NULL, NULL, 0, data_name->a->line);
     else
       cat->l.loop[current_block] = data_structure;
   }

   return (item);
}
    
#if 0

/* Return pointer to data value */
char *ccif_get_data_value(const Sym * const rec, const int ncontext) 
{
   int block;
      
   if ( ncontext < 0 || ncontext >= NCONTEXTS )
     ccif_signal(CCIF_CXTRANGE, "ccif_get_data_value", NULL, NULL, 0, ncontext, NCONTEXTS);

   if ( !context[ncontext].category )
     ccif_signal(CCIF_CXTEMPTY, "ccif_get_data_value", NULL, NULL, 0);

   block = context[ncontext].block;
   
   if ( rec->node[block]->token == Data_item )
      return ( rec->node[block]->down->a->text );
      
   if ( rec->node[block]->token == Loop ) {
   
      if ( rec->node[block] != context[ncontext].loop_root )
	ccif_signal(CCIF_NOTINLOOP, "ccif_get_data_value", NULL, NULL, 0, zzs_recname(rec),
		    ncontext);
   
      return ( context[ncontext].packet[ rec->loop_offset[block] + 1 ].loop_item->a->text );
   }
   
   ccif_signal(CCIF_BADAST, "ccif_get_data_value", NULL, NULL, 0, rec->node[block]->a->line);

}
 
#endif /* 0 */


/* Get data value by name.
*
* See value_manip.h for input and returned status values
*
* If name == NULL, assumes that record is passed in as *item.
* */

AST *ccif_get_value (char * const name, int *ncontext, int *status, const Sym ** const item)
{
   AST *node=NULL;
   Sym *cat;
   char *ptr;
   int i, block, looped;
   CONTEXT_LIST context;
   
   if ( *ncontext < 0 || *ncontext >= NCONTEXTS )
     ccif_signal(CCIF_CXTRANGE, "ccif_get_value", NULL, NULL, 0, *ncontext, NCONTEXTS);

   if ( *status == APPEND_ROW )
     ccif_signal(CCIF_NOAPPEND, "ccif_get_value", NULL, NULL, 0);
      
   context = ccif_context_list(*ncontext);
   if ( !context.category )
     ccif_signal(CCIF_CXTEMPTY, "ccif_get_value", NULL, NULL, 0);

   if ( name ) { 
     *item = zzs_get(name);
      if (! *item) 
	ccif_signal(CCIF_NOTINDICT, "ccif_get_value", NULL, NULL, 0, name);
   }
   else
     if ( ! *item ) 
       ccif_signal(CCIF_BADGET, "ccif_get_value", NULL, NULL, 0);
   
   if ( (*item)->true_name.rec ) (*item) = (*item)->true_name.rec;
   block = context.block;
   cat = (*item)->category.rec;

   if ( cat != context.category ) 
     ccif_signal(CCIF_WRONGCXT, "ccif_get_value", NULL, NULL, 0, 
	     *ncontext, zzs_recname(cat), zzs_recname( *item ) );
	     
	     
	     
   if ( cat->id <= block || 
       !cat->node[block] || 
       cat->node[block]->token == Data_item) {          /* cater for _item_ not being present */
	  if ( *status == ADVANCE_CONTEXT || 
	       *status == ADVANCE_AND_HOLD ) {                 /* Can still distinguish looped and non-looped */
	     *status = END_OF_CONTEXT;                        /* data from category record. */
	     ccif_free_context(ncontext);
	     return ( NULL );
	  }
   }

/*   looped = (context.category->node[block] && 
	     context.category->node[block]->token == Loop);

*/

/*   looped = (context.category->loop[block] != NULL); */
   

   /* This says whether the **item** occurs as looped data or not.
    * The category may still contain a loop_ structure, even if the
    * item itself is defined outside the loop - context on the loop
    * must be advanced if requested */
   looped = ( (*item)->id > block && (*item)->node[block] ) ?  ((*item)->node[block]->token == Loop) : 0;

   
   if (*status == ADVANCE_CONTEXT || *status == ADVANCE_AND_HOLD) {
      if ( !(ccif_advance_context(*ncontext)) ) {
	 if ( *status == ADVANCE_CONTEXT ) ccif_free_context(ncontext);
	 *status = END_OF_CONTEXT;
	 return (NULL);
      }
   }

   
   if ( !looped ) { 		       /* Not looped data */
      node = (*item)->id > block ? (*item)->node[block] : NULL;
      if ( ( node && (node = node->down) ) ) {
	 switch ( ccif_null_or_unknown(node) ) {
	  case ('?'): 
	    *status = NO_VALUE;
	    break;
	  case ('.'):
	    *status = SINGLE_NULL;
	    break;
	  default:
	    *status = SINGLE_VALUE;
	 }
      }
      else 
	*status = NO_VALUE;
      return ( node );
   }


/* Process looped data */


   i = context.lookup[(*item)->no_in_cat]; /* 0 if item is not present in loop */
   node = i ? context.packet[i].loop_item : NULL ; 

   
   if ( node ) {
      switch ( ccif_null_or_unknown(node) ) {
       case ('?'): 
	 *status = LOOP_UNKNOWN;
	 break;
       case ('.'):
	 *status = LOOP_NULL;
	 break;
       default:
	 *status = LOOP_VALUE;
      }
   }
   else 
     *status = LOOP_UNKNOWN;
   return ( node );
	  
}


/* Examines the value pointed to by a node, and returns '?' or '.' if it is
 * unknown or null respectively */

char ccif_null_or_unknown ( const AST * const node ) {
   
   const char *itmval;
   int itmlen;
   ccif_wo_quotes(node, &itmval, &itmlen);
   if ( itmlen == 1 )  {
      switch ( *itmval)  {
       case ('?'): 
	 return ( '?' );
       case ('.'):
	 return ('.');
      }
   }
   return ('\0');
}


/* Compare two data values (case insensitive). 
 * Need to allow for different quoting, etc. */
int ccif_data_val_casecmp( const AST *v1, const AST *v2) {
   
   const char *v1_start, *v2_start;
   int v1_len, v2_len, v1_null, v2_null;

#ifndef UNKNOWN_EQ
   int v1_mask, v2_mask;
#endif

   ccif_wo_quotes(v1, &v1_start, &v1_len);
   ccif_wo_quotes(v2, &v2_start, &v2_len);

#ifdef UNKNOWN_EQ
   if ( (v1_len == 1 && *v1_start == '?')
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */

   v1_null = (v1_len == 1 && *v1_start == '.');
   v2_null = (v2_len == 1 && *v2_start == '.');
   
   if ( v1_null && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && ! v1_null ) return (1);
   
   return zzs_keycmp (v1_start, v2_start);

#else

   v1_mask = 0x02;
   SET_COMPARISON_MASK(v1_len, *v1_start, v1_mask);
   v2_mask = 0x04;
   SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( v1_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (v1_mask | v2_mask) == 0x06 ) 
     return zzs_keycmp (v1_start, v2_start);

   /* Apply rules for (null < value < unknown) ordering */
   return ( v1_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */

}

/* Compare two data values (case sensitive). 
 * Need to allow for different quoting, etc. */
int ccif_data_val_cmp( const AST *v1, const AST *v2) {
   
   const char *v1_start, *v2_start;
   int v1_len, v2_len, v1_null, v2_null;
   
#ifndef UNKNOWN_EQ
   int v1_mask, v2_mask;
#endif

   ccif_wo_quotes(v1, &v1_start, &v1_len);
   ccif_wo_quotes(v2, &v2_start, &v2_len);

#ifdef UNKNOWN_EQ
   
   if ( (v1_len == 1 && *v1_start == '?')
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */
      

   v1_null = (v1_len == 1 && *v1_start == '.');
   v2_null = (v2_len == 1 && *v2_start == '.');
   
   if ( v1_null && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && ! v1_null ) return (1);
   
   return strncmp (v1_start, v2_start);

#else

   v1_mask = 0x02;
   SET_COMPARISON_MASK(v1_len, *v1_start, v1_mask);
   v2_mask = 0x04;
   SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( v1_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (v1_mask | v2_mask) == 0x06 ) 
     /*     return strncmp (v1_start, v2_start, MIN(v1_len, v2_len));
      */
     return strcmp (v1_start, v2_start);

   /* Apply rules for (null < value < unknown) ordering */
   return ( v1_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */

}


/* Compare two real values */
int ccif_data_val_realcmp ( const AST *v1, const AST *v2 ) {

   const char *v1_start, *v2_start;
   int v1_len, v2_len, v1_null, v2_null;
   float r1, r2;

#ifndef UNKNOWN_EQ
   int v1_mask, v2_mask;
#endif

   ccif_wo_quotes(v1, &v1_start, &v1_len);
   ccif_wo_quotes(v2, &v2_start, &v2_len);

#ifdef UNKNOWN_EQ

   if ( (v1_len == 1 && *v1_start == '?')
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */
      
   v1_null = (v1_len == 1 && *v1_start == '.');
   v2_null = (v2_len == 1 && *v2_start == '.');

   if ( v1_null && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && ! v1_null ) return (1);
   if ( v1_null && v2_null ) return (0);

   r1 = atof(v1->a->text);
   r2 = atof(v2->a->text);
   if ( r1 != r2 ) return ( r1 < r2 ? -1 : 1 );
   return (0);

#else

   v1_mask = 0x02;
   SET_COMPARISON_MASK(v1_len, *v1_start, v1_mask);
   v2_mask = 0x04;
   SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( v1_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (v1_mask | v2_mask) == 0x06 ) {
     r1 = atof(v1->a->text);
     r2 = atof(v2->a->text);
     if ( r1 != r2 ) return ( r1 < r2 ? -1 : 1 );
     return (0);
   }

   /* Apply rules for (null < value < unknown) ordering */
   return ( v1_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */
  

}

/* Compare two integers */
int ccif_data_val_intcmp ( const AST *v1, const AST *v2 ) {

   const char *v1_start, *v2_start;
   int v1_len, v2_len, v1_null, v2_null;
   int i1, i2;

#ifndef UNKNOWN_EQ
   int v1_mask, v2_mask;
#endif

   ccif_wo_quotes(v1, &v1_start, &v1_len);
   ccif_wo_quotes(v2, &v2_start, &v2_len);

#ifdef UNKNOWN_EQ

   if ( (v1_len == 1 && *v1_start == '?')
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */
      
   v1_null = (v1_len == 1 && *v1_start == '.');
   v2_null = (v2_len == 1 && *v2_start == '.');

   if ( v1_null && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && ! v1_null ) return (1);
   if ( v1_null && v2_null ) return (0);

   i1 = atoi(v1->a->text);
   i2 = atoi(v2->a->text);
   return ( i1 - i2 );

#else

   v1_mask = 0x02;
   SET_COMPARISON_MASK(v1_len, *v1_start, v1_mask);
   v2_mask = 0x04;
   SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( v1_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (v1_mask | v2_mask) == 0x06 ) {
     i1 = atoi(v1->a->text);
     i2 = atoi(v2->a->text);
     return ( i1 - i2 );
   }

   /* Apply rules for (null < value < unknown) ordering */
   return ( v1_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */

}



/* Compare a subkey to a data value (case insensitive). 
 * Need to allow for different quoting, etc.
 * To specify null or unknown for the key, don't quote the . or ? */
int ccif_data_key_casecmp( const SORT_KEY * key, const AST *v2) {
   
   const char *v2_start, *key_start = key->k.ckey;
   int v2_len, v2_null;

#ifdef UNKNOWN_EQ
   ccif_wo_quotes(v2, &v2_start, &v2_len);

   if ( ( key->s == '?' )
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */

   v2_null = (v2_len == 1 && *v2_start == '.');
   
   if ( key->s == '.' && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null &&  key->s != '.' ) return (1);
   if ( v2_null && key->s == '.' ) return (0);

   /* If v2 is a non-quoted text string, trim leading spaces
    * from subkey */
   if ( v2->token == Non_quoted_text_string )
      for (; isspace(*key_start); key_start++ );

   return (zzs_keycmp (key_start, v2_start));

#else
   int key_mask, v2_mask;

   if ( v2 ) {
     ccif_wo_quotes(v2, &v2_start, &v2_len);
     v2_mask = 0x04;
     SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);
   }
   else /* v2 == NULL   =>   unknown */
     v2_mask = 0x08;

   key_mask = 0x02;
   SET_COMPARISON_MASK(1, key->s, key_mask);
   
   if ( key_mask == v2_mask ) 
     return (0);
   
   /* If middle bits are set, need to compare values */
   if ( (key_mask | v2_mask) == 0x06 ) {
     
     /* If v2 is a non-quoted text string, trim leading spaces
      * from subkey */
     if ( v2->token == Non_quoted_text_string )
       for (; isspace(*key_start); key_start++ );

     return (zzs_keycmp (key_start, v2_start));
   }

   /* Apply rules for (null < value < unknown) ordering */
   return ( key_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */


}


/* Compare a subkey to a data value (case sensitive). 
 * Need to allow for different quoting, etc. */

int ccif_data_key_cmp( const SORT_KEY * key, const AST *v2) {

   const char *v2_start, *key_start = key->k.ckey;
   int v2_len, v2_null;


#ifdef UNKNOWN_EQ
   ccif_wo_quotes(v2, &v2_start, &v2_len);

   if ( ( key->s == '?' )
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */

   v2_null = (v2_len == 1 && *v2_start == '.');
   
   if ( key->s == '.' && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && key->s != '.' ) return (1);
   if ( v2_null && key->s == '.' ) return (0);

   /* If v2 is a non-quoted text string, trim leading spaces
    * from subkey */
   if ( v2->token == Non_quoted_text_string )
      for (; isspace(*key_start); key_start++ );
   
   return (strncmp (key_start, v2_start));
#else
   int key_mask, v2_mask;

   if ( v2 ) {
     ccif_wo_quotes(v2, &v2_start, &v2_len);
     v2_mask = 0x04;
     SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);
   }
   else /* v2 == NULL   =>   unknown */
     v2_mask = 0x08;

   key_mask = 0x02;
   SET_COMPARISON_MASK(1, key->s, key_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( key_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (key_mask | v2_mask) == 0x06 ) {

     /* If v2 is a non-quoted text string, trim leading spaces
      * from subkey */
     if ( v2->token == Non_quoted_text_string )
       for (; isspace(*key_start); key_start++ );

     return strcmp(key_start, v2_start);
   }

   /* Apply rules for (null < value < unknown) ordering */
   return ( key_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */


}


/* Compare a subkey to a real */
int ccif_data_key_realcmp (const SORT_KEY * key, const AST *v2) {

   const char *v2_start;
   int v2_len, v2_null;
   float v;


#ifdef UNKNOWN_EQ
   ccif_wo_quotes(v2, &v2_start, &v2_len);

   if ( ( key->s == '?' )
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */

   v2_null = (v2_len == 1 && *v2_start == '.');
   
   if ( key->s == '.' && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && key->s != '.' ) return (1);
   if ( v2_null && key->s == '.' ) return (0);

   v = atof(v2->a->text);
   if ( v != key->k.fkey ) return ( v < key->k.fkey ? -1 : 1 );
   return (0);

#else
   int key_mask, v2_mask;

   if ( v2 ) {
     ccif_wo_quotes(v2, &v2_start, &v2_len);
     v2_mask = 0x04;
     SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);
   }
   else /* v2 == NULL   =>   unknown */
     v2_mask = 0x08;

   key_mask = 0x02;
   SET_COMPARISON_MASK(1, key->s, key_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( key_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (key_mask | v2_mask) == 0x06 ) {
     v = atof(v2->a->text);
     if ( v != key->k.fkey ) return ( v < key->k.fkey ? -1 : 1 );
     return (0);
   }

   /* Apply rules for (null < value < unknown) ordering */
   return ( key_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */


}


/* Compare a subkey to an integer */
int ccif_data_key_intcmp (const SORT_KEY * key, const AST *v2) {

   const char *v2_start;
   int v2_len, v2_null, v;


#ifdef UNKNOWN_EQ
   ccif_wo_quotes(v2, &v2_start, &v2_len);

   if ( ( key->s == '?' )
       || (v2_len == 1 && *v2_start == '?') )
     return (0);   /* Unknowns always compare equal to anything */

   v2_null = (v2_len == 1 && *v2_start == '.');
   
   if ( key->s == '.' && ! v2_null ) return (-1);  /* NULL always sorts first */
   if ( v2_null && key->s != '.' ) return (1);
   if ( v2_null && key->s == '.' ) return (0);
   
   v = atoi(v2->a->text);
   return (key->k.ikey - v );

#else
   int key_mask, v2_mask;

   if ( v2 ) {
     ccif_wo_quotes(v2, &v2_start, &v2_len);
     v2_mask = 0x04;
     SET_COMPARISON_MASK(v2_len, *v2_start, v2_mask);
   }
   else /* v2 == NULL   =>   unknown */
     v2_mask = 0x08;

   key_mask = 0x02;
   SET_COMPARISON_MASK(1, key->s, key_mask);

   /* If masks are equal, both are unknown, or both null */
   if ( key_mask == v2_mask ) 
     return (0);

   /* If middle bits are set, need to compare values */
   if ( (key_mask | v2_mask) == 0x06 ) {
     v = atoi(v2->a->text);
     return ( key->k.ikey - v );
   }

   /* Apply rules for (null < value < unknown) ordering */
   return ( key_mask - v2_mask );

#endif /* #ifdef UNKNOWN_EQ */

   
}

/* Return pointer to start of true data value, and length of contents
* (i.e. without quoting, or leading and trailing spaces) */
void ccif_wo_quotes (const AST *v, const char **v_start, int * const v_len) {
   
   const char *v_end;
   switch (v->token) {
      
    case (Non_quoted_text_string):
      *v_start = v->a->text;
      v_end = v->a->text + strlen(v->a->text) - 1;
      for ( ;  isspace(**v_start); (*v_start)++);
      for ( ;  isspace(*v_end); v_end--);
      *v_len = v_end - *v_start + 1;
      return;

    case (Single_quoted_text_string):
      for ( *v_start = v->a->text; **v_start != '\''; (*v_start)++ );
      for ( v_end = v->a->text + strlen(v->a->text) - 1; *v_end != '\''; v_end--);
      (*v_start)++;
      v_end--;
      *v_len = v_end - *v_start + 1;
      return;

    case (Double_quoted_text_string):
      for ( *v_start = v->a->text; **v_start != '\"'; (*v_start)++ );
      for ( v_end = v->a->text + strlen(v->a->text) - 1; *v_end != '\"'; v_end--);
      (*v_start)++;
      v_end--;
      *v_len = v_end - *v_start + 1;
      return;

    case (Line_of_text):
      *v_start = v->a->text + 1;
      v_end = v->a->text + strlen(v->a->text) - 2;
      *v_len = v_end - *v_start + 1;
      
   }


}

/* Remove any associated comments from this node, which begin with "#CCIF_ERR ..." */
void ccif_clean_node ( const AST * const node ) {
   Attrib a, b;
   if ( !node || !node->a || !node->a->prev ) return;
   a = node->a;
   
   while (1) {
      while ( a->prev && strncmp(a->prev->text,"#CCIF_ERR", 9) )
	a = a->prev;
      
      /* Be cautious about this condition */
      if ( !a || !a->prev || ( a->prev->text && a->prev->text[0] != '#' ) ) break;
      
      b = a->prev;
      while ( b && !strncmp(b->text,"#CCIF_ERR", 9) ) {
	 b = b->prev;
	 zzfr_attr(&(b->next));
      }
      a->prev = b;
      a = b;
   }
}
      


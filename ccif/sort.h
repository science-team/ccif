/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#ifndef SORT_H
#define SORT_H

#include "sorlist.h"
#include "rb_structs.h"

#ifndef NSORT_TREES
#define NSORT_TREES 20
#endif

/* Structure used for head of red-black tree */

typedef struct _sort_tree {
   Rb_node tree;
   SList * sort;  /* List of SORT_ELEMENTS applied to category */
   const Sym * cat;
   int block;
} SORT_TREE_LIST;


/* Structure used to keep track of items to sort on. 
 * Item record needs to contain something, so that we can tell if
 * category needs re-sorting when a value in a row has changed.
 * (In practice, just remove row from tree, and re-insert it.)
 */
 

typedef struct _sort_element {
   SList ** sort_tree;           /* &(item->l.sort_tree[block]) - list of sort_tree_index's 
				  * which item is involved in in this block */
   AST * const * node_ptr;       /* address of node element of item record,
					* i.e. &(item->node[block]) */
   const int *loop_off_ptr;	  /* address of loop_offset element of item record,
				   * i.e. &(item->loop_offset[block]) */
   const char * name;             /* Pointer to item's name in string table
                                   * (for diagnostics and comparison); */
   int comp_type;         /* Type of comparison (based on value of _item_type_list.primitive_code):
			   *     'u' (uchar): case insensitive
			   *     'c' (char):  case sensitive
			   *     'n' (numb):  numeric (real)
			   *     'i' : numeric (integer)
			   */
} SORT_ELEMENT;


/* For searching, need to have a separate key type, since in the search tree itself
 * the key and val items of the nodes both point to the appropriate Row token. The function
 * ccif_keyrowcmp is used as the comparison function when a key is being searched for, and
 * the key parameter is a pointer to an array of SORT_KEY, with no more elements than
 * the number of data names on which the category has been sorted */

/* N.B. In
 * 
 * SORT_KEY *key;
 * 
 * key[0].ikey is the index of the last key value in list - this allows a search using a fewer
 * number of mmCIF data names than have been used for the sort. e.g. we can search for
 * a particular residue, without being forced to choose a particular atom.
 */

typedef struct _sort_key {
  union {
    char ckey[MAXCLLEN+1];
    int ikey;
    float fkey;
  } k;
  char s;    /* ' ', '?' or '.' for value: given, unknown or null. */
} SORT_KEY;


extern int ccif_rowcmp ( const char * const r1, const char * const r2);
extern int ccif_keyrowcmp ( const char * const k, const char * const r);
extern int ccif_new_sort( const Sym * cat, int * const status, const int block);
extern int ccif_grow_sort_list ( const Sym * const item, const int index);
extern void ccif_free_sort ( int * const index);
extern void ccif_free_sort_list ( SList **list );
extern int ccif_get_sort_info ( const int index, const Sym ** cat, 
			        int * const block, int * const sort_list );
extern void ccif_build_rbtree(const int index);
extern void ccif_print_sort_list ( char callback_msg[], void **ptr, int *count, int first );
extern Rb_node ccif_first_node(const int index);
extern Rb_node ccif_last_node(const int index);
extern int ccif_tree_init (const int index);
extern int ccif_empty_tree(const int index);
extern int ccif_block_of_sort (const int index);
extern void ccif_add_to_sorts( AST * node, Sym *cat, const int block );
extern void ccif_find_all_rb_nodes(const Sym *item, const int block, AST *row, Rb_node rbnodes[]);
extern Rb_node ccif_find_row( const int index, AST * row, int * n );
extern Rb_node ccif_find_key( const int index, SORT_KEY * key, int * n );
extern SList * ccif_get_sort_list(const int index);
extern Rb_node ccif_find_rb_node(const int index, Rb_node rbnode, const AST * const node); 
extern void ccif_redo_sort(const Sym * const item, const int block, const AST *node, Rb_node rbnodes[] );
#endif /* SORT_H */

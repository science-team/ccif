/* Rename error routines (from star_walk.c) if not already done */
#ifndef sorcerer_panic
#define mismatched_token ccif_sor_mismatched_token
#define mismatched_range ccif_sor_mismatched_range
#define missing_wildcard ccif_sor_missing_wildcard
#define no_viable_alt ccif_sor_no_viable_alt
#define sorcerer_panic ccif_sor_sorcerer_panic
#endif

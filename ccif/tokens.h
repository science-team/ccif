#ifndef tokens_h
#define tokens_h
/* tokens.h -- List of labelled tokens and stuff
 *
 * Generated from: star_map.g
 *
 * Terence Parr, Will Cohen, and Hank Dietz: 1989-1999
 * Purdue University Electrical Engineering
 * ANTLR Version 1.33MR22
 */
#define zzEOF_TOKEN 1
#define End_of_file 1
#define Whitespace 2
#define Data_name 3
#define Save_heading 4
#define Data_heading 5
#define Save 6
#define Global_heading 7
#define Loop 8
#define Stop 9
#define Non_quoted_text_string 10
#define Single_quoted_text_string 11
#define Double_quoted_text_string 12
#define Line_of_text 13
#define Semi_colon 14
#define Comment 15
#define Data_item 16
#define Star_file 17
#define Row 18

#ifdef __USE_PROTOS
extern  Attrib   ccif_antlr_text_string(AST**_root,int *_retsignal);
#else
extern  Attrib   ccif_antlr_text_string();
#endif

#ifdef __USE_PROTOS
void ccif_antlr_data_value(AST**_root,int *_retsignal);
#else
extern void ccif_antlr_data_value();
#endif

#ifdef __USE_PROTOS
void ccif_antlr_save_frame(AST**_root,int *_retsignal);
#else
extern void ccif_antlr_save_frame();
#endif

#ifdef __USE_PROTOS
void ccif_antlr_data(AST**_root,int *_retsignal);
#else
extern void ccif_antlr_data();
#endif

#ifdef __USE_PROTOS
extern  Attrib   ccif_antlr_global_block(AST**_root,int *_retsignal);
#else
extern  Attrib   ccif_antlr_global_block();
#endif

#ifdef __USE_PROTOS
extern  Attrib   ccif_antlr_data_block(AST**_root,int *_retsignal);
#else
extern  Attrib   ccif_antlr_data_block();
#endif

#ifdef __USE_PROTOS
extern  int   ccif_antlr_star_file(AST**_root,int *_retsignal, const int cifin );
#else
extern  int   ccif_antlr_star_file();
#endif

#ifdef __USE_PROTOS
extern  int   ccif_antlr_star_file_body(AST**_root,int *_retsignal);
#else
extern  int   ccif_antlr_star_file_body();
#endif

#ifdef __USE_PROTOS
extern  int   ccif_antlr_check_data_heading(AST**_root,int *_retsignal,  const int length  );
#else
extern  int   ccif_antlr_check_data_heading();
#endif

#ifdef __USE_PROTOS
extern  int   ccif_antlr_check_data_value(AST**_root,int *_retsignal,  const int length  );
#else
extern  int   ccif_antlr_check_data_value();
#endif

#endif
extern SetWordType Char_token_set[];
extern SetWordType setwd1[];
extern SetWordType setwd2[];
extern SetWordType setwd3[];
extern SetWordType Data_block_tokens_set[];

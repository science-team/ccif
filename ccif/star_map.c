/*
 * A n t l r  T r a n s l a t i o n  H e a d e r
 *
 * Terence Parr, Will Cohen, and Hank Dietz: 1989-1999
 * Purdue University Electrical Engineering
 * With AHPCRC, University of Minnesota
 * ANTLR Version 1.33MR22
 *
 *   /ebi/msd/software/dec/pkgs/pccts/bin/antlr -ga -gp ccif_antlr_ -fe star_err.c -fl star_parser.dlg -gt -e2 -gh star_map.g
 *
 */

#define ANTLR_VERSION	13322
#include "pcctscfg.h"
#include "pccts_stdio.h"

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "library.h"
#include "import_export.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

/* Allow disabling of mapping with -D_USE_MMAP=0, but not enabling
if we really don't have it */
#if !defined (_USE_MMAP) && defined (HAVE_MMAP)
#define _USE_MMAP 1
#endif

#if  !defined (HAVE_MMAP)
#define _USE_MMAP 0
#endif

/* In practice, every supported system has the newer mechanism... */
#include <stdarg.h>

#ifdef HAVE_REGEX_H
#include <regex.h>
#else
#include <rxposix.h>   /* from rxdispencer */
#include <rxgnucomp.h>
#endif

#define zzAST_DOUBLE /* At the moment, we only set the up/left pointers
* for AST's of writeable CIF's */

#include "ccif_malloc.h"
#include "rb.h"
#include "ccif_defines.h"

#include "charstru.h"

#include <errno.h>
#ifndef errno      /* Some implementations might have already declared this */
extern int errno;  /* as a macro. For those which haven't, a second, consistent */
#endif             /* extern declaration won't cause any problems */

/* NEVER use assert for "normal" ccif stuff. Use the mechanism
in condition.c/condition.list instead !!! */
#if defined (HAVE_ASSERT_H) && (defined(DEBUG) || defined(SQL_OUT) || defined (PTRS_OUT))
#include <assert.h>
#endif

/* Need this for errno stuff on ESV */
#ifdef ESV
/* #include <bsd/sys/types.h> */

#endif

#if 0
/* Allow use of malloc(3X) as opposed to the default malloc(3C)
* Link with -lmalloc */
#ifdef sgi
#include <malloc.h>
#endif

#endif /* 0 */

#ifdef VMS

#include rms
#include starlet
#include secdef
#include ssdef
#include stsdef
#include psldef
#include lnmdef
#include dvidef
#include descrip
#include stat
#include fibdef
#include iodef
#include atrdef
#include "fatdef.h"
#include file
#include unixio
#include unixlib

#define O_MODE 0   /* Default protection on open/create */

#else

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define O_MODE 0666   /* rw-rw-rw protection on open/create, subject to umask(2) */
#endif


/* Really need these now, since other files need the AST typedef.
Use ptr to point at other AST data required by context. Sigh, PCCTS could do with
'smart' nodes....  */

#define AST_FIELDS Attrib a; int token; void *ptr;

typedef enum _rwstat  { u=0, r=1, w=2 } ccif_rwstat;

#include "ast.h"		       /* Need typedef of AST for sym.h */

#ifndef CASTBase_h                     /* Allow sorcerer files to use identical AST */
typedef AST SORAST;
#define CASTBase_h
#endif

/* Why do we need these here? */
#include "sym.h"
#include "context.h"
#include "value_manip.h"


#include "astlib.h"
#include "smart_node.h"
#include "sort.h"


#include "condition.h"
#include "exit_handler.h"
__import__ int ccif_exit_status;  /* Defined and set in condition.c */

#include "data_block_list.h"
#include "f_interface.h"


extern char * ccif_file_name(const int unit);
extern char * ccif_file_io_name(const int unit);
extern ccif_rwstat ccif_block_status(const int block);
extern char * ccif_file_lookup (const int block);
extern AST * ccif_data_heading_lookup (const int block);
extern char * ccif_strdup(const char *);

#ifdef VMS   /* Stuff from ccif_vms.c */
extern void ccif_exit_vms_error(char *, int, int);
extern char * ccif_logname2filename(const char * const);
extern int truncate ( const char *, long int);
extern int unlink ( const char *);
extern int ccif_get_nameblock(const char * const filename, struct NAM *retnam);
extern int ccif_setup_output_file(const char * const filename);
#ifdef _MSC_VER
extern int wintruncate ( const char *, long int);
#define ccif_logname2filename(s) getenv(s)
#endif
#else
#define ccif_logname2filename(s) getenv(s)
#endif


/* Set/unset operation of fillAttr() */
extern void set_lex_act(const int func);

extern AST *zzmk_ast(AST *node, Attrib a, int t);

  
#define EXCEPTION_HANDLING
#define NUM_SIGNALS 3
#define GENAST

#include "ast.h"

#define zzSET_SIZE 4
#include "antlr.h"
#include "tokens.h"
#include "dlgdef.h"
#include "mode.h"
#ifndef PURIFY
#define PURIFY(r,s) memset((char *) &(r),'\0',(s));
#endif
#include "ast.c"
zzASTgvars

ANTLR_INFO


/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
*
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
*
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
********************************************************************/

static int input_is_cif; /* Set this via parameter of star_file rule. 0 => input is the dictionary 
1 => input is a CIF file
2 => parse as STAR file only
*/

extern void ast_append ( AST *, AST * );

AST *zzmk_ast(AST *node, Attrib a, int t) {
	if ( !node ) 
	ccif_signal(ANTLR_NULLAST, "zzmk_ast", NULL, NULL, 0);
	node->a = a;
	node->token = t;
	if ( a ) { 
		(a->nnodes)++; 
#if _USE_MMAP == 1
		if ( a->map_file ) (a->map_file->nnodes)++;
#endif
	}	
	return node;
}

  

void
#ifdef __USE_PROTOS
zzdflthandlers( int _signal, int *_retsignal )
#else
zzdflthandlers( _signal, _retsignal )
int _signal;
int *_retsignal;
#endif
{
	*_retsignal = _signal;
}


 Attrib  
#ifdef __USE_PROTOS
ccif_antlr_text_string(AST**_root,int *_retsignal)
#else
ccif_antlr_text_string(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	 Attrib  	 _retv;
	zzRULE;
	int _sva=1;
	Attrib s1, s2;
	AST *s1_ast=NULL, *s2_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( Attrib  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	zzmatch_wsig(Semi_colon, _handler); zzastDPush;
	s1 = zzaCur;

	_retv =  s1; _retv->col = 0;
 zzCONSUME;

	{
		zzBLOCK(zztasp2);
		int zzcnt=1;
		zzMake0;
		{
		do {
			zzmatch_wsig(Line_of_text, _handler); zzastDPush;
			s2 = zzaCur;

			(_retv)->length += strlen(( s2)->text); (_retv)->col--; 
			
#if _USE_MMAP == 0 /* Enlarge ($sc)->text, and copy further lines of text into it. */
			CCIF_REALLOC((_retv)->length + 2, char, (_retv)->text, \
			"rule text_string", (_retv)->line)
			strcat((_retv)->text, ( s2)->text);
#endif
 zzCONSUME;

			zzLOOP(zztasp2);
		} while ( (LA(1)==Line_of_text) );
		zzEXIT(zztasp2);
		}
	}
	zzmatch_wsig(Semi_colon, _handler); zzastDPush;
	((_retv)->length)++; 
#if _USE_MMAP == 0                    /* used ' ... + 2 ' in realloc above, so we can get away with this: */
	(_retv)->text[(_retv)->length - 1] = ';' ; 
#endif
	(_retv)->text[(_retv)->length]     = '\0' ; 
	(*_root) = zzmk_ast(zzastnew(),_retv,Line_of_text);
 zzCONSUME;

	zzEXIT(zztasp1);
	return _retv;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return _retv;
}

void
#ifdef __USE_PROTOS
ccif_antlr_data_value(AST**_root,int *_retsignal)
#else
ccif_antlr_data_value(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	zzRULE;
	int _sva=1;
	Attrib s, t;
	AST *s_ast=NULL, *t_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	zzMake0;
	*_retsignal = NoSignal;
	{
	Attrib sc;
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		if ( (setwd1[LA(1)]&0x2) ) {
			zzsetmatch_wsig(Char_token_set, _handler); zzsubchild(_root, &_sibling, &_tail);
			s = zzaCur;
			s_ast = zzastCur;

			zzaRet =  s;
 zzCONSUME;

		}
		else {
			if ( (LA(1)==Semi_colon) ) {
				 sc  = ccif_antlr_text_string(zzSTR,&_signal); 
				if (_signal) {
					switch ( _signal ) {  /* [t] */
					default :
						ccif_signal(ANTLR_BADSTR,  "rule \"data_value\"", NULL, NULL, 0, sc->line);   
						_signal=NoSignal;  /* MR7 */
						break;  /* MR7 */
					}
					if (_signal != NoSignal) goto _handler;  /* MR7 */
				} zzlink(_root, &_sibling, &_tail);
				t_ast = zzastCur;
				zzaRet = sc;
			}
			else {
				if (_sva) _signal=NoViableAlt;
				else _signal=NoSemViableAlt;
				goto _handler;  /* MR7 */
			}
		}
		zzEXIT(zztasp2);
		}
	}
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		while ( (LA(1)==Comment) ) {
			zzmatch_wsig(Comment, _handler);  zzCONSUME;
			zzLOOP(zztasp2);
		}
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return;
}

void
#ifdef __USE_PROTOS
ccif_antlr_save_frame(AST**_root,int *_retsignal)
#else
ccif_antlr_save_frame(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	zzRULE;
	int _sva=1;
	Attrib sh;
	AST *sh_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	zzMake0;
	*_retsignal = NoSignal;
	{
	/* char errmsg[81]; */
	zzmatch_wsig(Save_heading, _handler); zzsubroot(_root, &_sibling, &_tail);
	sh = zzaCur;
	sh_ast = zzastCur;

	zzaRet =  sh; 
#ifdef DEBUG
	ccif_signal(DICT_SAVEFRAME, NULL, NULL, NULL, 0,  sh->text, "parsing");
#endif             
	switch (input_is_cif) {
		case (0):
		if ( !zzs_get(  sh->text + 5 ) && !zzs_newadd(  sh->text + 5 ) ) 
		ccif_signal(DICT_CANTADD,"rule \"save_frame\"", 
		NULL, NULL, 0,  sh->line,  sh->text);
		break;
		case (1):
		ccif_signal(ANTLR_SAVE, "rule \"save_frame\"", 
		NULL, NULL, 0,  sh->line);
		case (2):
		break;
	}
 zzCONSUME;

	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		while ( (LA(1)==Comment) ) {
			zzmatch_wsig(Comment, _handler);  zzCONSUME;
			zzLOOP(zztasp2);
		}
		zzEXIT(zztasp2);
		}
	}
	{
		zzBLOCK(zztasp2);
		int zzcnt=1;
		zzMake0;
		{
		do {
			ccif_antlr_data(zzSTR,&_signal); if (_signal) goto _handler; zzlink(_root, &_sibling, &_tail);
			zzLOOP(zztasp2);
		} while ( (setwd1[LA(1)]&0x8) );
		zzEXIT(zztasp2);
		}
	}
	zzmatch_wsig(Save, _handler); zzsubchild(_root, &_sibling, &_tail); zzCONSUME;
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		while ( (LA(1)==Comment) ) {
			zzmatch_wsig(Comment, _handler);  zzCONSUME;
			zzLOOP(zztasp2);
		}
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return;
}

void
#ifdef __USE_PROTOS
ccif_antlr_data(AST**_root,int *_retsignal)
#else
ccif_antlr_data(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	zzRULE;
	int _sva=1;
	Attrib l, dnl, dvl, dn, dv;
	AST *l_ast=NULL, *dnl_ast=NULL, *dvl_ast=NULL, *dn_ast=NULL, *dv_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	zzMake0;
	*_retsignal = NoSignal;
	{
	int names=0, values=0, i;  AST *ASTPtr; Sym *category; int dv_signal = 0;
	if ( (LA(1)==Loop) ) {
		zzmatch_wsig(Loop, _handler); zzastDPush;
		l = zzaCur;
 zzCONSUME;
		{
			zzBLOCK(zztasp2);
			zzMake0;
			{
			while ( (LA(1)==Comment) ) {
				zzmatch_wsig(Comment, _blk16_alt3_handler); zzastDPush; zzCONSUME;
				zzLOOP(zztasp2);
			}
			zzEXIT(zztasp2);
			}
		}
		{
			zzBLOCK(zztasp2);
			int zzcnt=1;
			zzMake0;
			{
			do {
				zzmatch_wsig(Data_name, _blk16_alt3_handler); zzastDPush;
				dnl = zzaCur;

				names++; 
				dnl_ast = zzmk_ast(zzastnew(), dnl,Data_name) ; 
				if (names == 1) {
					(*_root) = zztmake( zzmk_ast(zzastnew(), l,Loop), zztmake( zzmk_ast(zzastnew(),NULL,Row), dnl_ast , NULL) , NULL); 
					ASTPtr = ast_bottom((*_root));
					ASTPtr->ptr = (void *) (*_root);
					CCIF_CALLOC(1, ccif_loop_dim, ((*_root))->ptr, "rule antlr_data", (0))
				}
				else
				ast_append ( ASTPtr, dnl_ast );
 zzCONSUME;

				{
					zzBLOCK(zztasp3);
					zzMake0;
					{
					while ( (LA(1)==Comment) ) {
						zzmatch_wsig(Comment, _blk16_alt3_handler); zzastDPush; zzCONSUME;
						zzLOOP(zztasp3);
					}
					zzEXIT(zztasp3);
					}
				}
				zzLOOP(zztasp2);
			} while ( (LA(1)==Data_name) );
			zzEXIT(zztasp2);
			}
		}
		ASTPtr = ((*_root))->down;
		{
			zzBLOCK(zztasp2);
			int zzcnt=1;
			zzMake0;
			{
			do {
				_ast = NULL; ccif_antlr_data_value(&_ast,&_signal); 
				if (_signal) {
					switch ( _signal ) {  /* [dvl] */
					case MismatchedToken :
						dv_signal = _signal;
						ccif_signal(ANTLR_MISTOK, "rule \"data\"", NULL, NULL, 0, ( dnl)->line);
						break;  /* MR7 */
					case NoViableAlt :
						dv_signal = _signal;
						ccif_signal(ANTLR_EMPTYLOOP, "rule \"data\"", NULL, NULL, 0, ( dnl)->line);
						values = -1;
						zzconsumeUntil(Data_block_tokens_set); 
						break;  /* MR7 */
					default :
						break;  /* MR7 */
					}
					if (_signal != NoSignal) goto _blk16_alt3_handler;  /* MR7 */
				}
				dvl_ast = zzastCur;
				values++; 
				
	                     if ( !dv_signal ) {
					
				if ( (values-1) % names == 0 ) {
						ast_append( ASTPtr, zztmake( zzmk_ast(zzastnew(),NULL,Row), dvl_ast , NULL) );
						ASTPtr = ast_tail(ASTPtr);
						ASTPtr->ptr = (void *) (*_root);
					}
					else {
						ast_append ( ASTPtr->down, dvl_ast); 
					}
				}
				else {
					ASTPtr = zztmake(ASTPtr, zzmk_ast(zzastnew(),NULL,Row), NULL);
					ASTPtr = ASTPtr->down;
					for ( i = 0; i < names; i++ )
					ast_append (ASTPtr, 
					zzmk_ast(zzastnew(), zzconstr_attr(Non_quoted_text_string,"?"), Non_quoted_text_string));
				}
				zzLOOP(zztasp2);
			} while ( (setwd1[LA(1)]&0x20) );
			zzEXIT(zztasp2);
			}
		}
		{
			zzBLOCK(zztasp2);
			zzMake0;
			{
			if ( (LA(1)==Stop) ) {
				zzmatch_wsig(Stop, _blk16_alt3_handler); zzastDPush; zzCONSUME;
				{
					zzBLOCK(zztasp3);
					zzMake0;
					{
					while ( (LA(1)==Comment) ) {
						zzmatch_wsig(Comment, _blk16_alt3_handler); zzastDPush; zzCONSUME;
						zzLOOP(zztasp3);
					}
					zzEXIT(zztasp3);
					}
				}
			}
			else {
				if ( (setwd1[LA(1)]&0x40) ) {
				}
				else {
					if (_sva) _signal=NoViableAlt;
					else _signal=NoSemViableAlt;
					goto _blk16_alt3_handler;  /* MR7 */
				}
			}
			zzEXIT(zztasp2);
			}
		}
		if ( values % names != 0 ) 
		ccif_signal(ANTLR_BADPCKT, NULL, NULL, NULL, 0,  l->line, names, values);
		SET_LOOP_DIMENSION(((*_root)), values/names, names, values % names)
	}
	else {
		if ( (LA(1)==Data_name) ) {
			zzmatch_wsig(Data_name, _handler); zzastDPush;
			dn = zzaCur;
 zzCONSUME;
			{
				zzBLOCK(zztasp2);
				zzMake0;
				{
				while ( (LA(1)==Comment) ) {
					zzmatch_wsig(Comment, _handler); zzastDPush; zzCONSUME;
					zzLOOP(zztasp2);
				}
				zzEXIT(zztasp2);
				}
			}
			_ast = NULL; ccif_antlr_data_value(&_ast,&_signal); 
			if (_signal) {
				switch ( _signal ) {  /* [dv] */
				case MismatchedToken :
					dv_signal = _signal;
					ccif_signal(ANTLR_MISTOK, "rule \"data\"", NULL, NULL, 0, ( dn)->line);
					break;  /* MR7 */
				case NoViableAlt :
					dv_signal = _signal;
					ccif_signal(ANTLR_BADITEM, "rule \"data\"", NULL, NULL, 0, ( dn)->line, ( dn)->text);
					zzconsumeUntil(Data_block_tokens_set); 
					break;  /* MR7 */
				default :
					break;  /* MR7 */
				}
				if (_signal != NoSignal) goto _handler;  /* MR7 */
			}
			dv_ast = zzastCur;
			(*_root) = zztmake(zzmk_ast(zzastnew(), dn,Data_item), 
			( !dv_signal ? dv_ast : zzmk_ast(zzastnew(), zzconstr_attr(Non_quoted_text_string,"?"), Non_quoted_text_string)) , NULL);
		}
		else {
			if (_sva) _signal=NoViableAlt;
			else _signal=NoSemViableAlt;
			goto _blk16_alt3_handler;  /* MR7 */
		}
	}
	zzEXIT(zztasp1);
	return;
	}
	/* exception handlers */
_blk16_alt3_handler:
	switch ( _signal ) {
	case NoSignal: break;  /* MR7 */
	case MismatchedToken :
		ccif_signal(ANTLR_BADLOOP, "rule \"data\"", NULL, NULL, 0, ( l)->line);
		zzconsumeUntil(Data_block_tokens_set);
		(*_root) = zzmk_ast(zzastnew(), zzempty_attr(), Loop);
		break;  /* MR7 */
	default :
		break;  /* MR7 */
	}
	goto _handler;  /* MR7 */
_handler:
	switch ( _signal ) {
	case NoSignal: break;  /* MR7 */
	case NoViableAlt :
		ccif_signal(ANTLR_BADDATA, "rule \"data\"", NULL, NULL, 0, zzline);   
		break;  /* MR7 */
	default :
		zzdflthandlers(_signal,_retsignal);
	}
	return;
}

 Attrib  
#ifdef __USE_PROTOS
ccif_antlr_global_block(AST**_root,int *_retsignal)
#else
ccif_antlr_global_block(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	 Attrib  	 _retv;
	zzRULE;
	int _sva=1;
	Attrib gh;
	AST *gh_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( Attrib  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	zzmatch_wsig(Global_heading, _handler); zzsubroot(_root, &_sibling, &_tail);
	gh = zzaCur;
	gh_ast = zzastCur;

	if ( input_is_cif == 1 )
	ccif_signal(ANTLR_GLOBAL, "rule \"global_block\"", NULL, NULL, 0, ( gh)->line);
	_retv =  gh;
 zzCONSUME;

	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		while ( (LA(1)==Comment) ) {
			zzmatch_wsig(Comment, _handler);  zzCONSUME;
			zzLOOP(zztasp2);
		}
		zzEXIT(zztasp2);
		}
	}
	{
		zzBLOCK(zztasp2);
		int zzcnt=1;
		zzMake0;
		{
		do {
			ccif_antlr_data(zzSTR,&_signal); if (_signal) goto _handler; zzlink(_root, &_sibling, &_tail);
			zzLOOP(zztasp2);
		} while ( (setwd2[LA(1)]&0x1) );
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return _retv;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return _retv;
}

 Attrib  
#ifdef __USE_PROTOS
ccif_antlr_data_block(AST**_root,int *_retsignal)
#else
ccif_antlr_data_block(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	 Attrib  	 _retv;
	zzRULE;
	int _sva=1;
	Attrib dh, d;
	AST *dh_ast=NULL, *d_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( Attrib  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	zzmatch_wsig(Data_heading, _handler); zzsubroot(_root, &_sibling, &_tail);
	dh = zzaCur;
	dh_ast = zzastCur;

	_retv =  dh;
 zzCONSUME;

	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		while ( (LA(1)==Comment) ) {
			zzmatch_wsig(Comment, _handler);  zzCONSUME;
			zzLOOP(zztasp2);
		}
		zzEXIT(zztasp2);
		}
	}
	{
		zzBLOCK(zztasp2);
		int zzcnt=1;
		zzMake0;
		{
		do {
			if ( (setwd2[LA(1)]&0x4) ) {
				ccif_antlr_data(zzSTR,&_signal); if (_signal) goto _handler; zzlink(_root, &_sibling, &_tail);
				d_ast = zzastCur;
			}
			else {
				if ( (LA(1)==Save_heading) ) {
					ccif_antlr_save_frame(zzSTR,&_signal); if (_signal) goto _handler; zzlink(_root, &_sibling, &_tail);
				}
				/* MR10 ()+ */ else {
					if ( zzcnt > 1 ) break;
					else {
						if (_sva) _signal=NoViableAlt;
						else _signal=NoSemViableAlt;
						goto _handler;  /* MR7 */
					}
				}
			}
			zzcnt++; zzLOOP(zztasp2);
		} while ( 1 );
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return _retv;
	}
	/* exception handlers */
_handler:
	switch ( _signal ) {
	case NoSignal: break;  /* MR7 */
	case MismatchedToken :
		ccif_signal(ANTLR_MISTOK, "rule \"data_block\"", NULL, NULL, 0, zzline);   
		break;  /* MR7 */
	case NoViableAlt :
		ccif_signal(ANTLR_NVALT, "rule \"data_block\"", NULL, NULL, 0, zzline);   
		break;  /* MR7 */
	default :
		zzdflthandlers(_signal,_retsignal);
	}
	return _retv;
}

 int  
#ifdef __USE_PROTOS
ccif_antlr_star_file(AST**_root,int *_retsignal, const int cifin )
#else
ccif_antlr_star_file(_root,_retsignal,cifin)
AST **_root;
int *_retsignal;
 const int cifin ;
#endif
{
	 int  	 _retv;
	zzRULE;
	int _sva=1;
	Attrib c, sfb;
	AST *c_ast=NULL, *sfb_ast=NULL;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( int  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	int cflag=0; 
	Attrib sfb; 
	input_is_cif =  cifin; 
	_retv = 0;
	set_lex_act(1);
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		if ( (LA(1)==Comment) ) {
			zzmatch_wsig(Comment, _handler); zzastDPush;
			c = zzaCur;

			cflag = 1;
 zzCONSUME;

			{
				zzBLOCK(zztasp3);
				zzMake0;
				{
				while ( (LA(1)==Comment) ) {
					zzmatch_wsig(Comment, _handler); zzastDPush; zzCONSUME;
					zzLOOP(zztasp3);
				}
				zzEXIT(zztasp3);
				}
			}
		}
		else {
			if ( (setwd2[LA(1)]&0x10) ) {
			}
			else {
				if (_sva) _signal=NoViableAlt;
				else _signal=NoSemViableAlt;
				goto _handler;  /* MR7 */
			}
		}
		zzEXIT(zztasp2);
		}
	}
	_ast = NULL;  _retv  = ccif_antlr_star_file_body(&_ast,&_signal); if (_signal) goto _handler;
	sfb_ast = zzastCur;
	if (cflag)
	(*_root) = zztmake( zzmk_ast(zzastnew(),NULL,Star_file), zzmk_ast(zzastnew(), c,Comment), sfb_ast , NULL);
	else
	(*_root) = zztmake( zzmk_ast(zzastnew(),NULL,Star_file), sfb_ast , NULL);
	zzmatch_wsig(End_of_file, _handler); zzastDPush; zzCONSUME;
	zzEXIT(zztasp1);
	return _retv;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return _retv;
}

 int  
#ifdef __USE_PROTOS
ccif_antlr_star_file_body(AST**_root,int *_retsignal)
#else
ccif_antlr_star_file_body(_root,_retsignal)
AST **_root;
int *_retsignal;
#endif
{
	 int  	 _retv;
	zzRULE;
	int _sva=1;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( int  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	Attrib sfb=NULL; zzaRet = NULL; _retv = 0;
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		while ( (setwd2[LA(1)]&0x40) ) {
			{
				zzBLOCK(zztasp3);
				zzMake0;
				{
				if ( (LA(1)==Data_heading) ) {
					 sfb  = ccif_antlr_data_block(zzSTR,&_signal); if (_signal) goto _handler; zzlink(_root, &_sibling, &_tail);
					if (!(zzaRet)) zzaRet = sfb;
				}
				else {
					if ( (LA(1)==Global_heading) ) {
						 sfb  = ccif_antlr_global_block(zzSTR,&_signal); if (_signal) goto _handler; zzlink(_root, &_sibling, &_tail);
						if (!(zzaRet)) zzaRet = sfb;
					}
					else {
						if (_sva) _signal=NoViableAlt;
						else _signal=NoSemViableAlt;
						goto _handler;  /* MR7 */
					}
				}
				zzEXIT(zztasp3);
				}
			}
			_retv++;
			zzLOOP(zztasp2);
		}
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return _retv;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return _retv;
}

 int  
#ifdef __USE_PROTOS
ccif_antlr_check_data_heading(AST**_root,int *_retsignal,  const int length  )
#else
ccif_antlr_check_data_heading(_root,_retsignal,length)
AST **_root;
int *_retsignal;
  const int length  ;
#endif
{
	 int  	 _retv;
	zzRULE;
	int _sva=1;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( int  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	_retv = 0;
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		if ( (LA(1)==Data_heading) ) {
			zzmatch_wsig(Data_heading, _handler); zzastDPush;
			if ( strlen(LATEXT(0)) ==  length )  _retv = Data_heading;
 zzCONSUME;

		}
		else {
			if ( (LA(1)==End_of_file) ) {
			}
			else {
				if (_sva) _signal=NoViableAlt;
				else _signal=NoSemViableAlt;
				goto _handler;  /* MR7 */
			}
		}
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return _retv;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return _retv;
}

 int  
#ifdef __USE_PROTOS
ccif_antlr_check_data_value(AST**_root,int *_retsignal,  const int length  )
#else
ccif_antlr_check_data_value(_root,_retsignal,length)
AST **_root;
int *_retsignal;
  const int length  ;
#endif
{
	 int  	 _retv;
	zzRULE;
	int _sva=1;
	int _signal=NoSignal;
	zzBLOCK(zztasp1);
	PURIFY(_retv,sizeof( int  	))
	zzMake0;
	*_retsignal = NoSignal;
	{
	int t_length; _retv = 0;
	{
		zzBLOCK(zztasp2);
		zzMake0;
		{
		if ( (setwd3[LA(1)]&0x2) ) {
			zzsetmatch_wsig(Char_token_set, _handler); zzastDPush;
			if ( strlen(LATEXT(0)) ==  length ) _retv = LA(0);
 zzCONSUME;

		}
		else {
			if ( (LA(1)==Semi_colon) ) {
				zzmatch_wsig(Semi_colon, _handler); zzastDPush;
				t_length = 2;
 zzCONSUME;

				{
					zzBLOCK(zztasp3);
					int zzcnt=1;
					zzMake0;
					{
					do {
						zzmatch_wsig(Line_of_text, _handler); zzastDPush;
						t_length += strlen(LATEXT(0));
 zzCONSUME;

						zzLOOP(zztasp3);
					} while ( (LA(1)==Line_of_text) );
					zzEXIT(zztasp3);
					}
				}
				zzmatch_wsig(Semi_colon, _handler); zzastDPush;
				if ( t_length == length ) _retv = Line_of_text;
 zzCONSUME;

			}
			else {
				if ( (LA(1)==End_of_file) ) {
				}
				else {
					if (_sva) _signal=NoViableAlt;
					else _signal=NoSemViableAlt;
					goto _handler;  /* MR7 */
				}
			}
		}
		zzEXIT(zztasp2);
		}
	}
	zzEXIT(zztasp1);
	return _retv;
	}
_handler:
	zzdflthandlers(_signal,_retsignal);
	return _retv;
}

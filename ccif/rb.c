/* $Id$ */
#include "rb.h"
#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Need PTR_FMT definition */
#include "config.h"
#include "ccif_defines.h"

/* Namespace modified to make it more sensible to include this stuff
 * in CCIF.    Peter Keller */

#define isred(n) (n->s.red)
#define isblack(n) (!isred(n))
#define isleft(n) (n->s.left)
#define isright(n) (!isleft(n))
#define isint(n) (n->s.internal)
#define isext(n) (!isint(n))
#define ishead(n) (n->s.head)
#define isroot(n) (n->s.root)
#define setred(n) n->s.red = 1
#define setblack(n) n->s.red = 0
#define setleft(n) n->s.left = 1
#define setright(n) n->s.left = 0
#define sethead(n) n->s.head = 1
#define setroot(n) n->s.root = 1
#define setint(n) n->s.internal = 1
#define setext(n) n->s.internal = 0
#define setnormal(n) { n->s.root = 0; n ->s.head = 0; }
#define sibling(n) ((isleft(n)) ? n->p.parent->c.child.right \
                                : n->p.parent->c.child.left)

#define mk_new_ext(new, kkkey, vvval) {\
  new = (Rb_node) malloc(sizeof(struct rb_node));\
  new->v.val = vvval;\
  new->k.key = kkkey;\
  setext(new);\
  setblack(new);\
  setnormal(new);\
}

void rb_mk_new_int(Rb_node l, Rb_node r, Rb_node p, int il)
{
  Rb_node new;

  new = (Rb_node) malloc(sizeof(struct rb_node));
  setint(new);
  setred(new);
  setnormal(new);
  new->c.child.left = l;
  new->c.child.right = r;
  new->p.parent = p;
  new->k.lext = l;
  new->v.rext = r;
  l->p.parent = new;
  r->p.parent = new;
  setleft(l);
  setright(r);
  if (ishead(p)) {
    p->p.root = new;
    setroot(new);
  } else if (il) {
    setleft(new);
    p->c.child.left = new;
  } else {
    setright(new);
    p->c.child.right = new;
  }
  rb_recolor(new);
}  
  
   
Rb_node rb_lprev(Rb_node n)
{
  if (ishead(n)) return n;
  while (!isroot(n)) {
    if (isright(n)) return n->p.parent;
    n = n->p.parent;
  }
  return n->p.parent;
}

Rb_node rb_rprev(Rb_node n)
{
  if (ishead(n)) return n;
  while (!isroot(n)) {
    if (isleft(n)) return n->p.parent;
    n = n->p.parent;
  }
  return n->p.parent;
}

Rb_node rb_make(void)
{
  Rb_node head;

  head = (Rb_node) malloc (sizeof(struct rb_node));
  head->c.list.flink = head;
  head->c.list.blink = head;
  head->p.root = head;
  head->k.key = "";
  sethead(head);
  return head;
}

Rb_node rb_find_key_n(Rb_node n, char * key, int * fnd)
{
  int cmp;

  *fnd = 0;
  if (!ishead(n)) {
    ccif_signal(REDBLACK_NONHEAD, "rb_find_key_n", NULL, NULL, 0, n);
  }
  if (n->p.root == n) return n;
  cmp = strcmp(key, n->c.list.blink->k.key);
  if (cmp == 0) {
    *fnd = 1;
    return n->c.list.blink; 
  }
  if (cmp > 0) return n; 
  else n = n->p.root;
  while (1) {
    if (isext(n)) return n;
    cmp = strcmp(key, n->k.lext->k.key);
    if (cmp == 0) {
      *fnd = 1;
      return n->k.lext;
    }
    if (cmp < 0) n = n->c.child.left ; else n = n->c.child.right;
  }
}

Rb_node rb_find_key(Rb_node n, char * key)
{
  int fnd;
  return rb_find_key_n(n, key, &fnd);
}

Rb_node rb_find_ikey_n(Rb_node n, int ikey, int * fnd)
{
  *fnd = 0;
  if (!ishead(n)) {
    ccif_signal(REDBLACK_NONHEAD, "rb_find_ikey_n", NULL, NULL, 0, n);
  }
  if (n->p.root == n) return n;
  if (ikey == n->c.list.blink->k.ikey) {
    *fnd = 1;
    return n->c.list.blink; 
  }
  if (ikey > n->c.list.blink->k.ikey) return n; 
  else n = n->p.root;
  while (1) {
    if (isext(n)) return n;
    if (ikey == n->k.lext->k.ikey) {
      *fnd = 1;
      return n->k.lext;
    }
    n = (ikey < n->k.lext->k.ikey) ? n->c.child.left : n->c.child.right;
  }
}

Rb_node rb_find_ikey(Rb_node n, int ikey)
{
  int fnd;
  return rb_find_ikey_n(n, ikey, &fnd);
}

Rb_node rb_find_gkey_n(Rb_node n, char * key, int (*fxn)(), int * fnd)
{
  int cmp;

  *fnd = 0;
  if (!ishead(n)) {
    ccif_signal(REDBLACK_NONHEAD, "rb_find_key_n", NULL, NULL, 0, n);
  }
  if (n->p.root == n) return n;
  cmp = (*fxn)(key, n->c.list.blink->k.key);
  if (cmp == 0) {
    *fnd = 1;
    return n->c.list.blink; 
  }
  if (cmp > 0) return n; 
  else n = n->p.root;
  while (1) {
    if (isext(n)) return n;
    cmp = (*fxn)(key, n->k.lext->k.key);
    if (cmp == 0) {
      *fnd = 1;
      return n->k.lext;
    }
    if (cmp < 0) n = n->c.child.left ; else n = n->c.child.right;
  }
}

Rb_node rb_find_gkey(Rb_node n, char * key, int (*fxn)() )
{
  int fnd;
  return rb_find_gkey_n(n, key, fxn, &fnd);
}

Rb_node rb_insert_b(Rb_node n, char * key, char * val)
{
  Rb_node newleft, newright, newnode, list, p;

  if (ishead(n)) {
    if (n->p.root == n) {         /* Tree is empty */
      mk_new_ext(newnode, key, val);
      rb_insert_list( &(newnode->c.rblist), &(n->c.rblist));
      n->p.root = newnode;
      newnode->p.parent = n;
      setroot(newnode);
      return newnode;
    } else {
      mk_new_ext(newright, key, val);
      rb_insert_list(&(newright->c.rblist), &(n->c.rblist));
      newleft = newright->c.list.blink;
      setnormal(newleft);
      rb_mk_new_int(newleft, newright, newleft->p.parent, isleft(newleft));
      p = rb_rprev(newright);
      if (!ishead(p)) p->k.lext = newright;
      return newright;
    }
  } else {
    mk_new_ext(newleft, key, val);
    rb_insert_list(&(newleft->c.rblist), &(n->c.rblist));
    setnormal(n);
    rb_mk_new_int(newleft, n, n->p.parent, isleft(n));
    p = rb_lprev(newleft);
    if (!ishead(p)) p->v.rext = newleft;
    return newleft;    
  }
}

void rb_recolor(Rb_node n)
{  
  Rb_node p, gp, s;
  int done = 0;

  while(!done) {
    if (isroot(n)) {
      setblack(n);
      return;
    }

    p = n->p.parent;

    if (isblack(p)) return;
    
    if (isroot(p)) {
      setblack(p);
      return;
    }

    gp = p->p.parent;
    s = sibling(p);
    if (isred(s)) {
      setblack(p);
      setred(gp);
      setblack(s);
      n = gp;
    } else {
      done = 1;
    }
  }
  /* p's sibling is black, p is red, gp is black */
  
  if ((isleft(n) == 0) == (isleft(p) == 0)) {
    rb_single_rotate(gp, isleft(n));
    setblack(p);
    setred(gp);
  } else {
    rb_single_rotate(p, isleft(n));
    rb_single_rotate(gp, isleft(n));
    setblack(n);
    setred(gp);
  }
}

void rb_single_rotate(Rb_node y, int l)
{
  int rl, ir;
  Rb_node x, yp;
  char *tmp;

  ir = isroot(y);
  yp = y->p.parent;
  if (!ir) {
    rl = isleft(y);
  }
  
  if (l) {
    x = y->c.child.left;
    y->c.child.left = x->c.child.right;
    setleft(y->c.child.left);
    y->c.child.left->p.parent = y;
    x->c.child.right = y;
    setright(y);  
  } else {
    x = y->c.child.right;
    y->c.child.right = x->c.child.left;
    setright(y->c.child.right);
    y->c.child.right->p.parent = y;
    x->c.child.left = y;
    setleft(y);  
  }

  x->p.parent = yp;
  y->p.parent = x;
  if (ir) {
    yp->p.root = x;
    setnormal(y);
    setroot(x);
  } else {
    if (rl) {
      yp->c.child.left = x;
      setleft(x);
    } else {
      yp->c.child.right = x;
      setright(x);
    }
  }
}
    
void rb_delete_node(Rb_node n)
{
  Rb_node s, p, gp;
  char ir;

  if (isint(n)) {
    ccif_signal(REDBLACK_NODELINT, "rb_delete_node", NULL, NULL, 0, n);
  }
  if (ishead(n)) {
    ccif_signal(REDBLACK_NODELHEAD, "rb_delete_node", NULL, NULL, 0, n);
  }
  rb_delete_item_list(&(n->c.rblist)); /* Delete it from the list */
  p = n->p.parent;  /* The only node */
  if (isroot(n)) {
    p->p.root = p;
    free(n);
    return;
  } 
  s = sibling(n);    /* The only node after deletion */
  if (isroot(p)) {
    s->p.parent = p->p.parent;
    s->p.parent->p.root = s;
    setroot(s);
    free(p);
    free(n);
    return;
  }
  gp = p->p.parent;  /* Set parent to sibling */
  s->p.parent = gp;
  if (isleft(p)) {
    gp->c.child.left = s;
    setleft(s);
  } else {
    gp->c.child.right = s;
    setright(s);
  }
  ir = isred(p);
  free(p);
  free(n);
  
  if (isext(s)) {      /* Update proper rext and lext values */
    p = rb_lprev(s); 
    if (!ishead(p)) p->v.rext = s;
    p = rb_rprev(s);
    if (!ishead(p)) p->k.lext = s;
  } else if (isblack(s)) {
    ccif_signal(REDBLACK_SIBBLACK, "rb_delete_node", NULL, NULL, 0);
  } else {
    p = rb_lprev(s);
    if (!ishead(p)) p->v.rext = s->c.child.left;
    p = rb_rprev(s);
    if (!ishead(p)) p->k.lext = s->c.child.right;
    setblack(s);
    return;
  }

  if (ir) return;

  /* recolor */
  
  n = s;
  p = n->p.parent;
  s = sibling(n);
  while(isblack(p) && isblack(s) && isint(s) && 
        isblack(s->c.child.left) && isblack(s->c.child.right)) {
    setred(s);
    n = p;
    if (isroot(n)) return;
    p = n->p.parent;
    s = sibling(n);
  }
  
  if (isblack(p) && isred(s)) {  /* Rotation 2.3b */
    rb_single_rotate(p, isright(n));
    setred(p);
    setblack(s);
    s = sibling(n);
  }
    
  { Rb_node x, z; char il;
    
    if (isext(s)) {
      ccif_signal(REDBLACK_SIBNOTINT, "rb_delete_node", NULL, NULL, 0);
    }

    il = isleft(n);
    x = il ? s->c.child.left : s->c.child.right ;
    z = sibling(x);

    if (isred(z)) {  /* Rotation 2.3f */
      rb_single_rotate(p, !il);
      setblack(z);
      if (isred(p)) setred(s); else setblack(s);
      setblack(p);
    } else if (isblack(x)) {   /* Recoloring only (2.3c) */
      if (isred(s) || isblack(p)) {
	ccif_signal(REDBLACK_23c, "rb_delete_node", NULL, NULL, 0);
      }
      setblack(p);
      setred(s);
      return;
    } else if (isred(p)) { /* 2.3d */
      rb_single_rotate(s, il);
      rb_single_rotate(p, !il);
      setblack(x);
      setred(s);
      return;
    } else {  /* 2.3e */
      rb_single_rotate(s, il);
      rb_single_rotate(p, !il);
      setblack(x);
      return;
    }
  }
}


void rb_print_tree(Rb_node t, int level)
{
  int i;
  if (ishead(t) && t->p.parent == t) {
    printf("tree 0x%" PTR_FMT(x) " is empty\n", t);
  } else if (ishead(t)) {
    printf("Head: 0x%" PTR_FMT(x) ".  Root = 0x%" PTR_FMT(x) "\n", t, t->p.root);
    rb_print_tree(t->p.root, 0);
  } else {
    if (isext(t)) {
      for (i = 0; i < level; i++) putchar(' ');
      printf("Ext node 0x%" PTR_FMT(x) ": %c,%c: p=0x%" PTR_FMT(x) ", k=%s\n", 
              t, isred(t)?'R':'B', isleft(t)?'l':'r', t->p.parent, t->k.key);
    } else {
      rb_print_tree(t->c.child.left, level+2);
      rb_print_tree(t->c.child.right, level+2);
      for (i = 0; i < level; i++) putchar(' ');
      printf("Int node 0x%" PTR_FMT(x) ": %c,%c: l=0x%" PTR_FMT(x) ", r=0x%" PTR_FMT(x) ", p=0x%" PTR_FMT(x) ", lr=(%s,%s)\n", 
              t, isred(t)?'R':'B', isleft(t)?'l':'r', t->c.child.left, 
              t->c.child.right, 
              t->p.parent, t->k.lext->k.key, t->v.rext->k.key);
    }
  }
}

void rb_iprint_tree(Rb_node t, int level)
{
  int i;
  if (ishead(t) && t->p.parent == t) {
    printf("tree 0x%" PTR_FMT(x) " is empty\n", t);
  } else if (ishead(t)) {
    printf("Head: 0x%" PTR_FMT(x) ".  Root = 0x%" PTR_FMT(x) ", < = 0x%" PTR_FMT(x) ", > = 0x%" PTR_FMT(x) "\n", 
            t, t->p.root, t->c.list.blink, t->c.list.flink);
    rb_iprint_tree(t->p.root, 0);
  } else {
    if (isext(t)) {
      for (i = 0; i < level; i++) putchar(' ');
      printf("Ext node 0x%" PTR_FMT(x) ": %c,%c: p=0x%" PTR_FMT(x) ", <=0x%" PTR_FMT(x) ", >=0x%" PTR_FMT(x) " k=%d\n", 
              t, isred(t)?'R':'B', isleft(t)?'l':'r', t->p.parent, 
              t->c.list.blink, t->c.list.flink, t->k.ikey);
    } else {
      rb_iprint_tree(t->c.child.left, level+2);
      rb_iprint_tree(t->c.child.right, level+2);
      for (i = 0; i < level; i++) putchar(' ');
      printf("Int node 0x%" PTR_FMT(x) ": %c,%c: l=0x%" PTR_FMT(x) ", r=0x%" PTR_FMT(x) ", p=0x%" PTR_FMT(x) ", lr=(%d,%d)\n", 
              t, isred(t)?'R':'B', isleft(t)?'l':'r', t->c.child.left, 
              t->c.child.right, 
              t->p.parent, t->k.lext->k.ikey, t->v.rext->k.ikey);
    }
  }
}
      
int rb_nblack(Rb_node n)
{
  int nb;
  if (ishead(n) || isint(n)) {
    ccif_signal(REDBLACK_NONEXT, "rb_nblack", NULL, NULL, 0, n);
  }
  nb = 0;
  while(!ishead(n)) {
    if (isblack(n)) nb++;
    n = n->p.parent;
  }
  return nb;
}

int rb_plength(Rb_node n)
{
  int pl;
  if (ishead(n) || isint(n)) {
    ccif_signal(REDBLACK_NONEXT, "rb_plength", NULL, NULL, 0, n);
  }
  pl = 0;
  while(!ishead(n)) {
    pl++;
    n = n->p.parent;
  }
  return pl;
}

void rb_free_tree(Rb_node n)
{
  if (!ishead(n)) {
    ccif_signal(REDBLACK_NONHEAD, "rb_free_tree", NULL, NULL, 0, n);
  }

  while(rb_first(n) != rb_nil(n)) {
    rb_delete_node(rb_first(n));
  }
  free(n);
}

char *rb_val(Rb_node n)
{
  return n->v.val;
}

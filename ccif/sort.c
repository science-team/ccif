/* $Id$ */ 

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "stdpccts.h"



/* Static list of sort trees */
static SORT_TREE_LIST sort_tree[NSORT_TREES];

/* Variable to allow the comparison functions to determine current
 * sorting order of items within a category */
static int ccif_sort_index = -1;


/* The SList functions only work with pointers, but it is silly to
 * continually be allocating and freeing pointers to single integers.
 * This array allows us to use pointers to integer values.
 * Initialised in ccif_init */
__export__ __noshare__ int ccif_sort_numbers[NSORT_TREES];


/* Functions involved with the use of red-black trees, for sorting
 * categories. */



/* Compares two rows, by the sort order specified by 
 * sort_tree[ccif_sort_index] (ccif_sort_index must be set outside
 * this function).
 *
 * Arguments are (char *), because that is what is sent by librb.
 * Cast to (AST *) here */

int ccif_rowcmp ( const char * const r1, const char * const r2) {

   register const AST *p1, *p2;
   const AST *node;
   register const SList *sort_list;
   register int result,i;
   const SORT_ELEMENT * sort_elem;
   double v1, v2;
   int i1, i2;
   
   if ( r1 == r2 ) return (0);	       /* Same row */
   if ( !r1 ) return (-1);
   if ( !r2 ) return (1);
   

   if ( ccif_sort_index < 0 ) {
      Sym * cat;
      char * catname = "<Not determinable>";
      node = (AST *)  ((AST *) r1)->ptr;  /* Loop token's node */
      node = node->down;                  /* loop header row */
      if ( node ) node = node->down;      /* First data name */
      if ( node && node->a && node->a->text ) {
	 cat = (zzs_get(node->a->text))->category.rec;
	 catname = zzs_recname(cat);
      }
      ccif_signal(REDBLACK_NOSORTORDER, "ccif_rowcmp", NULL, NULL, 0, catname);
   }
	 
   sort_list =   sort_tree[ccif_sort_index].sort;

   /* Loop over each loop offset selected for sorting */
   for (sort_list = sort_list->next; sort_list; sort_list = sort_list->next){
      
      sort_elem = (SORT_ELEMENT *) sort_list->elem;
      /* If value is not present, or is not inside a loop_ structure,
	 they will compare equal, so continue */
      node = *(sort_elem->node_ptr);
      if ( !node || ( node->token != Loop ) ) continue;
      
      /* Find nodes corresponding to data item */
      for ( p1 = ((AST *) r1)->down, p2 = ((AST *) r2)->down, i = 0;
	    i < *(sort_elem->loop_off_ptr);
	    i++ ) {
	 p1 = p1->right;
	 p2 = p2->right;
      }
	 
      /* Now compare items, returning if they are not equal */
      switch ( sort_elem->comp_type ) {
	 
       case ( 'u' ):
	 result = ccif_data_val_casecmp(p1, p2);
	 break;
	 
       case ( 'c' ):
	 result = ccif_data_val_cmp(p1, p2);
	 break;
	 
       case ( 'n' ):
	 result = ccif_data_val_realcmp(p1, p2);
	 break;
	 
       case ( 'i' ):
	 result = ccif_data_val_intcmp(p1, p2);
	 break;
	 
       default:
	 ccif_signal(CCIF_UNKWNCMP,"ccif_rowcmp", NULL, NULL, 0, 
		     sort_elem->comp_type, sort_elem->name);

      }			       /* switch ( sort_elem->comp_type ) */
      if (result) return (result);

   }				       /* for (; sort_list; ... */
	    
   return (0);			/* Rows compare equal */
}
     

/* Variation of ccif_rowcmp, to allow a key to be costructed for searching.
 * (Normally, the key is also the value, i.e. both are pointers to a
 * Row node )*/
int ccif_keyrowcmp ( const char * const k, const char * const r) {

   register const AST *p;
   const AST *node;
   SORT_KEY *key;
   register const SList *sort_list;
   register const SORT_ELEMENT *sort_elem;
   register int result,i,j;
   

   if ( ccif_sort_index < 0 ) {
      Sym * cat;
      char * catname = "<Not determinable>";
      node = (AST *)  ((AST *) r)->ptr;  /* Loop token's node */
      node = node->down;                  /* loop header row */
      if ( node ) node = node->down;      /* First data name */
      if ( node && node->a && node->a->text ) {
	 cat = (zzs_get(node->a->text))->category.rec;
	 catname = zzs_recname(cat);
      }
      ccif_signal(REDBLACK_NOSORTORDER, "ccif_rowcmp", NULL, NULL, 0, catname);
   }
	 
   if ( !r ) return (1);

   sort_list =   sort_tree[ccif_sort_index].sort;

   key = (SORT_KEY *) k;

   /* Loop over each loop offset selected for sorting */
   for (sort_list = sort_list->next, j = 1; 
	sort_list && j <= key[0].k.ikey ;
	sort_list = sort_list->next, j++){
      
      sort_elem =  (SORT_ELEMENT *) sort_list->elem;
      
      node = *(sort_elem->node_ptr);
#ifdef UNKNOWN_EQ
      /* If item is not present, don't compare */
      if ( !node  ) continue;
#else


#endif /* #ifdef UNKNOWN_EQ */


      if ( node->token == Data_item )
	p = node->down;
      else
	for ( p = ((AST *) r)->down, i = 0;   /* Find node corresponding to data item */
	     i < *(sort_elem->loop_off_ptr);
	     i++ ) 
	  
	  p = p->right;
      
      switch ( sort_elem->comp_type ) {

	/* Compare values based on type. If they compare equal,
         * go on to next value in sort list */
	 
       case ( 'u' ):
	 result = ccif_data_key_casecmp(&(key[j]), p);
	 break;
	 
       case ( 'c' ):
	 result = ccif_data_key_cmp(&(key[j]), p);
	 break;
	 
       case ( 'n' ):
	 result = ccif_data_key_realcmp( &(key[j]), p);
	 break;
	 
       case ( 'i' ):
	 result = ccif_data_key_intcmp( &(key[j]), p);
	 break;
	 
       default:
	 ccif_signal(CCIF_UNKWNCMP,"ccif_keyrowcmp", NULL, NULL, 0, 
		     sort_elem->comp_type, sort_elem->name);

      }			       /* switch ( sort_elem->comp_type ) */
      if ( result ) return (result);
      
   }				       /* for (; sort_list; ... */
	    
   return (0);			       /* key compares equal to row */
}
     


/* Sets up a new structure for a sort list */

int ccif_new_sort( const Sym * cat, int * const status, const int block) {
   
   int i;
   
   for ( i = 0; i < NSORT_TREES; i++ ) {
      if (  sort_tree[i].tree ) continue;
      
      sort_tree[i].cat = cat;
      sort_tree[i].block = block;
      sort_tree[i].sort = NULL;
      sort_tree[i].tree = rb_make();
      sort_tree[i].tree->k.key = NULL; /* For our purposes, a much better NULL value than
                                          "" */
      *status = 1;
      return (i);
   }
   
   ccif_signal(CCIF_NOFREESORT, "ccif_new_sort", NULL, NULL, 0);
   /* Stop certain compilers from moaning */
   return -1;
}
   
   

/* Does the business of adding items to a sort list */

int ccif_grow_sort_list ( const Sym * const item, const int index)
{
   Sym *cat = item->category.rec;
   SORT_ELEMENT * sort_item=NULL;
   SList *sort_list, *p;
   const ItemTypeList *type;
   int block = sort_tree[index].block;
   
   if ( cat != sort_tree[index].cat )
     ccif_signal(CCIF_BADADDSORT, "ccif_grow_sort_list", NULL, NULL, 0, 
		 zzs_recname(item), zzs_recname(sort_tree[index].cat));
   
   
   type = ITEM_TYPE(item);

   if ( !(type->single_line) ) {
     ccif_signal( CCIF_1LINESORT, "ccif_grow_sort_list", NULL, NULL, 0, 
		 zzs_recname(item) );
     return (0);
   }

#if 0   /* This should be OK/allowed */
   if ( block >= cat->id || !(cat->l.loop[block]) ) {
     AST * block_header = ccif_data_heading_lookup( block );
     ccif_signal(CCIF_SORTEMPTYCAT, "ccif_grow_sort_list", NULL, NULL, 0,
		 zzs_recname(item), 
		 block_header ? block_header->a->text : "<not determinable>" );
     return (0);
   }
#endif

   sort_list = sort_tree[index].sort;

   if ( sort_list ) {
   /* Go through existing list - if item is already in list, need
    * to move it to the end */

   /* Must always be looking at the next item, otherwise can't unlink if needed.
    * Remember that first item in list is sentinel node */
     for ( ; sort_list->next; sort_list = sort_list->next ) {
       
       /* If found in list, unlink */
       if ( ((SORT_ELEMENT *) sort_list->next->elem)->name == 
	       zzs_recname(item) ) {
	 
	 if ( !(sort_list->next->next) )
	   return (1);   /* Was last item anyway - don't need to do anything */
	 p = sort_list->next;
	 sort_list->next = sort_list->next->next;
	 sort_item = (SORT_ELEMENT *) p->elem;
	 free (p);
	 break;
       }
     }   /* for ( ; sort_list->next; sort_list = sort_list->next ) */
   }   /* if ( sort_list ) */

     
   if ( !sort_item ) {
      CCIF_CALLOC(1, SORT_ELEMENT, sort_item, "ccif_add_to_sort", (0));
      slist_add(&(item->l.sort_tree[block]), &(ccif_sort_numbers[index]) );
      sort_item->sort_tree = &(item->l.sort_tree[block]);
      sort_item->node_ptr = &(item->node[block]);
      sort_item->name = zzs_recname(item);
      sort_item->loop_off_ptr = &(item->loop_offset[block]);
      sort_item->comp_type = *( zzs_symname(type->primitive_code) );
      if ( type->basic_type == 2 )    /* i.e. int */
	sort_item->comp_type = 'i';
   }

   /* Add to end of list */
   slist_add( &(sort_tree[index].sort), sort_item);
   return (1);
}


/* Release whole sort structure */

void ccif_free_sort ( int * const index) {
  rb_free_tree(sort_tree[*index].tree);
  sort_tree[*index].cat = NULL;
  ccif_free_sort_list ( &(sort_tree[*index].sort) );
  *index = -1;
}


/* Free sort list, and all elements pointed to by list. */

void ccif_free_sort_list ( SList ** list ) {

   SList *cursor = *list, *tcursor, **tlist, *l;
   SORT_ELEMENT *e;
   int * i;

   /* Here, we iterate over the items which we were sorting on */
   while ( (e = (SORT_ELEMENT *) slist_iterate(*list, &cursor)) ) {
     tlist = e->sort_tree;
     tcursor = *tlist;

     /* Here, we go down the list of sort trees pointed to by item[block]->l.sort_tree,
      * to find the reference to the sort list we are freeing. We then unlink it.
      * These lists are normally going to be very short */
     for ( ; tcursor->next; tcursor = tcursor->next ) {
	i = (int *) tcursor->next->elem;
	if ( sort_tree[*i].sort == *list ) {
	   l = tcursor->next;
	   tcursor->next = tcursor->next->next;
	   free (l);
	   break;
	}
	
	if ( ! (*tlist)->next ) {
	   slist_free(*tlist);
	   *tlist = NULL;
	}
     }
	
     free (e);
  }

  slist_free( *list );

  *list = NULL;
}

  

/* Get category and block of a sort, and indicate whether there is a
 * sort list set up */

int ccif_get_sort_info ( const int index, const Sym ** cat, 
			int * const block, int * const sort_list ) {
   
   if ( sort_tree[index].tree ) {
      *cat = sort_tree[index].cat;
      *block = sort_tree[index].block;
      *sort_list = sort_tree[index].sort ? 1 : 0;
      return (1);
   }
   else
     return (0);
}


void ccif_build_rbtree(const int index) {

   const Sym * cat = sort_tree[index].cat;
   const int block = sort_tree[index].block;
   const AST *node;
   
   node = cat->l.loop[block];

   /* Don't attempt to do anything if there isn't enough stuff in the
      loop to make it viable */

   if ( !node || !node->down || !node->down->down 
       || !node->down->right )	/* I think that this covers it */
     return;

   ccif_sort_index = index;
   
   for ( node = node->down->right;
	node;
	node = node->right)
     rb_insertg( sort_tree[index].tree, (char *) node, NULL, ccif_rowcmp );
   
   ccif_sort_index = -1;
}

/* Callback function for CCIF_SORTED signal */

void ccif_print_sort_list ( char callback_msg[], void **ptr, int *count, int first ) {

  static SList *list, *cursor;
  SORT_ELEMENT *sort_item;

  if ( first ) {
    list = sort_tree[*count].sort;
    cursor = list;
    return;
  }

  sort_item = (SORT_ELEMENT *) slist_iterate(list, &cursor);
  if ( sort_item ) {
     callback_msg[0] = '\t';
     strcpy( &(callback_msg[1]), sort_item->name);
  }
  else
    callback_msg[0] = '\0';

}
  

/* Various things to allow access to a tree specified by index */

Rb_node ccif_first_node(const int index) {
   if ( sort_tree[index].tree )
     return ( rb_first(sort_tree[index].tree) );
   else
     return (NULL);
}

Rb_node ccif_last_node(const int index) {
  if ( sort_tree[index].tree )
     return ( rb_last(sort_tree[index].tree) );
   else
     return (NULL);
}

int ccif_tree_init (const int index) {
   return (rb_nil(sort_tree[index].tree) != (Rb_node) NULL);
}
	     

int ccif_empty_tree (const int index) {
  return (rb_empty(sort_tree[index].tree));
}


int ccif_block_of_sort (const int index) {
  return (sort_tree[index].block);
}


/* Add a row into all applicable sorts */

void ccif_add_to_sorts( AST * node, Sym *cat, const int block ) {

  int i;
  for (i = 0; i < NSORT_TREES; i++)
    if ( sort_tree[i].cat == cat && sort_tree[i].block == block ) {
      ccif_sort_index = i;
      rb_insertg( sort_tree[i].tree, (char *) node, NULL, ccif_rowcmp );
    }

  ccif_sort_index = -1;

}


/* The real core of the red-black searching stuff */

/* Find the node in a red-black tree which corresponds to 'row' */
Rb_node ccif_find_row( const int index, AST * row, int * n ) {

  Rb_node ret;
  
  if ( row->token != Row )
    ccif_signal(CCIF_NOTROW, "ccif_find_row", NULL, NULL, 0, index);

  if ( index < 0 || index >= NSORT_TREES )
    ccif_signal(CCIF_IDXRANGE, "ccif_find_row", NULL, NULL, 0, index, NSORT_TREES - 1);

  ccif_sort_index = index;
  ret = rb_find_gkey_n ( sort_tree[index].tree, (char *) row, ccif_rowcmp, n);
  ccif_sort_index = -1;
  return (ret);
}

/* Find the node in a red-black tree which satisfies 'key' */
Rb_node ccif_find_key( const int index, SORT_KEY * key, int * n ) {

  Rb_node ret;

  if ( index < 0 || index >= NSORT_TREES )
    ccif_signal(CCIF_IDXRANGE, "ccif_find_key", NULL, NULL, 0, index, NSORT_TREES - 1);

  ccif_sort_index = index;
  ret = rb_find_gkey_n ( sort_tree[index].tree, (char *) key, ccif_keyrowcmp, n);
  ccif_sort_index = -1;
  return (ret);
}


/* Re-position row in sort list, and applicable contexts.
   Assumes ( item->id > block ), checks everything else.
   rbnodes[] must already have been filled in by a call to ccif_find_all_rbnodes */

void ccif_redo_sort(const Sym * const item, const int block, const AST *node, Rb_node rbnodes[]) {

  SList *list=item->l.sort_tree[block], *cursor;
  const int *sort_index;
  Rb_node rbnode;
  int i=0;

  if ( ! list || node->token != Row )
    return;
  
  cursor = list;
  while ( sort_index = ( int *) slist_iterate(list, &cursor) ) {
    ccif_sort_index = *sort_index;
    
    /* Delete and re-insert row in each red-black tree, in which it plays a part */
    
    rb_delete_node(rbnodes[i]);
    rbnodes[i] = rb_insertg(sort_tree[*sort_index].tree, (char *) node, NULL, ccif_rowcmp);
    
    /* Update contexts which might point to this row */
    ccif_resort_context(*sort_index, (AST *) rbnodes[i]->k.key, rbnodes[i]);
    
    i++;
  }
  
  ccif_sort_index = -1;
  
}

/* Get a list of the exact red-black nodes which point to a row, and are involved
 * in a sort on item.
 * rbnodes must be dimensioned to at least NSORT_TREES */

void ccif_find_all_rb_nodes(const Sym *item, const int block, AST *row, Rb_node rbnodes[]){
  
  SList *list = item->l.sort_tree[block], *cursor = list;
  int i=0, *index, n;
  
  while( index = (int *) slist_iterate(list, &cursor) ) {
    ccif_sort_index = *index;
    
    rbnodes[i] = rb_find_gkey_n(sort_tree[*index].tree, (char *) row, ccif_rowcmp, &n);
    if ( !n )
      ccif_signal(CCIF_NOROWFIND, "ccif_find_all_rb_nodes", NULL, NULL, 0);
    rbnodes[i] = ccif_find_rb_node(*index, rbnodes[i], row);
    
    i++;
    
  }
}



/* Brute force routine, to find the actual red-black node, which points to
 * a loop packet.
 * Need this, because there is no guarantee that an arbitrary sort can
 * find a specific row */

Rb_node ccif_find_rb_node(const int index, Rb_node rbnode, const AST * const node) {
  
  Rb_node rbnode2 = rbnode;
  
  ccif_sort_index = index;
  
  /* Try down the list first : */
  while ( rbnode2 && !ccif_rowcmp ( (char *) rbnode2->k.key, (char *) node) ) {
    if ( (AST *) rbnode2->k.key == node ) {
      ccif_sort_index = -1;
      return (rbnode2);
    }
    rbnode2 = rb_next(rbnode2);
  }
  
  /* Now up the list */
  rbnode2 = rbnode;
  while ( rbnode2 && !ccif_rowcmp ( (char *) rbnode2->k.key, (char *) node) ) {
    if ( (AST *) rbnode2->k.key == node ) {
      ccif_sort_index = -1;
      return (rbnode2);
    }
    rbnode2 = rb_prev(rbnode2);
  }
  
  ccif_sort_index = -1;
  return (NULL);
  
  
}

/* Desparation function, if you must have access to the sort list.
 * Don't use unless absolutely necessary. */
SList * ccif_get_sort_list(const int index) {
  return ( sort_tree[index].sort );
}

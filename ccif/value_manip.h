/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* Input status values for getting value: */
#define KEEP_CONTEXT 1       /* Get item from existing context */
#define ADVANCE_CONTEXT 2     /* Advance context given by ncontext parameter to
			      * next packet before returning item */
#define APPEND_ROW      3     /* Convert to loop if necessary and append new row of
                               * unknowns. Context points at this new row */
#define ADVANCE_AND_HOLD 4    /* Same as ADVANCE_CONTEXT, except that context
                               * won't be freed when the end of the category is
			       * reached, IF category is present in a loop 
			       * structure */


/* Returned status value for getting value */

/* Note that these are carefully chosen, so that bitwise operations can
 * be used to alter one of their properties independently of the others,
 * and so that they are contiguous (i.e. they can be used as the 
 * expression in a Fortran computed goto).
 * 
 * value & 0x01 -> unknown
 * value ^ 0x06 -> dictionary value
 * value | 0x06 -> null
 * unknown | 0x04 -> dictionary value
 * etc., for both single and looped values
*/

#define NO_VALUE   0     /* Value not found in context. */
#define LOOP_UNKNOWN  1  /* Value specified in loop packet as '?' ('?' in a
			    data item is returned as NO_VALUE). Only returned
			    if there is no default value in the dictionary. */
#define SINGLE_VALUE 2   /* One occurence of data value in file, in a 
			    Data_item. No context assigned. */
#define LOOP_VALUE   3   /* Value retrieved from loop. Context assigned 
			    (even if it only has one row) */
#define SINGLE_DICT  4   /* Value not in file: returned value is default from
			    dictionary. Treated otherwise as SINGLE_VALUE.
			    Must only be returned from routines which know
			    how to convert from strings to target data type */
#define LOOP_DICT    5   /* Specified as '?' in a loop packet, but overridden
			    by dictionary default value. */
#define SINGLE_NULL  6   /* Value specified as '.' in a data item (can override
			    dictonary default value). At the moment, only
			    returned by routines which try to convert
			    values in CIF from a string to target data type */
#define LOOP_NULL    7   /* Value specified as '.' in a loop packet. See comment
			    for SINGLE_NULL */
#define END_OF_CONTEXT 8 /* Ran off end of context, or item is not in a loop
                            structure. (implies input status was
			    ADVANCE_CONTEXT). No value returned, and context
			    freed. */
#define PARENTHESIS_ESD 9 /* Special value for an esd extracted from a 
			     123.456(78)e+10 -type expression */

#define INVALID_TOKEN  10 /* Special value for CCIF_PUT_CHAR. Could not create a
                             STAR token from text provided. */

#include "ast.h"
#include "sort.h"

extern AST * ccif_get_value (char * const name, int *ncontext, 
				 int *status, const Sym ** const item);
extern void ccif_wo_quotes (const AST* , const char** , int * const);
extern Sym * ccif_add_value ( AST * const, AST * const,
                      const int, const int, const int) ;
extern char ccif_null_or_unknown ( const AST * const );
extern void ccif_clean_node ( const AST * const node );

extern int ccif_data_val_casecmp( const AST *v1, const AST *v2);
extern int ccif_data_val_cmp( const AST *v1, const AST *v2);
extern int ccif_data_val_realcmp ( const AST *v1, const AST *v2 );
extern int ccif_data_val_intcmp ( const AST *v1, const AST *v2 );

extern int ccif_data_key_realcmp (const SORT_KEY * key, const AST *v2);
extern int ccif_data_key_casecmp( const SORT_KEY *key, const AST *v2);
extern int ccif_data_key_cmp( const SORT_KEY *key, const AST *v2);
extern int ccif_data_key_intcmp (const SORT_KEY *key, const AST *v2);


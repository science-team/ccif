                                                                   /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "stdpccts.h"

/* This file is terribly organised, and badly needs re-doing. There is probably
 * scope for splitting this into three modules:
 * 
 *    A Fortran to C wrapper (The true original intention of f_interface.c)
 *    A file-handling module
 *    A value-handling module (i.e. one that deals with the semantics of CIF
                               data values, as opposed to value_manip.h, which
			       really deals with the nodes that point to
			       particular values)
 */


/* Need typedefs */
#include "sorcerer.h"
#ifdef ESV
#include <memory.h>
#endif

/* Fortran interface routines to CCIF library 

Fixme! Use regular expressions from dictionary, and regex package (one that works), to check
validity of input data. Don't rely on atof et al. (But only do this when we are sure that
the dictionary is correct in this respect.)
*/

#include "struct_manip1.h"
#include "struct_manip2.h"

static FILE *ccif_fileptr[MAX_CIF_FILES];

static int zztsp;

#ifdef VMS
static unsigned short int null_fid[3] = {0, 0, 0};  /* use memcmp with this, to identify DECnet files */
#endif

#if _USE_MMAP == 1

#ifdef VMS
extern int ccif_mapfile ( const char * const, FILE ** const, const CIF_FILE_LIST * const);
#else
extern int ccif_mapfile ( const char * const, FILE ** const);
#endif

__import__             MAP_FILE_RECORD *ccif_map_file_list[];
__export__ __noshare__ MAP_FILE_RECORD *ccif_current_mapfile=NULL;
__export__ __noshare__ int ccif_node_syntax_check=0;
#define CCIF_SCHK_ON   ccif_node_syntax_check=1;
#define CCIF_SCHK_OFF  ccif_node_syntax_check=0;

#else

#define CCIF_SCHK_ON 
#define CCIF_SCHK_OFF

#endif
char * ccif_strdup(const char *);


static void init_file_list(void) ;
static void add_to_data_block_list ( const int, const int, const int, AST * const) ;
static int ccif_file_open ( int * const cif_unit, char * const logname, 
                           const ccif_rwstat rwstat, int * const ifail) ;
static void duplicate_cif ( const int, const int, const char * const) ;
static int cif_unit_no (const char * const) ;
static void print_info (const int) ;
static void del_file_entry (const int unit) ;
static void housekeep_mapped_files(void);
static void print_info_callback(char callback_msg[], 
				void ** ptr, 
				const int * const count, 
				const int first);

static void ccif_output_fmt_callback(char callback_msg[],
				     void ** ptr,
				     const int * const count,
				     const int first);


__export__ __noshare__ 
      int ccif_number_of_blocks = 0;  /* Running total of number of data blocks in open CIF's */

/* N.B. In the following macro definitions, STRLEN(str) is the length of the variable as set in the
 * calling fortran module, i.e. equivalent to fortran LEN(STR).
 *
 */
#if CALL_LIKE_STARDENT
#define STRPTR(str)            str->Str_pointer
#define STRLEN(str)            str->Str_length
#else
#  if defined (VMS)
#define STRPTR(str)           str->dsc$a_pointer
#define STRLEN(str)           str->dsc$w_length
#  else
/* VERY IMPORTANT! Character arguments for HPUX, SUN and IRIS-like functions MUST be <name> and L<name> for
   this to work!
*/
#define STRPTR(str)           str
#define STRLEN(str)           L##str
#   endif
#endif

/* Put length of non-padded part of a fortran character parameter into length */
#define FLENGTH(str, length)  for ( length = STRLEN(str) - 1; \
                                   (long) length > -1 && STRPTR(str)[length] == ' ' ; \
                                    length--); \
                              length++; \
                              if ( length > MAXFLEN ) length = MAXFLEN - 1;

/* Put non-padded length of a chunk of a fotran buffer into length (length must be
 * initialised to maximum length first) */
#define FBLENGTH(str, off, length) for ( length-- ; (long) length > -1 && \
                                                    STRPTR(str)[off+length] == ' ' ; \
                                            length-- ) ;\
                                    length++; \
                                    if ( length > MAXFLEN ) length = MAXFLEN - 1;

#if CALL_LIKE_HPUX
   void ccif_init (const char * const symtab_f, const int Lsymtab_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_INIT (const struct Str_Desc * const symtab_f)
#endif
#if defined (VMS)
   void CCIF_INIT (const struct dsc$descriptor_s * const symtab_f)
#endif
#if CALL_LIKE_SUN 
   void ccif_init_ (const char *const symtab_f, const int Lsymtab_f)  
#endif
#if CALL_LIKE_IRIS
   void ccif_init (const int Lsymtab_f, const char *const symtab_f)  
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_INIT (const char *const symtab_f, const int Lsymtab_f)
# else
   void CCIF_INIT (const char *const symtab_f, const int Lsymtab_f)
# endif
#endif

{
   size_t Length;
   char symtab_c[80], *fname, *ioname;
   static int init=0;
   int i;

   __import__ __noshare__ int ttyout, ttyerr; /* From condition.c */
   __import__ __noshare__ int ccif_sort_numbers[NSORT_TREES];
   
   
   ccif_condition_init();

   if ( init ) {
      ccif_signal(CCIF_REINIT, "ccif_init", NULL, NULL, 0);
      return;
   }
   
   init = 1;

   ttyout = isouttty;
   ttyerr = iserrtty;

   for ( i = 0; i < NSORT_TREES; i++ )
     ccif_sort_numbers[i] = i;
   
   
   FLENGTH(symtab_f, Length)
   strncpy(symtab_c, STRPTR(symtab_f), Length);
   symtab_c[Length] = '\0';
   fname = ccif_logname2filename(symtab_c);
   if ( !fname ) 
      ccif_signal(CCIF_NOLOGNAME, "ccif_init", NULL, NULL, 0, symtab_c);

   
   ioname = getenv(symtab_c);
#if defined DEBUG || defined _MMAP_STATUS
   fprintf(stderr,mmap_status);
#endif
   zzs_undump(ioname, symtab_c, _USE_MMAP);  /* Can use mapping, because application progs 
                                            * won't modify string or hash tables,
                                            * and won't add or remove symbol table 
				            * records */

   /* Allocate a small buffer to the temporary string table, and allocate first byte.
      This is to allow an offset of 0 to be used as an 'OK' marker for category records.
      (See ccif_add_value, ccif_new_context, and comments in sym.h for loop_offset)
   */
   zzs_init(0,512);
   zzs_tmpstrdup("");

   init_file_list();


}

#if CALL_LIKE_HPUX
   void ccif_set_msg ( const char * const ctlname, 
		       const int * const new, int * const save, const int Lctlname)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_SET_MSG ( const struct Str_Desc * const ctlname,
		       const int * const new, int * const save)
#endif
#if defined (VMS)
   void CCIF_SET_MSG ( const struct dsc$descriptor_s * const ctlname,
		       const int * const new, int * const save)
#endif
#if CALL_LIKE_SUN
   void ccif_set_msg_ ( const char * const ctlname, 
		       const int * const new, int * const save, const int Lctlname)
#endif
#if CALL_LIKE_IRIS
   void ccif_set_msg ( const int Lctlname, const char * const ctlname, 
		       const int * const new, int * const save)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_SET_MSG ( const char * const ctlname, const int Lctlname, 
				 const int * const new, int * const save)
# else
   void CCIF_SET_MSG ( const char * const ctlname,
                       const int * const new, int * const save, const int Lctlname)
# endif
#endif

{

  char varname[MAXFLEN];
  size_t Length;

  FLENGTH(ctlname, Length);
  strncpy(varname, STRPTR(ctlname), Length);
  varname[Length] = '\0';
  *save = ccif_warnmsg_switch(varname, *new);


}
#if CALL_LIKE_HPUX
void ccif_load_cif (const char * const logname_f, int * const nblocks, 
		    int * const istat, const int Llogname_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_LOAD_CIF (const struct Str_Desc *const logname_f, 
		       int * const nblocks, int * const istat)
#endif
#if defined (VMS)
   void CCIF_LOAD_CIF (const struct dsc$descriptor_s *const logname_f, 
		       int * const nblocks, int * const istat)
#endif
#if CALL_LIKE_SUN
   void ccif_load_cif_ (const char * const logname_f, int * const nblocks, 
			int * const istat, const int Llogname_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_load_cif (const int Llogname_f, const char * const logname_f, 
		       int * const nblocks, int * const istat)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_LOAD_CIF (const char * const logname_f, const int Llogname_f, int * const nblocks, int * const istat)
# else
   void CCIF_LOAD_CIF (const char * const logname_f, int * const nblocks,
                        int * const istat, const int Llogname_f)
# endif
#endif
{
   size_t Length;
   char logname_c[MAXFLEN], *cifin, *filename;
   int new_blocks, cif_open, cif_unit, zzstp, mapped_file=(-2), i;

   #ifndef _MSC_VER
   CIF_FILE_LIST file_list_entry;
   #endif
   
   STreeParser cifparser;
   AST * star_root=NULL, *star_root_save;
   int antlr_retsignal;
   extern void ccif_sor_star_file( STreeParser *, AST **, const int);

#ifdef DEBUG
   time_t end_time, start_time;
#endif
   
   FLENGTH(logname_f, Length)
   strncpy(logname_c, STRPTR(logname_f), Length);
   logname_c[Length] = '\0';

   
#if 0
#if defined (sgi) && defined (DEBUG)
   mallopt(M_DEBUG, 1);
#endif
#endif /* 0 */

   *nblocks = *istat;
   *istat = 0;
   
   cif_open = ccif_file_open(&cif_unit, logname_c, r, nblocks); /* nblocks doubles up as ifail here */

   /* Soft error */
   if ( *nblocks == -1 ) {
     *istat = -1;
     return;
   }

   /* If file is already open, return. */
   if ( cif_open != cif_unit ) {
      *nblocks = -2;
      *istat = -2;
      return;
   }


   ccif_signal(CCIF_OPENING, NULL, NULL, NULL, 0, logname_c, ccif_file_name(cif_unit));
   cifin = ccif_file_io_name(cif_unit);

   /* Turn on attribute creation and lexical error reporting */
   set_lex_act(1);

   errno = 0;  /* EXIT_ON_SYS_ERROR uses this */
   if ( !(ccif_fileptr[cif_unit] = fopen(cifin, "r")) ) 

#if 0  /* Now handled in ccif_file_open */
     {
     if ( errno == ENOENT && nblocks == 1 ) {
       *nblocks = -1;
       return;
     }
#endif

     EXIT_ON_SYS_ERROR("ccif_load_cif", "Cannot open file CIFIN for reading");

#if 0  /* Now handled in ccif_file_open */
   }
#endif

#ifdef DEBUG
   start_time = time(NULL);
#endif

   zzastMARK;

#if _USE_MMAP == 1

   file_list_entry = ccif_file_list_entry(cif_unit);

   mapped_file = ccif_mapfile(cifin, &(ccif_fileptr[cif_unit])
#ifdef VMS
      , &file_list_entry
#endif
   );

#ifdef DEBUG     
   if ( mapped_file == -1 ) {
     printf("\nNot mapping %s: maximum number of mapped files exceeded\n",cifin);
   }
   
   if ( mapped_file == -2 ) {
     printf("\nNot mapping %s: ccif_mapfile returns -2\n",cifin);
   }


#endif

   if ( mapped_file >= 0 && mapped_file < MAX_MAPPED_FILES ) {
     ccif_current_mapfile = ccif_map_file_list[mapped_file];
     zzmode(START);
     ANTLRs( (new_blocks = ccif_antlr_star_file(&star_root, &antlr_retsignal, 1)), \
         ccif_current_mapfile->start );
     ccif_current_mapfile = NULL;
   }
   else
 
#endif /* #if _USE_MMAP == 1 */
   {
      ANTLRm( (new_blocks = ccif_antlr_star_file(&star_root, &antlr_retsignal, 1))
, ccif_fileptr[cif_unit], START);
      fclose(ccif_fileptr[cif_unit]);
   }

   zzastREL;

   /* Handle signal returned by parser */
   switch ( antlr_retsignal ) {
   case NoSignal:
     break;

   case MismatchedToken:
     ccif_signal(ANTLR_MISTOK, "\"ccif_load_cif\" (caught parser exception)\n\n\
            ======================================\n\
            >>>> Remainder of file unusable!! <<<<\n\
            ======================================\n", 
		 NULL, NULL, 0, zzline);
     break;

   case NoViableAlt:
     ccif_signal(ANTLR_NVALT, "\"ccif_load_cif\" (caught parser exception)\n\n\
            ======================================\n\
            >>>> Remainder of file unusable!! <<<<\n\
            ======================================\n", 
		 NULL, NULL, 0, zzline);
     break;

   default:
     ccif_signal(ANTLR_UNKPSIG, "\"ccif_load_cif\" (caught parser exception)\n\n\
            ======================================\n\
            >>>> Remainder of file unusable!! <<<<\n\
            ======================================\n", 
		 NULL, NULL, 0, zzline);
     break;

   }

   *istat = antlr_retsignal;

#ifdef DEBUG
   end_time = time(NULL);
   printf("Time taken to parse file: %d seconds\n", end_time - start_time);
   start_time = time(NULL);
#endif



   /* Probably don't need to doubly-link AST, since we are not allowed to make
    * changes to it */
   

   STreeParserInit (&cifparser);
   ccif_number_of_blocks += new_blocks;
   star_root_save = star_root;
   ccif_sor_star_file (&cifparser, &star_root, ccif_number_of_blocks - new_blocks);

#ifdef DEBUG
   end_time = time(NULL);
#endif

   add_to_data_block_list (ccif_number_of_blocks - new_blocks, new_blocks, cif_unit, star_root_save);
   
   print_info(cif_unit);

#ifdef DEBUG
   printf("Time taken to walk AST: %d seconds\n", end_time - start_time);

#if _USE_MMAP == 1
  printf("\nMapped file information: (CCIF_LOAD_CIF('%s',...))\n", logname_c);
  _MAPPED_FILE_INFO
#endif  /* _USE_MMAP == 1 */
#endif /*DEBUG */

   *nblocks = new_blocks;
}


#if CALL_LIKE_HPUX
   void ccif_new_cif (const char * const logname_f, const char * const new_logname_f, int * const nblocks, 
		      const int Llogname_f, const int Lnew_logname_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_NEW_CIF (const struct Str_Desc * const logname_f, 
		      const struct Str_Desc * const new_logname_f, int * const nblocks)
#endif
#if defined (VMS)
   void CCIF_NEW_CIF (const struct dsc$descriptor_s *const logname_f, 
		      const struct dsc$descriptor_s *const new_logname_f, int * const nblocks)
#endif
#if CALL_LIKE_SUN
   void ccif_new_cif_ (const char * const logname_f, const char * const new_logname_f, int * const nblocks, 
		       const int Llogname_f, const int Lnew_logname_f)  
#endif
#if CALL_LIKE_IRIS
   void ccif_new_cif (const int Llogname_f, const char * const logname_f, 
		       const int Lnew_logname_f, const char * const new_logname_f, int * const nblocks)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_NEW_CIF (const char * const logname_f, const int Llogname_f, 
				const char * const new_logname_f, const int Lnew_logname_f,
				int * const nblocks) 
# else
   void CCIF_NEW_CIF (const char * const logname_f, const char * const new_logname_f, int * const nblocks,
                       const int Llogname_f, const int Lnew_logname_f)
# endif
#endif

{
   size_t Length;
   char logname_c[MAXFLEN], new_logname_c[MAXFLEN], *cifin, *cifout;
   int new_blocks, cif_open, cif_unit, i, ifail = *nblocks;
   
   #ifndef _MSC_VER
   STreeParser cifparser;
   #endif

   FLENGTH(logname_f, Length)
   strncpy(logname_c, STRPTR(logname_f), Length);
   logname_c[Length] = '\0';

   FLENGTH(new_logname_f, Length)
   strncpy(new_logname_c, STRPTR(new_logname_f), Length);
   new_logname_c[Length] = '\0';
   
   cif_open = ccif_file_open(&cif_unit, new_logname_c, w, nblocks);
   
   /* If logical file is already open, return... */
   if ( cif_open != cif_unit )  {
      *nblocks = -1;
      return;
   }

   ccif_signal(CCIF_OPENING, NULL, NULL, NULL, 0, new_logname_c, ccif_file_name(cif_unit));
   /*   cifin = ccif_file_io_name(cif_unit); */

   if ( logname_c[0] ) {    /* output CIF to be generated from input CIF */
      i = cif_unit_no(logname_c);
      if ( i == -1 ) ccif_file_open( &i, logname_c, r, &ifail );

      if ( ifail == -1 ) {   /* Soft error */
        *nblocks = -1;
        return;
      }

      duplicate_cif (i, cif_unit, new_logname_c);
      *nblocks = ccif_no_of_blocks(i);
   }
   else {
      *nblocks = 0;
   }
   print_info(cif_unit);

#if defined DEBUG && _USE_MMAP == 1
  printf("\nMapped file information: (CCIF_NEW_CIF('%s','%s'...))\n", 
	 logname_c[0]?logname_c:"<NULL>", new_logname_c);
   _MAPPED_FILE_INFO
#endif

}



/* Input status:     0 => dump output file, if not already dumped, and it was opened for writing
                     1 => dump output file regardless
                     2 => close without dumping

   Returned status:  0 => file not dumped
                     1 => file dumped
*/

#if CALL_LIKE_HPUX
   void ccif_close_cif (const char * const logname_f, int * const status, const int Llogname_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_CLOSE_CIF (const struct Str_Desc * const logname_f, int * const status)
#endif
#if defined (VMS)
   void CCIF_CLOSE_CIF (const struct dsc$descriptor_s *const logname_f, int * const status)
#endif
#if CALL_LIKE_SUN
   void ccif_close_cif_ (const char * const logname_f, int * const status, const int Llogname_f)  
#endif
#if CALL_LIKE_IRIS
   void ccif_close_cif (const int Llogname_f, const char * const logname_f, int * const status)  
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
     void __stdcall CCIF_CLOSE_CIF(const char * const logname_f, const int Llogname_f,
				   int * const status)  
# else
   void CCIF_CLOSE_CIF (const char * const logname_f, int * const status, const int Llogname_f)
# endif
#endif

{

  size_t Length;
  char logname_c[MAXFLEN];
  CIF_FILE_LIST file_list_entry;
  int unit, i;

  STreeParser cifparser;
  AST * star_root;
  extern void ccif_print_star_file( STreeParser *, AST **, const int);

  FLENGTH(logname_f, Length)
  strncpy(logname_c, STRPTR(logname_f), Length);
  logname_c[Length] = '\0';

  unit = cif_unit_no (logname_c);
  if ( unit == -1 ) 
    ccif_signal(CCIF_CIFNOTOPEN, "ccif_close_cif", NULL, NULL, 0, logname_c);


  file_list_entry = ccif_file_list_entry(unit);


  if ( ( *status == 1 || ( *status == 0 && !file_list_entry.data_blocks->printed ) )
            && file_list_entry.status == w ) {

    star_root = file_list_entry.data_blocks->root;
    STreeParserInit (&cifparser);
    ccif_print_star_file (&cifparser, &star_root, unit );
    *status = 1;
  }
  else
    *status = 0;

  del_file_entry(unit);

  if ( file_list_entry.data_blocks->nfiles > 1 ) { /* Don't touch AST */
    (file_list_entry.data_blocks->nfiles)--;
    return;
  }

  free (file_list_entry.data_blocks->block_id);
  free (file_list_entry.data_blocks->block_node);
  free (file_list_entry.data_blocks->audit);
  ccif_ast_free(file_list_entry.data_blocks->root);
  free (file_list_entry.data_blocks);

#if defined DEBUG && _USE_MMAP == 1
  printf("\nMapped file information: (CCIF_CLOSE_CIF('%s',...))\n", logname_c);
  _MAPPED_FILE_INFO
#endif

  _HOUSEKEEP_MAPPED_FILES

}


#if CALL_LIKE_HPUX
     void ccif_output_fmt (const char * const name_f, 
			   const char * const flags_f,
			   int * const width, int * const prec,
			   const char * const fmt_f,
			   int * const istat,
			   const int Lname_f, const int Lflags_f, const int Lfmt_f)
#endif                     
#if CALL_LIKE_STARDENT
   void CCIF_OUTPUT_FMT (const struct Str_Desc *const name_f, 
			 const struct Str_Desc *const flags_f, 
			 int * const width, int * const prec, 
			 const struct Str_Desc *const fmt_f,
			 int * const istat)
#endif 
#if defined (VMS)
   void CCIF_OUTPUT_FMT (const struct dsc$descriptor_s *const name_f, 
			 const struct dsc$descriptor_s *const flags_f, 
			 int * const width, int * const prec, 
			 const struct dsc$descriptor_s *const fmt_f,
			 int * const istat)
#endif 
#if CALL_LIKE_SUN
     void ccif_output_fmt_ (const char * const name_f, 
			   const char * const flags_f,
			   int * const width, int * const prec,
			   const char * const fmt_f,
			   int * const istat,
			   const int Lname_f, const int Lflags_f, const int Lfmt_f)
#endif                          
#if CALL_LIKE_IRIS
     void ccif_output_fmt (const int Lname_f, const char * const name_f, 
			   const int Lflags_f, const char * const flags_f,
			   int * const width, int * const prec,
			   const int Lfmt_f, const char * const fmt_f,
			   int * const istat)
#endif                               
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_OUTPUT_FMT (const char * const name_f, const int Lname_f,  
				   const char * const flags_f, const int Lflags_f,
				   int * const width, int * const prec,
				   const char * const fmt_f, const int Lfmt_f,
				   int * const istat)
# else
     void CCIF_OUTPUT_FMT (const char * const name_f,
                           const char * const flags_f,
                           int * const width, int * const prec,
                           const char * const fmt_f,
                           int * const istat,
                           const int Lname_f, const int Lflags_f, const int Lfmt_f)
# endif
#endif

{
   char name_c[MAXCLLEN+1], flags_c[5], test[MAXCLLEN+1], fmt_buf[81], *cptr;
   int lname, lfmt, i, Length;
   Sym *item;
   ItemTypeList *type_ptr;
   
   static const char * const allowed_flags = "-+ 0";
   static int t1int=123, t2int;
   static float t1float=1.23e4, t2float;
   static char *t1char = "abcxyz", t2char[10];
   
   FLENGTH(name_f, lname)
   strncpy(name_c, STRPTR(name_f), lname);
   name_c[lname] = '\0';

   item = zzs_get(name_c);
   if ( !item ) {
     ccif_signal(CCIF_NOITEM, "ccif_output_fmt", ccif_output_fmt_callback, NULL, 0, name_c);
     *istat = -1;
     return;
   }
   
   

   /* 
      Start processing bits of format spec.
      Conversion character:
   */

   FLENGTH(fmt_f, lfmt);
   if ( lfmt > 1 ) {
     ccif_signal(CCIF_BADFMTCHAR, "ccif_output_fmt", ccif_output_fmt_callback, NULL, 0, name_c);
     *istat = -2;
     return;
   }

   /* Checking of conversion character vs. item type below */


   /* Deal with category case explicitly first */
   if ( item->category.rec == item ) {
     CCIF_CALLOC(1, char, item->fmt, "ccif_output_fmt", (0));

     if ( *(STRPTR(fmt_f)) == 'n' )
       item->fmt[0] = '\n';
     else
       item->fmt[0] = ' ';
     
     *istat = 0;
     return;
   }

   /* 
      Flags:
   */
   
   FLENGTH(flags_f, Length);
   if ( Length > 4 ) {
     ccif_signal( CCIF_BADFMTFLAGS, "ccif_output_fmt", ccif_output_fmt_callback, NULL, 0, name_c, allowed_flags);
     *istat = -2;
     return;
   }
       
   strncpy(flags_c, STRPTR(flags_f), Length);
   flags_c[Length] = '\0';

   for ( cptr = flags_c; *cptr; cptr++ ) {
     if ( *cptr == '_' ) *cptr = ' ';
     if ( ! strchr(allowed_flags, *cptr) ) {
       ccif_signal( CCIF_BADFMTFLAGS, "ccif_output_fmt", ccif_output_fmt_callback, NULL, 0, name_c, allowed_flags);
       *istat = -2;
       return;
     }
   }
   

   type_ptr = ITEM_TYPE(item);

   /* Bit of format specifier apropriate to all types */
   fmt_buf[0] = '%';
   strcpy(fmt_buf+1, flags_c);
   if ( *width > 0 )
     sprintf ( fmt_buf + strlen(fmt_buf), "%d", *width );

   i = 0;
   switch ( type_ptr->basic_type) {

    case (1):                 /* char/text */
      strcat(fmt_buf,"s");
      sprintf(test, fmt_buf, t1char);
      sscanf(test, "%s", t2char);
      i = !strcmp(t1char, t2char);
      break;

    case (2):                 /* int */
      if ( ! strchr ( "id", *(STRPTR(fmt_f)) ) ) {
	ccif_signal(CCIF_BADFMTCHAR, "ccif_output_fmt", ccif_output_fmt_callback, NULL, 0, name_c);
	i = -1;
	break;
      }

      if ( *prec > 0 )
	sprintf ( fmt_buf + strlen(fmt_buf), ".%d%c", *prec, *(STRPTR(fmt_f)) );
      else
	sprintf ( fmt_buf + strlen(fmt_buf), "%c", *(STRPTR(fmt_f)) );

      sprintf(test, fmt_buf, t1int);
      sscanf(test, "%d", &t2int);
      i = (t1int == t2int);
      break;

    case (3):
      if ( ! strchr ("feEgG", *(STRPTR(fmt_f)) ) ) {
	ccif_signal(CCIF_BADFMTCHAR, "ccif_output_fmt", ccif_output_fmt_callback, NULL, 0, name_c);
	i = -1;
	break;
      }

      if ( *prec > 0 )
	sprintf ( fmt_buf + strlen(fmt_buf), ".%d%c", *prec, *(STRPTR(fmt_f)) );
      else
	sprintf ( fmt_buf + strlen(fmt_buf), "%c", *(STRPTR(fmt_f)) );


#ifdef OLD_SPRINTF
      i = 1;
#else
      i = sprintf(test, fmt_buf, t1float);
#endif
      break;

    default:
      if ( *prec > 0 )
	sprintf ( fmt_buf + strlen(fmt_buf), ".%d%c", *prec, *(STRPTR(fmt_f)) );
      else
	sprintf ( fmt_buf + strlen(fmt_buf), "%c", *(STRPTR(fmt_f)) );
#ifdef DEBUG
      fprintf(stderr,"Cannot determine basic type of item %s; applying format string \"%s\" unconditionally!\n",
	     name_c, fmt_buf);
#endif
      i = 1;
   }
   

#ifdef DEBUG
   fprintf (stderr, "Format \"%s\" applied to \"%s\"\n", fmt_buf, name_c);
#endif

   if ( item->fmt ) free (item->fmt);
   item->fmt = NULL;
   if ( item->true_name.rec )
     item->true_name.rec->fmt= NULL;

   if ( i < 0 ) /* incorrect conversion character supplied */
     return;

   if ( !i ) {
     ccif_signal(CCIF_BADFMT, "ccif_output_fmt", NULL, NULL, 0, name_c, fmt_buf);
     *istat = -2;
   }
   else {
     item->fmt = strdup(fmt_buf);
     if ( !item->fmt ) 
       ccif_signal (CCIF_NOMEM, "ccif_output_fmt", NULL, NULL, 0);

     item->width = *width;

     if ( item->true_name.rec ) {
       item->true_name.rec->fmt = item->fmt;
       item->true_name.rec->width = *width;
     *istat = 0;
   }
 }
}



static void ccif_output_fmt_callback(char callback_msg[],
				     void ** ptr,
				     const int * const count,
				     const int first) {
  if ( first ) 
    strcpy(callback_msg, "Not setting an output format string");
  else
    callback_msg[0] = '\0';
}

#if CALL_LIKE_HPUX
   void ccif_set_line_limit(const char * const logname_f, 
			    const int * const limit, const int Llogname_f)
#endif                     
#if CALL_LIKE_STARDENT
   void CCIF_SET_LINE_LIMIT(const struct Str_Desc * const logname_f, 
			    const int * const limit)
#endif 
#if defined (VMS)
   void CCIF_SET_LINE_LIMIT(const struct dsc$descriptor_s * const logname_f, 
			    const int * const limit)
#endif 
#if CALL_LIKE_SUN               
   void ccif_set_line_limit_(const char * const logname_f, 
			     const int * const limit, const int Llogname_f)
#endif                          
#if CALL_LIKE_IRIS
   void ccif_set_line_limit(const int Llogname_f, const char * const logname_f, 
			    const int * const limit, )
#endif                               
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_SET_LINE_LIMIT (const char * const logname_f, const int Llogname_f, 
				       const int * const limit)
# else
   void CCIF_SET_LINE_LIMIT(const char * const logname_f,
                             const int * const limit, const int Llogname_f)
# endif
#endif

{
  char logname_c[81];
  size_t Length;
  int unit;
  CIF_FILE_LIST file_list_entry;

  FLENGTH(logname_f, Length);
  strncpy(logname_c, STRPTR(logname_f), Length);
  logname_c[Length] = '\0';

  unit = cif_unit_no(logname_c);

  if ( unit == -1 ) {
    ccif_signal( (CCIF_CIFNOTOPEN & ~CCIF_M_SEVERITY ) | CCIF_K_WARN, "CCIF_SET_LINE_LIMIT", NULL, NULL, 0, logname_c);
    return;
  }

  file_list_entry = ccif_file_list_entry(unit);

  if ( file_list_entry.status != w ) {
    ccif_signal( (CCIF_READONLY & ~CCIF_M_SEVERITY ) | CCIF_K_WARN, "CCIF_SET_LINE_LIMIT", NULL, NULL, 0, logname_c);
      return;
  }

  if ( *limit < 1 || *limit > ZZLEXBUFSIZE ) {
    ccif_signal( CCIF_LINETOOLONG, "CCIF_SET_LINE_LIMIT", NULL, NULL, 0, *limit, ZZLEXBUFSIZE);
    return;
  }

  ccif_change_line_limit(unit, *limit);
  
}
      
	       

#if CALL_LIKE_HPUX
   void ccif_setup_context (const char * const name_f, char * const cat_f, const int * const block, 
                              int * const ncontext, int * const istat, const char * const disp_f,
                              const int Lname_f, const int Lcat_f, const int Ldisp_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_SETUP_CONTEXT (const struct Str_Desc * const name_f, struct Str_Desc * const cat_f, 
                               const int * const block, int * const ncontext, int * const istat, 
                               const struct Str_Desc * const disp_f)
#endif
#if defined (VMS)
   void CCIF_SETUP_CONTEXT (const struct dsc$descriptor_s *const name_f, struct dsc$descriptor_s * const cat_f,
                            const int * const block, int * const ncontext, int * const istat,
                            const struct dsc$descriptor_s *const disp_f)
#endif
#if CALL_LIKE_SUN
   void ccif_setup_context_ (const char * const name_f, char * const cat_f, const int * const block,
                              int * const ncontext, int * const istat, const char * const disp_f,
                              const int Lname_f, const int Lcat_f, const int Ldisp_f)  
#endif
#if CALL_LIKE_IRIS
   void ccif_setup_context (const int Lname_f, const char * const name_f, const int Lcat_f, char * const cat_f,
                               const int * const block, int * const ncontext, int * const istat, const int Ldisp_f,
                               const char * const disp_f)  
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_SETUP_CONTEXT (const char * const name_f, const int Lname_f, 
				      char * const cat_f, const int Lcat_f, 
				      const int * const block, int * const ncontext, 
				      int * const istat, 
				      const char * const disp_f, const int Ldisp_f)  
# else
   void CCIF_SETUP_CONTEXT (const char * const name_f, char * const cat_f, const int * const block,
                              int * const ncontext, int * const istat, const char * const disp_f,
                              const int Lname_f, const int Lcat_f, const int Ldisp_f)
# endif
#endif
{
   size_t Length;
   char name_c[MAXCLLEN+1], disp_c[10], *cat_c;
   const Sym *item;
   int ro, loop;
   
   CONTEXT_LIST context_entry;

   FLENGTH(name_f, Length)
   strncpy(name_c, STRPTR(name_f), Length);
   name_c[Length] = '\0';

   FLENGTH(disp_f, Length)
   Length = MIN(Length, 9);
   strncpy(disp_c, STRPTR(disp_f), Length);
   disp_c[Length] = '\0';

   ro = (!zzs_keycmp("ro",disp_c) || !zzs_keycmp("readonly",disp_c ) );
   loop = !zzs_keycmp(disp_c, "loop");
   
   item = zzs_get(name_c);
   if ( !item ) {
     ccif_signal(CCIF_NOITEM, "ccif_setup_context", NULL, NULL, 0, name_c);
     *ncontext = -1;
     *istat = NO_VALUE;
     return;
   }
   *ncontext = ccif_new_context( item->category.rec, istat, *block, ro);

   memset( STRPTR(cat_f), ' ', STRLEN(cat_f) );
   cat_c = zzs_recname(item->category.rec);
   strncpy( STRPTR(cat_f), cat_c, MIN(STRLEN(cat_f), strlen(cat_c)) );


   if ( *ncontext >= 0 ) {
      context_entry = ccif_context_list((const int) *ncontext);
   
      if ( *istat == CAT_NOT_PRESENT ) {
	 AST * loop_root = ccif_new_category(&context_entry, loop);
	 if ( loop ) {
	    struct _packet packet_ptr = {NULL, NULL};
	    context_entry.packet = &packet_ptr;
	    context_entry.packet[0].loop_item = loop_root->down;
	    context_entry.loop_add = 1;
	    ccif_refresh_contexts(context_entry);
	 }
      }
      else {
	 if ( loop )  {
	    if ( ccif_data_items_to_loop(&context_entry) ) {
	       ccif_refresh_contexts(context_entry);
	    }
	 }
      }
   }				       /* if ( *ncontext >= 0 ) */



   
#if DEBUG
   { 
      int rows, cols, ovf;
      if ( *istat == CAT_NOT_PRESENT && !ro ) {
	fprintf(stderr, "Context set up for new category %s: number %d\n", cat_c, *ncontext);
      }
      else if ( *istat == LOOP_CONTEXT) {
      LOOP_DIMENSION(item->category.rec->node[*block], rows, cols, ovf)
        fprintf(stderr, "Context set up for existing looped category %s: number %d, with %d rows, %d columns and %d overflow values\n",
                cat_c, *ncontext, vals, cols, ovf );

      }
      else if ( *istat == ITEM_CONTEXT ) {
	fprintf(stderr, "Context set up for existing non-looped category %s\n", cat_c);
      }
   }
#endif

   return;
}



#if CALL_LIKE_HPUX
   void ccif_release_context ( int * const context )
#endif
#if CALL_LIKE_STARDENT
   void CCIF_RELEASE_CONTEXT ( int * const context )
#endif
#if defined (VMS)
   void CCIF_RELEASE_CONTEXT ( int * const context )
#endif
#if CALL_LIKE_SUN
   void ccif_release_context_ ( int * const context )
#endif
#if CALL_LIKE_IRIS
   void ccif_release_context ( int * const context )
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_RELEASE_CONTEXT ( int * const context )
# else
   void CCIF_RELEASE_CONTEXT ( int * const context )
# endif
#endif

{

#ifdef DEBUG
  fprintf(stderr,"Releasing context %d\n",*context);
#endif
  ccif_free_context(context);  /* ccif_free_context checks that context is in range */

}


#if CALL_LIKE_HPUX
   void ccif_context_status ( const int * const ncontext, int * const block, 
                              char * const catname_f, int * const ncols, 
			      int * const sindex, int * const status, const int Lcatname_f )
#endif
#ifdef CALL_LIKE_STARDENT
   void CCIF_CONTEXT_STATUS ( const int * const ncontext, int * const block,
                              struct Str_Desc * const catname_f, int * const ncols,
                              int * const sindex, int * const status )
#endif
#if defined (VMS)
   void CCIF_CONTEXT_STATUS ( const int * const ncontext, int * const block,
                              struct dsc$descriptor_s * const catname_f, int * const ncols,
                              int * const sindex, int * const status )
#endif
#if CALL_LIKE_SUN
   void ccif_context_status_ ( const int * const ncontext, int * const block, 
                              char * const catname_f, int * const ncols,
                              int * const sindex, int * const status, const int Lcatname_f )
#endif
#if CALL_LIKE_IRIS
   void ccif_context_status ( const int * const ncontext, int * const block, 
                              const int Lcatname_f, char * const catname_f, 
                              int * const sindex, int * const ncols,  int * const status )
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_CONTEXT_STATUS ( const int * const ncontext, int * const block, 
					char * const catname_f, const int Lcatname_f,
					int * const ncols,
					int * const sindex, int * const status)
# else
   void CCIF_CONTEXT_STATUS ( const int * const ncontext, int * const block,
                              char * const catname_f, int * const ncols,
                              int * const sindex, int * const status, const int Lcatname_f )
# endif
#endif
{

  CONTEXT_LIST context;
  const char * catname_c;
  int lcatname_c;

   memset( STRPTR(catname_f), ' ', STRLEN(catname_f) );
  *status = -1;
  *block = -1;
  *ncols = -1;

  if ( *ncontext < 0 || *ncontext >= NCONTEXTS)
    return;

  context = ccif_context_list(*ncontext);
  if ( !context.category )
    return;

  *ncols = context.ncols;
  *block = context.block;
  *status = context.rwstat;
  *sindex = context.tree_index;
  catname_c = zzs_recname(context.category);
  lcatname_c = strlen(catname_c);
  strncpy(STRPTR(catname_f), catname_c, MIN( lcatname_c, STRLEN(catname_f) )); 

}

#if CALL_LIKE_HPUX
   void ccif_copy_row ( int * const cxin, int * const cxout, int * const status )
#endif
#if CALL_LIKE_STARDENT
   void CCIF_COPY_ROW ( int * const cxin, int * const cxout, int * const status )
#endif
#if defined (VMS)
   void CCIF_COPY_ROW ( int * const cxin, int * const cxout, int * const status )
#endif
#if CALL_LIKE_SUN
   void ccif_copy_row_ ( int * const cxin, int * const cxout, int * const status )
#endif
#if CALL_LIKE_IRIS
   void ccif_copy_row ( int * const cxin, int * const cxout, int * const status )
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_COPY_ROW ( int * const cxin, int * const cxout, int * const status )
# else
   void CCIF_COPY_ROW ( int * const cxin, int * const cxout, int * const status )
# endif
#endif
{
   
   CONTEXT_LIST contextin, contextout;
   AST * inptr_name, * inptr_value, *outptr_value;
   int inblock, outblock;
   const Sym *item;
   
   if ( *cxin == *cxout ) 
     ccif_signal(CCIF_SAMECXT, "ccif_copy_row", NULL, NULL, 0);

   contextout = ccif_context_list(*cxout);
   if ( contextout.rwstat != w )
     ccif_signal(CCIF_ROCXT, "ccif_copy_row", NULL, NULL, 0, *cxout);

   outblock = contextout.block;
   if ( contextout.category->node[outblock]->token != Loop )
     ccif_signal(CCIF_TONONLOOPED, "ccif_copy_row", NULL, NULL, 0);
       
   contextin = ccif_context_list(*cxin);
   inblock = contextin.block;

   if ( contextin.category != contextout.category )
     ccif_signal(CCIF_DIFFCAT, "ccif_copy_row", NULL, NULL, 0);
       
   
    inptr_name = contextin.category->node[inblock];

   /* Fixme! Ought to be valid, to copy from non-looped data into a loop row.
    * Needs a category scope in the Sym structure */
   if ( inptr_name->token != Loop )
     ccif_signal(CCIF_FROMNONLOOPED, "ccif_copy_row", NULL, NULL, 0);

   inptr_name = inptr_name->down->down;


   switch (*status) {
      
    case (APPEND_ROW):
      ccif_add_row(*cxout);
      *status = KEEP_CONTEXT;
      contextout = ccif_context_list(*cxout); /* Just in case contextout.packet has been moved by
					         realloc in ccif_refresh_contexts */
      break;			/* Is this right? */
       
    case (ADVANCE_CONTEXT):
    case (ADVANCE_AND_HOLD):
      outptr_value = ccif_get_value(inptr_name->a->text, cxout, status, &item);
      if (*status == END_OF_CONTEXT) return;
   }
      
   if ( contextin.packet[0].loop_item == contextout.packet[0].loop_item )
     return;
   
      
   for ( inptr_value = contextin.packet[1].loop_item;
	 inptr_name;
	 inptr_name = inptr_name->right, 
	    inptr_value = inptr_value->right ) {

      outptr_value = ccif_get_value(inptr_name->a->text, cxout, status, &item);
      if ( !outptr_value ) {
	 ccif_new_data_item( inptr_name->a->text, 0, NULL, &contextout );
	 *status = KEEP_CONTEXT;
	 ccif_refresh_contexts(contextout);
	 outptr_value = ccif_get_value(NULL, cxout, status, &item);
      }
      
      zzcr_ast(outptr_value, &(inptr_value->a), inptr_value->token, NULL);
   }
}
      


/* Data retrieval routines. The itmval_f parameter is to return the text as-is (except for numeric data
 * which is specified as '?', '.', when it is returned with quotes and leading/trailing spaces trimmed off).
   rval,ival,cval are for processable items (cval will always have quotes and leading/trailing spaces 
   trimmed off).
 */

#if CALL_LIKE_HPUX
   void ccif_get_real (const char * const itmnam_f, char * const itmval_f, float *rval, int *ncntxt, 
                       int *istat, const int Litmnam_f, const int Litmval_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_GET_REAL (const struct Str_Desc * const itmnam_f, struct Str_Desc * const itmval_f, float *rval, 
                       int *ncntxt, int *istat)
#endif
#if defined (VMS)
   void CCIF_GET_REAL (const struct dsc$descriptor_s * const itmnam_f, struct dsc$descriptor_s * const itmval_f, 
                       float *rval, int *ncntxt, int *istat)
#endif
#if CALL_LIKE_SUN
   void ccif_get_real_ (const char *const itmnam_f, char * const itmval_f, float *rval, int *ncntxt, 
                        int *istat, const int Litmnam_f, const int Litmval_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_get_real (const int Litmnam_f, const char *const itmnam_f, const int Litmval_f, 
                        char * const itmval_f, float *rval, int *ncntxt, int *istat)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_GET_REAL (const char *const itmnam_f, const int Litmnam_f, 
				 char * const itmval_f, const int Litmval_f, 
				 float *rval, int *ncntxt, int *istat)
# else
   void CCIF_GET_REAL (const char *const itmnam_f, char * const itmval_f, float *rval, int *ncntxt,
                        int *istat, const int Litmnam_f, const int Litmval_f)
# endif
#endif

{ 
   size_t Length, itmlen;
   char itmnam_c[MAXCLLEN+1], *itmval_c;
   const Sym *item=NULL;
   AST *node;
   ItemTypeList *type_ptr;
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';
   
   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   type_ptr = ITEM_TYPE(item);
      
   if ( zzs_keycmp( zzs_symname(type_ptr->primitive_code), "numb") ) 
     ccif_signal(CCIF_NOTNUM, "ccif_get_real", NULL, NULL, 0, itmnam_c);
   else {
      if ( type_ptr->basic_type == 2 ) {
         ccif_signal(CCIF_ISINT, "ccif_get_real", NULL, NULL, 0, itmnam_c);
      }
   }
   
   memset ( STRPTR(itmval_f), ' ', STRLEN(itmval_f) );
   
   switch ( *istat ) {

     case ( END_OF_CONTEXT ) :
       return;

     case ( SINGLE_VALUE ):
     case ( LOOP_VALUE ):
   
        *rval = atof(node->a->text); /* Numerics must not be quoted, so don't use itmval_c for this */
        itmlen = MIN( STRLEN(itmval_f), strlen(node->a->text) );
        strncpy ( STRPTR(itmval_f), node->a->text, itmlen );    /* This will not append a '\0' */
        return;
   
     case ( NO_VALUE ):
     case ( LOOP_UNKNOWN ):
       if ( MIN(item->type_code[0] , item->type_code[1]) < 0 || 
           ( item->type_code[0] == 0 && item->type_code[1] == 0) )  /* Fixme! probably a simpler equivalent */
          *(STRPTR(itmval_f)) = '?';
       else {
          *rval = item->defval.dfloat;
          *istat |= 4;   /* Change to SINGLE_DICT/LOOP_DICT */
       }
       return;
       
     case ( SINGLE_NULL ):
     case ( LOOP_NULL ):
        *(STRPTR(itmval_f)) = '.';
        return;
   }
}






#if CALL_LIKE_HPUX
   void ccif_get_real_esd (const char * const itmnam_f, 
                           char * const itmval_f, float * const rval, 
                           char * const esdval_f, float * const esd,
                           int * const ncntxt, int *const istat, int * const istat_esd,
                           const int Litmnam_f, const int Litmval_f, const int Lesdval_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_GET_REAL_ESD (const struct Str_Desc * const itmnam_f, 
                           struct Str_Desc * const itmval_f, float * const rval, 
                           struct Str_Desc * const esdval_f, float * const esd,
                           int *const ncntxt, int * const istat, int * const istat_esd)
#endif
#if defined (VMS)
   void CCIF_GET_REAL_ESD (const struct dsc$descriptor_s * const itmnam_f, 
                           struct dsc$descriptor_s * const itmval_f,  float * const rval,
                           struct dsc$descriptor_s * const esdval_f,  float * const esd,
                           int *const ncntxt, int * const istat, int * const istat_esd)
#endif
#if CALL_LIKE_SUN
   void ccif_get_real_esd_ (const char * const itmnam_f, 
                           char * const itmval_f, float * const rval, 
                           char * const esdval_f, float * const esd,
                           int * const ncntxt, int *const istat, int * const istat_esd,
                           const int Litmnam_f, const int Litmval_f, const int Lesdval_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_get_real_esd (const int Litmnam_f, const char *const itmnam_f, 
                            const int Litmval_f, char * const itmval_f, float * const rval, 
                            const int Lesdval_f, char * const esdval_f, float * const esd,
                            int * const ncntxt, int *const istat, int * const istat_esd)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_GET_REAL_ESD (const char * const itmnam_f, const int Litmnam_f,
				     char * const itmval_f, const int Litmval_f,
				     float * const rval, 
				     char * const esdval_f, const int Lesdval_f, 
				     float * const esd, int * const ncntxt, 
				     int *const istat, int * const istat_esd)
# else
   void CCIF_GET_REAL_ESD (const char * const itmnam_f,
                           char * const itmval_f, float * const rval,
                           char * const esdval_f, float * const esd,
                           int * const ncntxt, int *const istat, int * const istat_esd,
                           const int Litmnam_f, const int Litmval_f, const int Lesdval_f)
# endif
#endif
{ 
   size_t Length, itmlen, esdlen;
   char itmnam_c[MAXCLLEN+1], *itmval_c, *esdval_c;
   const Sym *item=NULL, *assoc_esd=NULL;
   AST *node, *node_esd;
   ItemTypeList *type_ptr;

   regmatch_t pmatch[NMATCH];
   int status;
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';
   
   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   type_ptr = ITEM_TYPE(item);
      
   if ( zzs_keycmp( zzs_symname(type_ptr->primitive_code), "numb") ) 
     ccif_signal(CCIF_NOTREAL, "ccif_get_real_esd", NULL, NULL, 0, itmnam_c);
   else {
      if ( type_ptr->basic_type == 2 ) 
	ccif_signal(CCIF_ISINT, "ccif_get_real_esd", NULL, NULL, 0, itmnam_c);
   }
   
   memset ( STRPTR(itmval_f), ' ', STRLEN(itmval_f) ) ;
   memset ( STRPTR(esdval_f), ' ', STRLEN(esdval_f) ) ;
  

   switch ( *istat ) {
   
      case ( END_OF_CONTEXT ):
        return;

      case ( NO_VALUE ):
      case ( LOOP_UNKNOWN ):
        if ( MIN(item->type_code[0], item->type_code[1]) < 0 ||
           ( item->type_code[0] == 0 && item->type_code[1] == 0) ) { /* Fixme! probably a simpler equivalent */
            *(STRPTR(itmval_f)) = '?';
            *(STRPTR(esdval_f)) = '?';
         }
         else {
            *rval = item->defval.dfloat;
            *istat |= 4;                             /* Change to SINGLE_DICT/LOOP_DICT */
            if ( (assoc_esd = item->associated_esd.rec)  &&
                  !( ( MIN(assoc_esd->type_code[0], assoc_esd->type_code[1]) < 0 ||
                       ( assoc_esd->type_code[0] == 0 && assoc_esd->type_code[1] == 0) 
                     ) 
                   )
               ) {
                *esd = assoc_esd->defval.dfloat;
                *istat_esd = *istat;
             }
         }
         return;
         
      case ( SINGLE_VALUE ):
      case ( LOOP_VALUE ):      

        itmlen = MIN( STRLEN(itmval_f), strlen(node->a->text) );
        strncpy ( STRPTR(itmval_f), node->a->text, itmlen );    /* This will not append a '\0' */

         status = regexec( type_ptr->preg, node->a->text, NMATCH, pmatch, 0);
         if ( status ) {
            char re_msg[450];
            regerror( status, type_ptr->preg, re_msg, sizeof re_msg);
            sprintf(errmsg, "ccif_get_real_esd: value %s fails to match _item_type_list.construct at line %d!\n\
Returning (null) ", node->a->text, node->a->line);
            strcat(errmsg, re_msg);
            *istat |= 6;
            return;
         }
         
         if ( pmatch[0].rm_eo != strlen(node->a->text) || pmatch[0].rm_so != 0 ) {
            ccif_signal(CCIF_BADESDCHAR, "ccif_get_real_esd", NULL, NULL, 0, node->a->text, node->a->line);
            *istat |= 6;
            return;
         }

         
         if ( item->esd && pmatch[item->esd].rm_so != -1 ) {  /* esd in brackets, e.g. 123.456(78)e-45 */
            int exponent = 0, esd_offset = 0;
            double mantissa;
            char *exp_ptr = &( node->a->text[ pmatch[item->esd].rm_eo ] ), *dp = strchr(node->a->text, '.'), *eod;
            if ( toupper( *exp_ptr ) == 'E' )
               exponent = atoi( exp_ptr+1 );
            mantissa = strtod(node->a->text, &eod);                            /* Will scan up to '(...)' */ 
            *rval = mantissa * pow(10., exponent);
            
            if ( dp ) 
               esd_offset = dp - eod + 1;  /* negative */
            *esd = atoi( &( node->a->text[ pmatch[item->esd].rm_so + 1] ) ) * pow(10., exponent + esd_offset) ;
            *istat_esd = PARENTHESIS_ESD;
            
         }
         else {
            *rval = atof(node->a->text); /* Numerics must not be quoted, so don't use itmval_c for this */

            if ( (assoc_esd = item->associated_esd.rec) ) {         /* separate data item. This bit is
                                                                             essentially the same as 
                                                                             ccif_get_real (sigh). */
               *istat_esd = KEEP_CONTEXT;
                node_esd = ccif_get_value(NULL, ncntxt, istat_esd, &assoc_esd);
 
               switch ( *istat_esd ) {
                  case (NO_VALUE):
                  case (LOOP_UNKNOWN):

                     if ( MIN(assoc_esd->type_code[0], assoc_esd->type_code[1]) < 0 
                           || ( assoc_esd->type_code[0] == 0  && assoc_esd->type_code[1] == 0) ) 
                                                                        /* Fixme! probably a simpler equivalent */
                        *(STRPTR(esdval_f)) = '?';
                     else {
                         *esd = assoc_esd->defval.dfloat;
                         *istat_esd |= 4;  /* Change to SINGLE_DICT/LOOP_DICT */
                     }
                     break;
                     
                  case (SINGLE_VALUE):
                  case (LOOP_VALUE):
                     *esd = atof(node_esd->a->text);
                     esdlen = MIN( STRLEN(esdval_f), strlen(node_esd->a->text) );
                     strncpy ( STRPTR(esdval_f), node_esd->a->text, esdlen);
                     break;
                  case ( SINGLE_NULL ):
                  case ( LOOP_NULL ):
                     *(STRPTR(esdval_f)) = '.';
                     break;
                     
               }  /* switch ( *istat_esd ) */
            }   /* if ( assoc_esd = item->associated_esd ) */

            else {  /* No esd available */
               *(STRPTR(esdval_f)) = '?';
               *istat_esd = *istat & 0x01;
            }
            
         }  /* if ( item->esd && pmatch[item->esd].rm_so != -1 ) */

   } /* switch ( *istat ) */
}





#if CALL_LIKE_HPUX
   void ccif_get_int (const char * const itmnam_f, char * const itmval_f, int *ival, int *ncntxt, 
                       int *istat, const int Litmnam_f, const int Litmval_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_GET_INT (const struct Str_Desc * const itmnam_f, struct Str_Desc * const itmval_f, int *ival, 
                       int *ncntxt, int *istat)
#endif
#if defined (VMS)
   void CCIF_GET_INT (const struct dsc$descriptor_s * const itmnam_f, struct dsc$descriptor_s * const itmval_f, 
                       int *ival, int *ncntxt, int *istat)
#endif
#if CALL_LIKE_SUN
   void ccif_get_int_ (const char * const itmnam_f, char * const itmval_f, int *ival, int *ncntxt, 
                        int *istat, const int Litmnam_f, const int Litmval_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_get_int (const int Litmnam_f, const char * const itmnam_f, const int Litmval_f, 
                        char * const itmval_f, int *ival, int *ncntxt, int *istat, )
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_GET_INT (const char * const itmnam_f, const int Litmnam_f,
				char * const itmval_f, const int Litmval_f,
				int *ival, int *ncntxt, int *istat)
# else 
   void CCIF_GET_INT (const char * const itmnam_f, char * const itmval_f, int *ival, int *ncntxt,
                        int *istat, const int Litmnam_f, const int Litmval_f)
# endif
#endif
{ 
   size_t Length, itmlen;
   char itmnam_c[MAXCLLEN+1], *itmval_c;
   const Sym *item=NULL;
   AST *node;
   ItemTypeList *type_ptr;
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';
   
      
   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   type_ptr = ITEM_TYPE(item);
   
   
   if ( zzs_keycmp( zzs_symname(type_ptr->primitive_code), "numb") ) 
     ccif_signal(CCIF_NOTNUM, "ccif_get_int", NULL, NULL, 0, itmnam_c);
   else {
      if ( type_ptr->basic_type == 3 ) 
	ccif_signal(CCIF_ISREAL, "ccif_get_int", NULL, NULL, 0, itmnam_c);
   }


   
   memset ( STRPTR(itmval_f), ' ', STRLEN(itmval_f) ) ;
   

   switch ( *istat ) {

     case ( END_OF_CONTEXT ) :
       return;

     case ( SINGLE_VALUE ):
     case ( LOOP_VALUE ):
   
        *ival = atoi(node->a->text); /* Numerics must not be quoted, so don't use itmval_c for this */
        itmlen = MIN( STRLEN(itmval_f), strlen(node->a->text) );
        strncpy ( STRPTR(itmval_f), node->a->text, itmlen );    /* This will not append a '\0' */
        break;
   
     case ( NO_VALUE ):
     case ( LOOP_UNKNOWN ):
       if ( MIN(item->type_code[0] , item->type_code[1]) < 0 || 
           ( item->type_code[0] == 0 && item->type_code[1] == 0) )  /* Fixme! probably a simpler equivalent */
          *(STRPTR(itmval_f)) = '?';
       else {
          *ival = item->defval.dint;
          *istat |= 4;   /* Change to SINGLE_DICT/LOOP_DICT */
       }
       break;
       
     case ( SINGLE_NULL ):
     case ( LOOP_NULL ):
        *(STRPTR(itmval_f)) = '.';
        break;
   }
}


#if CALL_LIKE_HPUX
   void ccif_get_char (const char * const itmnam_f, char * const itmval_f, char * const cval, 
		       int * const lval, int * const ncntxt, int *const istat, 
		       const int Litmnam_f, const int Litmval_f, const int Lcval)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_GET_CHAR (const struct Str_Desc * const itmnam_f, struct Str_Desc * const itmval_f, 
                       struct Str_Desc * const cval, int * const lval, int * const ncntxt, 
		       int * const istat)
#endif
#if defined (VMS)
   void CCIF_GET_CHAR (const struct dsc$descriptor_s * const itmnam_f, 
                       struct dsc$descriptor_s * const itmval_f, 
                       struct dsc$descriptor_s * const cval, int * const lval, 
		       int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_SUN
   void ccif_get_char_ (const char * const itmnam_f, char * const itmval_f, char * const cval, 
		       int * const lval, int * const ncntxt, int *const istat, 
		       const int Litmnam_f, const int Litmval_f, const int Lcval)
#endif
#if CALL_LIKE_IRIS
   void ccif_get_char (const int Litmnam_f, const char * const itmnam_f, const int Litmval_f, 
                        char * const itmval_f, const int Lcval, char * const cval, int * const lval,
			int * const ncntxt, int * const istat,)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_GET_CHAR (const char * const itmnam_f, const int Litmnam_f,
				 char * const itmval_f, const int Litmval_f,
				 char * const cval, const int Lcval,  
				 int * const lval, int * const ncntxt, int *const istat)
# else
   void CCIF_GET_CHAR (const char * const itmnam_f, char * const itmval_f, char * const cval,
                       int * const lval, int * const ncntxt, int *const istat,
                       const int Litmnam_f, const int Litmval_f, const int Lcval)
# endif
#endif
{ 
   size_t Length, itmlen, vallen;
   char itmnam_c[MAXCLLEN+1]; 
   const char *itmval_c;
   const Sym *item=NULL;
   char *nline;
   AST *node;
   ItemTypeList * type_ptr;
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';
   
   FLENGTH(itmval_f, vallen);
      
   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   type_ptr = ITEM_TYPE(item);

   if ( !zzs_keycmp( zzs_symname(type_ptr->primitive_code), "numb") ) 
     ccif_signal(CCIF_ISNUM, "ccif_get_char", NULL, NULL, 0, itmnam_c);
   else {
     if ( !type_ptr->single_line ) 
       ccif_signal(CCIF_ISTEXT, "ccif_get_char", NULL, NULL, 0, itmnam_c, STRLEN(itmval_f));
   }
   
  
   memset ( STRPTR(itmval_f), ' ', STRLEN(itmval_f) ) ;
   memset ( STRPTR(cval), ' ', STRLEN(cval) ) ;
   

   switch ( *istat ) {

     case ( END_OF_CONTEXT ) :
       return;

     case ( SINGLE_VALUE ):
     case ( LOOP_VALUE ):
 
       itmlen = MIN( STRLEN(itmval_f), strlen(node->a->text) );
       strncpy ( STRPTR(itmval_f), node->a->text, itmlen );    /* This will not append a '\0' */

       ccif_wo_quotes( node, &itmval_c, lval );

       if ( node->token == Line_of_text ) {
       nline = strchr(itmval_c, '\n');
       
       if ( nline ) {
         /* If the actual data value consists of more than one line, take the first line only..... */
         if ( nline != itmval_c + *lval - 1 ) {
           ccif_signal(CCIF_MULTILINE,"ccif_get_char", NULL, NULL, 0, itmnam_c);
           *lval = nline - itmval_c;
         }
         else
           /* ...otherwise just trim off trailing newline and treat as single-line data */
           (*lval)--;
       }

       }
       strncpy ( STRPTR(cval), itmval_c, MIN ( STRLEN(cval), *lval ) );   /* This will not append a '\0' */
       break;

     case ( NO_VALUE ):
     case ( LOOP_UNKNOWN ):
       if ( MIN(item->type_code[0] , item->type_code[1]) < 0 || 
           ( item->type_code[0] == 0 && item->type_code[1] == 0) )  /* Fixme! probably a simpler equivalent */
          *(STRPTR(itmval_f)) = '?';
       else {
	 int nchart = -1;
	 char *dtext;
	  *istat |=4;
	  switch ( type_ptr->basic_type ) {
	  case ( 1 ):    /* text/char */
	    dtext = zzs_symname(item->defval.dtext);
	    itmlen = MIN ( STRLEN(cval), strlen(dtext) );
	    
	    strncpy ( STRPTR(cval), zzs_symname(item->defval.dtext), itmlen );
	    break;
	    
	  case ( 2 ):  /* Application developers have already been warned that they are 
                             using the wrong retrieval function, so I'm not going to be too
                             sophisticated about this.
                             Users of well-written programs will never end up down here */
	    nchart = sprintf ( STRPTR(cval), "%d", item->defval.dint );
	    break;
	    
	  case ( 3 ):  /* Same again..... */
        	 nchart = sprintf ( STRPTR(cval), "%f", item->defval.dfloat );
        	 break;
		 
	  }
	  /* Restore space character for Fortran character type */
	  if ( nchart > 0 ) *(STRPTR(cval) + nchart) = ' ';
       }
       break;
       
     case ( SINGLE_NULL ):
     case ( LOOP_NULL ):
        *(STRPTR(itmval_f)) = '.';
        break;
   }

}



#if CALL_LIKE_HPUX
   void ccif_get_text (const char * const itmnam_f, int *itmpos, int * const nline, char * const tval, 
                       const int * const ntval, int * const ncntxt, int * const istat, 
                       const int Litmnam_f, const int Ltval)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_GET_TEXT (const struct Str_Desc * const itmnam_f, int * const itmpos, int * const nline, 
		       struct Str_Desc * const tval, const int * const ntval, int * const ncntxt, 
		       int * const istat)
#endif
#if defined (VMS)
   void CCIF_GET_TEXT (const struct dsc$descriptor_s * const itmnam_f,  int * const itmpos, 
		       int * const nline, struct dsc$descriptor_s * const tval, 
		       const int * const ntval, int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_SUN
   void ccif_get_text_ (const char * const itmnam_f, int *itmpos, int * const nline, char * const tval, 
                       const int * const ntval, int * const ncntxt, int * const istat, 
                       const int Litmnam_f, const int Ltval)
#endif
#if CALL_LIKE_IRIS
   void ccif_get_text (const int Litmnam_f, const char * const itmnam_f, int * const itmpos, 
			int * const nline, const int Ltval, char * const tval, const int * const ntval, 
			int * const ncntxt, int *istat)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_GET_TEXT (const char * const itmnam_f, const int Litmnam_f,
				 int *itmpos, int * const nline, 
				 char * const tval, const int Ltval, 
				 const int * const ntval, int * const ncntxt, 
				 int * const istat)
# else
   void CCIF_GET_TEXT (const char * const itmnam_f, int *itmpos, int * const nline, char * const tval,
                       const int * const ntval, int * const ncntxt, int * const istat,
                       const int Litmnam_f, const int Ltval)
# endif
#endif

{ 
   size_t Length, line_l, f_line_len;
   char itmnam_c[MAXCLLEN+1], *line_s, *line_e, *f_line_ptr;
   const char *itmval_c;
   const Sym *item=NULL;
   AST *node;
   ItemTypeList * type_ptr;
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';
   
      
   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   type_ptr = ITEM_TYPE(item);

   if ( !zzs_keycmp( zzs_symname(type_ptr->primitive_code), "numb") ) 
     ccif_signal(CCIF_ISNUM, "ccif_get_text", NULL, NULL, 0, itmnam_c);
#ifdef DEBUG
   else {
      if ( type_ptr->single_line ) 
	ccif_signal(CCIF_NOTMLINE, "ccif_get_text", NULL, NULL, 0, itmnam_c);
   }
#endif

   memset ( STRPTR(tval), ' ', STRLEN(tval) );
   
   switch ( *istat ) {
      
    case ( END_OF_CONTEXT ) :
      return;
      
    case ( NO_VALUE ):
    case ( LOOP_UNKNOWN ):
      if ( MIN(item->type_code[0] , item->type_code[1]) < 0 || 
	  ( item->type_code[0] == 0 && item->type_code[1] == 0) ) { /* Fixme! probably a simpler equivalent */
	 *(STRPTR(tval)) = '?';
	 *itmpos = -1;
	 return;
      }
      line_s = zzs_symname(item->defval.dtext) + *itmpos;
      *istat |= 4;  /* Change to SINGLE_DICT/LOOP_DICT */
      break;
      
    case ( SINGLE_VALUE ):
    case ( LOOP_VALUE ):
      
      /* Actual data value might be a non/single/double quoted text string, even though the
       * data type allows it to be text */
      if ( zzset_el(node->token, Char_token_set) ) {   /* DLG generates this - we might as well use it.
							* See the PCCTS book section 2.7.5 (Resynchronising the
							  Parser). */
	 ccif_wo_quotes( node, &itmval_c, nline);   /* In ccif_get_char, the 'as-is' text is only returned for
						         diagnostics. We don't need it here, so strip quotes. */
	 memset ( STRPTR(tval), ' ', STRLEN(tval) );
	 strncpy ( STRPTR(tval), itmval_c, *nline );   /* We can take it as read here, that tval is long enough */
	 *itmpos = -1; 
	 return;
      }
	 
      line_s = &(node->a->text[*itmpos]);
      break;
      
    case ( SINGLE_NULL ):
    case ( LOOP_NULL ):
      *(STRPTR(tval)) = '.';
      *itmpos = -1;
      return;
   }
   
     
   f_line_len = STRLEN(tval) / *ntval;
      
   for ( *nline = 0, f_line_ptr = STRPTR(tval); 
         *nline < *ntval; 
         (*nline)++, f_line_ptr += f_line_len ) {
	    
            line_e = strchr(line_s, '\n');

      /*  A lot of horrid fiddling around to cope with various possibilities for the end
       * of the text. */
	   if ( ! line_e ) {
	      line_l = strlen(line_s);
              strncpy ( f_line_ptr, line_s, MIN( f_line_len, line_l ) );
              *itmpos = 0;
              (*nline)++;
              return;
	    }

	   if ( ! line_e[1] ) {
	      line_l = strlen(line_s) - 1;  /* lose newline */
              strncpy ( f_line_ptr, line_s, MIN( f_line_len, line_l  ) );
              *itmpos = 0;
              (*nline)++;
              return;
	   }

           if ( type_ptr->single_line && *itmpos == 0 && *nline == 1 ) {
             ccif_signal(CCIF_CHARISTEXT, "ccif_get_text", NULL, NULL, 0, itmnam_c);
           }
     
           strncpy ( f_line_ptr, line_s, MIN( f_line_len, (line_e - line_s) ) );
           line_s = line_e + 1;
	 }
   
      
        
   if ( ! line_e[0] ) {
      *itmpos = 0;
       return;
   }
   *itmpos = line_e - node->a->text + 1;
   return; 

}


/* Data writing routines */


#if CALL_LIKE_HPUX
   void ccif_put_real (const char * const itmnam_f, const float * const rval, int * const ncntxt, 
                       int * const istat, const int Litmnam_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_PUT_REAL (const struct Str_Desc * const itmnam_f, const float * const rval, 
                       int * const ncntxt, int * const istat)
#endif
#if defined (VMS)
   void CCIF_PUT_REAL (const struct dsc$descriptor_s * const itmnam_f, 
                       const float * const rval, int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_SUN
   void ccif_put_real_ (const char *const itmnam_f, const float * const rval, int * const ncntxt, 
                        int * const istat, const int Litmnam_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_put_real (const int Litmnam_f, const char *const itmnam_f, 
                        const float * const rval, int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_PUT_REAL (const char *const itmnam_f, const int Litmnam_f,
				 const float * const rval, int * const ncntxt, 
				 int * const istat)
# else
   void CCIF_PUT_REAL (const char *const itmnam_f, const float * const rval, int * const ncntxt,
                        int * const istat, const int Litmnam_f)
# endif
#endif
{
   char itmnam_c[MAXCLLEN+1], text[MAXCLLEN+1];
   int Length;
   const Sym *item;
   AST *node;
   ItemTypeList *type_ptr;
   Attrib a = NULL;
   CONTEXT_LIST context_entry = ccif_context_list( *ncntxt );
   int block = context_entry.block;
   Rb_node rbnodes[NSORT_TREES];
   
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';

   if ( *istat == APPEND_ROW ) {
      ccif_add_row(*ncntxt);
      *istat = KEEP_CONTEXT;
   }
   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   
   if ( !node ) {
      ccif_new_data_item( itmnam_c, 0, NULL, &context_entry );
      ccif_refresh_contexts(context_entry);
      context_entry = ccif_context_list( *ncntxt );
      *istat = KEEP_CONTEXT;
      node = ccif_get_value (NULL, ncntxt, istat, &item);
   }
   
   type_ptr = ITEM_TYPE(item);

   if ( type_ptr->basic_type != 3 ) 
     ccif_signal(CCIF_NOTREAL, "ccif_put_real", NULL, NULL, 0, itmnam_c);
   
   sprintf(text, (item->fmt ? item->fmt : "%f"), *rval);
   set_lex_act(1);

   if (item->node[block]->token == Loop )
     ccif_find_all_rb_nodes(item, block, context_entry.packet[0].loop_item, rbnodes);

   zzcr_ast(node, &a, Non_quoted_text_string, text);
   /* Style - if first in loop packet, start on new line */
   a->col = ! item->loop_offset[block] && item->node[block]->token == Loop ? 1 : 0;

   if ( item->node[block]->token == Loop )
     ccif_redo_sort(item, block, context_entry.packet[0].loop_item, rbnodes);

}



#if CALL_LIKE_HPUX
   void ccif_put_real_esd (const char *const itmnam_f, const float * const rval, const float * const esd,
			   int * const ncntxt, int * const istat, const char * const disp_f, 
			   const int Litmnam_f, const int Ldisp_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_PUT_REAL_ESD (const struct Str_Desc * const itmnam_f, const float * const rval,
			   const float * const esd, int * const ncntxt, int * const istat,
			   const struct Str_Desc * const disp_f)
#endif
#if defined (VMS)
   void CCIF_PUT_REAL_ESD (const struct dsc$descriptor_s * const itmnam_f, 
			   const float * const rval, const float * const esd, int * const ncntxt, 
			   int * const istat, const struct dsc$descriptor_s * const disp_f)
#endif
#if CALL_LIKE_SUN
   void ccif_put_real_esd_ (const char *const itmnam_f, const float * const rval, const float * const esd,
			    int * const ncntxt, int * const istat, const char * const disp_f, 
			    const int Litmnam_f, const int Ldisp_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_put_real_esd (const int Litmnam_f, const char *const itmnam_f, const float * const rval, 
			    const float * const esd, int * const ncntxt, int * const istat, 
			    const int Ldisp_f, const char * const disp_f)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_PUT_REAL_ESD (const char *const itmnam_f, const int Litmnam_f,
				     const float * const rval, const float * const esd,
				     int * const ncntxt, int * const istat, 
				     const char * const disp_f, const int Ldisp_f)
# else
   void CCIF_PUT_REAL_ESD (const char *const itmnam_f, const float * const rval, const float * const esd,
                            int * const ncntxt, int * const istat, const char * const disp_f,
                            const int Litmnam_f, const int Ldisp_f)
# endif
#endif

{
   char itmnam_c[MAXCLLEN+1], text[MAXCLLEN+1], esd_text[MAXCLLEN+1];
   int Length, parens_esd;
   const Sym *item, *esd_item;
   AST *node, *esd_node;
   ItemTypeList *type_ptr;
   Attrib a = NULL, esd_a = NULL;
   CONTEXT_LIST context_entry = ccif_context_list( *ncntxt );
   int block = context_entry.block;
   Rb_node rbnodes[NSORT_TREES];
   
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';

   parens_esd = !zzs_keyncmp("paren", STRPTR(disp_f), 5);
   
   if ( *istat == APPEND_ROW ) {
      ccif_add_row(*ncntxt);
      *istat = KEEP_CONTEXT;
   }

   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   type_ptr = ITEM_TYPE(item);

   if ( type_ptr->basic_type != 3 || ( !item->esd && ! item->associated_esd.rec ) ) 
     ccif_signal(CCIF_NOTREALESD, "ccif_put_real_esd", NULL, NULL, 0, itmnam_c);

   sprintf(text, (item->fmt ? item->fmt : "%f"), *rval);

   parens_esd = parens_esd && item->esd;
   
   if ( !node ) {
      ccif_new_data_item( itmnam_c, 0, NULL, &context_entry );
      ccif_refresh_contexts(context_entry);
      context_entry = ccif_context_list( *ncntxt );
      *istat = KEEP_CONTEXT;
      node = ccif_get_value (NULL, ncntxt, istat, &item);
   }
   
/* Same again for esd. Need to know if there is an associated_esd data item or value 
   in the AST, even if we are outputting the esd in parentheses */

   esd_item = item->associated_esd.rec;
   if ( esd_item ) {
      *istat = KEEP_CONTEXT;
      esd_node = ccif_get_value(NULL, ncntxt, istat, &esd_item);
   }
   
   
   if ( !parens_esd ) {
     if ( !esd_item ) 
       ccif_signal(CCIF_BADESD, "ccif_put_real_esd", NULL, NULL, 0, zzs_recname(item) );
      
      if ( !esd_node ) {
	 CONTEXT_LIST context_entry_esd = ccif_context_list( *ncntxt );
	 ccif_new_data_item( zzs_recname(esd_item), 0, NULL, &context_entry );
	 ccif_refresh_contexts(context_entry);
         context_entry_esd = ccif_context_list( *ncntxt );
         context_entry = ccif_context_list( *ncntxt );
	 *istat = KEEP_CONTEXT;
	 esd_node = ccif_get_value (NULL, ncntxt, istat, &esd_item);
      }

      sprintf(esd_text, (esd_item->fmt ? esd_item->fmt : "%f"), *esd);
      set_lex_act(1);

   if (item->node[block]->token == Loop )
     ccif_find_all_rb_nodes(item, block, context_entry.packet[0].loop_item, rbnodes);

      zzcr_ast(node, &a, Non_quoted_text_string, text);
      zzcr_ast(esd_node, &esd_a, Non_quoted_text_string, esd_text);

      /* Style - if first in loop packet, start on new line */
      a->col = ! item->loop_offset[block] && item->node[block]->token == Loop ? 1 : 0;
      esd_a->col = ! esd_item->loop_offset[block] && esd_item->node[block]->token == Loop ? 1 : 0;
      if ( item->node[block]->token == Loop )
        ccif_redo_sort(item, block, context_entry.packet[0].loop_item, rbnodes);
      
   }
   else {			       /* put esd in parentheses */
      char *exp_ptr = strpbrk(text,"Ee"),
	   *dp = strchr(text,'.'),
	   *esd_exp_ptr, *esd_dp, *o_ptr;
      int exponent = ( exp_ptr ? atoi(exp_ptr+1) : 0 ), esd_exponent, esd_digits, lsd;
      float esd_mantissa;
      sprintf(esd_text, "%e", *esd);
      esd_exp_ptr = strpbrk(esd_text, "Ee");
      esd_dp = strchr(esd_text, '.');
      esd_exponent = atoi(esd_exp_ptr+1);   /* output with %e, so will always have an exponent */
      
      /* Work out magnitude of least significant digit of rval */
      if ( !exp_ptr ) exp_ptr = strchr(text,'\0');
      lsd = exponent - ( dp ? exp_ptr - dp - 1 : 0 );
      /* Difference between lsd and esd_exponent tells us how many digits from esd we want */
      *esd_exp_ptr = '\0';
      esd_mantissa = atof(esd_text);   /* This will be between 1 and 10 */
      esd_digits = esd_mantissa * pow(10., (esd_exponent - lsd ) ) + 0.5;
      if ( exponent ) {
	 strcpy(esd_text, exp_ptr);
	 o_ptr = exp_ptr;
      } 
      else {
	 *esd_text = '\0';
	 o_ptr = strchr(text,'\0');
      }
      sprintf(o_ptr, "(%d)%s", esd_digits, esd_text);
      set_lex_act(1);

      if (item->node[block]->token == Loop )
        ccif_find_all_rb_nodes(item, block, context_entry.packet[0].loop_item, rbnodes);

      zzcr_ast(node, &a, Non_quoted_text_string, text);

      /* Style - if first in loop packet, start on new line */
      a->col = ! item->loop_offset[block] && item->node[block]->token == Loop ? 1 : 0;
      if ( item->node[block]->token == Loop )
        ccif_redo_sort(item, block, context_entry.packet[0].loop_item,rbnodes);


      if ( esd_node ) {
	 zzcr_ast(esd_node, &esd_a, Non_quoted_text_string, ".");
	 esd_a->col = ! esd_item->loop_offset[block] && esd_item->node[block]->token == Loop ? 1 : 0;
      }
   }
   /* Fixme! check against construct? */
}
      



#if CALL_LIKE_HPUX
   void ccif_put_int (const char * const itmnam_f, const int * const ival, int * const ncntxt, 
                       int * const istat, const int Litmnam_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_PUT_INT (const struct Str_Desc * const itmnam_f, const int * const ival, 
                       int * const ncntxt, int * const istat)
#endif
#if defined (VMS)
   void CCIF_PUT_INT (const struct dsc$descriptor_s * const itmnam_f, 
                       const int * const ival, int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_SUN
   void ccif_put_int_ (const char *const itmnam_f, const int * const ival, int * const ncntxt, 
                        int * const istat, const int Litmnam_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_put_int (const int Litmnam_f, const char *const itmnam_f, 
                        const int * const ival, int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_PUT_INT (const char *const itmnam_f, const int Litmnam_f, 
				const int * const ival, int * const ncntxt, 
				int * const istat)
# else
  void CCIF_PUT_INT (const char *const itmnam_f, const int * const ival, int * const ncntxt,
                        int * const istat, const int Litmnam_f)
# endif
#endif

{

   char itmnam_c[MAXCLLEN+1], text[MAXCLLEN+1];
   int Length;
   const Sym *item;
   AST *node;
   ItemTypeList *type_ptr;
   Attrib a = NULL;
   CONTEXT_LIST context_entry = ccif_context_list( *ncntxt );
   int block = context_entry.block;
   Rb_node rbnodes[NSORT_TREES];
   
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';

   if ( *istat == APPEND_ROW ) {
      ccif_add_row(*ncntxt);
      *istat = KEEP_CONTEXT;
   }

   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   
   if ( !node ) {
      ccif_new_data_item( itmnam_c, 0, NULL, &context_entry );
      ccif_refresh_contexts(context_entry);
      context_entry = ccif_context_list( *ncntxt );
      *istat = KEEP_CONTEXT;
      node = ccif_get_value (NULL, ncntxt, istat, &item);
   }
   
   type_ptr = ITEM_TYPE(item);

   if ( type_ptr->basic_type != 2 ) 
     ccif_signal(CCIF_NOTINT, "ccif_put_int", NULL, NULL, 0, itmnam_c);
   
   sprintf(text, (item->fmt ? item->fmt : "%d"), *ival);
   set_lex_act(1);

   if (item->node[block]->token == Loop )
     ccif_find_all_rb_nodes(item, block, context_entry.packet[0].loop_item, rbnodes);

   zzcr_ast(node, &a, Non_quoted_text_string, text);
   /* Style - if first in loop packet, start on new line */
   a->col = ! item->loop_offset[block] && item->node[block]->token == Loop ? 1 : 0;

   if ( item->node[block]->token == Loop )
     ccif_redo_sort(item, block, context_entry.packet[0].loop_item, rbnodes );

}


#if CALL_LIKE_HPUX
   void ccif_put_char (const char * const itmnam_f, const char * const cval_f, int * const ncntxt, 
                       int * const istat, const int Litmnam_f, const int Lcval_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_PUT_CHAR (const struct Str_Desc * const itmnam_f, const struct Str_Desc * const cval_f, 
                       int * const ncntxt, int * const istat)
#endif
#if defined (VMS)
   void CCIF_PUT_CHAR (const struct dsc$descriptor_s * const itmnam_f, 
		       const struct dsc$descriptor_s * const cval_f, 
                       int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_SUN
   void ccif_put_char_ (const char *const itmnam_f, const char * const cval_f, int * const ncntxt, 
                        int * const istat, const int Litmnam_f, const int Lcval_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_put_char (const int Litmnam_f, const char *const itmnam_f, 
                        const int Lcval_F, const char * const cval_f, int * const ncntxt, int * const istat)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_PUT_CHAR (const char *const itmnam_f, const int Litmnam_f,
				 const char * const cval_f, const int Lcval_f, 
				 int * const ncntxt, int * const istat)
# else
   void CCIF_PUT_CHAR (const char *const itmnam_f, const char * const cval_f, int * const ncntxt,
                        int * const istat, const int Litmnam_f, const int Lcval_f)
#endif
#endif

{
   int itmlen, vallen, token_type, i, null=0, unknown=0, was_truncated=0;
   Attrib a=NULL, errcmt1, errcmt2, errtxt, save_prev;
   char err_buf[MAXCLLEN+13],	               /* Allow for adding "#CCIF_ERR " to start of text, on error */
     *check_buf = err_buf+7,	       /* Allow for adding quoting & lexical context */
     *cval_c = err_buf+10, 
     *cval_end,
     sprint_buf[MAXCLLEN+1],
     itmnam_c[MAXCLLEN+1];
   
   /* Quoting to try, in order, if text as given does not form a valid STAR text token */
   static char *quoting[3][2] = { {"  '",   "' "    },
	                          {"  \"",  "\" "   },
	                          {"\n;\n",  "\n;" } 
                                };
     
   AST *node, *dummy_node;
   int antlr_retsignal;
   const Sym *item;
   ItemTypeList *type_ptr;

   CONTEXT_LIST context_entry = ccif_context_list( *ncntxt );
   int block = context_entry.block;

   Rb_node rbnodes[NSORT_TREES];

   FLENGTH(itmnam_f, itmlen)
   strncpy(itmnam_c, STRPTR(itmnam_f), itmlen);
   itmnam_c[itmlen] = '\0';


   FLENGTH(cval_f, vallen);
   
   /* Allow use of this function to output a CIF null or unknown for any
    * data type */
   if ( vallen == 1 ) {
      if (*(STRPTR(cval_f)) == '?')
	unknown = 1;
      else if (*(STRPTR(cval_f)) == '.')
	null = 1;
   }
       
   

   if ( *istat == APPEND_ROW ) {
      ccif_add_row(*ncntxt);
      *istat = KEEP_CONTEXT;
   }

   node = ccif_get_value (itmnam_c, ncntxt, istat, &item);
   
   if ( !node ) {
      ccif_new_data_item( itmnam_c, 0, NULL, &context_entry );
      ccif_refresh_contexts(context_entry);  
      context_entry = ccif_context_list( *ncntxt );
      *istat = KEEP_CONTEXT;
      node = ccif_get_value (NULL, ncntxt, istat, &item);
   }

   type_ptr = ITEM_TYPE(item);

   if ( type_ptr->basic_type != 1 && !null && !unknown ) 
     ccif_signal(CCIF_NOTCHAR, "ccif_put_char", NULL, NULL, 0, itmnam_c);
      
   if ( vallen > MAXCLLEN ) {
     was_truncated = 1;
     strncpy(cval_c, STRPTR(cval_f), MAXCLLEN);
     cval_end = &(cval_c[MAXCLLEN]);
     *cval_end = '\0';
     ccif_signal(CCIF_VALTOOLONG, "ccif_put_char", NULL, NULL, 0, itmnam_c, MAXCLLEN, cval_c);
   } 
   else {
     strncpy(cval_c, STRPTR(cval_f), vallen);
     cval_end = &(cval_c[vallen]);
     *cval_end = '\0';
     
   }
   
   strcat(cval_end, "  ");	       /* Provide trailing context for scanner */
   
   /* trim leading spaces for token check */
   for ( ; isspace(*cval_c); cval_c++ );
   
   strncpy(check_buf, "   ", 3);		       /* Leading whitespace context */
   
                                                       /* find STAR token type */
   zzmode(NORMAL);
   dummy_node = NULL;
   set_lex_act(0);
   zzastMARK;
   CCIF_SCHK_ON;
   ANTLRs(( token_type = ccif_antlr_check_data_value(&dummy_node, &antlr_retsignal,  strlen(cval_c) - 2) ), check_buf);
   CCIF_SCHK_OFF;
   zzastREL;
   set_lex_act(1);

   /* Text as supplied does not match a STAR token. Try all applicable quoting... */
   if ( !token_type ) {
      if ( vallen <= MAXCLLEN - 2 ) {
	 cval_c = check_buf+2;
	 for ( i = 0; i < 2 ; i++ ) {
	    strncpy(check_buf, quoting[i][0], 3);
	    strcpy(cval_end, quoting[i][1]);
            zzmode(NORMAL);                                              	    /* try again..... */
	    dummy_node = NULL;
	    set_lex_act(0);
	    zzastMARK;
            CCIF_SCHK_ON;
	    ANTLRs(( token_type = ccif_antlr_check_data_value(&dummy_node, &antlr_retsignal, strlen(cval_c) - 1 ) ), check_buf);
            CCIF_SCHK_OFF;
	    zzastREL;
	    set_lex_act(1);
	    if ( token_type ) {
	      cval_end++;  /* Advance end of token by one character \' or \" */
	      break;
	    }
	 }
      }
      if ( !token_type && !type_ptr->single_line ) {   /* multi-line text if allowed */
	 cval_c = check_buf+1;
	 strncpy(check_buf, quoting[2][0], 3);
	 strcpy(cval_end, quoting[2][1]);
         zzmode(NORMAL);                                                      	 /* ...and again */
	 dummy_node = NULL;
	 set_lex_act(0);
	 zzastMARK;
         CCIF_SCHK_ON;
	 ANTLRs(( token_type = ccif_antlr_check_data_value(&dummy_node, &antlr_retsignal, strlen(cval_c)) ), check_buf);
         CCIF_SCHK_OFF;
	 zzastREL;
	 set_lex_act(1);
	 if ( token_type )
	   cval_end += 2; /* Advance end of token by two characters ( newline + ';') */
      }
	 
   }
   
   set_lex_act(1);  		       /* This also affects the operation of zzcr_attrib */

   if ( token_type ) {		       /* Generate a new attribute for this node */
      *cval_end = '\0';
      if ( null || unknown ) 
	sprintf(sprint_buf, "%*s", item->width, cval_c);
      else
	sprintf(sprint_buf, (item->fmt ? item->fmt : "%s"), cval_c);

      if (item->node[block]->token == Loop )
        ccif_find_all_rb_nodes(item, block, context_entry.packet[0].loop_item, rbnodes);
      
      zzcr_ast(node, &a, token_type, sprint_buf);

      /* Style - if first in loop packet, start on new line */
      a->col = ! item->loop_offset[block] && item->node[block]->token == Loop ? 1 : 0;
      if ( item->node[block]->token == Loop )
        ccif_redo_sort(item, block, context_entry.packet[0].loop_item, rbnodes );
      ccif_clean_node(node);	       /* remove any "#CCIF_ERR ..." comments ... */

      if (was_truncated) {
	zzcr_attr(&errcmt1, Comment, "#CCIF_ERR The following value was too long, so has been trunctated");
	save_prev = a->prev;
	errcmt1->prev = save_prev;
	a->prev = errcmt1;
	a->status = error;
      }
   }
   else {
      save_prev = node->a ? node->a->prev : NULL;
      zzcr_attr(&errcmt1, Comment, "#CCIF_ERR (null), because ccif_put_char could not make input into valid");
      zzcr_attr(&errcmt2, Comment, "#CCIF_ERR STAR text. Offending text is (or starts) as follows:");
      cval_c = check_buf + 3;
      strncpy(err_buf, "#CCIF_ERR ", 10);
      cval_c[ MIN(65,vallen) ] = '\0';
      zzcr_attr(&errtxt, Comment, err_buf);

      if ( item->node[block]->token == Loop )
        ccif_redo_sort(item, block, context_entry.packet[0].loop_item, rbnodes );      
      zzcr_ast(node, &a, Non_quoted_text_string, ".");
      if ( item->node[block]->token == Loop )
        ccif_redo_sort(item, block, context_entry.packet[0].loop_item, rbnodes );

      a = node->a;

      errcmt1->next = errcmt2;
      errcmt1->prev = save_prev;
      errcmt1->col = 1;
      
      errcmt2->prev = errcmt1;
      errcmt2->next = errtxt;
      errcmt2->col = 1;
      
      errtxt->prev = errcmt2;
      errtxt->next = a;
      errtxt->col = 1;
      
      a->prev = errtxt;
      a->col = 1;
      a->status = error;
      *istat = INVALID_TOKEN;
   
   }
}

   


#if CALL_LIKE_HPUX
   void ccif_put_text (const char * const itmnam_f, const int * const nlines, const char * const text, 
			const int * const ntval, int * const ncntxt, int * const status, 
			const char * const disp_f, const int Litmnam_f, const int Ltext, const int Ldisp_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_PUT_TEXT (const struct Str_Desc * const itmnam_f, const int * const nlines, 
		       const struct Str_Desc * const text, const int * const ntval, 
		       int * const ncntxt, int * const status, const struct Str_Desc * const disp_f)
#endif
#if defined (VMS)
   void CCIF_PUT_TEXT (const struct dsc$descriptor_s * const itmnam_f, const int * const nlines,
		       struct dsc$descriptor_s * text, const int * const ntval, int * const ncntxt, 
		       int * const status, const struct dsc$descriptor_s * const disp_f)
#endif
#if CALL_LIKE_SUN
   void ccif_put_text_ (const char * const itmnam_f, const int * const nlines, const char * const text, 
			const int * const ntval, int * const ncntxt, int * const status, 
			const char * const disp_f, const int Litmnam_f, const int Ltext, const int Ldisp_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_put_text (const int Litmnam_f, const char * const itmnam_f, const int * const nlines,
			const int Ltext, const char * const text, const int * const ntval, 
			int * const ncntxt, int * const status, const int Ldisp_f, const char * const disp_f)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_PUT_TEXT (const char * const itmnam_f, const int Litmnam_f, 
				 const int * const nlines,  
				 const char * const text, const int Ltext,  
				 const int * const ntval, int * const ncntxt, 
				 int * const status, 
				 const char * const disp_f, const int Ldisp_f)
# else
   void CCIF_PUT_TEXT (const char * const itmnam_f, const int * const nlines, const char * const text,
                        const int * const ntval, int * const ncntxt, int * const status,
                        const char * const disp_f, const int Litmnam_f, const int Ltext, const int Ldisp_f)
# endif
#endif

{
   size_t Length, f_line_len = STRLEN(text)/(*ntval), displen;
   char itmnam_c[MAXCLLEN+1], line[MAXCLLEN+1], disp_c[10], *o_ptr;
   const char *line_s, *line_e;
   const Sym *item;
   AST *node;
   Attrib a = NULL;
   ItemTypeList *type_ptr;
   int i, *llengths, length_c = 0;
   static char * trunc_msg1 = "#CCIF_ERR Input to CCIF_PUT_TEXT contained a premature semi-colon.",
               * trunc_msg2 = "#CCIF_ERR The following text may be incomplete.";
   
   FLENGTH(itmnam_f, Length)
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';

   FLENGTH(disp_f, displen)
   strncpy(disp_c, STRPTR(disp_f), displen);
   disp_c[displen] = '\0';

   CCIF_CALLOC(*nlines, int, llengths, "ccif_put_text", (0))

   if ( *status == APPEND_ROW ) {
      ccif_add_row(*ncntxt);
      *status = KEEP_CONTEXT;
   }

   node = ccif_get_value (itmnam_c, ncntxt, status, &item);
   
   if ( !node ) {
      CONTEXT_LIST context_entry = ccif_context_list( *ncntxt );
      ccif_new_data_item( itmnam_c, 0, NULL, &context_entry );
      ccif_refresh_contexts(context_entry);  
      context_entry = ccif_context_list( *ncntxt );
      *status = KEEP_CONTEXT;
      node = ccif_get_value (NULL, ncntxt, status, &item);
   }
   
   type_ptr = ITEM_TYPE(item);
   
   /* Find out the length of each line, and the total space required to store the text.
    * Cannot use the FLENGTH macro for this */
   
   for ( i = 0, line_s = STRPTR(text);  i < *nlines; i++ , line_s += f_line_len ) {
      for ( llengths[i] = f_line_len, line_e = line_s + f_line_len - 1;
	   *line_e == ' ' && line_e >= line_s;
	   llengths[i]-- , line_e--) ;
      length_c += (llengths[i] + 1);  /* ... +1 to store the newlines */
   }

   				       /* a has been initialised to NULL. */
   if ( !zzs_keycmp(disp_c, "new") || ccif_null_or_unknown(node) ) {

      if ( *(STRPTR(text)) == ';' ) {
	 zzcr_ast (node, &a, Line_of_text, "");   
	 a->text = NULL;
	 node->a->col = 0;
	 o_ptr = NULL;
      }
      else {
	 zzcr_ast (node, &a, Line_of_text, ";\n;");
	 node->a->col = -2;
	 o_ptr = node->a->text;        /* Something non-null for now */
      }
   }
   else {
      if ( node->token != Line_of_text) {   /* fix up as text, so that new stuff is appended */
	 char buf[90];
	 strcpy(buf, ";\n");
	 strcat(buf, node->a->text);
	 strcat(buf, "\n;");
	 zzcr_ast (node, &a, Line_of_text, buf);
	 node->a->col = -3;
      }
      else {  			       /* copy mapped text to allocated area to allow appending */
	 switch ( node->a->status ) {
	  case (mapped):
	    node->a->status = output;
	  case (constant):
	    node->a->text = ccif_strdup(node->a->text);
	 }
      }
      o_ptr = node->a->text;	       /* Something non-null for now */
   }

   				       /* Allow for possible bounding "....\n;" and terminating NULL */
   CCIF_REALLOC( (strlen(node->a->text) + length_c + 5), char,  node->a->text, "ccif_put_text", (0))
     				       /* Set starting point for output (wipe out previous trailing ';')  */
   o_ptr = o_ptr ? strchr(node->a->text, '\0') - 1 : node->a->text ;

   for ( i = 0, line_s = STRPTR(text); i < *nlines; i++ , line_s += f_line_len ) {
      if ( *line_s == ';' ) {
	 if ( i < *nlines - 1 ) {
	    Attrib a1, a2;
	    ccif_signal(CCIF_SEMI, "ccif_put_text", NULL, NULL, 0);
	    zzcr_attr(&a1, Comment, trunc_msg1);
	    zzcr_attr(&a2, Comment, trunc_msg2);
	    a1->prev = node->a->prev;
	    a1->next = a2;
	    a1->col = 1;
	    a2->prev = a1;
	    a2->next = node->a;
	    a2->col = 1;
	    node->a->prev = a2;
	    node->a->status = error;
	 }
	 break;
      }
      strncpy(o_ptr, line_s, llengths[i]);
      o_ptr += llengths[i];
      *(o_ptr++) = '\n';
      node->a->col--;
   }
   strcpy(o_ptr, ";");
   if ( node->a->status != error )
     ccif_clean_node(node);	       /* remove any "#CCIF_ERR ..." comments */
   
}



      
	     
   
   
   /* The returned value of the btype parameter is crucial to choosing the correct CCIF data get/put
   routine.
   
   The values are:

     0   => not given/determinable  
     1   => text       }   These three are set from the information in the ITEM_TYPE_LIST
     2   => integer    }   category information in the dictionary. See the end_type_entry()
     3   => float      }   function in cifdic_to_symtab.inc
     4   => float with esd (parenthesised, or associated)
*/

#if CALL_LIKE_HPUX
   void ccif_item_type (const char * const itmnam_f, char * const code_f, 
                         char * const pcode_f, int * const btype, int * const sline,
                         const int Litmnam_f, const int Lcode_f, const int Lpcode_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_ITEM_TYPE (const struct Str_Desc * const itmnam_f, struct Str_Desc * const code_f,
                        struct Str_Desc * const pcode_f, int * const btype, int * const sline)
#endif
#if defined (VMS)
   void CCIF_ITEM_TYPE (const struct dsc$descriptor_s * const itmnam_f, 
                        struct dsc$descriptor_s * const code_f, struct dsc$descriptor_s * const pcode_f,
                        int * const btype, int * const sline)
#endif
#if CALL_LIKE_SUN
   void ccif_item_type_ (const char *const itmnam_f, char * const code_f, char * const pcode_f,
                         int * const btype, int * const sline, const int Litmnam_f, const int Lcode_f,
                         const int Lpcode_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_item_type (const int Litmnam_f, const char *const itmnam_f, const int Lcode_f,
                         char * const code_f, const int Lpcode_f, char * const pcode_f, int * const btype,
                         int * const sline)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_ITEM_TYPE (const char *const itmnam_f, const int Litmnam_f,
				  char * const code_f, const int Lcode_f,
				  char * const pcode_f, const int Lpcode_f,
				  int * const btype, int * const sline)
# else
   void CCIF_ITEM_TYPE (const char *const itmnam_f, char * const code_f, char * const pcode_f,
                         int * const btype, int * const sline, const int Litmnam_f, const int Lcode_f,
                         const int Lpcode_f)
# endif
#endif

{ 
   size_t Length, itmlen, codelen, pcodelen;
   char itmnam_c[MAXCLLEN+1], *itmval_c, *tcode, *tpcode;
   const Sym *item;
   ItemTypeList *type_ptr;
   
   FLENGTH(itmnam_f, Length) 
   strncpy(itmnam_c, STRPTR(itmnam_f), Length);
   itmnam_c[Length] = '\0';
   
   item = zzs_get(itmnam_c);
   if ( !item ) 
      *btype = -1;
   else {
      type_ptr = ITEM_TYPE(item);
      memset (STRPTR(code_f), ' ', STRLEN(code_f));
      memset (STRPTR(pcode_f), ' ', STRLEN(pcode_f));
      if ( type_ptr ) {
         *sline = type_ptr->single_line;
         *btype = type_ptr->basic_type;
         tcode = zzs_symname(type_ptr->code);
         tpcode = zzs_symname(type_ptr->primitive_code);
         memcpy (code_f, tcode, MIN( strlen(tcode), STRLEN(code_f)) ) ;
         memcpy (pcode_f, tpcode, MIN( strlen(tpcode), STRLEN(pcode_f)) ) ;
         
         if ( *btype == 3 && ( item->esd || item->associated_esd.rec ) )
            *btype = 4;
      }
      else
         *btype = 0;
   }
}


/***********************
 ** Sorting functions **
 ***********************/

/* Prepares for a sort on a category.
 * Status returns: 1 => success
                   0 => category does not exist in block
 */

#if CALL_LIKE_HPUX
   void ccif_setup_sort (const char * const catname_f, const int * const block, 
                        int * const index, int * const status, const int Lcatname_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_SETUP_SORT (const struct Str_Desc * const catname_f, const int * const block,
                        int * const index, int * const status)
#endif
#if defined (VMS)
   void CCIF_SETUP_SORT (const struct dsc$descriptor_s *const catname_f, const int * const block,
                        int * const index, int * const status)
#endif
#if CALL_LIKE_SUN
   void ccif_setup_sort_ (const char * const catname_f, const int * const block, 
                         int * const index, int * const status, const int Lcatname_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_setup_sort (const int Lcatname_f, const char * const catname_f, 
                         int * const index, int * const status, const int * const block)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_SETUP_SORT (const char * const catname_f, const int Lcatname_f, 
				   const int * const block, 
				   int * const index, int * const status)
# else
   void CCIF_SETUP_SORT (const char * const catname_f, const int * const block,
                         int * const index, int * const status, const int Lcatname_f)
# endif
#endif

{

   char catname_c[MAXCLLEN+1];
   size_t Length;
   Sym *cat;
   


   FLENGTH(catname_f, Length)
   strncpy(catname_c, STRPTR(catname_f), Length);
   catname_c[Length] = '\0';

   cat = zzs_get(catname_c);
   if ( !cat )			/* Force severe status for exit */
     ccif_signal( (CCIF_NOITEM & ~CCIF_M_SEVERITY) | CCIF_K_SEVERE, "ccif_setup_sort",
		 NULL, NULL, 0, catname_c);

   cat = cat->category.rec;
   zzs_expand_item_record(cat, *block + 1);
   if ( ! cat->l.loop[*block] ) {
     *status = 0;
     return;
   }


   *index = ccif_new_sort( cat, status, *block );
   
}


/* Adds an item to the sort list for an instance of a category.
   *status:  1 => success
             0 => failure

  <sigh> all the routines in f_interface.c should look like this one, i.e. just
  a wrapper, and little else.
 */

#if CALL_LIKE_HPUX
   void ccif_add_to_sort (const char * const itmname_f, const int * const index,
                        int * const status, const int Litmname_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_ADD_TO_SORT (const struct Str_Desc * const itmname_f, const int * const index,
                        int const status)
#endif
#if defined (VMS)
   void CCIF_ADD_TO_SORT (const struct dsc$descriptor_s *const itmname_f, const int * const index,
                        int * const status)
#endif
#if CALL_LIKE_SUN
   void ccif_add_to_sort_ (const char * const itmname_f, const int * const index, 
                         int * const status, const int Litmname_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_add_to_sort (const int Litmname_f, const char * const itmname_f, 
                         int * const index, const int * const status)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_ADD_TO_SORT (const char * const itmname_f, const int Litmname_f,  
                         int * const index, int * const status)
# else
   void CCIF_ADD_TO_SORT (const char * const itmname_f, const int * const index,
                         int * const status, const int Litmname_f)
# endif
#endif
{

   char itmname_c[MAXCLLEN+1];
   size_t Length;
   Sym *item, *cat;
   
   FLENGTH(itmname_f, Length)
   strncpy(itmname_c, STRPTR(itmname_f), Length);
   itmname_c[Length] = '\0';


   item = zzs_get(itmname_c);
   *status = 0; /* Assume failure first */
   if ( !item )
     ccif_signal( (CCIF_NOITEM & ~CCIF_M_SEVERITY) | CCIF_K_SEVERE, "ccif_add_to_sort",
		 NULL, NULL, 0, itmname_c);
   if ( item->true_name.rec) item = item->true_name.rec;
   cat = item->category.rec;
   
   if ( item == cat ) {
     ccif_signal( CCIF_ADDSORTCAT, "ccif_add_to_sort", NULL, NULL, 0, itmname_c );
     return;
   }

   zzs_expand_item_record(item, ccif_block_of_sort(*index) + 1);
   *status = ccif_grow_sort_list( item, *index );

}



/* Build red-black tree itself */

#if CALL_LIKE_HPUX
   void ccif_do_sort (const int * const index, int * const status)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_DO_SORT (const int * const index, int const status)
#endif
#if defined (VMS)
   void CCIF_DO_SORT (const int * const index, int * const status)
#endif
#if CALL_LIKE_SUN
   void ccif_do_sort_ (const int * const index, int * const status)
#endif
#if CALL_LIKE_IRIS
   void ccif_do_sort (const int * const index, int * const status)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_DO_SORT (const int * const index, int * const status)
# else
   void CCIF_DO_SORT (const int * const index, int * const status)
# endif
#endif

{
   const Sym *cat, *key;
   AST * node, *block_header;
   const char *block_name, *logname;
   int block, sort_list;

#ifdef DEBUG
   time_t end_time, start_time;
#endif

   if ( !( ccif_get_sort_info(*index, &cat, &block, &sort_list) ) )
     ccif_signal( CCIF_BADINDEX, "ccif_do_sort", NULL, NULL, 0, *index);
   
   
   block_header = ccif_data_heading_lookup( block );
   if ( block_header && block_header->a && block_header->a->text )
     block_name = block_header->a->text;
   else
     block_name = "<not determinable>" ;

   logname = ccif_logname_lookup( block );


#if 0  /* Again, this should be OK */
   if ( block >= cat->id || ! cat->l.loop[block] ) {
     AST * block_header = ccif_data_heading_lookup( block );
     ccif_signal(CCIF_SORTEMPTYCAT, "ccif_do_sort", NULL, NULL, 0,
		 zzs_recname(cat), block_header);
     *status = 0;
     return;
   }
#endif   

   
   /* If no sort order already specified, sort on key items of category */

   if ( !sort_list ) {
      
      if ( !(cat->key.rec) ) {
	 ccif_signal(CCIF_NOKEYSORT, "ccif_do_sort", NULL, NULL, 0, zzs_recname(cat));
      }
      else {
	 for (key = cat->key.rec; key; key = key->key.rec) 
	   ccif_grow_sort_list (key, block);
      }

   }


#ifdef DEBUG
   start_time = time(NULL);
#endif

   ccif_build_rbtree(*index);

#ifdef DEBUG
   end_time = time(NULL);
#endif

   ccif_signal(CCIF_SORTED, NULL, ccif_print_sort_list, NULL, *index, 
	       zzs_recname(cat), block_name, logname);

#ifdef DEBUG
   printf("Time taken for sort: %d seconds\n", end_time - start_time);
#endif
   
   *status = 1;
   

}

/*
   Input: status = 0  => if contexts are still linked to this sort,
                         don't free, Return number of linked 
                         contexts in status.
          status = 1 =>  Unlink any linked contexts, without changing
                         their current packet. Return number of
                         contexts unlinked (as a NEGATIVE number)
          status = 2 =>  as for 1, but reset contexts to first
                         packet in loop.

*/


#if CALL_LIKE_HPUX
   void ccif_release_sort (int * const index, int * const status)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_RELEASE_SORT (int * const index, int * const status)
#endif
#if defined (VMS)
   void CCIF_RELEASE_SORT (int * const index, int * const status)
#endif
#if CALL_LIKE_SUN
   void ccif_release_sort_ (int * const index, int * const status)
#endif
#if CALL_LIKE_IRIS
   void ccif_release_sort (int * const index, int * const status)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_RELEASE_SORT (int * const index, int * const status)
# else
   void CCIF_RELEASE_SORT (int * const index, int * const status)
# endif
#endif

{

  int n;


  if ( *index >= 0 && *index < NSORT_TREES ) {
#ifdef DEBUG
    fprintf(stderr,"Releasing sort %d\n",*index);
#endif

    n  = ccif_sort_link_number(*index);

    if ( ! *status && n ) {
      *status = n;
      return;
    }
    
    if ( n ) {
      ccif_unlink_sort(*index, *status == 2 );
      *status = -n;
    }
    ccif_free_sort(index);
    *index = -1;
  }
}


/* Associates a context with a sort */

#if CALL_LIKE_HPUX
   void ccif_sort_context (const int * const index, const int * const ncontext, 
			   int * const status)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_SORT_CONTEXT (const int * const index, const int * const ncontext, 
			   int * const status)
#endif
#if defined (VMS)
   void CCIF_SORT_CONTEXT (const int * const index, const int * const ncontext, 
			   int * const status)
#endif
#if CALL_LIKE_SUN
   void ccif_sort_context_ (const int * const index, const int * const ncontext, 
			   int * const status)
#endif
#if CALL_LIKE_IRIS
   void ccif_sort_context (const int * const index, const int * const ncontext, 
			   int * const status)
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_SORT_CONTEXT (const int * const index, const int * const ncontext, 
			   int * const status)
# else
   void CCIF_SORT_CONTEXT (const int * const index, const int * const ncontext,
                           int * const status)
# endif
#endif

{

  const Sym *scat, *ccat;
  int sblock, cblock, sorted;
  CONTEXT_LIST context;
  int reset = *status;

  if ( *index < 0 || *index >= NSORT_TREES )
    ccif_signal(CCIF_IDXRANGE, "ccif_sort_context", NULL, NULL, 0, *index, NSORT_TREES - 1);

  if ( *ncontext < 0 || *ncontext >= NCONTEXTS )
    ccif_signal(CCIF_CXTRANGE, "ccif_sort_context", NULL, NULL, 0, *ncontext, NCONTEXTS - 1);


  *status = ccif_get_sort_info( *index, &scat, &sblock, &sorted);

  if ( ! *status ) 
    ccif_signal(CCIF_BADINDEX, "ccif_sort_context", NULL, NULL, 0, *index);

  context = ccif_context_list(*ncontext);

  ccat = context.category;
  if ( ! ccat )
    ccif_signal(CCIF_BADCONTEXT, "ccif_sort_context", NULL, NULL, 0, *ncontext);

  cblock = context.block;

  if (cblock != sblock || ccat !=scat) {
    ccif_signal(CCIF_WRONGSORTCXT, "ccif_sort_context", NULL, NULL, 0, 
		*ncontext, zzs_recname(ccat), cblock,
		*index, zzs_recname(scat), sblock );
    *status = 0;
    return;
  }

  if ( ! ccat->l.loop[cblock] ) {
    ccif_signal(CCIF_NOTLOOPED, "ccif_sort_context", NULL, NULL, 0, 
		zzs_recname(ccat), cblock );
    *status = 0;
    return;
  }


  ccif_link_sort_to_context(*index, *ncontext, reset);
  *status = 1;

}


#if CALL_LIKE_HPUX
void ccif_search_context  ( const int * const index, const int * const ncontext,
                            const int * const nvals, const int * const ikey,
                            const float * const fkey, const char * const ckey,
                            const int * const ckey_len, int * const status,
			    const int Lckey)
#endif
#if CALL_LIKE_STARDENT
void CCIF_SEARCH_CONTEXT  (const int * const index, const int * const ncontext,
			   const int * const nvals, const int * const ikey,
                            const float * const fkey, 
			   const struct Str_Desc * const ckey,
                            const int * const ckey_len, int * const status)
#endif 
#if defined(VMS)
void CCIF_SEARCH_CONTEXT  (const int * const index, const int * const ncontext,
                            const int * const nvals, const int * const ikey,
                            const float * const fkey, const struct dsc$descriptor_s * const ckey,
                            const int * const ckey_len, int * const status)
#endif
#if CALL_LIKE_SUN
void ccif_search_context_ ( const int * const index, const int * const ncontext,
                            const int * const nvals, const int * const ikey,
                            const float * const fkey, const char * const ckey,
                            const int * const ckey_len, int * const status,
			    const int Lckey)
#endif
#if CALL_LIKE_IRIS
void ccif_search_context ( const int * const index, const int * const ncontext,
			   const int * const nvals, const int * const ikey,
			   const float * const fkey, 
			   const int Lckey, const char * const ckey,
			   const int * const ckey_len, int * const status)
#endif                               
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_SEARCH_CONTEXT ( const int * const index, const int * 
					const ncontext,
					const int * const nvals, const int * const ikey,
					const float * const fkey, 
					const char * const ckey, const int Lckey,
					const int * const ckey_len, int * const status)
# else
void CCIF_SEARCH_CONTEXT ( const int * const index, const int * const ncontext,
                            const int * const nvals, const int * const ikey,
                            const float * const fkey, const char * const ckey,
                            const int * const ckey_len, int * const status,
                            const int Lckey)
# endif
#endif

{

  static SORT_KEY * key = NULL;
  static int nkey = 0;

  int i, length, offset;
  SList *sort_list, *cursor;
  const SORT_ELEMENT * s;
  Rb_node rbnode;
  const char *ckey_start;


  /* Grow as required - don't bother to shrink it again. */
  if (  nkey < *nvals ) {      /* Always need one more element in key than number of vals */
    CCIF_REALLOC(*nvals + 1, SORT_KEY, key, "ccif_search_context", (0));
    nkey = *nvals + 1;
  }


  sort_list = ccif_get_sort_list(*index);
  cursor = sort_list;
  key[0].k.ikey = *nvals;
  
  for ( i = 1, offset = 0;
       i <= *nvals && sort_list; 
       i++, offset += *ckey_len) {

    s = (SORT_ELEMENT *) slist_iterate(sort_list, &cursor);
    length = *ckey_len;
    FBLENGTH(ckey, offset, length);


    if ( length == 1 && strchr( "?.", (STRPTR(ckey)[offset]) ) )
      key[i].s = (STRPTR(ckey)[offset]);
    else {
      key[i].s = ' ';

      switch ( s->comp_type ) {

        case ('u'):
        case ('c'):
	  memset (key[i].k.ckey, ' ', MAXCLLEN);
          strncpy(key[i].k.ckey, &(STRPTR(ckey)[offset]), length);
          key[i].k.ckey[offset+length] = '\0';
          break;

        case ('i'):
          key[i].k.ikey = ikey[i-1];
	  break;

        case ('n'):
          key[i].k.fkey = fkey[i-1];
          break;

        default:
          /* Force severe status for exit */
          ccif_signal((CCIF_UNKWNCMP & ~CCIF_M_SEVERITY) | CCIF_K_SEVERE , 
                      "ccif_search_context", NULL, NULL, 0, 
                      s->comp_type, "?" );
        }
    }
  }

  rbnode = ccif_find_key (*index, key, status);
  ccif_point_context(*ncontext, (AST *) rbnode->k.key, rbnode);

#ifdef DEBUG
  ccif_signal(CCIF_PRINTROW, "ccif_search_context", ccif_print_row, NULL, *ncontext,
	      *ncontext);
#endif
}

char * ccif_strdup( const char *s1 ) {
   char *c;
   int len = strlen(s1);
   if ( !len ) return NULL;
   CCIF_CALLOC(len+1, char, c, "ccif_strdup", (0))
   memcpy(c, s1, len);
   return (c);
}


/**************************Private data *************************************/

static CIF_FILE_LIST *ccif_file_list=NULL;

static struct _data_block_lookup {
   const CIF_FILE_LIST *file_list_entry;   /* Use the compiler as a check to make sure that this item
					    * is never used to modify the file list, without explicit 
					    * casting */
   AST * data_heading;
} *ccif_data_block_lookup=NULL ;

static int oflag; /* Set to appropriate open/create mode */




/**************************Private routines**********************************/

static void init_file_list(void) {
   
   
   CCIF_CALLOC(MAX_CIF_FILES, CIF_FILE_LIST, ccif_file_list, \
         "init_data_block", (0) )

}


/* Fill in the following members of the appropriate data_block_list, with the info from the star file:
 *    nblocks, block_id, block_node, root.
 * Also, fill in data_block_lookup array, so that the file which contains a particular block id
 * can be found quickly.
*/

static void add_to_data_block_list ( const int start_block, const int new_blocks, const int cif_unit, 
                                    AST * const star_file )  {

   int i,j;
   AST *ptr = star_file->down;
   DATA_BLOCK_LIST *data_blocks = ccif_file_list[cif_unit].data_blocks;

   CCIF_REALLOC((ccif_number_of_blocks + new_blocks), struct _data_block_lookup, ccif_data_block_lookup, \
      "add_to_data_block_list", (0) )
   
   memset ( ccif_data_block_lookup + ccif_number_of_blocks, 0, new_blocks * sizeof *ccif_data_block_lookup );


   CCIF_CALLOC(new_blocks, AST *, data_blocks->block_node, "add_to_data_block_list", (0) )
     
   CCIF_CALLOC(new_blocks, int, data_blocks->block_id, "add_to_data_block_list", (0) )

   CCIF_CALLOC(new_blocks, int, data_blocks->audit, "add_to_data_block_list", (0) )

   data_blocks->nblocks = new_blocks;
   data_blocks->root = star_file;


   
   if ( ! ptr ) return;
   if ( ptr->token == Comment ) ptr = ptr->right;
   
   for ( i = 0; ptr; i++, ptr = ptr->right ) {
   
      for ( j = 0; j < i; j++ ) {
         if ( !zzs_keycmp(ptr->a->text, data_blocks->block_node[j]->a->text) ) 
	   ccif_signal(CCIF_NONUNIQUEBLK, "add_to_data_block_list", NULL, NULL, 0, 
	                  ccif_file_list[cif_unit].filename, ptr->a->text);	                
      }

      data_blocks->block_node[i] = ptr;
      data_blocks->block_id[i] = start_block + i;
      ccif_data_block_lookup[start_block + i].file_list_entry = &(ccif_file_list[cif_unit]);
      ccif_data_block_lookup[start_block + i].data_heading = ptr;
   }
}


/* Find unused CIF file unit, and check to see if file name corresponds
* to an already opened unit.
* return value is same as *cif_unit, if file is not already open */


static int ccif_file_open ( int * const cif_unit, char * const logname, 
                                              const ccif_rwstat rwstat, int * const ifail ) {

   char *cifname;  /* Use *filename to report errors, etc, to avoid echoing passwords in 
                      DECnet filenames */
#ifdef VMS
   struct NAM namblk;
   char rsa[NAM$C_MAXRSS+1], esa[NAM$C_MAXRSS+1];
   char *f_ptr = rsa, **filename = &f_ptr; /* '&rsa' does not seem to be available to the
                                               program under AXP */
#else
   struct stat buf;
   char **filename = &cifname;
#endif
   

   int ccp4_newflag; /* 0 => CCP4_OPEN is UNKNOWN.
		      Check this on each call, since enterprising developers
		      may change the environment of their own programs */
   char * ccp4_open_status;

   int status, cif_open = -1, i, decnet = 0, create_new_file_ok = 0,
      next_free_unit, cif_already_open_read, cif_already_open_write,
      f_desc;
   CIF_FILE_LIST *cif_list_ptr;


   ccp4_open_status = getenv("CCP4_OPEN");
#ifdef VMS
   if ( ( ccp4_newflag = (!ccp4_open_status || zzs_keycmp(ccp4_open_status, "UNKNOWN") ) ) )
     oflag = O_RDWR | O_CREAT | O_TRUNC ;
   else
     oflag = O_RDWR | O_CREAT ;
#else
   if ( ( ccp4_newflag = (!ccp4_open_status || zzs_keycmp(ccp4_open_status, "UNKNOWN") ) ) )
     oflag = O_WRONLY | O_CREAT | O_TRUNC | O_EXCL ;
   else
     oflag = O_WRONLY | O_CREAT | O_TRUNC ;
#endif

   cifname = getenv(logname);
   if ( !cifname ) {
     cifname = ccif_strdup(logname);   
     ccif_signal( (CCIF_NOLOGNAME & ~CCIF_M_SEVERITY ) | CCIF_K_INFORM , "ccif_file_open", 
		 NULL, NULL, 0, logname);
   }
   else 
      /* ... so that we know that it is always OK to free() it when the file 
         is closed... */
     cifname = ccif_strdup(cifname);


/* Find next free cif unit */
   for ( i = 0; i < MAX_CIF_FILES; i++ ) {
      if ( !ccif_file_list[i].logname ) {
         next_free_unit = i;
         break;
      }
   }
   if ( i >= MAX_CIF_FILES ) 
     ccif_signal(CCIF_MAXCIFS, "ccif_file_open", NULL, NULL, 0);
               

   errno = 0;  /* Library function coming up... */

#ifdef VMS
   namblk.nam$l_rsa = rsa;
   namblk.nam$b_rss = NAM$C_MAXRSS;
   namblk.nam$l_esa = esa;
   namblk.nam$b_ess = NAM$C_MAXRSS;
   if ( (status = ccif_get_nameblock (cifname, &namblk)) ) 
#else
   if ( (status = stat(cifname, &buf)) ) 
#endif
     {
	if (errno == ENOENT ) {            /* File does not exist */
	   
	   if (rwstat == w ) {              /* Create new file */
	      *cif_unit = next_free_unit;
	      goto open_new_output_file;
	   }
	   else {             /* request to open file for reading, so barf, unless *ifail==1 */

             if ( *ifail == 0 ) {
               sprintf(errmsg, 
                       "Cannot open logical file %s for reading!\n", logname);
               EXIT_ON_SYS_ERROR("ccif_file_open", errmsg);
             }
             else {
               *ifail = -1;
               ccif_signal(CCIF_NOSUCHFILE, "ccif_file_open", NULL, NULL, 0, logname, *filename);
               free(cifname);
               return(-1);
             }

	   }
	}                     /* if (errno == ENOENT ) */
	else {                /* Some other stat error, so barf.
                               VMS: ccif_get_nameblock sets errno and vaxc$errno */
	   sprintf(errmsg, "Cannot stat logical file %s\n", logname);
	   EXIT_ON_SYS_ERROR("ccif_file_open", errmsg)
	}
     }                     /* if ( (status = stat(cifname, &buf)) ) */
   
   else {   /* File exists - proceed with extreme caution.... */

#ifdef VMS   /* Fundemental point - are we dealing with a decnet file or not? */
      decnet = !memcmp(namblk.nam$w_fid, null_fid, sizeof null_fid);
#endif

         /* First find out if file has already been assigned to a CIF unit */

      cif_already_open_read = -1;
      cif_already_open_write = -1;
      
      for ( i = 0; i < MAX_CIF_FILES; i++) {
         if (ccif_file_list[i].logname) {

/* These are the conditions of equality of i-node/device nos. (Unix), FID's (VMS non-decnet)
   or Resultant Strings (VMS decnet) */
#ifdef VMS
            if (  
              ( !decnet && 
	            !memcmp( ccif_file_list[i].namblock.nam$w_fid, namblk.nam$w_fid, sizeof null_fid ) ) 
              || ( decnet &&
                    !zzs_keycmp(ccif_file_list[i].filename, namblk.nam$l_rsa) )
            ) 
#else    
            if ((ccif_file_list[i].st_ino == buf.st_ino) && (ccif_file_list[i].st_dev == buf.st_dev)) 
#endif
	      {
		 if ( ccif_file_list[i].status == w )
		   cif_already_open_write = i;
		 else
		   cif_already_open_read = i;
	      }
	    
         }                                         /* if (ccif_file_list[i].logname) */
      }                                           /* for ( i = 0; i < MAX_CIF_FILES; i++) */
   
  /* Now, check very carefully how to deal with this. If rwstat == r, and file does not 
   * exist, will have already exited */

      if ( rwstat == w ) {
         if ( cif_already_open_write != -1 ) {    /* duplicate entry */
            *cif_unit = next_free_unit;
            cif_open = cif_already_open_write;
            goto already_open;
         }
         if ( cif_already_open_read != -1 ) {  /* Under VMS ONLY, can continue, if CCP4_OPEN is NEW */
#ifdef VMS
            if ( ccp4_newflag ) {
               *cif_unit = next_free_unit;
               goto open_new_output_file;
            }
            else 
	      ccif_signal(CCIF_VMSSAMEIO, "ccif_file_open", NULL, NULL, 0, *filename);
#else
	    ccif_signal(CCIF_UNXSAMEIO, "ccif_file_open", NULL, NULL, 0, *filename);
#endif
         }   /* if ( cif_already_open_read != -1 ) */

         /* At this point, know that the file exists, but has not yet been opened. Just go to
            open_new_output_file, relying on open to fail if we try something which is forbidden */
         *cif_unit = next_free_unit;
         goto open_new_output_file;

      } /*  if ( rwstat == w ) */

      else {  /* assign existing file to a CIF unit, for input */

         if ( cif_already_open_read != -1 ) { /* Already opened for reading - duplicate */
            *cif_unit = next_free_unit;
            cif_open = cif_already_open_read;
            goto already_open;
         }
         if (cif_already_open_write != -1 ) 
	   ccif_signal(CCIF_SAMEIO, "ccif_file_open", NULL, NULL, 0, *filename);

         /* Here, know that file has not yet been assigned to a CIF unit */
         *cif_unit = next_free_unit;
         goto open_new_input_file;
      }  /*  if ( rwstat == w ) */

   }    /* if ( (status = stat(cifname, &buf)) ) */



   
already_open:
   cif_list_ptr = &(ccif_file_list[*cif_unit]);
   *cif_list_ptr = ccif_file_list[cif_open];
   cif_list_ptr->logname = ccif_strdup(logname);
   cif_list_ptr->data_blocks->nfiles++;
#ifdef VMS
/*   cif_list_ptr->name = cifname; */
   strcpy(cif_list_ptr->filename, ccif_file_list[cif_open].filename);
   cif_list_ptr->namblock.nam$l_rsa = cif_list_ptr->filename;
   strcpy(cif_list_ptr->name,     ccif_file_list[cif_open].name);
   cif_list_ptr->namblock.nam$l_esa =cif_list_ptr->name;
#else
   cif_list_ptr->filename = cifname;
#endif
   ccif_signal(CCIF_OPEN, NULL, NULL, NULL, 0, 
               logname, ccif_file_list[cif_open].logname);
   return (cif_open); 



open_new_output_file:

/* oflag has been set so that:
       Unix and CCP4_OPEN is NEW     => open fails if file already exists.
       Unix and CCP4_OPEN is UNKNOWN => open truncates an existing file
       VMS and CCP4_OPEN is NEW      => open creates a new version of an existing file
       VMS and CCP4_OPEN is UNKNOWN  => open opens existing file, but will not truncate it.
                                        Need to do this with RMS, and set file header 
                                        characteristics correctly using QIO (groan!)
                                        This is done in the truncate() clone in CCIF_VMS.C
*/

   if ( !ccp4_newflag ) {
      errno = 0;  /* Library function coming up... */

      #ifdef _MSC_VER
      status = wintruncate(cifname, 0);
      #elif _WIN32 // Mingw32
      // wintruncate looks broken (note fh=0 before _close(fh))
      // so we don't use it here
      status = creat(cifname, O_MODE);
      if (status != -1)
	status = close(status);
      #else
      status = truncate(cifname, 0);
      #endif

      if (status) {
         if ( errno != ENOENT ) {
	    sprintf(errmsg, "Cannot truncate %s\n", *filename);
	    EXIT_ON_SYS_ERROR("ccif_file_open", errmsg)
	 }
      }
#ifdef VMS
      else {
         if ( !decnet )
            ccif_setup_output_file(cifname);
      }
#endif
   }

   errno = 0;  /* Library function coming up... */
   f_desc = open(cifname, oflag, O_MODE);  
   if ( f_desc < 0 ) {
      sprintf(errmsg, "Cannot open %s\n", *filename);
      EXIT_ON_SYS_ERROR("ccif_file_open", errmsg)
   }

#ifndef VMS
   errno = 0;  /* Library function coming up... */
   status = fstat(f_desc, &buf);
   if ( status < 0 ) {
      sprintf(errmsg, "Cannot fstat %s\n", *filename);
      EXIT_ON_SYS_ERROR("ccif_file_open", errmsg)
   }
#endif
   close(f_desc);

open_new_input_file:

   cif_list_ptr = &(ccif_file_list[*cif_unit]);
#ifdef VMS
   cif_list_ptr->namblock.nam$l_rsa = cif_list_ptr->filename;
   cif_list_ptr->namblock.nam$b_rss = NAM$C_MAXRSS;
   cif_list_ptr->namblock.nam$l_esa = cif_list_ptr->name;
   cif_list_ptr->namblock.nam$b_ess = NAM$C_MAXRSS;
/*   cif_list_ptr->name = cifname; */
   ccif_get_nameblock(cifname, &(cif_list_ptr->namblock) );   /* this sets cif_list_ptr->filename/name */
   cif_list_ptr->decnet = !memcmp(namblk.nam$w_fid, null_fid, sizeof null_fid);
#else
   cif_list_ptr->st_ino = buf.st_ino;
   cif_list_ptr->st_dev = buf.st_dev;
   cif_list_ptr->filename = cifname;
#endif
   cif_list_ptr->status = rwstat;
   cif_list_ptr->logname = ccif_strdup(logname);
   cif_list_ptr->line_limit = MAXCLLEN;  /* Initially, enforce standard line limit */

/* For an input file, most of the members of this structure are set in add_to_data_block_list, 
 * after parsing the file. For an output file, they are set during the building of the 
 * AST, or duplication of an input file to an output file */

   CCIF_CALLOC(1, DATA_BLOCK_LIST, cif_list_ptr->data_blocks, "ccif_file_open", (0))
   cif_list_ptr->data_blocks->nfiles++;

   return (*cif_unit);
}


/* Delete file entry associated with unit. Calling routine must free data block list
   if required */

static void del_file_entry(const int unit) {

  free (ccif_file_list[unit].logname);

#ifdef VMS
/*  free (ccif_file_list[unit].name); */
  ccif_file_list[unit].name[0] = '\0';
  ccif_file_list[unit].filename[0] = '\0';
  ccif_file_list[unit].namblock = cc$rms_nam;
#else
  free (ccif_file_list[unit].filename);
  ccif_file_list[unit].filename = NULL;
#endif


  ccif_file_list[unit].logname = NULL;
  ccif_file_list[unit].data_blocks = NULL;
  ccif_file_list[unit].status = u;
  ccif_file_list[unit].line_limit=MAXCLLEN; /* return to default */

}   

static void duplicate_cif ( const int from_file, const int to_file, const char * const new_logname) {

   
   int new_blocks = ccif_file_list[from_file].data_blocks->nblocks;
   STreeParser cifparser;
   DATA_BLOCK_LIST *data_blocks = ccif_file_list[to_file].data_blocks;
   AST *star_root, *star_root_save;
   extern void ccif_sor_star_file( STreeParser *, AST **, const int);
   
   ccif_file_list[to_file].data_blocks->nblocks = new_blocks;

   data_blocks->nblocks = new_blocks;
   CCIF_CALLOC( new_blocks, AST *, data_blocks->block_node, "duplicate_cif", (0) )
   CCIF_CALLOC( new_blocks, int,   data_blocks->block_id,   "duplicate_cif", (0) )
   CCIF_CALLOC( new_blocks, int,   data_blocks->audit,      "duplicate_cif", (0) )
   
/* ccif_ast_dup doubly-links the new AST */
   star_root = ccif_ast_dup(ccif_file_list[from_file].data_blocks->root);

   data_blocks->root = star_root;
   STreeParserInit (&cifparser);
   ccif_number_of_blocks += new_blocks;
   star_root_save = star_root;
   ccif_sor_star_file ( &cifparser, &star_root, ccif_number_of_blocks - new_blocks);
   add_to_data_block_list (ccif_number_of_blocks - new_blocks, new_blocks, to_file, star_root_save);
} 


static int cif_unit_no(const char * const logname) {
   int i;
   if ( !ccif_file_list ) return (-1);
   for (i = 0; i < MAX_CIF_FILES; i++) 
      if ( ccif_file_list[i].logname && !strcmp(logname, ccif_file_list[i].logname) ) return i;
   return (-1);
}


static void print_info(const int unit) {

   ccif_signal(CCIF_OPENED, NULL, 
	       print_info_callback, 
	       ccif_file_list[unit].data_blocks->block_node, 
	       ccif_file_list[unit].data_blocks->nblocks,
	       ccif_file_list[unit].status == r ? "read-only for input" : "for output");
	       
}


static void print_info_callback(char callback_msg[], 
				void ** ptr, 
				const int * const count, 
				const int first) {
   
   
   if ( first ) {
      if ( *count ) {
	 strcpy(callback_msg, "The file contains the following data blocks:");
	 *ptr = *((AST **) *ptr);
      }
      else {
	 strcpy(callback_msg, "The file has been initialised with no data blocks.");
	 *ptr = NULL;
      }
      return;
   }

   if ( !(*ptr) ) {
      callback_msg[0] = '\0';
      return;
   }
   
   sprintf(callback_msg,"     %s (at line %d)", ((AST *) *ptr)->a->text, ((AST *) *ptr)->a->line);
   *ptr = ((AST *) *ptr)->right;
}


/************************Public routines which need/alter private data***************************/


ccif_rwstat ccif_block_status ( const int block ) {
   return (ccif_data_block_lookup[block].file_list_entry->status);
}

char * ccif_file_lookup ( const int block ) {
   return (ccif_data_block_lookup[block].file_list_entry->filename);
}

AST * ccif_data_heading_lookup( const int block ) {      
   return (ccif_data_block_lookup[block].data_heading);
}

/* File name (for information, etc.) */
char * ccif_file_name (const int unit) {
   return (ccif_file_list[unit].filename);
}
      
      
/* File name to be used for I/O */

char * ccif_file_io_name (const int unit) {
#ifdef VMS
   return (ccif_file_list[unit].name);
#else
   return (ccif_file_list[unit].filename);
#endif
}

/* Set line limit for output file (assumes calling routine checks that limit is sensible */
void ccif_change_line_limit(const int unit, const int limit) {
  ccif_file_list[unit].line_limit = limit;
}

/* Logical name containing block */
const char * ccif_logname_lookup(const int block) {
  return (ccif_data_block_lookup[block].file_list_entry->logname);
}

int ccif_no_of_blocks ( const int unit) {
   return (ccif_file_list[unit].data_blocks->nblocks);
}

/* Return copy of file list entry */
CIF_FILE_LIST ccif_file_list_entry(const int unit) {
   return (ccif_file_list[unit]);
}

      

/* This routine is here to avoid exporting ccif_data_block_lookup to other modules.
 * It assumes that file_list_entry->data_blocks has been fully updated, and that
 * ccif_number_of_blocks is current.
 * It is unnecessary, in general, to go through the whole of the data_blocks structure,
 * but doing it makes fewer demands on the calling routine.
 */

void ccif_add_blocks_to_lookup(const CIF_FILE_LIST * const file_list_entry) {
   int i, id;
   
   CCIF_REALLOC(ccif_number_of_blocks, struct _data_block_lookup, ccif_data_block_lookup, \
      "ccif_add_blocks_to_lookup", (0) )
     
   for ( i = 0; i < file_list_entry->data_blocks->nblocks; i++ ) {
      id = file_list_entry->data_blocks->block_id[i];
      ccif_data_block_lookup[id].file_list_entry = file_list_entry;
      ccif_data_block_lookup[id].data_heading = file_list_entry->data_blocks->block_node[i];
   }
}

void ccif_del_blocks_from_lookup(const CIF_FILE_LIST * const file_list_entry) {
   int i, id;

/* Don't do anything if there is more than one logical name pointing at 
   this data_block_list structure*/
   if ( file_list_entry->data_blocks->nfiles > 1 ) return;

   for ( i = 0; i < file_list_entry->data_blocks->nblocks; i++ ) {
      id = file_list_entry->data_blocks->block_id[i];
      ccif_data_block_lookup[id].file_list_entry = NULL;
      ccif_data_block_lookup[id].data_heading = NULL;
   }
}


      
      
/* Arguably, this routine should be before the declaration of ccif_file_list */

#if CALL_LIKE_HPUX
   void ccif_print_cif (const char * const logname_f, const int Llogname_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_PRINT_CIF (const struct Str_Desc * const logname_f)
#endif
#if defined (VMS)
   void CCIF_PRINT_CIF (const struct dsc$descriptor_s *const logname_f)
#endif
#if CALL_LIKE_SUN
   void ccif_print_cif_ (const char * const logname_f, const int Llogname_f)  
#endif
#if CALL_LIKE_IRIS
   void ccif_print_cif (const int Llogname_f, const char * const logname_f)  
#endif
#if CALL_LIKE_MVS                    
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_PRINT_CIF  (const char * const logname_f, const int Llogname_f)  
# else
   void CCIF_PRINT_CIF (const char * const logname_f, const int Llogname_f)
# endif
#endif


{
   size_t Length;
   char logname_c[MAXFLEN], *cif;
   int i;
   
   STreeParser cifparser;
   AST * star_root;
   extern void ccif_print_star_file( STreeParser *, AST **, const int);
   
   FLENGTH(logname_f, Length)
   strncpy(logname_c, STRPTR(logname_f), Length);
   logname_c[Length] = '\0';

      
   i = cif_unit_no (logname_c);
   if ( i == -1 ) 
     ccif_signal(CCIF_CIFNOTOPEN, "ccif_print_cif", NULL, NULL, 0, logname_c);
   
   if ( ccif_file_list[i].status != w ) 
     ccif_signal(CCIF_READONLY, "ccif_print_cif", NULL, NULL, 0, logname_c);
   
   star_root = ccif_file_list[i].data_blocks->root;
   STreeParserInit (&cifparser);
   ccif_print_star_file ( &cifparser, &star_root, i );

   ccif_file_list[i].data_blocks->printed = 1;
}
   
/* Returned values in istat:
 * -1  => logical file lognam_f not found or opened
 *  0  => not found or created in file
 *  1  => block found in file
 *  2  => block created in file
 */

      
#if CALL_LIKE_HPUX
   void ccif_block_by_name (const char * const lognam_f, const char * const blknam_f, 
                       int *blk_id, int *istat, const char * const new_f,
		       const int Llognam_f, const int Lblknam_f, const int Lnew_f)
#endif
#if CALL_LIKE_STARDENT
   void CCIF_BLOCK_BY_NAME (const struct Str_Desc * const lognam_f, const struct Str_Desc * const blknam_f, 
		       int *blk_id, int *istat, const struct Str_Desc * const new_f)
#endif
#if defined (VMS)
   void CCIF_BLOCK_BY_NAME (const struct dsc$descriptor_s * const lognam_f, 
		       const struct dsc$descriptor_s * const blknam_f, int *blk_id, int *istat,
                       const struct dsc$descriptor_s * const new_f)
#endif
#if CALL_LIKE_SUN
   void ccif_block_by_name_ (const char *const lognam_f, char * const blknam_f, int *blk_id, int *istat,
			const char * const new_f,
			const int Llognam_f, const int Lblknam_f, const int Lnew_f)
#endif
#if CALL_LIKE_IRIS
   void ccif_block_by_name (const int Llognam_f, const char *const lognam_f, const int Lblknam_f, 
                        char * const blknam_f, int *blk_id, int *istast,
			const int Lnew_f, const char * const new_f)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall  CCIF_BLOCK_BY_NAME (const char *const lognam_f, const int Llognam_f,
				       char * const blknam_f, const int Lblknam_f,
				       int *blk_id, int *istat,
				       const char * const new_f, const int Lnew_f)
# else
   void CCIF_BLOCK_BY_NAME (const char *const lognam_f, char * const blknam_f, int *blk_id, int *istat,
                        const char * const new_f,
                        const int Llognam_f, const int Lblknam_f, const int Lnew_f)
# endif
#endif
{
   char lognam_c[MAXCLLEN+1], blknam_c[MAXCLLEN+1], *blknam_ptr=blknam_c;
   size_t loglen, blklen, newlen;
   int i, j, new, token, zzstp;
   AST *dummy_node;
   int antlr_retsignal;

   DATA_BLOCK_LIST *data_blocks;

   
   FLENGTH(lognam_f, loglen)
   strncpy(lognam_c, STRPTR(lognam_f), loglen);
   lognam_c[loglen] = '\0';
   
   FLENGTH(blknam_f, blklen)
   strncpy(blknam_c, STRPTR(blknam_f), blklen);
   blknam_c[blklen] = '\0';

   FLENGTH(new_f, newlen)
   new = ( !zzs_keyncmp("new", STRPTR(new_f), newlen) ) || 
         ( !zzs_keyncmp("create", STRPTR(new_f), newlen) );
   

   /* Check that blknam_c conforms to the STAR syntax for data headings */
   zzmode(NORMAL);
   set_lex_act(0);  /* switch off error reporting and attribute creation by the lexer */
   dummy_node = NULL;
   zzastMARK;
   CCIF_SCHK_ON;
   ANTLRs((token = ccif_antlr_check_data_heading(&dummy_node, &antlr_retsignal,  strlen(blknam_c))), blknam_c);
   CCIF_SCHK_OFF;
   zzastREL;
   set_lex_act(1);
   
   if ( !token ) {
      ccif_signal(CCIF_BADBLOCKNAME, "ccif_block_by_name", NULL, NULL, 0, blknam_c);
      *istat = 0;
      return;
   }
   
   for (i = 0, *istat = -1; i < MAX_CIF_FILES; i++) {
      if ( ccif_file_list[i].logname && !strcmp(lognam_c, ccif_file_list[i].logname) ) {
	 data_blocks = ccif_file_list[i].data_blocks;
         for (j = 0, *istat = 0; j < data_blocks->nblocks; j++) {
            if ( !zzs_keycmp(blknam_c, data_blocks->block_node[j]->a->text) ) {
               *blk_id = data_blocks->block_id[j];
	       *istat = 1;
               return;
            }
         }  /* for (j = 0, *blk_id = -1; ... ) */
	 
	 /* Data block does not exist in file, and not allowed to create it */
	 if ( !new || ccif_file_list[i].status != w ) return;
	 
	 /* Stuff to create new data block here */
	 ccif_new_data_blocks ( ccif_file_list[i].data_blocks, &(blknam_ptr), 1 );
	 ccif_add_blocks_to_lookup( &(ccif_file_list[i]) );

	 /* Get id of new block */

         data_blocks = ccif_file_list[i].data_blocks;
         for (j = 0, *istat = 0; j < data_blocks->nblocks; j++) {
            if ( !zzs_keycmp(blknam_c, data_blocks->block_node[j]->a->text) ) {
               *blk_id = data_blocks->block_id[j];
	       *istat = 2;
	       ccif_signal(CCIF_NEWBLOCK, NULL, NULL, NULL, 0, 
			   blknam_c, ccif_file_list[i].logname);
               return;
            }
         }  /* for (j = 0, *blk_id = -1; ... ) */

         return;

      }  /* if ( ccif_file_list[i].logname ... ) */
   }  /* for (i = 0, *istat = -1; ... */
}   




#if CALL_LIKE_HPUX
void ccif_block_by_index (const char *const lognam_f, const int * const idx, char * const blknam_f, 
			   int * const blk_id, int *const istat,
			   const int Llognam_f, const int Lblknam_f)
#endif
#if CALL_LIKE_STARDENT
void CCIF_BLOCK_BY_INDEX (const struct Str_Desc * const lognam_f, const int * const idx,
			  struct Str_Desc * const blknam_f, int *blk_id, int *istat)
#endif
#if defined (VMS)
void CCIF_BLOCK_BY_INDEX (const struct dsc$descriptor_s * const lognam_f, const int * const idx,
			  struct dsc$descriptor_s * const blknam_f, int *blk_id, int *istat)
#endif
#if CALL_LIKE_SUN
void ccif_block_by_index_ (const char *const lognam_f, const int * const idx, char * const blknam_f, 
			   int * const blk_id, int *const istat,
			   const int Llognam_f, const int Lblknam_f)
#endif
#if CALL_LIKE_IRIS
void ccif_block_by_index (const int Llognam_f, const char *const lognam_f, const int * const idx,
			   const int Lblknam_f, char * const blknam_f, int * const blk_id, int *const istat)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
void __stdcall CCIF_BLOCK_BY_INDEX (const char *const lognam_f, const int Llognam_f, 
				    const int * const idx, char * const blknam_f, 
				    const int Lblknam_f, int * const blk_id, 
				    int *const istat)
# else
void CCIF_BLOCK_BY_INDEX (const char *const lognam_f, const int * const idx, char * const blknam_f,
                           int * const blk_id, int *const istat,
                           const int Llognam_f, const int Lblknam_f)
# endif
#endif

{
   char lognam_c[81], *blknam_c;
   size_t loglen, blklen;
   int i, j, new;
   
   FLENGTH(lognam_f, loglen)
   if (loglen >= 81)
       loglen = 80;
   strncpy(lognam_c, STRPTR(lognam_f), loglen);
   lognam_c[loglen] = '\0';
   
   memset( STRPTR(blknam_f), ' ', STRLEN(blknam_f) );
   
   *istat = 0;
   if ( *idx <= 0 ) return;
   
   for (i = 0; i < MAX_CIF_FILES; i++) {
      if ( ccif_file_list[i].logname && !strcmp(lognam_c, ccif_file_list[i].logname) ) {
	 DATA_BLOCK_LIST *data_blocks = ccif_file_list[i].data_blocks;
	 if ( *idx <= data_blocks->nblocks ) {
	    blknam_c = data_blocks->block_node[(*idx) - 1]->a->text;
	    *istat = 1;
	    *blk_id = data_blocks->block_id[(*idx) - 1];
	    strncpy( STRPTR(blknam_f), blknam_c, strlen(blknam_c) );
	 }                             /* if ( *idx < data_blocks->nblocks ) */
	 return;
      } 			       /* if ( ccif_file_list[i].logname ... */
   }				       /* for (i = 0; i < MAX_CIF_FILES; i++) */
   *istat = -1;
   return;
}


#if _USE_MMAP == 1

static void housekeep_mapped_files(void) {
   int i; 
   __import__ MAP_FILE_RECORD *ccif_map_file_list[MAX_MAPPED_FILES]; 

#ifdef DEBUG
   printf("Mapped file info (void housekeep_mapped_files):\n");
   _MAPPED_FILE_INFO;
#endif

   for (i = 0; i < MAX_MAPPED_FILES; i++) {
      if ( ccif_map_file_list[i] ) {

         if ( ccif_map_file_list[i]->nnodes < 0 ) 
	   ccif_signal(CCIF_MAPPEDFILES, "housekeep_mapped_files", NULL, NULL, 0, 
		       ccif_map_file_list[i]->filename); 

         if ( !ccif_map_file_list[i]->nnodes ) { 

#ifdef DEBUG
	   fprintf(stderr,"Unmapping file %s\n",ccif_map_file_list[i]->filename);
#endif

#ifdef VMS
           if (! ccif_map_file_list[i]->mapped) {
              free (ccif_map_file_list[i]->start);
              free (ccif_map_file_list[i]->filename);
	      free (ccif_map_file_list[i]);
	      ccif_map_file_list[i] = NULL;
           }
           else {
              if ( ccif_vms_munmap( &(ccif_map_file_list[i]->start), 
                       ccif_map_file_list[i]->channel) ) {
                 sprintf(errmsg, "ccif warning: cannot unmap %s", ccif_map_file_list[i]->filename); 
	         perror(errmsg); 
              }
              else {
                 free (ccif_map_file_list[i]->filename);
                 free (ccif_map_file_list[i]);
                 ccif_map_file_list[i] = NULL;
              }
           }
	                
#else
	   if ( munmap(ccif_map_file_list[i]->start, ccif_map_file_list[i]->length) ) { 
              sprintf(errmsg, "ccif warning: cannot unmap %s", ccif_map_file_list[i]->filename); 
	      perror(errmsg); 
	    } 
            else { 
               fclose(ccif_map_file_list[i]->fileptr);
	       free (ccif_map_file_list[i]->filename);
	       free (ccif_map_file_list[i]);
	       ccif_map_file_list[i] = NULL;
	     } /* if ( munmap( ... ) ) */
#endif  /* VMS */
	 } /* if ( !ccif_map_file_list[i]->nnodes ) */
       } /* if ( ccif_map_file_list[i] ) */
    } /* for (i = 0; i < MAX_MAPPED_FILES; i++) */
 }

#endif /* _USE_MMAP == 1 */

/****** Malloc tracing stuff *********/
#if defined MALLOC_TRACE && CALL_LIKE_SUN
   void ccif_malloc_trace_ ( void ) {
      static FILE *ccif_mem_prof;
      ccif_mem_prof = fopen("ccif_mem_prof.out","w");
      mal_setstatsfile(ccif_mem_prof);
      mal_trace(1);
}
#else

#if CALL_LIKE_HPUX || CALL_LIKE_IRIS
   void ccif_malloc_trace (void)
#endif
#if defined VMS || CALL_LIKE_STARDENT
   void CCIF_MALLOC_TRACE (void)
#endif
#if CALL_LIKE_SUN 
   void ccif_malloc_trace_ (void)
#endif
#if CALL_LIKE_MVS
# if CALL_LIKE_MVS == 1
   void __stdcall CCIF_MALLOC_TRACE (void)
# else
   void CCIF_MALLOC_TRACE (void)
# endif
#endif
{}
#endif

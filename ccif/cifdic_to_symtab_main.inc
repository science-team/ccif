                                                              /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

int cifdic_to_symtab_main (int arg1c, char **arg1v) {

	char symbol[MAXCLLEN+1], *ddl_key, *ddl_item, *dictionary_cat;
	FILE *dictin=NULL, *symdump=stdout;
	STreeParser symparser;
	int hsize, i, antlr_retsignal, zztsp;
	AST *star_root = NULL, *star_root_save;
	extern void ccif_antlr_star_file ( AST **, int *, const int);
	static ItemTypeList null_type = {0, 0, 0, 0, 0, 0, NULL}; /* used to set up null type */
	
	int old_symtab_arg, dict_arg, symtab_arg, hashsize_arg, hashdump_arg, minargs;

	int save_block_seq_no;


#if _USE_MMAP == 1
#ifdef VMS
        extern int ccif_mapfile ( const char * const a, 
                                  FILE ** const b, 
                                  const CIF_FILE_LIST * const c);
	CIF_FILE_LIST file_entry;
#else
	extern int ccif_mapfile ( const char * const, FILE ** const);
#endif
	MAP_FILE_RECORD *map_file;
	__import__ MAP_FILE_RECORD *ccif_map_file_list[];
        __import__ MAP_FILE_RECORD *ccif_current_mapfile;
	int m;
#endif

	/* Are we merging into an existing dictionary */
	if ( arg1c < 2 || ! strcmp(arg1v[1], "-a" ) ) {
	  minargs = 5;
	  old_symtab_arg = 2;
	  dict_arg = 3;
	  symtab_arg = 4;
	  hashsize_arg = 0;
	  hashdump_arg = 5;
	}
	else {
	  minargs = 4;
	  old_symtab_arg = 0;
	  dict_arg = 1;
	  symtab_arg = 2;
	  hashsize_arg = 3;
	  hashdump_arg = 4;
	}


	/* Very basic check on validity of arguments */
	if ( ( arg1c != minargs && arg1c != minargs+1) || ( hashsize_arg && (hsize = atoi(arg1v[hashsize_arg])) < 1) ) {
	  fprintf (stderr,"cifdic_to_symtab <dictionary> <output symbol table> <hash size> [<hash dump file>]\n");
	  fprintf (stderr,"   or\n");
	  fprintf (stderr,"cifdic_to_symtab -a <input symbol table> <dictionary> <output symbol table> [<hash dump file>]\n");
	  return (EXIT_FAILURE);
        }

	if ( arg1c == minargs+1 ) {
	  symdump = fopen(arg1v[hashdump_arg], "w");
	  if ( !symdump ) {
	    fprintf (stderr,"cifdic_to_symtab: cannot open output file for hash table list!\n");
	    return (EXIT_FAILURE);
	  }
	}
	

	if ( ! old_symtab_arg  ) {
	  zzs_init (hsize, 32768);
	  zzs_strdup("");    /* Make strings[0] be '\0', so an offset of zero into strings is a
				null value */
	}
	else {
	  fprintf(stderr, "Loading input symbol table for merging....");
	  /* Undump without memory-mapping */
	  zzs_undump(arg1v[old_symtab_arg],arg1v[old_symtab_arg],0);
	  /* Initialise temporary string table with reasonably-sized buffer */
	  zzs_init (0, 32768);
	  fprintf(stderr,"done\n");
	}	  

        /* Parse dictionary, and generate AST. */

	fprintf(stderr, "Loading dictionary....");
        set_lex_act(1);
        zzastMARK;


#if defined VMS && _USE_MMAP == 1
        /* In order to map correctly, need enough of a file_entry
	   filled in to fool ccif_mapfile into thinking that we are
	   dealing with a CIF */
        file_entry.namblock.nam$l_rsa = file_entry.filename;
        file_entry.namblock.nam$l_esa = file_entry.name;
	file_entry.namblock.nam$b_rss = NAM$C_MAXRSS;
	file_entry.namblock.nam$b_ess = NAM$C_MAXRSS;
	if ( ccif_get_nameblock(arg1v[dict_arg], &(file_entry.namblock)) ) {
	  sprintf(errmsg, "Cannot get nameblock of file %s", arg1v[dict_arg]);
	  EXIT_ON_SYS_ERROR("cifdic_to_symtab_main", errmsg);
	}
	if ( !file_entry.namblock.nam$w_fid[0] && !file_entry.namblock.nam$w_fid[1]
	    && !file_entry.namblock.nam$w_fid[2] ) {
	  file_entry.decnet = 1;
	}
	else {
	  file_entry.decnet = 0;
	}

#endif


#if _USE_MMAP == 1
	m = ccif_mapfile(arg1v[dict_arg], &dictin
#ifdef VMS
			 , &file_entry
#endif
        );

	if ( m >= 0 && m < MAX_MAPPED_FILES ) {
	  ccif_current_mapfile = ccif_map_file_list[m];
	  ANTLRs( ccif_antlr_star_file(&star_root, &antlr_retsignal, 0), ccif_map_file_list[m]->start ) ;
	  ccif_current_mapfile = NULL;
	}
	else
#endif
	{
	   dictin = fopen(arg1v[dict_arg], "r");
	   ANTLRm( ccif_antlr_star_file(&star_root, &antlr_retsignal, 0), dictin, START) ;
	   fclose(dictin);
	}

        zzastREL;
	fprintf(stderr, "dictionary loaded.\n");


       /* Add item names for DDL items which are not immediate properties of dictionary items
          to symbol table */

        for (i = 0, zzs_scope(&dictionary_scope); 
             (dictionary_cat = dictionary_items[i].name) ; 
             ) {
          Sym *cat = zzs_tmpadd(dictionary_cat), *p, *q;
          
	  cat->category.rec = cat;
	  
	  for (i++; 
	       !dictionary_items[i].category_list && dictionary_items[i].name ; 
	       i++) {
	     q = zzs_tmpadd(dictionary_items[i].name);
	     set_cat_ptr_implicitly(q);
	     q->no_in_cat = q->category.rec->no_in_cat++;
	  }
       }
       zzs_noscope();


       /* First tree walk, to add properties of the dictionary as a whole, and "properties of
          dictionary item properties". These are then in place for the second tree walk; this
	  is how forward references are dealt with */
          
	STreeParserInit (&symparser);
	star_root_save = star_root;
	ccif_number_of_blocks = 3;  /* One for each of global_, datablocks and save frames. */
	if ( ! old_symtab_arg )
	  slist_add_to_head( &(ccif_item_type_list[0]), &null_type ); /* null entry for unknown _item_type.code - 
									 which is mandatory, but the category
									 ITEM_TYPE is not (!) */
	
	fprintf(stderr, "Starting first tree walk....");
	save_block_seq_no = block_seq_no;
	sor_star_file (&symparser, &star_root);
	fprintf(stderr, "tree walk completed.\n");
	zzs_rmscope(&dictionary_scope);
	star_root = star_root_save;


	
	/* Add item names which are going to be used to get info from the dictionary
	 * into the symbol table. The symbol table records will be unlinked before dumping
	 * to disk. The names themselves are added to the temporary string table, so won't
	 * be left over in the final dumped file
	 */

        for (i = 0, zzs_scope(&save_frame_scope); 
             (ddl_key = save_frame_items[i].name) ; ) {
          Sym *cat, *p, *q;
          
          p = zzs_tmpadd(ddl_key);
	  strncpy (symbol, ddl_key + 1, MAXCLLEN);
	  *(strchr(symbol, '.')) = '\0';
	  cat = zzs_tmpadd(symbol);
	  cat->category.rec = cat;
          set_cat_ptr_implicitly(p);
	  p->no_in_cat = p->category.rec->no_in_cat++;
	  
	  for (i++; 
	       save_frame_items[i].specified ; 
	       i++) {
	     q = zzs_tmpadd(save_frame_items[i].name);
	     set_cat_ptr_implicitly(q);
	     q->no_in_cat = q->category.rec->no_in_cat++;
	  }
       }
       zzs_noscope();

        /* Walk AST, adding info about data items into symbol table */
	
	STreeParserInit (&symparser); 
	ccif_number_of_blocks = 1;
	block_seq_no = save_block_seq_no;  /* reset block id's */
	star_root_save = star_root;
	
	fprintf(stderr, "Starting second tree walk....");
	sor_star_file (&symparser, &star_root_save);
	fprintf(stderr, "tree walk completed.\n");

#if 0
	/* Pass over every entry in symbol table to set mandatory_scope items.
	With the current implementation, it is not necessary to
	do this with a Sorcerer tree parser.
	*/

        fprintf(stderr, "Adding mandatory scopes....");
        for (i=0; i < hsize; i++) {
           Sym *p;
           for (p = zzs_start_of_bucket(i); p; p = p->next) {
              if ( ( p->mandatory_code == yes || p->mandatory_code == implicit) && !p->true_name.rec ) {
                 zzs_scope( &(p->category.rec->mandatory_scope.rec) );
                 zzs_add_to_scope(p);
              }
           }
        }
        fprintf(stderr, "mandatory scopes added\n");

#endif /* 0 */

	/* Pass over every entry in symbol table to set item_list pointers
	With the current implementation, it is not necessary to
	do this with a Sorcerer tree parser. There is no other way of
	guaranteeing that something is only done once for each item, without
	assuming dictionary-writing conventions.

	Also set item_type of child items to that of their parents.
	*/

        fprintf(stderr, "Grouping items in their categories....");
        for (i=0; i < zzs_hsize(); i++) {
           Sym *p;
           for (p = zzs_start_of_bucket(i); p; p = p->next) {
	     if ( p != p->category.rec && ! p->true_name.rec ) {
	       if ( ! p->category.rec ) {
		 ccif_signal(DICT_NOCAT, "cifdic_to_symtab_main", NULL, NULL, 0, zzs_recname(p));
		 continue;
	       }
	       /* Insert into category's item list, if it has not already been done */
	       if ( ! p->item_list.rec ) {
		 p->item_list.rec = p->category.rec->item_list.rec;
		 p->category.rec->item_list.rec = p;
		 p->no_in_cat = p->category.rec->no_in_cat++;
	       }
	     }

	     set_item_type(p);

           }
        }
        fprintf(stderr, "done\n");

   
	zzs_noscope();
	zzs_rmscope(&save_frame_scope);

        fprintf(stderr, "Dumping symbol table....");
	zzs_dump (arg1v[symtab_arg], symdump);
	fflush(stdout);
	fprintf(stderr, "symbol table dumped.\n");


	return(EXIT_SUCCESS);
}




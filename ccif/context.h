/* Returned status values for setting up a new context: */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define BADCAT          -1 /* Category bad for some reason (e.g. contains undefined data names) */
#define CAT_NOT_PRESENT 0  /* Category not present in specified block.
                              If file has been opened for output, and disposition
			      of context is not read-only, then a context has
			      been assigned to this category and block */
#define LOOP_CONTEXT    1  /* Category occurs wholly or partly in a loop 
                              in specified block */
#define ITEM_CONTEXT    2  /* Category occurs as data item(s) in specified block */


/* Need definition of AST now */
#include "ast.h"

#ifndef NCONTEXTS
#define NCONTEXTS 20
#endif


typedef struct _context {
    Sym *category;
    int ncols,	/* This is a slight misnomer. It actually refers to the number of elements currently
		 dimensioned for context.packet. When a loop is changed, ccif_refresh_contexts
		 updates this item */
     block;
    ccif_rwstat rwstat;  /* Copy it here from ccif_data_blocks for convenient checking */
   
/* The following items are used for processing requests for loops */
   struct _packet {
      Sym *rec;         /* Pointer to symbol table record */
      AST *loop_item;
   } *packet;
   
   int *lookup;     /* Lookup array to translate between rec->no_in_cat and loop_offset. */

   int tree_index;    /* Index of tree associated with this context */
   Rb_node tree_node; /* Used for traversal where category has been sorted */

   int loop_add;    /* 0 => add new items as data items
		     * 1 => add new items to a loop, creating if necessary */
}  CONTEXT_LIST;


extern void ccif_free_context(int * const); 
extern int ccif_advance_context(const int i); 
extern int ccif_new_context(Sym *rec, int *status, const int block, const int);
extern CONTEXT_LIST ccif_context_list(const int ncontext);
extern void ccif_refresh_contexts(CONTEXT_LIST changed_context ); 
extern void ccif_point_context ( const int ncontext, const AST * r ,Rb_node rbnode);
extern void ccif_link_sort_to_context( const int index, const int ncontext, const int reset );
extern int ccif_sort_link_number(const int index );
void ccif_resort_context(const int sort_index, const AST * node, Rb_node rbnode);
extern void ccif_unlink_sort(const int index, const int reset );
#ifdef DEBUG
extern void ccif_print_row(char callback_msg[], void **ptr, int *count, int first);
#endif


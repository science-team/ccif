/* Condition data for CCIF. Generated automatically by condition.awk */

/* This file must typedef uint32 as a 32-bit unsigned integer value */
#include "uint32.h"

/* This file must define the macros __import__, __export__ and __noshare__ */
#include "import_export.h"

/* Length of message buffer for callback functions */
#define CALLBACK_LEN 512


/* Breakdown of condition value (not using control bits, at the moment */

/* Array of environment variables or control strings, to turn of messages 
    by severity */
static char *message_control_vars[] = {
   "CCIF_WARN",
   "CCIF_SUCCESS",
   "CCIF_ERROR",
   "CCIF_INFORM",
   "CCIF_SEVERE"
};

/* Severity bits */
#define CCIF_M_SEVERITY 0x07  /* Severity mask */
#define CCIF_K_ERROR 2
#define CCIF_K_SUCCESS 1
#define CCIF_K_INFORM 3
#define CCIF_K_SEVERE 4
#define CCIF_K_WARN 0

/* Message bits */
#define CCIF_V_MSG_NO 3   /* Position of message number */
#define CCIF_S_MSG_NO 13  /* Size of message number */
#define CCIF_M_MSG_NO ( 0x1fff << CCIF_V_MSG_NO ) /* Message number mask */

/* Facility bits */
#define CCIF_V_FAC_NO 16  /* Position of facility number */
#define CCIF_S_FAC_NO 12  /* Size of facility number */
#define CCIF_M_FAC_NO ( 0x0fff << CCIF_V_FAC_NO ) /* Facility number mask */

static char callback_msg[CALLBACK_LEN];



/* Condition code value definitions: */

/* Facility CCIF */
__export__ const uint32       CCIF_NOTNUM          = 000000000000;
__export__ const uint32       CCIF_ISINT           = 000000000010;
__export__ const uint32       CCIF_ISREAL          = 000000000020;
__export__ const uint32       CCIF_ISNUM           = 000000000030;
__export__ const uint32       CCIF_ISTEXT          = 000000000040;
__export__ const uint32       CCIF_MULTILINE       = 000000000050;
__export__ const uint32       CCIF_CHARISTEXT      = 000000000060;
__export__ const uint32       CCIF_NOTMLINE        = 000000000073;
__export__ const uint32       CCIF_NOTREAL         = 000000000104;
__export__ const uint32       CCIF_NOTREALESD      = 000000000114;
__export__ const uint32       CCIF_NOTINT          = 000000000124;
__export__ const uint32       CCIF_NOTCHAR         = 000000000134;
__export__ const uint32       CCIF_BADESD          = 000000000144;
__export__ const uint32       CCIF_NOLOGNAME       = 000000000154;
__export__ const uint32       CCIF_OPENING         = 000000000163;
__export__ const uint32       CCIF_OPENED          = 000000000173;
__export__ const uint32       CCIF_OPEN            = 000000000203;
__export__ const uint32       CCIF_DICTLOADED      = 000000000213;
__export__ const uint32       CCIF_NEWBLOCK        = 000000000223;
__export__ const uint32       CCIF_REINIT          = 000000000230;
__export__ const uint32       CCIF_NOITEMIP        = 000000000240;
__export__ const uint32       CCIF_NOITEM          = 000000000250;
__export__ const uint32       CCIF_UNKWNCMP        = 000000000262;
__export__ const uint32       CCIF_NOMEM           = 000000000274;
__export__ const uint32       CCIF_SORTEMPTYCAT    = 000000000302;
__export__ const uint32       CCIF_ADDSORTCAT      = 000000000312;
__export__ const uint32       CCIF_1LINESORT       = 000000000322;
__export__ const uint32       CCIF_NOKEYSORT       = 000000000334;
__export__ const uint32       CCIF_SORTED          = 000000000343;
__export__ const uint32       CCIF_BADADDSORT      = 000000000352;
__export__ const uint32       CCIF_NOFREESORT      = 000000000364;
__export__ const uint32       CCIF_BADINDEX        = 000000000374;
__export__ const uint32       CCIF_IDXRANGE        = 000000000404;
__export__ const uint32       CCIF_NOTROW          = 000000000414;
__export__ const uint32       CCIF_CXTRANGE        = 000000000424;
__export__ const uint32       CCIF_BADCONTEXT      = 000000000434;
__export__ const uint32       CCIF_WRONGSORTCXT    = 000000000442;
__export__ const uint32       CCIF_NOROWFIND       = 000000000454;
__export__ const uint32       CCIF_NOTLOOPED       = 000000000460;
__export__ const uint32       CCIF_NEEDROW         = 000000000474;
__export__ const uint32       CCIF_PRINTROW        = 000000000503;
__export__ const uint32       CCIF_BADRBNODE       = 000000000514;
__export__ const uint32       CCIF_LINEBAD         = 000000000524;
__export__ const uint32       CCIF_CIFNOTOPEN      = 000000000534;
__export__ const uint32       CCIF_SAMECXT         = 000000000544;
__export__ const uint32       CCIF_ROCXT           = 000000000554;
__export__ const uint32       CCIF_ROCXTADD        = 000000000564;
__export__ const uint32       CCIF_TONONLOOPED     = 000000000574;
__export__ const uint32       CCIF_DIFFCAT         = 000000000604;
__export__ const uint32       CCIF_FROMNONLOOPED   = 000000000614;
__export__ const uint32       CCIF_NONUNIQUEBLK    = 000000000624;
__export__ const uint32       CCIF_MAXCIFS         = 000000000634;
__export__ const uint32       CCIF_SAMEIO          = 000000000644;
__export__ const uint32       CCIF_VMSSAMEIO       = 000000000654;
__export__ const uint32       CCIF_UNXSAMEIO       = 000000000664;
__export__ const uint32       CCIF_READONLY        = 000000000674;
__export__ const uint32       CCIF_1LOOP           = 000000000704;
__export__ const uint32       CCIF_CXTEMPTY        = 000000000714;
__export__ const uint32       CCIF_NOTINLOOP       = 000000000724;
__export__ const uint32       CCIF_BADAST          = 000000000734;
__export__ const uint32       CCIF_NOAPPEND        = 000000000740;
__export__ const uint32       CCIF_BADGET          = 000000000754;
__export__ const uint32       CCIF_WRONGCXT        = 000000000764;
__export__ const uint32       CCIF_NOTINDICT       = 000000000774;
__export__ const uint32       CCIF_CATNOTCOMPLETE  = 000000001000;
__export__ const uint32       CCIF_FOPEN           = 000000001014;
__export__ const uint32       CCIF_NOSUCHFILE      = 000000001020;
__export__ const uint32       CCIF_BADLOOP         = 000000001034;
__export__ const uint32       CCIF_PARTLOOP        = 000000001044;
__export__ const uint32       CCIF_NOFREECONTEXTS  = 000000001054;
__export__ const uint32       CCIF_NULLSYMPTR      = 000000001064;
__export__ const uint32       CCIF_NOUSEMMAP       = 000000001074;
__export__ const uint32       CCIF_NOREC           = 000000001104;
__export__ const uint32       CCIF_BADCAT          = 000000001114;
__export__ const uint32       CCIF_NOKEY           = 000000001124;
__export__ const uint32       CCIF_DBNAMELEN       = 000000001130;
__export__ const uint32       CCIF_DNNOTUNIQ       = 000000001140;
__export__ const uint32       CCIF_STRTABVAL       = 000000001153;
__export__ const uint32       CCIF_STRTABLEN       = 000000001160;
__export__ const uint32       CCIF_BADFMT          = 000000001170;
__export__ const uint32       CCIF_BADFMTFLAGS     = 000000001200;
__export__ const uint32       CCIF_BADFMTCHAR      = 000000001210;
__export__ const uint32       CCIF_BADESDCHAR      = 000000001220;
__export__ const uint32       CCIF_VALTOOLONG      = 000000001232;
__export__ const uint32       CCIF_LINETOOLONG     = 000000001240;
__export__ const uint32       CCIF_SEMI            = 000000001250;
__export__ const uint32       CCIF_BADBLOCKNAME    = 000000001262;
__export__ const uint32       CCIF_BADOUTPUT       = 000000001270;
__export__ const uint32       CCIF_MAPPEDFILES     = 000000001300;
__export__ const uint32       CCIF_SYSERR          = 000000001314;

/* Facility DICT */
__export__ const uint32       DICT_BADDEF          = 000000200004;
__export__ const uint32       DICT_ENTRYMISS       = 000000200014;
__export__ const uint32       DICT_NOMANDITEM      = 000000200024;
__export__ const uint32       DICT_NOMANDCAT       = 000000200034;
__export__ const uint32       DICT_NOESDREGEXP     = 000000200044;
__export__ const uint32       DICT_BADESDEXP       = 000000200054;
__export__ const uint32       DICT_NSUBEXP         = 000000200064;
__export__ const uint32       DICT_NOSEQ           = 000000200070;
__export__ const uint32       DICT_NONUMEXP        = 000000200100;
__export__ const uint32       DICT_NOCHAREXP       = 000000200110;
__export__ const uint32       DICT_BADKEYCAT       = 000000200124;
__export__ const uint32       DICT_CREATEREC       = 000000200130;
__export__ const uint32       DICT_UPDATE          = 000000200143;
__export__ const uint32       DICT_UPDATEP         = 000000200153;
__export__ const uint32       DICT_ALIASUPDATE     = 000000200163;
__export__ const uint32       DICT_IMPUPDATE       = 000000200173;
__export__ const uint32       DICT_NOTYPE          = 000000200200;
__export__ const uint32       DICT_NODEFTYPE       = 000000200210;
__export__ const uint32       DICT_NODEFBTYPE      = 000000200220;
__export__ const uint32       DICT_DIFFBTYPE       = 000000200230;
__export__ const uint32       DICT_CANTADD         = 000000200244;
__export__ const uint32       DICT_SAVEFRAME       = 000000200253;
__export__ const uint32       DICT_NOCAT           = 000000200260;

/* Facility ANTLR */
__export__ const uint32      ANTLR_BADLOOP         = 000000400000;
__export__ const uint32      ANTLR_BADPCKT         = 000000400010;
__export__ const uint32      ANTLR_MISTOK          = 000000400020;
__export__ const uint32      ANTLR_NVALT           = 000000400030;
__export__ const uint32      ANTLR_BADDATA         = 000000400044;
__export__ const uint32      ANTLR_UNKPSIG         = 000000400050;
__export__ const uint32      ANTLR_BADITEM         = 000000400060;
__export__ const uint32      ANTLR_BADSTR          = 000000400070;
__export__ const uint32      ANTLR_EMPTYLOOP       = 000000400100;
__export__ const uint32      ANTLR_GLOBAL          = 000000400114;
__export__ const uint32      ANTLR_SAVE            = 000000400124;
__export__ const uint32      ANTLR_NULLAST         = 000000400134;
__export__ const uint32      ANTLR_STKOVFLOW       = 000000400144;

/* Facility DLG */
__export__ const uint32        DLG_LEXERR          = 000000600002;
__export__ const uint32        DLG_NOLASTTOK       = 000000600014;

/* Facility SOR */
__export__ const uint32        SOR_NOTINRANGE      = 000001000002;
__export__ const uint32        SOR_NULL            = 000001000012;
__export__ const uint32        SOR_NOTWILD         = 000001000022;
__export__ const uint32        SOR_WRONGTOK        = 000001000032;
__export__ const uint32        SOR_NULL2           = 000001000042;
__export__ const uint32        SOR_NOVIALTNULL     = 000001000052;
__export__ const uint32        SOR_NOVIALT         = 000001000062;
__export__ const uint32        SOR_PANIC           = 000001000074;

/* Facility REDBLACK */
__export__ const uint32   REDBLACK_NOSORTORDER     = 000001200004;
__export__ const uint32   REDBLACK_NONHEAD         = 000001200014;
__export__ const uint32   REDBLACK_NODELINT        = 000001200024;
__export__ const uint32   REDBLACK_NODELHEAD       = 000001200034;
__export__ const uint32   REDBLACK_SIBBLACK        = 000001200044;
__export__ const uint32   REDBLACK_SIBNOTINT       = 000001200054;
__export__ const uint32   REDBLACK_23c             = 000001200064;
__export__ const uint32   REDBLACK_NONEXT          = 000001200074;

/* Facility ASTLIB */
__export__ const uint32     ASTLIB_NOMEM           = 000001400004;

/* Facility RX */
__export__ const uint32         RX_REGEXECERR      = 000001600004;
__export__ const uint32         RX_REGCOMPERR      = 000001600014;
__export__ const uint32         RX_REGCOMPERR2     = 000001600024;

/* Facility VMS */
__export__ const uint32        VMS_SYSMSG          = 000002000004;
__export__ const uint32        VMS_CRTL            = 000002000014;
__export__ const uint32        VMS_TRNLNM          = 000002000024;

/* Facility RMS */
__export__ const uint32        RMS_NOTSEQ          = 000002200004;
__export__ const uint32        RMS_NOCRINFO        = 000002200014;
__export__ const uint32        RMS_RECTOOLONG      = 000002200024;
__export__ const uint32        RMS_NORECTYPE       = 000002200034;

/* Facility EXIT */
__export__ const uint32       EXIT_FAIL            = 000002400004;

/* Messages, grouped by facility: */

static MESSAGE  CCIF_msgs[90] = {
   {
      "NOTNUM",
      "%s is not a numeric type. You may get a junk value!",
     -1
   },
   {
      "ISINT",
      "%s is an integer type. I hope that you know what you are doing!",
     -1
   },
   {
      "ISREAL",
      "%s is a real numeric type. I hope that you know what you are doing!",
     -1
   },
   {
      "ISNUM",
      "%s is a numeric type - it will be returned as a string!",
     -1
   },
   {
      "ISTEXT",
      "%s is a text type. You will get a maximum of %d characters!",
     -1
   },
   {
      "MULTILINE",
      "The value of %s is multi-line text. You will only get the first line!",
     -1
   },
   {
      "CHARISTEXT",
      "The value of %s is multi-line text. This is forbidden by the mmCIF dictionary!",
     -1
   },
   {
      "NOTMLINE",
      "%s is not an item which may span lines. You may make your program simpler\nby using one of the other ccif_get* functions....",
     -1
   },
   {
      "NOTREAL",
      "%s is not a numeric real data type!",
     -1
   },
   {
      "NOTREALESD",
      "%s is not a numeric real+esd data type!",
     -1
   },
   {
      "NOTINT",
      "%s is not a numeric integer data type!",
     -1
   },
   {
      "NOTCHAR",
      "%s is not a text or char data type!",
     -1
   },
   {
      "BADESD",
      "Cannot output esd as associated data item for %s!\nPlease check your program logic against the CIF dictionary entry for this item!",
     -1
   },
   {
      "NOLOGNAME",
      "No translation for logical name \"%s\"!",
     -1
   },
   {
      "OPENING",
      "\n\n --- Opening CIF ---\nLogical name: \"%s\", full name \"%s\"",
     -1
   },
   {
      "OPENED",
      " --- CIF opened %s ---",
     -1
   },
   {
      "OPEN",
      " --- CIF Logical name: \"%s\", full name \"%s\" already open ---\n(     Equivalent to logical name \"%s\")",
     -1
   },
   {
      "DICTLOADED",
      "\n --- CIF Dictionary loaded ---\nLogical name: \"%s\", full name \"%s\"",
     -1
   },
   {
      "NEWBLOCK",
      "\nNew data block \"%s\" created for logical unit \"%s\"",
     -1
   },
   {
      "REINIT",
      "ccif_init called more than once",
     -1
   },
   {
      "NOITEMIP",
      "Line %d:\tdata name \"%s\" not present in dictionary",
     -1
   },
   {
      "NOITEM",
      "Data name or category \"%s\" not defined in dictionary",
     -1
   },
   {
      "UNKWNCMP",
      "Don't recognize comparison type \"%c\" for data item \"%s\"",
     -1
   },
   {
      "NOMEM",
      "Out of memory",
     -1
   },
   {
      "SORTEMPTYCAT",
      "Cannot specify \"%s\" for sorting in block \"%s\": no looped data in category.\nCheck the returned status value from ccif_init_sort",
     -1
   },
   {
      "ADDSORTCAT",
      "ccif_add_to_sort called with \"%s\", which is the name of a category",
     -1
   },
   {
      "1LINESORT",
      "Data item \"%s\" is allowed to have multi-line text, so cannot use for sorting.\nIngoring.",
     -1
   },
   {
      "NOKEYSORT",
      "Serious problem with trying to sort category \"%s\". Giving up",
     -1
   },
   {
      "SORTED",
      "\n --- Category \"%s\" in block \"%s\" of logical file \"%s\" ---\n --- has been sorted on the following items: ---",
     -1
   },
   {
      "BADADDSORT",
      "Cannot add item \"%s\" to sort list for category \"%s\" - ignoring",
     -1
   },
   {
      "NOFREESORT",
      "No free sorting structures.\nUse \"ccif_free_sort\", or compile with a higher value of NSORT_TREES",
     -1
   },
   {
      "BADINDEX",
      "Sort number %d has not been initialised!",
     -1
   },
   {
      "IDXRANGE",
      "Sort index %d out of range (must be between 0 and %d inclusive)",
     -1
   },
   {
      "NOTROW",
      "Key supplied for searching tree %d is not a pointer to a Row node",
     -1
   },
   {
      "CXTRANGE",
      "Context %d out of range (must be between 0 and %d inclusive)",
     -1
   },
   {
      "BADCONTEXT",
      "Context %d has not been initialised!",
     -1
   },
   {
      "WRONGSORTCXT",
      "Not associating category with sort:\n\tContext %d refers to category \"%s\" in block %d\n\tSort %d refers to category \"%s\" in block %d!",
     -1
   },
   {
      "NOROWFIND",
      "Serious error with trying to find row in sort tree",
     -1
   },
   {
      "NOTLOOPED",
      "Category \"%s\" in block %d has no looped data",
     -1
   },
   {
      "NEEDROW",
      "Must be called with a pointer to a row node!",
     -1
   },
   {
      "PRINTROW",
      "Search completed.",
     -1
   },
   {
      "BADRBNODE",
      "Node of red-black sort tree does not point at specified loop packet!",
     -1
   },
   {
      "LINEBAD",
      "Multi-line text not allowed in this context!",
     -1
   },
   {
      "CIFNOTOPEN",
      "\"%s\" not open!",
     -1
   },
   {
      "SAMECXT",
      "Input and output context are identical!",
     -1
   },
   {
      "ROCXT",
      "Context number %d is read-only!",
     -1
   },
   {
      "ROCXTADD",
      "Context on file %s has not been opened for writing!\nCannot add data item %s.",
     -1
   },
   {
      "TONONLOOPED",
      "Cannot copy a row to non-looped data!",
     -1
   },
   {
      "DIFFCAT",
      "Cannot copy row between different categories!",
     -1
   },
   {
      "FROMNONLOOPED",
      "Cannot copy row from non-looped data (yet)!",
     -1
   },
   {
      "NONUNIQUEBLK",
      "File %s (line %d)\n:more than one data block called %s!",
     -1
   },
   {
      "MAXCIFS",
      "Attempted to open too many CIF's!",
     -1
   },
   {
      "SAMEIO",
      "Attempted to open %s for both input and output!",
     -1
   },
   {
      "VMSSAMEIO",
      "Attempted to use %s for both input and output with CCP4_OPEN set to UNKNOWN!",
     -1
   },
   {
      "UNXSAMEIO",
      "Error opening %s\nCannot use the same filename for input and output under Unix!",
     -1
   },
   {
      "READONLY",
      "Logical file %s has been opened read-only!",
     -1
   },
   {
      "1LOOP",
      "Line %d: a category may not be defined in more than one loop!",
     -1
   },
   {
      "CXTEMPTY",
      "Attempted to retrieve value using empty context!",
     -1
   },
   {
      "NOTINLOOP",
      "Requsted item %s is not in loop pointed to by context %d!",
     -1
   },
   {
      "BADAST",
      "Problem with abstract syntax tree: token at line %d is not a Data_item or Loop!",
     -1
   },
   {
      "NOAPPEND",
      "Will not try to append row. Are you calling one of the CCIF_GET_* routines with\na status of APPEND_ROW?",
     -1
   },
   {
      "BADGET",
      "name and &item are both NULL!",
     -1
   },
   {
      "WRONGCXT",
      "Context %d points at category %s; cannot retrieve value of %s!",
     -1
   },
   {
      "NOTINDICT",
      "No dictionary entry for %s!",
     -1
   },
   {
      "CATNOTCOMPLETE",
      "Could not open context on category '%s'; item '%s' not defined in dictionary!",
     -1
   },
   {
      "FOPEN",
      "Cannot open file %s for %s!",
     -1
   },
   {
      "NOSUCHFILE",
      "Cannot open logical file \"%s\" - file \"%s\" does not exist!",
     -1
   },
   {
      "BADLOOP",
      "Line %d: Loop contains items from more than one category!",
     -1
   },
   {
      "PARTLOOP",
      "Line %d: Attempt to process loop with incomplete loop packet",
     -1
   },
   {
      "NOFREECONTEXTS",
      "No free contexts available.\n If your program needs more, increase NCONTEXTS in value_manip.h, and rebuild libccif.\nOtherwise, check that your program frees unexhausted contexts.\n",
     -1
   },
   {
      "NULLSYMPTR",
      "Null symbol table entry pointer!",
     -1
   },
   {
      "NOUSEMMAP",
      "zzs_undump called with use_mmap != 0, but code compiled with _USE_MMAP == 0!",
     -1
   },
   {
      "NOREC",
      "Cannot find symbol table record for item \"%s\"!\n(Probably not defined in dictionary)",
     -1
   },
   {
      "BADCAT",
      "Something has gone horribly wrong with category %s!",
     -1
   },
   {
      "NOKEY",
      "No _category_key.id items defined for category \"%s\"\nCannot convert data items to loop!",
     -1
   },
   {
      "DBNAMELEN",
      "Datablock name\n%s\nis too long for a single line of a CIF.\nTruncating to %d characters!",
     -1
   },
   {
      "DNNOTUNIQ",
      "Line %d: Data name %s (or its alias(es)) occur more than \nonce in the data file (or dictionary save frame)!\nExtra occurrences ignored",
     -1
   },
   {
      "STRTABVAL",
      "String table contains \"%s\" at offset %d",
     -1
   },
   {
      "STRTABLEN",
      "Attempt to print value off the end of string table! (offset was %d)",
     -1
   },
   {
      "BADFMT",
      "Cannot sprintf a sample value of \"%s\" using \"%s\"!\nUsing default format",
     -1
   },
   {
      "BADFMTFLAGS",
      "Invalid flags specified for output format of \"%s\"!\nAllowed characters are: \"%s\"",
     -1
   },
   {
      "BADFMTCHAR",
      "Invalid format conversion character given for \"%s\"!\nUse one of \"idfeEgG\" apropriate to type of data item",
     -1
   },
   {
      "BADESDCHAR",
      "Spurious characters in value \"%s\" (line %d)!\nReturning (null) ",
     -1
   },
   {
      "VALTOOLONG",
      "Supplied value for \"%s\" is greater than maximum length of lines in CIF's.\nTruncating to %d characters:\n%s",
     -1
   },
   {
      "LINETOOLONG",
      "Line limit %d invalid: must be between 1 and %d!",
     -1
   },
   {
      "SEMI",
      "Premature semi-colon in supplied text. Truncating",
     -1
   },
   {
      "BADBLOCKNAME",
      "\"%s\" is an invalid data block heading! Returning block_not_found!",
     -1
   },
   {
      "BADOUTPUT",
      "CCIF was requested to write output to file \"%s\" which might have generated an illegal STAR file.\nPlease examine the comments which have been written to the file.",
     -1
   },
   {
      "MAPPEDFILES",
      "Mapped file houskeeping problem with %s - fewer than 0 nodes. Soldiering on...",
     -1
   },
   {
      "SYSERR",
      "System error: %s\nSystem message was:\n%s",
     -1
   }
};

static MESSAGE  DICT_msgs[23] = {
   {
      "BADDEF",
      "Save frame %s appears to be garbled!",
     -1
   },
   {
      "ENTRYMISS",
      "Lost track of entry for %s!",
     -1
   },
   {
      "NOMANDITEM",
      "Mandatory item %s not specified in %s!",
     -1
   },
   {
      "NOMANDCAT",
      "Mandatory category %s not specified in %s!",
     -1
   },
   {
      "NOESDREGEXP",
      "_item_type_conditions.code of 'esd' for item %s requires an applicable _item_type_list.construct!",
     -1
   },
   {
      "BADESDEXP",
      "_item_type_list.construct for %s does not seem to allow an appended esd,\nbut the definition of %s requires one!",
     -1
   },
   {
      "NSUBEXP",
      "You seem to have a very complicated construct for type %s!\nTry increasing the value of NMATCH (ccif_defines.h) and re-building",
     -1
   },
   {
      "NOSEQ",
      "%s: _item_type_conditions_code 'seq' not implemented (yet)!",
     -1
   },
   {
      "NONUMEXP",
      "No construct given for numeric type %s - assuming real.\nPlease consider supplying a regular expression in _item_type_list.construct for this type",
     -1
   },
   {
      "NOCHAREXP",
      "No construct given for non-numeric type %s - assuming multi-line text.\nPlease consider supplying a regular expression in _item_type_list.construct for this type",
     -1
   },
   {
      "BADKEYCAT",
      "Cannot add %s to key items list!\n(_category_key.id is %s, but it should be the name of a category).",
     -1
   },
   {
      "CREATEREC",
      "Creating record for %s!",
     -1
   },
   {
      "UPDATE",
      "%s already defined for %s!\nUpdating value at line %d of dictionary",
     -1
   },
   {
      "UPDATEP",
      "%s already defined for %s!\nUsing value of parent item",
     -1
   },
   {
      "ALIASUPDATE",
      "Alias \"%s\" already defined! Updating value at line %d of dictionary",
     -1
   },
   {
      "IMPUPDATE",
      "%s already defined for %s!\nUpdating value implicitly from line %d of dictionary",
     -1
   },
   {
      "NOTYPE",
      "Cannot find _item_type_list.code \"%s\" for %s!",
     -1
   },
   {
      "NODEFTYPE",
      "Not storing default value of %s for %s\n(_item_type.code unknown or not specified)",
     -1
   },
   {
      "NODEFBTYPE",
      "Not storing default value of %s for %s\n(don't know basic_type of item)",
     -1
   },
   {
      "DIFFBTYPE",
      "Lost default value for %s (changed type of item)",
     -1
   },
   {
      "CANTADD",
      "Line %s: Cannot add \"%s\" to symbol table!",
     -1
   },
   {
      "SAVEFRAME",
      "Processing \"%s\" (%s pass)",
     -1
   },
   {
      "NOCAT",
      "Category pointer of \"%s\" not set. Ignoring this data item",
     -1
   }
};

static MESSAGE  ANTLR_msgs[13] = {
   {
      "BADLOOP",
      "Syntax error at line %d:\tbadly formed loop",
     -1
   },
   {
      "BADPCKT",
      "Line %d: %d names and %d values in loop",
     -1
   },
   {
      "MISTOK",
      "Syntax error at line %d:\tmis-matched token",
     -1
   },
   {
      "NVALT",
      "Syntax error at line %d:\tno viable alternative",
     -1
   },
   {
      "BADDATA",
      "Syntax error near line %d:\texpecting looped data or a <data-name> - <data-value> pair",
     -1
   },
   {
      "UNKPSIG",
      "Unknown parser exception raised at line %d",
     -1
   },
   {
      "BADITEM",
      "Syntax error at line %d:\tdata name \"%s\" must be followed by a data value!",
     -1
   },
   {
      "BADSTR",
      "Probable syntax error near line %d:\tcannot match semi-colon bounded text string",
     -1
   },
   {
      "EMPTYLOOP",
      "Syntax error at line %d:\tLoops with no data not allowed!",
     -1
   },
   {
      "GLOBAL",
      "Syntax error at line %d:\tGlobal blocks not allowed in CIF's!",
     -1
   },
   {
      "SAVE",
      "Syntax error at line %d:\tSave frames not allowed in CIF's!",
     -1
   },
   {
      "NULLAST",
      "Cannot make an AST node with a null pointer!",
     -1
   },
   {
      "STKOVFLOW",
      "fatal: attrib/AST stack overflow %s(%d)!",
     -1
   }
};

static MESSAGE  DLG_msgs[2] = {
   {
      "LEXERR",
      "%s near line %d (text was '%s')",
     -1
   },
   {
      "NOLASTTOK",
      "Line %d: Cannot get last token in file!",
     -1
   }
};

static MESSAGE  SOR_msgs[8] = {
   {
      "NOTINRANGE",
      "parse error: expected token range %d..%d found token %d",
     -1
   },
   {
      "NULL",
      "parse error: expected token range %d..%d found NULL tree",
     -1
   },
   {
      "NOTWILD",
      "parse error: expected any token/tree found found NULL tree",
     -1
   },
   {
      "WRONGTOK",
      "parse error: expected token %d found token %d",
     -1
   },
   {
      "NULL2",
      "parse error: expected token %d found NULL tree",
     -1
   },
   {
      "NOVIALTNULL",
      "parse error: in rule %s, no viable alternative for NULL tree",
     -1
   },
   {
      "NOVIALT",
      "parse error: in rule %s, no viable alternative for tree",
     -1
   },
   {
      "PANIC",
      "Message was:\n%s",
     -1
   }
};

static MESSAGE  REDBLACK_msgs[8] = {
   {
      "NOSORTORDER",
      "No sort order available for comparing rows!\nCategory was: \"%s\"",
     -1
   },
   {
      "NONHEAD",
      "Function called on non-head node: 0x%x",
     -1
   },
   {
      "NODELINT",
      "Cannot delete an internal node: 0x%x",
     -1
   },
   {
      "NODELHEAD",
      "Cannot delete the head of an rb_tree: 0x%x",
     -1
   },
   {
      "SIBBLACK",
      "Internal deletion problem - sibling is black",
     -1
   },
   {
      "SIBNOTINT",
      "Internal deletion problem - sibling not an internal node",
     -1
   },
   {
      "23c",
      "Internal deletion problem - 2.3c incorrect",
     -1
   },
   {
      "NONEXT",
      "Function called on non-external node 0x%x",
     -1
   }
};

static MESSAGE  ASTLIB_msgs[1] = {
   {
      "NOMEM",
      "Out of memory",
     -1
   }
};

static MESSAGE  RX_msgs[3] = {
   {
      "REGEXECERR",
      "regexec error: %s",
     -1
   },
   {
      "REGCOMPERR",
      "regcomp error number %d: %s\nOffending construct is: \"%s\"",
     -1
   },
   {
      "REGCOMPERR2",
      "regcomp error: %s\nSuggests that your string table is corrupt!",
     -1
   }
};

static MESSAGE  VMS_msgs[3] = {
   {
      "SYSMSG",
      "VMS system error at: File \"%s\" line %d\nMessage was:\n%s",
     -1
   },
   {
      "CRTL",
      "VMS system error (reported by C run-time library): %s\nMessage was:\n%s",
     -1
   },
   {
      "TRNLNM",
      "Serious problem with $TRNLNM!",
     -1
   }
};

static MESSAGE  RMS_msgs[4] = {
   {
      "NOTSEQ",
      "%s is an indexed or relative file. I will only process sequential files!",
     -1
   },
   {
      "NOCRINFO",
      "File \"%s\": \nI cannot process variable-length records without carriage control information!",
     -1
   },
   {
      "RECTOOLONG",
      "Some records in file \2%s\" are too long to process!",
     -1
   },
   {
      "NORECTYPE",
      "Don't recognise record type of file \"%s\"!",
     -1
   }
};

static MESSAGE  EXIT_msgs[1] = {
   {
      "FAIL",
      "Exit called (unsuccessful termination) at line %d of file %s",
     -1
   }
};

/* Facilities, pointing to the message lists */
static FACILITY CCIF_facs[11] = {
   {"CCIF", CCIF_msgs, 90},
   {"DICT", DICT_msgs, 23},
   {"ANTLR", ANTLR_msgs, 13},
   {"DLG", DLG_msgs, 2},
   {"SOR", SOR_msgs, 8},
   {"REDBLACK", REDBLACK_msgs, 8},
   {"ASTLIB", ASTLIB_msgs, 1},
   {"RX", RX_msgs, 3},
   {"VMS", VMS_msgs, 3},
   {"RMS", RMS_msgs, 4},
   {"EXIT", EXIT_msgs, 1}
};

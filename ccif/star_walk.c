/*
 * S O R C E R E R  T r a n s l a t i o n  H e a d e r
 *
 * SORCERER Developed by Terence Parr, Aaron Sawdey, & Gary Funck
 * Parr Research Corporation, Intrepid Technology, University of Minnesota
 * 1992-1994
 * SORCERER Version 13322
 */
#define SORCERER_VERSION	13322
#define SORCERER_NONTRANSFORM
#include "pcctscfg.h"
#include <stdio.h>
#include <setjmp.h>
/* rename error routines; used in macros, must use /lib/cpp */
#define mismatched_token ccif_sor_mismatched_token
#define mismatched_range ccif_sor_mismatched_range
#define missing_wildcard ccif_sor_missing_wildcard
#define no_viable_alt ccif_sor_no_viable_alt
#define sorcerer_panic ccif_sor_sorcerer_panic


/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#define tokens_h   /* Inhibit declaration of tokens from tokens.h */
#include "stdpccts.h"

#include "sorcerer.h"
#include "tokdefs.h"
extern void ccif_sor_data(STreeParser *_parser, SORAST **_root,  const int current_block  );
extern void ccif_sor_star_file(STreeParser *_parser, SORAST **_root,  const int start_block  );

void
#ifdef __USE_PROTOS
_refvar_inits(STreeParser *p)
#else
_refvar_inits(p)
STreeParser *p;
#endif
{
}



/* We are not using Sorcerer in transform mode, but would still like to have
the AST support functions available */
#include "sorcerer.c"  
#include "astlib.c"

#include "s_errsupport.c"

__import__ int ccif_number_of_blocks;

static int print_first_value = 0;


void ccif_sor_data(STreeParser *_parser, SORAST **_root,  const int current_block  )
{
	SORAST *_t = *_root;
	SORAST *dn=NULL;
	SORAST *dv=NULL;
	SORAST *l=NULL;
	SORAST *dnl=NULL;
	SORAST *r=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Data_item) ) {
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Data_item);
		dn=(SORAST *)_t;
		_DOWN;
		_MATCHRANGE(Non_quoted_text_string,Line_of_text);
		dv=(SORAST *)_t;
		_RIGHT;
		if ( !_parser->guessing ) {
		ccif_add_value(dn, dn, 0, current_block, 1);   
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
	_GUESS;
	if ( !_gv && _t!=NULL && (_t->token==Loop) ) {
		int col=0, rownum = 0;  
		{_SAVE; TREE_CONSTR_PTRS;
		if ( _t!=NULL && (_t->token==Loop) ) {
			{_SAVE; TREE_CONSTR_PTRS;
			_MATCH(Loop);
			_DOWN;
			_WILDCARD;
			_RESTORE;
			}
			_RIGHT;
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "data", _t);
		}
		_RESTORE;
		}
		_GUESS_DONE;
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Loop);
		l=(SORAST *)_t;
		_DOWN;
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Row);
		_DOWN;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Data_name) ) {
				_MATCH(Data_name);
				dnl=(SORAST *)_t;
				_RIGHT;
				if ( !_parser->guessing ) {
				ccif_add_value(l, dnl, col++, current_block, 1);   
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
		{int _done=0;
		do {
			if ( _t!=NULL && (_t->token==Row) ) {
				{_SAVE; TREE_CONSTR_PTRS;
				_MATCH(Row);
				r=(SORAST *)_t;
				_DOWN;
				if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
				_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
					{int _done=0;
					do {
						if ( _t!=NULL && (_t->token==Non_quoted_text_string||_t->token==Single_quoted_text_string||
						_t->token==Double_quoted_text_string||_t->token==Line_of_text) ) {
							_MATCHRANGE(Non_quoted_text_string,Line_of_text);
							_RIGHT;
						}
						else {
						if ( _t==NULL ) {
							_done = 1;
						}
						else {
							if ( _parser->guessing ) _GUESS_FAIL;
							no_viable_alt(_parser, "data", _t);
						}
						}
					} while ( !_done );
					}
				}
				else {
					if ( _parser->guessing ) _GUESS_FAIL;
					no_viable_alt(_parser, "data", _t);
				}
				_RESTORE;
				}
				_RIGHT;
				if ( !_parser->guessing ) {
				if ( print_first_value ) { printf( "Packet %d", ++rownum);
					if ( r->down->a->line > 0 ) printf( " (line %d)",r->down->a->line);
					printf( ": %s\n", r->down->a->text); 
				}
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "data", _t);
			}
			}
		} while ( !_done );
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
	if ( _parser->guessing ) _GUESS_DONE;
	if ( _t!=NULL && (_t->token==Loop) ) {
		_MATCH(Loop);
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "data", _t);
	}
	}
	}
	*_root = _t;
}

void ccif_sor_star_file(STreeParser *_parser, SORAST **_root,  const int start_block  )
{
	SORAST *_t = *_root;
	SORAST *sf=NULL;
	SORAST *dh=NULL;
	_GUESS_BLOCK;
	if ( _t!=NULL && (_t->token==Star_file) ) {
		int current_block = start_block; 
		print_first_value = getenv("CCIF_PFV") ? 1 : 0;   
		{_SAVE; TREE_CONSTR_PTRS;
		_MATCH(Star_file);
		sf=(SORAST *)_t;
		_DOWN;
		if ( _t!=NULL && (_t->token==Comment) ) {
			_MATCH(Comment);
			_RIGHT;
		}
		else {
		if ( _t==NULL || (_t->token==Data_heading) ) {
		}
		else {
			if ( _parser->guessing ) _GUESS_FAIL;
			no_viable_alt(_parser, "star_file", _t);
		}
		}
		{int _done=0;
		while ( !_done ) {
			if ( _t!=NULL && (_t->token==Data_heading) ) {
				{_SAVE; TREE_CONSTR_PTRS;
				_MATCH(Data_heading);
				dh=(SORAST *)_t;
				_DOWN;
				{int _done=0;
				do {
					if ( _t!=NULL && (_t->token==Loop||_t->token==Data_item) ) {
						ccif_sor_data(_parser, &_t,  current_block );
					}
					else {
					if ( _t==NULL ) {
						_done = 1;
					}
					else {
						if ( _parser->guessing ) _GUESS_FAIL;
						no_viable_alt(_parser, "star_file", _t);
					}
					}
				} while ( !_done );
				}
				_RESTORE;
				}
				_RIGHT;
				if ( !_parser->guessing ) {
				current_block++;   
				}
			}
			else {
			if ( _t==NULL ) {
				_done = 1;
			}
			else {
				if ( _parser->guessing ) _GUESS_FAIL;
				no_viable_alt(_parser, "star_file", _t);
			}
			}
		}
		}
		_RESTORE;
		}
		_RIGHT;
	}
	else {
		if ( _parser->guessing ) _GUESS_FAIL;
		no_viable_alt(_parser, "star_file", _t);
	}
	*_root = _t;
}

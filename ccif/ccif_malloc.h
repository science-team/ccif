/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* Bombproof memory allocation without extra indirection.
 * msg should be a pointer to a string, giving the name of the
 * invoking routine or rule */


#define CCIF_MALLOC(nelem, type, ptr, msg, line) { \
      ptr = (type *) malloc((nelem) * sizeof (type)); \
      if (!ptr) \
         ccif_signal(CCIF_NOMEM, msg, NULL, NULL, 0); \
}

#define CCIF_CALLOC(nelem, type, ptr, msg, line) { \
      ptr = (type *) calloc((nelem), sizeof (type)); \
      if (!ptr) \
         ccif_signal(CCIF_NOMEM, msg, NULL, NULL, 0); \
}

#ifdef OLD_REALLOC
/* Some older implementations of realloc behave anomalously, when given a null pointer.
 * Modern environments treat
 * 
 *    char * ptr=NULL;
 *    ptr = (char *) realloc(ptr, size);
 * 
 * equivalently to
 * 
 *    ptr = (char *) malloc(size);
 * 
 */
#define CCIF_REALLOC(nelem, type, ptr, msg, line ) { \
      if ( ! ptr ) { \
         ptr = (type *) malloc((nelem) * sizeof (type)); } \
      else { \
         ptr = (type *) realloc(ptr, (nelem) * sizeof (type)); } \
      if (!ptr) \
         ccif_signal(CCIF_NOMEM, msg, NULL, NULL, 0); \
}
#else
#define CCIF_REALLOC(nelem, type, ptr, msg, line) { \
      ptr = (type *) realloc(ptr, (nelem) * sizeof (type)); \
      if (!ptr) \
         ccif_signal(CCIF_NOMEM, msg, NULL, NULL, 0); \
}
#endif /* OLD_REALLOC */

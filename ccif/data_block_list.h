/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* This typedef sets up the data structure which is used to map a logical name/data block name
* pair to a block id. This cannot be put easily into stdpccts.h since it needs the defintion
* of AST, which is only set up at the end of the #header section.
*/

#ifdef VMS
/* typedef unsigned short ino_t; */
#endif


typedef struct _data_block_list {
   int nblocks;
   int nfiles;        /* Number of CIF_FILE_LIST entries which are pointing at this structure. */
   int *block_id;     /* Closing and re-using block id's may mean that they */
   AST **block_node;  /* become non-contiguous */
   AST *root;
   int *audit;        /* Keep a flag for each data block, to know if the AUDIT record has been changed yet.
		       * Fixme! This is not implemented yet. */
   Attrib err;        /* Pointer to first attribute in file which contains text which is not a valid
		       * STAR token */
   int printed;       /* Avoid printing out a file multiple times */
} DATA_BLOCK_LIST;
   

typedef struct _cif_file_list {
#ifdef VMS
   int decnet;     /* Save a bit of faffing around */
   struct NAM namblock;
   char filename[NAM$C_MAXRSS+1]; /* point data_block_list[n].namblock.nam$l_rsa at this, and assign
                                     NAM$C_MAXRSS to data_block_list.namblock.nam$b_rss */
   char name[NAM$C_MAXRSS+1];     /* Same for data_block_list[n].namblock.nam$l_esa. This contains
                                     unconcealed password in DECnet names */
#else
   ino_t st_ino;
   dev_t st_dev;
   char * filename;
#endif
  char *logname;

  struct _data_block_list * data_blocks;
  ccif_rwstat status; /* CCP4 makes a rigid distinction between input files and output files.
			       * Keep read/write status here */
  int line_limit;     /* Allow certain types of file to break the 80 character/line limit */
} CIF_FILE_LIST;


extern CIF_FILE_LIST ccif_file_list_entry(const int unit);
extern void ccif_add_blocks_to_lookup(const CIF_FILE_LIST * const file_list_entry);


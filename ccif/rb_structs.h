/* $Id$ */
#ifndef RB_STRUCTS_H
#define RB_STRUCTS_H

/* Separate out structure typedefs for rb.c and list.c */

typedef struct rb_list {
  struct rb_list *flink;
  struct rb_list *blink;
} *rb_List;


typedef struct {
  unsigned red : 1 ;
  unsigned internal : 1 ;
  unsigned left : 1 ;
  unsigned root : 1 ;
  unsigned head : 1 ;
} rb_status;

typedef struct rb_node {
  union {
    struct {
      struct rb_node *flink;
      struct rb_node *blink;
    }  list;
    struct rb_list
       rblist;	      /* Use this member to pass into list library */
    struct {
      struct rb_node *left;
      struct rb_node *right;
    } child;
  } c;
  union {
    struct rb_node *parent;
    struct rb_node *root;
  } p;
  rb_status s;
  union {
    int ikey;
    char *key;
    struct rb_node *lext;
  } k;
  union {
    char *val;
    struct rb_node *rext;
  } v;
} *Rb_node;

#endif /* RB_STRUCTS_H */

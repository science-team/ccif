                                                                      /* -*-C-*- */
/* $Id$ */
/*
 * Simple symbol table manager using coalesced chaining to resolve collisions
 *
 * Doubly-linked lists are used for fast removal of entries.
 *
 * 'sym.h' must have a definition for typedef "Sym".  Sym must include at
 * minimum the following fields:
 *
 *		...
 *		size_t symbol;
 *		struct ... *next, *prev, **head, *scope;
 *		unsigned int hash;
 *		...
 *
 * 'template.h' can be used as a template to create a 'sym.h'.
 *
 * 'head' is &(table[hash(itself)]).
 * The hash table is not resizable at run-time.
 * The scope field is used to link all symbols of a current scope together.
 * Scope() sets the current scope (linked list) to add symbols to.
 * Any number of scopes can be handled.  The user passes the address of
 * a pointer to a symbol table
 * entry (INITIALIZED TO NULL first time).
 *
 * Available Functions:
 *
 *	zzs_init(s1,s2)	--	Create hash table with size s1, string table size s2.
 *	zzs_done()		--	Free hash and string table created with zzs_init().
 *	zzs_add(key,rec)--	Add 'rec' with key 'key' to the symbol table.
 *	zzs_newadd(key)	--	create entry; add using 'key' to the symbol table.
 *	zzs_get(key)	--	Return pointer to last record entered under 'key'
 *						Else return NULL
 *	zzs_del(p)		--	Unlink the entry associated with p.  This does
 *						NOT free 'p' and DOES NOT remove it from a scope
 *						list.  If it was a part of your intermediate code
 *						tree or another structure.  It will still be there.
 *			  			It is only removed from further consideration
 *						by the symbol table.
 *	zzs_keydel(s)	--	Unlink the entry associated with key s.
 *						Calls zzs_del(p) to unlink.
 *	zzs_scope(sc)	--	Specifies that everything added to the symbol
 *			   			table with zzs_add() is added to the list (scope)
 *						'sc'.  'sc' is of 'Sym **sc' type and must be
 *						initialized to NULL before trying to add anything
 *						to it (passing it to zzs_scope()).  Scopes can be
 *					    switched at any time and merely links a set of
 *						symbol table entries.  If a NULL pointer is
 *						passed, the current scope is returned.
 *	zzs_rmscope(sc)	--	Remove (zzs_del()) all elements of scope 'sc'
 *						from the symbol table.  The entries are NOT
 *						free()'d.  A pointer to the first
 *			   			element in the "scope" is returned.  The user
 *			   			can then manipulate the list as he/she chooses
 *			   			(such as freeing them all). NOTE that this
 *			   			function sets your scope pointer to NULL,
 *			   			but returns a pointer to the list for you to use.
 *	zzs_stat()		--	Print out the symbol table and some relevant stats.
 *	zzs_new(key)	--	Create a new record with calloc() of type Sym.
 *			   			Add 'key' to the string table and make the new
 *			   			records 'symbol' pointer point to it.
 *	zzs_strdup(s)	--	Add s to the string table and return a pointer
 *			   			to it.  Very fast allocation routine
 *						and does not require strlen() nor calloc().
 *
 * Example:
 *
 *	#include <stdio.h>
 *	#include "sym.h"
 *
 *	main()
 *	{
 *	    Sym *scope1=NULL, *scope2=NULL, *a, *p;
 *	
 *	    zzs_init(101, 100);
 *	
 *	    a = zzs_new("Apple");	zzs_add(a->symbol, a);	-- No scope
 *	    zzs_scope( &scope1 );	-- enter scope 1
 *	    a = zzs_new("Plum");	zzs_add(a->symbol, a);
 *	    zzs_scope( &scope2 );	-- enter scope 2
 *	    a = zzs_new("Truck");	zzs_add(a->symbol, a);
 *	
 *    	p = zzs_get("Plum");
 *    	if ( p == NULL ) fprintf(stderr, "Hmmm...Can't find 'Plum'\n");
 *	
 *    	p = zzs_rmscope(&scope1)
 *    	for (; p!=NULL; p=p->scope) {printf("Scope1:  %s\n", p->symbol);}
 *    	p = zzs_rmscope(&scope2)
 *    	for (; p!=NULL; p=p->scope) {printf("Scope2:  %s\n", p->symbol);}
 * }
 *
 * Terence Parr
 * Purdue University
 * February 1990
 *
 * CHANGES
 *
 *	Terence Parr
 *	May 1991
 *		Renamed functions to be consistent with ANTLR
 *		Made HASH macro
 *		Added zzs_keydel()
 *		Added zzs_newadd()
 *		Fixed up zzs_stat()
 *
 *	July 1991
 *		Made symbol table entry save its hash code for fast comparison
 *			during searching etc...
 */

/* Modifications - Peter Keller July 1995.
* Allow for an expandable string table - 
* 
*    strp and the symbol field of Sym are now offsets from strings (type size_t)
*    zzs_strdup now returns size_t rather than char*
*    zzs_strdup now attempts to expand string table using realloc on overflow,
*      by STRING_GROW bytes.
*    Hash function and string comparison function are case-independent. VAXen
*    (and maybe others) don't have a strcasecmp function in their RTL's
*/

#if 0

#include <stdio.h>
#include <ctype.h>
#if __STDC__ == 1
#include <string.h>
#include <stdlib.h>
#else
#include "malloc.h"
#endif
#ifdef MEMCHK
#include "trax.h"
#endif
#include "sym.h"
#include "charstru.h"
#include "ast.h"
#include "exit_handler.h"
#define StrSame		0

#ifdef MALLOC_TRACE
#include <prof_malloc.h>
#endif

#else /* #if 0 */

#include "stdpccts.h"
#include "sorlist.h"
#define StrSame		0

#endif /* #if 0 */

#ifndef STRING_GROW
#define STRING_GROW 512
#endif

#ifndef COUNT_LEN
#define COUNT_LEN 50
#endif

#ifdef _AIX
#include "sym.h"
#endif

static Sym **CurScope = NULL;
static size_t size = 0;
static Sym **table=NULL;

#ifdef DEBUG    /* Allow dbx to find this */
char *strings;
char *tmpstrings;
#else
static char *strings;
static char *tmpstrings;
#endif

/* Imported from lib_dict2tab.sor: the serial number for the next data cell
 * is kept in the dictionary now, to allow new info to be merged */
__import__ int block_seq_no;


/* Declaration for callback routine to dump dictionary information */
static void dict_info_callback ( char callback_msg[CALLBACK_LEN], 
				const void ** ptr, 
				int * const idx, 
				const int first_call);



/* Macro to dump a list to disk - used in zzs_dump() below.
 Iterate twice over each list, dumping the number of items first, then the items themselves.
 This is crude, but we are not particularly concerned with efficiency here.
 Doing it this way, allows the undumping and referencing of this information to be
 done properly by applications. */

   
#define zzs_DUMP_LIST(elem_type, s_list) { \
   int i, j[3]; \
   long align; \
   SList *cursor; \
   elem_type *ptr; \
             \
     for ( i = 0; i < 3; i++ ) { \
      for ( j[i] = 0, cursor = s_list[i]; \
           ptr = slist_iterate(s_list[i], &cursor); \
	   (j[i])++) ; \
      fwrite ( &(j[i]),    sizeof (int),      1,        dump); \
   } \
\
   align = ftell(dump); \
   i = sizeof (elem_type) - (align - 1) % sizeof (elem_type) - 1;   /* No. of padding bytes required */ \
   fseek(dump, i, SEEK_CUR); \
\
   for ( i = 0; i < 3; i++ ) { \
      for ( cursor = s_list[i]; \
	   ptr = slist_iterate(s_list[i], &cursor); \
	   ) \
	fwrite ( ptr, sizeof (elem_type), 1, dump ); \
   } \
} 


/* Macro to read a dumped list from disk, where the file is memory-mapped.
 * mptr and map_ptr are set up externally to the macro */

#define zzs_UNDUMP_LIST_MAP(elem_type, list_ptr, list_len) {\
   int iu; \
   for ( iu=0; iu<3; iu++ ) { \
      list_len[iu] = *( (int *) mptr ); \
         mptr += sizeof (int); \
      } \
\
      align = mptr - map_file->start; \
      mptr += sizeof (elem_type) - (align - 1) % sizeof (elem_type) - 1;  /* Padding bytes to start of next item*/ \
\
      for ( iu=0; iu<3; iu++ ) { \
         if ( list_len[iu] ) { \
            list_ptr[iu] = (elem_type *) mptr ; \
            mptr += list_len[iu] * sizeof (elem_type); \
         } \
      } \
}

/* Macro to read a dumped list from disk, where the file is not memory-mapped.
 * File pointer dump is opened externally to the macro 
 * Restores the SList's, to allow for the possbility of new dictionary block
 * information being added. */

#define zzs_UNDUMP_LIST_FREAD(elem_type, list_ptr, list_len, s_list) { \
   int iu,ju,is; \
   long align; \
   fread ( list_len,    sizeof (int),                3,    dump ); \
   align = ftell(dump); \
   ju = sizeof (elem_type) - (align - 1) % sizeof (elem_type) - 1;   /* No. of padding bytes to be skipped */ \
   fseek(dump, ju, SEEK_CUR); \
\
   for ( iu=0; iu<3; iu++ ) { \
      if ( list_len[iu] ) { \
         CCIF_CALLOC(list_len[iu], elem_type, list_ptr[iu], \
                  "zzs_undump (zzs_UNDUMP_LIST_FREAD)", (0) ) \
         fread ( list_ptr[iu],  sizeof (elem_type),  list_len[iu],  dump ); \
         for ( is = 0; is < list_len[iu]; is++ ) \
            slist_add(&s_list[iu], list_ptr[iu]+is); \
      } \
   } \
}



static size_t strp;  /* This is an offset from &(strings[0]) */
static size_t strsize = 0;
static size_t tmpstrp;
static size_t tmpstrsize = 0;
static size_t nrecs;



/* Non-symbol table things */

static ItemTypeList *item_type_list[3]; /* keep track of items defined in global_ blocks,
                                         * data blocks and save frames in separate lists */
static int ntype_list[3];               /* Number of elements in each list */

static Dictionary *dictionary[3];   /* In principle, a save-frame which is taken from one
                                       dictionary to another, may have different contents of the
                                       DICTIONARY category from its enclosing data block. */
static int ndictinfo[3];            /* Number of DICTIONARY records at each scope level */

                      /* referenced by lib_dict2tab.c */
__export__ __noshare__ SList 
                 * ccif_item_type_list[3] = {NULL, NULL, NULL}, 
                 * ccif_dictionary[3] = {NULL, NULL, NULL};       


void zzs_init(int sz, int strs)
{
  if ( sz > 0 && ! table ) {
    CCIF_CALLOC(sz, Sym *, table, "zzs_init", (0))
      size = sz;
  }

  if ( strs <= 0 ) return;
  if ( ! strings ) {
    CCIF_CALLOC(strs, char, strings, "zzs_init", (0));
    strsize = strs;
    strp = 0;
  }

  if ( ! tmpstrings ) {
    CCIF_CALLOC(strs, char, tmpstrings, "zzs_init", (0));
    tmpstrsize = strs;
    tmpstrp = 0;
  }
}

void zzs_done()
{
	if ( table != NULL ) free( table );
	if ( strings != NULL ) free( strings );
	if ( tmpstrings != NULL ) free (tmpstrings);
}

void zzs_add(char *key, Sym *rec)
{
	register unsigned int h=0;
	register char *p=key;
/*	extern Sym *Globals; */
	
	HASH(p, h);
	rec->hash = h;					/* save hash code for fast comp later */
	h %= size;
	
	if ( CurScope != NULL ) {rec->scope.rec = *CurScope; *CurScope = rec;}
	rec->next = table[h];			/* Add to doubly-linked list */
	rec->prev = NULL;
	if ( rec->next != NULL ) (rec->next)->prev = rec;
	table[h] = rec;
	rec->head = &(table[h]);
}

Sym * zzs_get(const char * const key)
{
  register unsigned int h=0;
  register const char *p=key;
  register Sym *q;
  
  HASH(p, h);
  
  for (q = table[h%size]; q != NULL; q = q->next)
    {
      if ( q->hash == h )		/* do we even have a chance of matching? */
	if ( ( q->fmt && q->fmt == tmpstrings && zzs_keycmp(key, &(tmpstrings[q->symbol]) ) == StrSame )
	     || zzs_keycmp(key, &(strings[q->symbol]) ) == StrSame 
	     ) 
	  return( q );
    }
  return( NULL );
}

/*
 * Unlink p from the symbol table.  Hopefully, it's actually in the
 * symbol table.
 *
 * If p is not part of a bucket chain of the symbol table, bad things
 * will happen.
 *
 * Will do nothing if all list pointers are NULL
 */
void zzs_del(register Sym *p)
{
	if ( p == NULL ) ccif_signal(CCIF_NULLSYMPTR, "zzs_del", NULL, NULL, 0);
	if ( p->prev == NULL )	/* Head of list */
	{
		register Sym **t = p->head;
		
		if ( t == NULL ) return;	/* not part of symbol table */
		(*t) = p->next;
		if ( (*t) != NULL ) (*t)->prev = NULL;
	}
	else
	{
		(p->prev)->next = p->next;
		if ( p->next != NULL ) (p->next)->prev = p->prev;
	}
	p->next = p->prev = NULL;	/* not part of symbol table anymore */
	p->head = NULL;
}

void zzs_keydel(char *key)
{
	Sym *p = zzs_get(key);

	if ( p != NULL ) zzs_del( p );
}

/* S c o p e  S t u f f */

/* Set current scope to 'scope'; return current scope if 'scope' == NULL */
Sym ** zzs_scope(Sym **scope)
{
	if ( scope == NULL ) return( CurScope );
	CurScope = scope;
	return( scope );
}


/* Reset scope (CurScope is static, so unavailable to other modules) */
void zzs_noscope(void) {
   CurScope = NULL;
}

/* Add item already in symbol table to current scope */
void zzs_add_to_scope (Sym *r) {
   if ( r->scope.rec == NULL ) {
      r->scope.rec = *CurScope;
      *CurScope = r;
   }
}

/* Remove a scope described by 'scope'.  Return pointer to 1st element in scope */
Sym *zzs_rmscope(register Sym **scope)
{
	register Sym *p;
	Sym *start;

	if ( scope == NULL ) return(NULL);
	start = p = *scope;
	for (; p != NULL; p=p->scope.rec) { zzs_del( p ); }
	*scope = NULL;
	return( start );
}


/* Initialise records in scope for a particular block */
void zzs_init_scope ( register Sym **scope, const int block ) {
   
   register Sym *p;
   
   for ( p = *scope; p; p = p->scope.rec ) {

      if ( p->id > block ) {
	 p->node[block] = NULL;

	 /* loop_offset is now used for category records */
#if 0
	 if ( p != p->category.rec )     /* loop_offset is not used in category records */
	   p->loop_offset[block] = 0;
	 else 
#else
	   p->loop_offset[block] = 0;
#endif
	 if ( p == p->category.rec )
	   {
	     p->l.loop[block] = NULL;
	     p->last[block] = NULL;
	   }
      }
   }
}
	 
   
   
void zzs_stat(FILE *symdump)
{
	static unsigned short count[COUNT_LEN];
	unsigned int i,n=0,low=0, hi=0;
	register Sym **p;
	float avg=0.0, sdev=0.0;

	char *env_dumptypes = getenv("CCIF_DUMPTYPES");

	ItemTypeList *type_ptr;

	if ( ! env_dumptypes || 
	     ( zzs_keycmp(env_dumptypes,"yes") &&  zzs_keycmp(env_dumptypes,"on") ) )
	  env_dumptypes = 0;

	
	for (i=0; i<COUNT_LEN; i++) count[i] = 0;
	for (p=table; p<&(table[size]); p++)
	{
		register Sym *q = *p;
		unsigned int len;
		
		if ( q != NULL && low==0 ) low = p-table;
		len = 0;
		if ( q != NULL ) fprintf (symdump,"[%" PTR_FMT(d) "]", p-table);
		while ( q != NULL )
		{
			len++;
			n++;
			type_ptr = ITEM_TYPE(q);
			if ( ! env_dumptypes ) {
			  fprintf (symdump," %s\n", &strings[q->symbol]);
			}
			else {
			  fprintf (symdump," %s %s\n", &strings[q->symbol], zzs_symname(type_ptr->code));
			}
			q = q->next;
			if ( q == NULL ) fprintf (symdump,"\n");
		}
		if ( len>=COUNT_LEN ) printf("zzs_stat: count table too small\n");
		else count[len]++;
		if ( *p != NULL ) hi = p-table;
	}

   	nrecs = n;   /* Keep this for zzs_dump */
        fprintf (symdump,"Storing %d recs used %" PTR_FMT(d) " hash positions out of %" LONG_FMT(d) "\n",
			n, size-count[0], size);
	fprintf (symdump,"%f %% utilization\n",
			((float)(size-count[0]))/((float)size));
	for (i=0; i<COUNT_LEN; i++)
	{
		if ( count[i] != 0 )
		{
			avg += (((float)(i*count[i])));
		        sdev += (float) (count[i]*i*i);
			fprintf (symdump,"Buckets of len %d == %d (%f %% of recs)\n",
					i, count[i], 100.0*((float)(i*count[i]))/((float)n));
		}
	}
	avg /= (float) size;
	sdev = sqrt( sdev/(float) size - avg*avg );
	fprintf (symdump,"Avg bucket length %f\n", avg);
        fprintf (symdump,"Std. dev. of bucket length: %f\n",sdev);
	fprintf (symdump,"Range of hash function: %d..%d\n", low, hi);
}

/*
 * Given a string, this function allocates and returns a pointer to a
 * symbol table record whose "symbol" pointer is reset to a position
 * in the string table.
 */
Sym * zzs_new(char *text)
{
	Sym *p;
	size_t zzs_strdup(char *);
	
        CCIF_CALLOC(1, Sym, p, "zzs_new", (0))
	p->symbol = zzs_strdup(text);
	
	return p;
}

/* create a new symbol table entry and add it to the symbol table */
Sym * zzs_newadd(char *text)
{
	Sym *p = zzs_new(text);
	if ( p != NULL ) zzs_add(text, p);
	return p;
}

/* create a new symbol table entry and add it to the symbol table.
 * Items added with this routine are intended to be volatile, and will not
 * be dumped
 */
Sym * zzs_tmpadd(char *text) {

  Sym *p;
  size_t zzs_tmpstrdup(char *);

  CCIF_CALLOC(1, Sym, p, "zzs_tmpadd", (0));
  p->symbol = zzs_tmpstrdup(text);
  /* Mark this record as being volatile */
  p->fmt = tmpstrings;

  /* Add to hash table */
  if ( p != NULL ) zzs_add(text, p);
  return p;
}
  

/* Add a string to the string table and return a pointer to it.
 * Bump the pointer into the string table to next avail position.
 */
size_t zzs_strdup(register char *s)
{
	register char *start;
        size_t ostrp=strp;

   
   if ( strp + strlen(s) >= strsize-2 )  {
      CCIF_REALLOC(strsize + STRING_GROW, char, strings, "zzs_strdup", (0))
      strsize += STRING_GROW;
   }
   start = strings+strp;
      
   while ( *s != '\0' ) { *start++ = *s++; strp++; }
   *start++ = '\0'; strp++;
   return( ostrp );
}


/* Same as zzs_strdup, but uses temporary string table */

size_t zzs_tmpstrdup(register char *s) {

  register char *start;
  size_t ostrp=tmpstrp;
   
   if ( tmpstrp + strlen(s) >= tmpstrsize-2 )  {
      CCIF_REALLOC(tmpstrsize + STRING_GROW, char, tmpstrings, "zzs_tmpstrdup", (0))
      tmpstrsize += STRING_GROW;
   }
   start = tmpstrings+tmpstrp;
      
   while ( *s != '\0' ) { *start++ = *s++; tmpstrp++; }
   *start++ = '\0'; tmpstrp++;
   return( ostrp );
}


/* Same as zzs_strdup, but compresses certain C-style escaped
* characters */
size_t zzs_strcdup (register char *s)
{
	register char *start;
        size_t ostrp=strp, len;

   if ( strp + strlen(s) >= strsize-2 )  {  /* strlen(s) may be a slight overestimate of the
					     * number of bytes needed to hold the string in
					     * this case, but no matter */
      CCIF_REALLOC(strsize + STRING_GROW, char, strings, "zzs_strcdup", (0))
      strsize += STRING_GROW;
   }
   start = strings+strp;
   
      
   while ( *s != '\0' ) {
      if ( *s == '\\' && s[1] )   {
	 switch ( s[1] ) {
	  case ('n'):
	    *start++ = '\n';
	    s+=2;
	    strp++;
	    break;
	  case ('t'):
	    *start++ = '\t';
	    s+=2;
	    strp++;
	    break;
	  case ('\\'):
	    *start++ = '\\';
	    s+=2;
	    strp++;
	    break;
	  default:
	    *start++ = *s++;
	    *start++ = *s++;
	    strp += 2;
	 }
      }
      else
      	*start++ = *s++; strp++; 
   }

   *start++ = '\0'; strp++;
   return( ostrp );
      
}

/* strcasecmp clone, because not all RTL's have it */

int zzs_keycmp(const char *s1, const char *s2) {
   register const char *ptr1=s1, *ptr2=s2;
   char ch1, ch2;

   if (ptr1 == ptr2) return 0;  /* Strings in same place */
   
   do {
      ch1 = TOLOWER_ADV_PTR(ptr1);
      ch2 = TOLOWER_ADV_PTR(ptr2);
   } while ( (ch1 == ch2) && ch1 ) ; /* Don't need to test for both
                                     * ch1 and ch2 being '\0' */
   
   return ( (int) (ch1 - ch2) );  /* Same value as strcasecmp */
}


/* strncasecmp clone, because not all RTL's have it */

int zzs_keyncmp(const char *s1, const char *s2, const size_t n) {
   register const char *ptr1=s1, *ptr2=s2;
   char ch1, ch2;
   int i=0;

   if (ptr1 == ptr2) return 0;  /* Strings in same place */

   
   do {
      ch1 = TOLOWER_ADV_PTR(ptr1);
      ch2 = TOLOWER_ADV_PTR(ptr2);
      i++;
   } while ( (ch1 == ch2) && ch1 && i < n ) ; /* Don't need to test for both
                                     * ch1 and ch2 being '\0' */
   
   return ( (int) (ch1 - ch2) );  /* Same value as strncasecmp */
}



/* Dump and undump routines */

/* Each structure is dumped on a boundary offset into the file, equal to its own width. */

void zzs_dump (char *filename, FILE *symdump) {
   FILE *dump;
   int id, i;
   long align;
   Sym **p, *q;
   size_t blkseq = block_seq_no; /* Convert to size_t for dumping */
   
   SList *cursor;
   void *ptr;

   if ( ! ( dump = fopen(filename,"wb") ) ) 
     ccif_signal(CCIF_FOPEN, "zzs_dump", NULL, NULL, 0, filename, "writing");

   zzs_stat(symdump);
   
   zzs_DUMP_LIST(ItemTypeList, ccif_item_type_list)
   zzs_DUMP_LIST(Dictionary, ccif_dictionary)

   /* Alignment for sym numbers - almost certaintly unnecessary on every supported system */
   align = ftell(dump);
   i = sizeof (size_t) - (align - 1) % sizeof (size_t) - 1;   /* No. of padding bytes required */
   fseek(dump, i, SEEK_CUR);

   fwrite ( &blkseq,  sizeof blkseq,  1,       dump);
   fwrite ( &size,    sizeof size,    1,       dump);
   fwrite ( &nrecs,   sizeof nrecs,   1,       dump);
   fwrite ( &strsize, sizeof strsize, 1,       dump);
   fwrite ( &strp,    sizeof strp,    1,       dump);
   fwrite ( strings,  sizeof (char),  strsize, dump);


   /* Alignment for Sym records */
   align = ftell(dump);
   i = sizeof (Sym) - (align - 1) % sizeof (Sym) - 1;   /* No. of padding bytes required */
   fseek(dump, i, SEEK_CUR);
   
   /* Set up id's */
   for ( p = table, id = 0; p < &(table[size]) ; p++) {
      for ( q = *p ; q ; q = q->next, id++ )  {
	 q->id = id;
      }
   }
      
/* Convert pointers to id's and dump symbol table */
   for ( p = table; p < &(table[size]) ; p++) {
      for ( q = *p ; q ; q = q->next )  {

	 if ( q->scope.rec )           q->scope.id           = q->scope.rec->id + 1;
	 if ( q->mandatory_scope.rec ) q->mandatory_scope.id = q->mandatory_scope.rec->id + 1;
	 if ( q->key.rec )             q->key.id             = q->key.rec->id + 1;
	 if ( q->true_name.rec )       q->true_name.id       = q->true_name.rec->id + 1;
	 if ( q->parent.rec )          q->parent.id          = q->parent.rec->id + 1;
	 if ( q->category.rec )        q->category.id        = q->category.rec->id + 1;
	 if ( q->associated_esd.rec )  q->associated_esd.id  = q->associated_esd.rec->id + 1;
	 if ( q->item_list.rec )       q->item_list.id       = q->item_list.rec->id + 1;
	 fwrite ( q,  sizeof (Sym),  1,  dump );
      }
   }
   
   fclose (dump);
}


/* This routine relies on calloc initialising allocated space to zeros 
*  IF the system supports memory mapping, AND IF the program is not going
*  to modify the string table or hash table AND IF no symbol table 
*  records are going to be added or removed, call with use_mmap == 1
*  to save memory. */


void zzs_undump ( const char * const ioname, const char * const logname, 
                   const int use_mmap) {
   FILE *dump=NULL;
   int id, m, i, j, *iptr;
   register Sym **p, *q, *init;

   regex_t *preg_ptr;
   int status;
   ItemTypeList *type_ptr;
   
   size_t strs, tmp_strp, sz;
   size_t blkseq;
   char *mptr, *filename = ccif_logname2filename(logname);
   long align=0;

   char *env_dictstat;

#if _USE_MMAP == 1

#ifdef VMS
   extern int ccif_mapfile ( const char * const, FILE ** const, const CIF_FILE_LIST * const );
#else
   extern int ccif_mapfile ( const char * const, FILE ** const );
#endif
   MAP_FILE_RECORD *map_file;
   __import__ MAP_FILE_RECORD *ccif_map_file_list[];
#endif
   

   /* Mustn't pass filename as null into ccif_signal: vprintf on Solaris can't
      handle it
   */
   if ( ! filename ) filename = "No translation";

   if ( use_mmap ) {
#if _USE_MMAP == 1
      m = ccif_mapfile(ioname, &dump
#ifdef VMS
         , NULL
#endif
      );



      map_file = ccif_map_file_list[m];
      map_file->nnodes = 1000; 	/* Avoid accidental unmapping */ 
      mptr = map_file->start;
      
      zzs_UNDUMP_LIST_MAP(ItemTypeList, item_type_list, ntype_list)
      zzs_UNDUMP_LIST_MAP(Dictionary, dictionary, ndictinfo)

      align = mptr - map_file->start;
      mptr += sizeof (size_t) - (align - 1) % sizeof (size_t) - 1; /* Padding bytes to start of next item*/
      
      blkseq = *( (size_t *) mptr);
      mptr += sizeof blkseq;
      sz = *( (size_t *) mptr);
      mptr += sizeof sz;
      nrecs = *( (size_t *) mptr);
      mptr += sizeof nrecs;
      strs = *( (size_t *) mptr);
      mptr += sizeof strs;
      tmp_strp = *( (size_t *) mptr);
      mptr += sizeof tmp_strp;

      block_seq_no = blkseq;

#else
      ccif_signal(CCIF_NOUSEMMAP, "zzs_undump", NULL, NULL, 0);
#endif
   }
   else {
      if ( ! ( dump = fopen(ioname,"rb") ) ) 
	ccif_signal(CCIF_FOPEN, "zzs_undump", NULL, NULL, 0, filename, "reading");

      zzs_UNDUMP_LIST_FREAD(ItemTypeList, item_type_list, ntype_list, ccif_item_type_list)
      zzs_UNDUMP_LIST_FREAD(Dictionary, dictionary, ndictinfo, ccif_dictionary)

         
      align = ftell(dump);
      j = sizeof (size_t) - (align - 1) % sizeof (size_t) - 1; /* No. of padding bytes to be skipped */
      fseek(dump, j, SEEK_CUR);
      
      fread ( &blkseq,   sizeof blkseq,  1,       dump);
      fread ( &sz,       sizeof sz,      1,       dump);
      fread ( &nrecs,    sizeof nrecs,   1,       dump);
      fread ( &strs,     sizeof strs,    1,       dump);
      fread ( &tmp_strp, sizeof strp,    1,       dump);
      
      block_seq_no = blkseq;
   }
   
   zzs_done();

   zzs_init (sz, (use_mmap ? 0 : strs) );
   strp = tmp_strp;

   if ( use_mmap) {
#if _USE_MMAP == 1
     
     /* Restore string table */
     strings = mptr;
     strsize = strs;  /* This may not have been set in zzs_init for use_mmap == 1 */
     
     /* Restore symbol table records */
     mptr = strings + strsize;
     align = mptr - map_file->start;
     mptr += sizeof (Sym) - (align - 1) % sizeof (Sym) - 1;   /* No. of padding bytes to be skipped */
     
     init = (Sym *) mptr;
     
#else
     ccif_signal(CCIF_NOUSEMMAP, "zzs_undump", NULL, NULL, 0);
#endif
   }
   else {
     /* Restore string table */
     fread ( strings,  sizeof (char),  strsize, dump);
     
     /* Restore symbol table records */
     CCIF_CALLOC(nrecs, Sym, init, "zzs_undump", (0))
       align = ftell(dump);
     i = sizeof (Sym) - (align - 1) % sizeof (Sym) - 1;   /* No. of padding bytes to be skipped */
     fseek(dump, i, SEEK_CUR);
     fread (init,  sizeof (Sym),  nrecs,  dump);
   }
    
/* Restore hash table and pointers linking records. Also, set the id member to 0, ready
 * to be used for keeping track of memory allocation for node and loop_offset members.
 */
   for ( q = init; q < init + nrecs ; q++) {

      Sym **head;

      head = &(table[q->hash % size]);
      *head = q;
      
      while (1) {
	 q->head = head;
	 q->id = 0;

	 if ( q->scope.id )             q->scope.rec           = &(init[q->scope.id - 1]);
	 if ( q->mandatory_scope.id )   q->mandatory_scope.rec = &(init[q->mandatory_scope.id - 1]);
	 if ( q->key.id )               q->key.rec             = &(init[q->key.id - 1]);
	 if ( q->true_name.id )         q->true_name.rec       = &(init[q->true_name.id -1]);
	 if ( q->parent.id )            q->parent.rec          = &(init[q->parent.id - 1]);
	 if ( q->category.id)           q->category.rec        = &(init[q->category.id - 1]);
	 if ( q->associated_esd.id)     q->associated_esd.rec  = &(init[q->associated_esd.id - 1]);
	 if ( q->item_list.id )         q->item_list.rec       = &(init[q->item_list.id - 1]);

	 if ( ! q->next ) break;  /* Rely on set pointers being non-null, even though their
				   * values are not actually correct */

	 (q->next = q + 1)->prev = q;
	 q++;
      } /* while (1) */
	 
   }  /* for ( q = init; q < init + nrecs ; q++) */

   
/* Compile constructs in ITEM_TYPE_LIST category */

   CCIF_CALLOC(ntype_list[0]+ntype_list[1]+ntype_list[2], regex_t, preg_ptr, "zzs_undump", (0) )

   for (i = 0; i < 3; i ++ ) {

      for (j = 0; j < ntype_list[i]; j++) {
	 
	 item_type_list[i][j].preg = preg_ptr++;
         status = regcomp( item_type_list[i][j].preg, zzs_symname(item_type_list[i][j].construct), REG_EXTENDED);
         if ( status ) {
            char re_msg[450];
	    regerror( status, item_type_list[i][j].preg, re_msg, sizeof re_msg);
	    ccif_signal(RX_REGCOMPERR2, "zzs_undump", NULL, NULL, 0, re_msg);
	 }
       } /* for (j = 0; j < ntype_list[i], j++)  */
    } /* for (i = 0; i < 3; i ++ ) */

   

   if ( !use_mmap) fclose (dump);

#ifndef DEBUG
   env_dictstat = getenv("CCIF_DICTSTAT");
   if ( env_dictstat && 
	( !zzs_keycmp(env_dictstat,"yes") || !zzs_keycmp(env_dictstat,"on") ) )
#endif
	zzs_stat(stdout);
	
   ccif_signal(CCIF_DICTLOADED, NULL, dict_info_callback, NULL, 0,
	       logname,filename);


   
#ifdef VMS
   free(filename);
#endif
   
}


static void dict_info_callback ( char callback_msg[CALLBACK_LEN], 
				const void ** ptr, 
				int * const idx, 
				const int first_call) {
   static int i,j, info_ok;
   

   if (first_call) {
      i = 0;
      j = 0;
      info_ok = (ndictinfo[0] || ndictinfo[1] || ndictinfo[2]);
      if (info_ok)
	strncpy(callback_msg, "Information from DICTIONARY category:", CALLBACK_LEN-1);
      else
	strncpy(callback_msg, "No information about dictionary available\n", CALLBACK_LEN-1);
      callback_msg[CALLBACK_LEN-1] = '\0';
      return;
   }

   
   if ( !info_ok ) {
      callback_msg[0] = '\0';
      return;
   }
   
   while ( i < 3 ) {
      if ( ! (j < ndictinfo[i]) ) {
	 i++;
	 j = 0;
	 continue;
      }
      
      sprintf(callback_msg, "Title: %-30s   Version: %-10s   Datablock id: %-15s", 
	      &(strings[ dictionary[i][j].title ]),
	      &(strings[ dictionary[i][j].version ]),
	      &(strings[ dictionary[i][j].datablock_id ]) );
      j++;
      return;
   }
   callback_msg[0] = '\0';
}


/* Return a pointer to the appropriate row of the ITEM_TYPE_LIST category 
   A bit of implied DDL knowledge - works its way up to the ultimate parent
   */

ItemTypeList * zzs_get_item_type ( const Sym * const item)
{
  const Sym * i;
  int t0, t1;
  for (i = item; i->parent.rec; i = i->parent.rec) ;
  t0 = abs(i->type_code[0]);
  t1 = abs(i->type_code[1]);
  return ( &(item_type_list[t0][t1]) );
}

   
/* Return a pointer to the symbol name of an item (do it this way, so that
*  we can keep the string table static).
* */
  
char *zzs_symname (const size_t offset)  {
   if ( offset < strsize-1 && strings[offset] )
     return ( &strings[offset] );
   else
     return ( "<unknown item>" );
}

char *zzs_tmpsymname (const size_t offset)  {
   if ( offset < tmpstrsize - 1 && tmpstrings[offset] )
     return ( &tmpstrings[offset] );
   else
     return ( "<unknown item>" );
}



char *zzs_recname (const Sym * const rec ) {

  if ( ! rec->fmt || rec->fmt != tmpstrings ) {
    if ( *(strings + rec->symbol) )
      return ( strings + rec->symbol );
    else
      return ( "<unknown item>" );
  }
  else {
    if ( *(tmpstrings + rec->symbol) )
      return ( tmpstrings + rec->symbol );
    else
      return ( "<unknown item>" );
  }
    
}
  
void zzs_put_string_offset(size_t off)  {
   if (off < strp)
     ccif_signal(CCIF_STRTABVAL, NULL, NULL, NULL, 0, strings+off, off);
   else
     ccif_signal(CCIF_STRTABLEN, "zzs_put_string_offset", NULL, NULL, 0, off);
}

/* Make sure the various block-wise pointers have at least
 * blocks elements allocated */

void zzs_expand_item_record(Sym * item, const int blocks) {

  int new_blocks = blocks - item->id;

  if ( new_blocks <= 0 ) return;

  if ( item->category.rec != item ) {

  /* items */
    CCIF_REALLOC(blocks,  int,     item->loop_offset, "zzs_expand_item", (0) );
    CCIF_REALLOC(blocks,  SList *, item->l.sort_tree, "zzs_expand_item", (0) );
    CCIF_REALLOC(blocks,  AST *,   item->node,        "zzs_expand_item", (0) );

    memset ( item->loop_offset + item->id, 0, new_blocks * sizeof (int) );
    memset ( item->l.sort_tree + item->id, 0, new_blocks * sizeof (SList *) );
    memset ( item->node + item->id,        0, new_blocks * sizeof (AST *) );

  }
  else {

    /* categories */
    CCIF_REALLOC(blocks,  int,     item->loop_offset, "zzs_expand_item", (0) );
    CCIF_REALLOC(blocks, AST *, item->last,   "zzs_expand_item", (0));
    CCIF_REALLOC(blocks, AST *, item->l.loop, "zzs_expand_item", (0));
    CCIF_REALLOC(blocks, AST *, item->node,   "zzs_expand_item", (0));
    
    memset ( item->loop_offset + item->id, 0, new_blocks * sizeof (int) );
    memset ( item->last + item->id,    0, new_blocks * sizeof (AST *));
    memset ( item->l.loop + item->id,  0, new_blocks * sizeof (AST *));
    memset ( item->node + item->id,    0, new_blocks * sizeof (AST *));

  }

  item->id = blocks;

}

/* Don't use this function unless you really have to! */
Sym * zzs_start_of_bucket(const int i) {
   return ( i < size? table[i] : NULL );
}

int zzs_hsize() {
  return size;
}



/* Edit Makefile manually to enable these features. 
   Needs linking against static library.
   Not intended to be portable/generally used */
#ifdef DICT_ANALYSIS
#include "dict_analysis.c"
#endif

/* $Id$ */
#ifndef RB_H
#define RB_H
/* Namespace modified to make it more sensible to include this stuff
 * in CCIF.    Peter Keller */

#include "rb_structs.h"

extern Rb_node rb_make(void);
extern Rb_node rb_insert_b(Rb_node n, char * key, char * val);

extern Rb_node rb_find_key(Rb_node n, char * key);
extern Rb_node rb_find_ikey(Rb_node n, int ikey);
extern Rb_node rb_find_gkey(Rb_node n, char * key, int (*fxn)() );

extern Rb_node rb_find_key_n(Rb_node n, char * key, int * fnd);
extern Rb_node rb_find_ikey_n(Rb_node n, int ikey, int * fnd);
extern Rb_node rb_find_gkey_n(Rb_node n, char * key, int (*fxn)(), int * fnd);
extern void rb_delete_node(Rb_node n);
extern void rb_free_tree(Rb_node n);  /* Deletes and frees an entire tree */
extern char *rb_val(Rb_node n);  /* Returns node->v.val (this is to shut
				     lint up */

/* Added during change to fully-prototyped code. --- PAK */
extern void rb_mk_new_int(Rb_node l, Rb_node r, Rb_node p, int il);
extern Rb_node rb_lprev(Rb_node n);
extern Rb_node rb_rprev(Rb_node n);
extern void rb_recolor(Rb_node n);
extern void rb_single_rotate(Rb_node y, int l);
extern void rb_print_tree(Rb_node t, int level);
extern void rb_iprint_tree(Rb_node t, int level);
extern int rb_nblack(Rb_node n);
extern int rb_plength(Rb_node n);



#define rb_insert_a(n, k, v) rb_insert_b(n->c.list.flink, k, v)
#define rb_insert(t, k, v) rb_insert_b(rb_find_key(t, k), k, v)
#define rb_inserti(t, k, v) rb_insert_b(rb_find_ikey(t, k), (char *) k, v)
#define rb_insertg(t, k, v, f) rb_insert_b(rb_find_gkey(t, k, f), k, v)
#define rb_first(n) (n->c.list.flink)
#define rb_last(n) (n->c.list.blink)
#define rb_next(n) (n->c.list.flink)
#define rb_prev(n) (n->c.list.blink)
#define rb_empty(t) (t->c.list.flink == t)
#ifndef rb_nil
#define rb_nil(t) (t)
#endif

#define rb_traverse(ptr, lst) \
  for(ptr = rb_first(lst); ptr != rb_nil(lst); ptr = rb_next(ptr))

#endif /* RB_H */

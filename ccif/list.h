#ifndef RBLIST_H
#define RBLIST_H

/* 
 * $Source$
 * $Revision$
 * $Date$
 * $Author$
 */

/* This is the header file for the list manipulation routines in list.c.
 * Any struct can be turned into a list as long as its first two fields are
 * flink and blink. */

/* Namespace modified to make it more sensible to include this stuff
 * in CCIF.    Peter Keller */

#include "rb_structs.h"
#include "condition.h"

/* Nil, first, next, and prev are macro expansions for list traversal 
 * primitives. */

#define rb_nil_list(l) (l)
#define rb_first_list(l) (l->flink)
#define rb_last_list(l) (l->blink)
#define rb_next_list(n) (n->flink)
#define rb_prev_list(n) (n->blink)

#define rb_mklist(t) ((t *) rb_make_list (sizeof(t)))

/* These are the routines for manipluating lists */

void rb_insert_list(rb_List node, rb_List list);    /* Inserts a node to the end of a list */
void rb_delete_item_list(rb_List node);          /* Deletes an arbitrary node */
rb_List rb_make_list(int size);       /*Creates a new list */
rb_List rb_get_node_list(rb_List list);             /* Allocates a node to be inserted into the list */
void rb_free_node_list(rb_List node, rb_List list); /* Deallocates a node from the list */

#endif /* RBLIST_H */

/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "stdpccts.h"
#include <fcntl.h>

#ifndef _WIN32
#include <sys/mman.h>
#else
#include <memory.h>
#endif

#include <errno.h>

#if defined (_AIX) || defined(___AIX)
#include <stdio.h>
#endif

#if _USE_MMAP == 1	  
   
__export__ __noshare__ MAP_FILE_RECORD *ccif_map_file_list[MAX_MAPPED_FILES];


/* Returns:   -1 for maximum number of mapped files exceeded
              -2 for not mapping file
               0 ... MAX_MAPPED_FILES -1 for file mapped.
*/

int ccif_mapfile ( const char * const filename, 
                   FILE ** const fileptr ) {

   void *instring;

#if defined (__convex__) || defined (__convexc__)
   unsigned fsize;
#else
   size_t fsize;
#endif


   MAP_FILE_RECORD * map_ptr;
   struct stat statbuf;
   int i;
   

 /* Find first available entry in mapped file list. A file might be still
  * mapped, even though its associated file list entry has been closed, if
  * Attribs which are in use still point to addresses in the mapped space.
  * We cannot re-use this address space, though, since the new file needs
  * to be re-parsed and re-tokenised (sigh). At least this is all virtual
  * memory, not real RAM....
  */
   
   
   for ( i = 0; i < MAX_MAPPED_FILES; i++ ) {
      if ( ! ccif_map_file_list[i] ) break;
   }

   if ( i >= MAX_MAPPED_FILES ) return (-1);
   


   if ( ! *fileptr ) {   /* file may be already open. Make sure that *fileptr is set to NULL
			  * in calling routine, if not. */
      if ( !(*fileptr = fopen(filename,"r") ) )  
	ccif_signal(CCIF_FOPEN, "ccif_mapfile", NULL, NULL, 0, filename, "reading");
   }

   fstat( fileno(*fileptr), &statbuf );
   fsize = statbuf.st_size;
   
#ifdef ESV

   CCIF_MALLOC(fsize+1, instring, "ccif_mapfile", (0))

   fread (instring, sizeof (*instring), fsize, *fileptr);
   instring[fsize] = '\0' ;

#else
/* PROT_WRITE and MAP_PRIVATE allow changes to be made to the mapped memory,
* without changing the underlying file
*/

errno = 0;  /* EXIT_ON_SYS_ERROR uses this */
   
#if defined (__convex__) || defined (__convexc__)
   instring = mmap( NULL, &fsize, PROT_READ|PROT_WRITE, 
		   MAP_PRIVATE|MAP_FILE|MAP_DEBUG, 
		   fileno(*fileptr), 0);
#else
   /* define symbols for flags that are required on some systems,
      but not defined on all */
#ifndef MAP_FILE
#define MAP_FILE 0
#endif
#ifndef MAP_VARIABLE
#define MAP_VARIABLE 0
#endif

   instring = mmap( NULL, fsize, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_VARIABLE|MAP_FILE,
                    fileno(*fileptr), 0);
#endif
   if ( instring == (char *) EOF )  
      EXIT_ON_SYS_ERROR("ccif_mapfile", "Cannot map file");

#endif

   CCIF_CALLOC(1, MAP_FILE_RECORD, map_ptr, "ccif_mapfile", 0)
   map_ptr->start = (char *) instring;
   map_ptr->length = fsize;
   map_ptr->fileptr = *fileptr;
   map_ptr->filename = ccif_strdup(filename);
   map_ptr->st_ino = statbuf.st_ino;
   map_ptr->st_dev = statbuf.st_dev;
   ccif_map_file_list[i] = map_ptr;

   return (i);

}

#endif /* #if _USE_MMAP == 1 */

#if 0
void closecif ( void ) {
   if (!cifin) {
      fclose (cifin);
      cifin=NULL;
   }
}
#endif


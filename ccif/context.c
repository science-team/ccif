/* $Id$ */
/* Routines to manipulate static context information.
 * N.B. These routines assume that the {node,last,loop_offset}
 * members of category/item records in the symbol table are
 * correct/up-to-date!
 */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "stdpccts.h"



static CONTEXT_LIST context[NCONTEXTS];


/* Return next available context. Keep this routine hidden. 
   rec is the record for the category which the context will point to. */
   
int ccif_new_context(Sym *rec, int *status, const int block, const int ro) {
   int i,j;
   register AST *p = NULL;
   register Sym *ptr;
   char *item_text;
   int rows, cols, ovf;

   extern ccif_rwstat ccif_block_status ( const int ) ;
   
   ccif_rwstat c_rwstat = ro ? r : ccif_block_status(block) ;
   
   *status = CAT_NOT_PRESENT;
   
   /* Return without assigning context, if category is not present, and context or file is read-only */
   if ( ((block >= rec->id) || (!rec->node[block])) && c_rwstat == r )  
      return (-1);

   /* Return without assigning context if looped, and we have an incomplete loop packet
    * Don't attempt to process loop: the very first data value might be the one that is
    * causing problems 
    */
   
   if (rec->l.loop && rec->l.loop[block]) {
	    
     LOOP_DIMENSION(rec->l.loop[block], rows, cols, ovf)

       if ( ovf != 0 ) {
	 /* Downgrade severity to warn: the calling application should handle this cleanly */
	 ccif_signal( ( CCIF_PARTLOOP  & ~CCIF_M_SEVERITY ) | CCIF_K_WARN , "ccif_new_context", NULL, NULL, 0,
		     rec->l.loop[block]->a->line) ;
	 *status = BADCAT;
	 return (-1);
       }
   }

   for (i=0; i < NCONTEXTS ; i++)
      if ( !(context[i].category) ) {
         context[i].category = rec;  /* If category is not present as a loop, these */
	 context[i].block = block;   /* are the only items set. */
	 context[i].rwstat = c_rwstat;
	 
	 context[i].tree_index = -1;
	 
	 /* If we are allowed to write to this file, using this context, assign
	  * a context, even if category is not present. It can then be used to create
	  * the category in the file */
	 if ( (block >= rec->id) || (!rec->node[block]) )  
	    return i;
	 
         if (rec->l.loop[block]) {
	    
	    CCIF_CALLOC(rec->no_in_cat, int, context[i].lookup, "ccif_new_context", (0) )
	    CCIF_CALLOC(cols + 1, struct _packet, context[i].packet, "ccif_new_context", (0) )
	    context[i].ncols = cols;
               
            context[i].packet[0].loop_item = rec->l.loop[block]->down;
            ccif_advance_context(i);
	    *status = LOOP_CONTEXT;
	    
	    for ( p = rec->l.loop[block]->down->down, j = 1; p; p = p->right, j++ ) {  
	                            /* Not j = 0 etc. : skip the placeholder node at the start of each row */
               ptr = zzs_get(p->a->text);

	       /* Case where category is not completely described by dictionary */
	       if ( !ptr ) {
		 item_text = p && p->a->text ? p->a->text : "Unobtainable item name";
		 *status = BADCAT;
		 break;
	       }

               if ( ptr->category.rec != rec )
                  ccif_signal(CCIF_BADLOOP, "new_context", NULL, NULL, 0, 
			      ptr->node[block]->a->line);
               context[i].lookup[ptr->no_in_cat] = j;
               context[i].packet[j].rec = ptr;
            }
         }
	 else  {
	   /* non-looped context: here must rely on category having been marked as bad to pick up
	    * incompletely described categories */
	   if ( block < rec->id && rec->loop_offset[block] ) {
	     item_text = zzs_tmpsymname(rec->loop_offset[block]);
	     *status = BADCAT;
	   }
	   else {
	     *status = ITEM_CONTEXT;
	   }
	 }

	 /* Was this category completely described in dictionary? */
	 if ( *status == BADCAT ) {
	   free(context[i].lookup); 
	   context[i].lookup = NULL;
	   free(context[i].packet);
	   context[i].packet = NULL;
	   ccif_signal(CCIF_CATNOTCOMPLETE, "new_context", NULL, NULL, 0, 
		       zzs_recname(rec), item_text );
	   return (-1);
	 }
	 
         return i;
      }

   ccif_signal(CCIF_NOFREECONTEXTS, "ccif_new_context", NULL, NULL, 0);
   /* stop certain compilers moaning */
   return 0;
}


int ccif_advance_context(const int ncontext) 
{
   int i;
   AST *p;
   Rb_node rb_ptr=NULL;
   int block = context[ncontext].block, 
     tree_index = context[ncontext].tree_index;
   
   if ( ! (context[ncontext].category->l.loop[block]) )
     return 0;  /* Always return 0 for non-loop contexts. */

   if ( !context[ncontext].packet )
     return 0;


   if ( tree_index >= 0 ) {
     if ( !context[ncontext].tree_node )
       rb_ptr = ccif_first_node(tree_index);
     else {
       if ( context[ncontext].tree_node != ccif_last_node(tree_index) )
         rb_ptr = rb_next(context[ncontext].tree_node);
     }
     
     p = rb_ptr ? (AST *) rb_ptr->k.key : NULL;
     context[ncontext].tree_node = rb_ptr;
   }
   else
     p = context[ncontext].packet[0].loop_item->right;

   if ( p ) {
      context[ncontext].packet[0].loop_item = p;
      for (i = 1, p = p->down; 
           i <= context[ncontext].ncols  && p; 
           i++, p = p->right) 
         context[ncontext].packet[i].loop_item = p;
         
      if ( i != context[ncontext].ncols + 1 )
	ccif_signal(CCIF_PARTLOOP, "ccif_advance_context", NULL, NULL, 0,
         (context[ncontext].packet[1].loop_item)->a->line);
            
      return 1;
   }
   else
      return 0;
}


/* Free context */
void ccif_free_context(int * const ncontext) 
{
   if ( *ncontext >= 0 && *ncontext < NCONTEXTS ) {
      context[*ncontext].category = NULL;
      context[*ncontext].rwstat = u;
      context[*ncontext].ncols = 0;
      context[*ncontext].block = 0;
      context[*ncontext].tree_index = -1;
      context[*ncontext].tree_node = NULL;

      if ( context[*ncontext].packet ) {
	 free ( context[*ncontext].packet ) ;
	 context[*ncontext].packet = NULL ; 
      }
      if ( context[*ncontext].lookup ) {
	 free ( context[*ncontext].lookup ) ;
	 context[*ncontext].lookup = NULL;
      }
      *ncontext = -1;
   } 
#if 0
   /* If this is ever restored, make a new CCIF signal for it */
   else {
     fprintf(stderr,"ccif_free_context: Attempt to free out-of-range context!\n");
   }
#endif
}


/* Returns a copy of the context list entry */
CONTEXT_LIST ccif_context_list(const int ncontext) {
   return context[ncontext];
}

/* Makes sure that the information of contexts referring to a loop is up-to-date,
 * after the structure (number of columns) of the loop has been changed.
 * 
 * Also update the symbol table records' node and loop_offset members.
 * 
 * changed_context is a copy of the context entry which has been used to change the
 * loop. It is necessary to do this, because of the possibility of having multiple
 * contexts having being opened on the same category.
 */

void ccif_refresh_contexts(CONTEXT_LIST changed_context){
   int i, j, rows=0, cols=(-1), ovf=0, block=changed_context.block;
   AST *name_ptr, *loop_ptr, *loop_root_i,
     *loop_root = changed_context.category->l.loop[block];
   Sym * rec;
   
   if ( !loop_root ) {
      if ( !changed_context.packet ) {
	 return;  /* Not on a loop now, and was not previous to change. */
      }
   }
   else {
      
   /* Take care to account for the fact that changed_context may
    * have been converted from a seqence of data items, so ncols
    * of other contexts on the same category will not be accurate,
    * and packet[0] will need to be initialised.
    */

      LOOP_DIMENSION(loop_root, rows, cols, ovf)
   }

     
   for ( i = 0; i < NCONTEXTS; i++ ) {
      if ( context[i].category == changed_context.category && context[i].block == block ) {   /* context[i] needs
											       updating */
	 context[i].loop_add = changed_context.loop_add;
	 if ( cols != context[i].ncols || cols == 0 )   /* cols == 0 caters for special case of a brand new loop 
							 * cols == -1 caters for case when the last column in the
							 * loop was deleted, and whole loop structure removed from
							 * AST */
	   CCIF_REALLOC((cols + 1) , struct _packet, context[i].packet, \
                        "ccif_refresh_contexts", (0) )
	     
	 if ( !context[i].lookup && cols != -1 ) 
	    CCIF_CALLOC(context[i].category->no_in_cat, int, context[i].lookup, \
                  "ccif_refresh_contexts", (0))
	    
	 if ( cols == -1 ) {
	    context[i].packet = NULL;  /* has been free()'d by CCIF_REALLOC( (-1 + 1) ... above */
	    			       /* Keep lookup - may use it again later. If not, will be
					* free()'d when context released */
	    continue;
	 }
  

	 loop_root_i = context[i].category->l.loop[block];


	 if ( !rows && loop_root_i ) {         /* Newly created loop structure - don't attempt to
						* fill in packet information. */
	    context[i].packet[0].loop_item = loop_root_i->down;   /* Row containing data names */
	    context[i].packet[0].rec = NULL;
	    continue;
	 }
	 

	 /* Fixme! (not really for here) - must make sure that routine which converts data items
	  * to a loop updates category pointers */
		    
	 if ( !context[i].ncols ) {     /* newly-created loop - context should point at first row */
	    context[i].packet[0].loop_item = loop_root_i->down->right;
	    context[i].packet[0].rec = NULL;
	 }
	 
	 /* Refresh lookup and packet information */
	 memset (context[i].packet+1, 0, cols * sizeof (struct _packet) );    /* The +1 ensures that we still know
									         where the start of the packet is */

	 
	 for ( j = 1, name_ptr = loop_root_i->down->down, loop_ptr = context[i].packet[0].loop_item->down;
	       name_ptr;
	       j++, name_ptr = name_ptr->right, loop_ptr = loop_ptr->right ) {
	    rec = zzs_get(name_ptr->a->text);  /* Assume that this has already been checked */
	    context[i].packet[j].rec = rec;
	    context[i].lookup[rec->no_in_cat] = j;
	    context[i].packet[j].loop_item = loop_ptr;

	    /* Update symbol table records as well */
#if 0	    /* Not really - be consistent and make sure in struct_manip.c 
	       routines that they are correct */
	    rec->loop_offset[block] = j - 1;
	    rec->node[block] = loop_root_i;
#endif
	 }
	 
	 context[i].ncols = cols > 0 ? cols : 0;
      } /* if ( context[i].category == changed_context.category .... */
   }  /* for ( i = 0; i < NCONTEXTS; i++ ) */
}


/* Point context at new row. Cannot assume that sorting items define
 * each row uniquely, so calling routine is responsible for setting
 * rbnode correctly at the moment. (Might do something about this at
 * a later stage) */

void ccif_point_context ( const int ncontext, const AST * row, Rb_node rbnode ) {

  int n;
  const AST *r = row;

  if ( !r ) return;

  if ( r->token != Row )
    ccif_signal(CCIF_NEEDROW, "ccif_point_context", NULL, NULL, 0);

  if ( ncontext < 0 || ncontext >= NCONTEXTS )
    ccif_signal(CCIF_CXTRANGE, "ccif_point_context", NULL, NULL, 0, ncontext, NCONTEXTS);

  if ( rbnode != NULL && (AST *) rbnode->k.key != row )
    ccif_signal(CCIF_BADRBNODE, "ccif_point_context", NULL, NULL, 0);

    context[ncontext].packet[0].loop_item = (AST *) r;
    for ( n = 1, r = r->down; 
	  n <= context[ncontext].ncols; 
	  n++, r = r->right )
      context[ncontext].packet[n].loop_item = (AST *) r;

  if ( context[ncontext].tree_node ) 
    context[ncontext].tree_node = rbnode;

}


/* Actually associate the context with the sort list 
 * The calling routine is responsible for ensuring that
 * they refer to the same category/block */

void ccif_link_sort_to_context( const int index, const int ncontext, const int reset )
{
  int n;
  AST * node;
  Rb_node rbnode;

  context[ncontext].tree_index = index;

  if ( reset ) {                           /* Point at first node in sorted order */
    rbnode = ccif_first_node(index);
    if ( rbnode && rbnode->k.key)  {
      node = (AST *) rbnode->k.key;
      context[ncontext].tree_node = rbnode;
    }
    else
      node = NULL;
    
    node = rbnode && rbnode->k.key  ? (AST *) context[ncontext].tree_node->k.key : NULL;
    ccif_point_context ( ncontext, node, context[ncontext].tree_node );
  }
  else {

    /* Find node in tree which points to current row */
    context[ncontext].tree_node = 
      ccif_find_row(index, context[ncontext].packet[0].loop_item, &n);
    if ( !n )
      ccif_signal(CCIF_NOROWFIND, "ccif_link_sort_to_context", NULL, NULL, 0);

    node = context[ncontext].packet[0].loop_item;
    rbnode = context[ncontext].tree_node;
    if ( (AST *) rbnode->k.key != node ) 
      context[ncontext].tree_node = ccif_find_rb_node(index, rbnode, node);
  }
}


/* Return number of contexts using a sort list */

int ccif_sort_link_number(const int index) {
  int i, n = 0;
  for (i = 0; i < NCONTEXTS; i++)
    if ( context[i].tree_node && context[i].tree_index == index ) n++;
  return (n);
}


/* If a row has changed position in a red-black tree, update
 * any contexts which happen to point to this row, and are
 * using the same tree for sorted access */

void ccif_resort_context(const int sort_index, const AST * node, Rb_node rbnode) {
  int i;
  
  for (i = 0; i < NCONTEXTS; i++) {
    if ( context[i].tree_index == sort_index &&
        context[i].packet  &&
	context[i].packet[0].loop_item == node )
      ccif_point_context (i, node, rbnode);
  }
}

void ccif_unlink_sort(const int index, const int reset) {
  int i, j, block;
  Sym *cat;
  AST *node;

  if ( reset ) {
    for ( i = 0; i < NCONTEXTS; i++) {
      if ( context[i].tree_node && context[i].tree_index == index ) {

#ifdef DEBUG
	printf("ccif_unlink_sort: Unlinking/resetting context %d from sort %d\n",
	       i, context[i].tree_index);
#endif

        context[i].tree_node = NULL;
        context[i].tree_index = -1;
        block = context[i].block;
        cat = context[i].category;
        node = cat->l.loop[block]->down->right;

	ccif_point_context(i, node, NULL);

      }   /* if ( context[i].tree_node && context[i].tree_index == index ) */
    }     /* for ( i = 0; i < NCONTEXTS; i++) */

  }
  else {
    for ( i = 0; i < NCONTEXTS; i++) {
      if ( context[i].tree_node && context[i].tree_index == index ) {

#ifdef DEBUG
	printf("ccif_unlink_sort: Unlinking context %d from sort %d\n",
	       i, context[i].tree_index);
#endif

        context[i].tree_node = NULL;
        context[i].tree_index = -1;
      }
    }
  }   /* if (reset) */
}


#ifdef DEBUG
void ccif_print_row(char callback_msg[], void **ptr, 
		    int *count, int first) {
  
  static const AST *name;
  static int block, item;
  
  if ( first ) {
    sprintf(callback_msg, "Current row of context %d is:", *count);
    block = context[*count].block;
    name = context[*count].category->l.loop[block]->down->down;
    item = 1;
    return;
  }
  
     if ( name ) {
       sprintf(callback_msg, "%s %s", name->a->text,
	       context[*count].packet[item].loop_item->a->text);
       item++;
       name = name->right;
       return;
     }
  
  callback_msg[0] = '\0';
}

#endif /* DEBUG */










/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* Routines which modify the AST representation of the CIF.
 * N.B. All routines which add or remove data items from the AST
 * should update the symbol table entry pointers {node,last,loop_offset}
 * for the category and item. */

#include "stdpccts.h"

/* Need typedefs */
#include "sorcerer.h"
#ifdef ESV
#include <memory.h>
#endif

#include "astlib.h"

#include "struct_manip1.h"
#include "struct_manip2.h"

static AST empty_node;  /* Null-content node for initialisation */

static struct attrib ccif_null  = {-1, 0, 1, 0, 0, "." , NULL, NULL, NULL, constant},
              ccif_unknown      = {-1, 0, 1, 0, 0, "?" , NULL, NULL, NULL, constant};


__import__ int ccif_number_of_blocks;

/* Routines to change the structure of a CIF (add data blocks, etc.) */


/* Define placeholder token for start of STAR file. It is the
 * responsibility of the calling module to ensure that the status
 * of the file is w!
 */

void ccif_new_star_file ( DATA_BLOCK_LIST * const block_list_entry ) {

/* zzastnew() is used to calloc memory for the node, so its pointers will be NULL */

   if ( !block_list_entry->root ) 
     block_list_entry->root = ast_node(Star_file, NULL);
   return;
}

/* Set up new data blocks.
 * N.B. (1) data_block_name's must already have "data_" prefix. 
 *
 * (2) It is the responsibility of the calling module to ensure that the status
 * of the file is w!
 * 
 * (3) It is also the responsibility of the calling module to call 
 * ccif_add_blocks_to_lookup() after returning, to ensure that the
 * lookup array is up-to-date!
 */

void ccif_new_data_blocks ( DATA_BLOCK_LIST * const block_list_entry, 
			  char *data_block_name[], const int nblocks) {
   
   Attrib a;
   int i, j;
   AST dummy_node = empty_node;

   if ( nblocks < 1 ) return;
   
   CCIF_REALLOC((block_list_entry->nblocks+nblocks), int, \
      block_list_entry->block_id, "ccif_new_data_blocks", 0)
   CCIF_REALLOC((block_list_entry->nblocks+nblocks), AST *, \
      block_list_entry->block_node, "ccif_new_data_blocks", 0)
   CCIF_REALLOC((block_list_entry->nblocks+nblocks), int, \
      block_list_entry->audit, "ccif_new_data_blocks", 0)
    
   if ( !block_list_entry->root ) ccif_new_star_file(block_list_entry);
   
/* Make sibling list of new data blocks */
   for ( i = block_list_entry->nblocks, j = 0 ; 
	j < nblocks; 
	i++, j++) {
      
     if ( strlen(data_block_name[j]) > MAXCLLEN ) {
       ccif_signal(CCIF_DBNAMELEN, "ccif_new_data_blocks", NULL, NULL, 0, data_block_name[j], MAXCLLEN);
       data_block_name[j][MAXCLLEN] = '\0';
     }
   

     zzcr_attr( &a, Data_heading, data_block_name[j] );            /* Create attribute */
     block_list_entry->block_node[i] = ast_node(Data_heading, a);  /* Make node */
     block_list_entry->block_id[i] = ccif_number_of_blocks++;
     ast_append(&dummy_node, block_list_entry->block_node[i]);
     
   }
   
/* Add to root (if no data blocks already exist) or on to end of existing data blocks */
   if ( !block_list_entry->nblocks )
     ast_make(block_list_entry->root, block_list_entry->block_node[0], NULL );
   else
     ast_append( ast_bottom(block_list_entry->root), block_list_entry->block_node[ block_list_entry->nblocks ] );

   block_list_entry->nblocks += nblocks;

}

/* Adds new data name to context, pointing value node to a
 * (or with a value of unknown, if a == NULL).
 * Calling routine has responsibility for checking that 
 * data_name is a valid name, and does not exist as a data item
 * already.
 * 
 * context will usually be a copy of the true context entry, obtained in the calling 
 * routine by ccif_context_list()
 * 
 * After this routine has been called (perhaps multiple times ON THE SAME context),
 * ccif_refresh_contexts() MUST be called to make sure that the contents of the static
 * context information in value_manip.c is up-to-date! This is because this routine
 * only changes the structure of the AST and the node pointer in the symbol table
 * record.
 */
void ccif_new_data_item( char * const new_data_name,
			const int token,
			const Attrib a,
			const CONTEXT_LIST * const context ) {

   
   Attrib name;

#ifdef UNKNOWN_EQ
   Attrib ccif_init_attrib = &ccif_null;
#else
   Attrib ccif_init_attrib = &ccif_unknown;
#endif

   Sym *item = zzs_get(new_data_name);
   int rows, cols=0, ovf=0, 
     block = context->block;
   char attrib_buf[MAXCLLEN+1];
   AST *ptr, *data_block = ccif_data_heading_lookup(block), 
     
				       /* This initialisation is OK, because
					* ccif_new_category will have been called
					* first, if the category does not already
					* exist in the data block. This ensures that 
					* category->{node,loop,last} have had enough
					* elements (re)alloc'd */
     *loop_root = context->category->l.loop[block];

   if ( item->id > block && item->node[block] ) 
     return;    /* Already present in data block */
   
/*   loop_root = (loop_root && loop_root->token == Loop) ? loop_root : NULL; */

   if ( context->rwstat != w ) 
     ccif_signal(CCIF_ROCXTADD, "ccif_new_data_item", NULL, NULL, 0, ccif_file_lookup(block), new_data_name );

   if ( !a && item->width ) {
#ifdef UNKNOWN_EQ
     sprintf(attrib_buf, "%*c", item->width, '.');
#else
     sprintf(attrib_buf, "%*c", item->width, '?');
#endif
     zzcr_attr(&ccif_init_attrib, Non_quoted_text_string, attrib_buf);
   }
   
                         /***********************/
   if ( !loop_root ) {   /* Data item structure */
                         /***********************/

      /* Create attribute for data_name */
      zzcr_attr(&name, Data_item, (char *) new_data_name);   
      name->col = 1;		       /* Style - start a new line */

      /* Make Data_item structure */
      if ( a ) {
	 ptr = ast_make(ast_node (Data_item, name),
			ast_node (token, a),
			NULL
		       );
      }
      else {
	 ptr = ast_make(ast_node (Data_item, name),
			ast_node (Non_quoted_text_string, ccif_init_attrib),
			NULL
			);
      }
	 
      if ( context->category->node[block] ) {     /* If there is already at least one data item from
						  * this category present in the data block, add new
						  * item after last one */
	 ast_insert_after(context->category->last[block], ptr);
      }
      else {                                      /* Otherwise, add to end of data block */
	 if ( !data_block->down )   /* Empty data block */
	   ast_make(data_block, ptr, NULL);
	 else
	   ast_append(data_block->down, ptr);
	 context->category->node[block] = ptr;
      }
      context->category->last[block] = ptr;
   }				       /*  if ( !loop_root ) */
                                       

                                       /*********************************************/
   else { 			       /* Loop structure - add new rightmost column */
                                       /*********************************************/
      int new_cols;
      int block_token = a ? token : Non_quoted_text_string;
      Attrib block_a = a ? a : ccif_init_attrib;
      ptr = loop_root;

      /* Increment count of columns which is stored in AST. 
         Don't increment cols - it doubles up as loop_offset 
         for the new item below.
       */
      LOOP_DIMENSION(ptr, rows, cols, ovf)
      new_cols = cols + 1;
      SET_LOOP_DIMENSION(ptr, rows, new_cols, ovf) 

      /* Create attribute for data_name */
      zzcr_attr(&name, Data_name, new_data_name);
      name->col = 1;
      
      /* Add data name to end of loop_header, and Attrib *a or unknown value to each row */
      ptr = ptr->down;
      if ( ptr->down ) {
	 ast_append(ptr->down, ast_node (Data_name, name) );
	 for ( ptr = ptr->right; ptr; ptr = ptr->right ) {
/*
	    Attrib a;
	    zzcr_attr(&a, Non_quoted_text_string, "?");
	    ast_append(ptr->down, ast_node (Non_quoted_text_string, a) );
*/
	    ast_append(ptr->down, ast_node (block_token, block_a) );
	    
	    block_a->col = cols ? 0 : 1 ;       /* Style - if this is the first column, start on a new row */
	 }
      }
      else {
	ast_make(ptr, ast_node (Data_name, name), NULL );
	 for ( ptr = ptr->right; ptr; ptr = ptr->right ) {
/*
	    Attrib a;
	    zzcr_attr(&a, Non_quoted_text_string, "?");
*/
	    ast_make(ptr, ast_node (block_token, block_a), NULL );
	    block_a->col = cols ? 0 : 1 ;       /* Style - if this is the first column, start on a new row */
	 }
      }
      
      ptr = loop_root;
      
      /* If this was the first column in the loop, and we have at least one row, 
       * we need to attach it to the ast */
      if ( new_cols == 1 && rows > 0 ) {
	 

	 if ( context->category->node[block] ) {     /* If there is already at least one data item from
						      * this category present in the data block, add new
						      * loop after last one */
	    ast_insert_after(context->category->last[block], ptr);
	 }
	 else {
	    if ( !data_block->down )   /* Empty data block */
	      ast_make(data_block, ptr, NULL);
	    else
	      ast_append(data_block->down, ptr);
	    
	    context->category->node[block] = loop_root;
	    context->category->last[block] = loop_root;
	 }
	 context->category->l.loop[block] = loop_root;
      }
      
   }  /* if ( !loop_root ) */
   
   /* Update symbol table record */
   
   /* Enlarge areas to store node pointers and loop offsets if necessary */
   zzs_expand_item_record(item, block+1);

   item->loop_offset[block] = cols;
   item->node[block] = ptr;
}
      
		 
/* Set up structures for new data category. context is a copy of the
 * context list entry. If it has set up a new category as a loop, returns
 * a pointer to the loop_ node, otherwise NULL.
 * 
 * Call ccif_refresh_contexts() on return!
 */

AST * ccif_new_category (CONTEXT_LIST * const context, int loop) {
   
   int block = context->block;
   Attrib loop_attr;
   AST *ptr=NULL, *data_block = ccif_data_heading_lookup(block);

   
   if ( context->rwstat != w ) 
     ccif_signal(CCIF_ROCXTADD, "ccif_new_category", NULL, NULL, 0, 
	      ccif_file_lookup(block), zzs_recname(context->category) );

   zzs_expand_item_record(context->category, block+1);
   
   if ( loop ) {
      
      /* Create attributes for loop */
      zzcr_attr(&loop_attr, Loop, "loop_");
      
      /* Create embryonic loop structure */
      ptr = ast_make(ast_node (Loop, loop_attr),
		     ast_node (Row, NULL),
		     NULL
		    );
      ptr->down->ptr = (void *) ptr;

      CCIF_CALLOC(1, ccif_loop_dim, ptr->ptr, "ccif_new_category", (0))
      SET_LOOP_DIMENSION(ptr, 0, 0, 0);

      context->category->l.loop[block] = ptr;

#if 0  /* New loop is now only attached to AST when it actually contains data */
      /* Attach to end of data block */
      if ( !data_block->down )   /* Empty data block */
	ast_make(data_block, ptr);
      else
	ast_append(data_block->down, ptr);
#endif /* 0 */

   } /* if ( loop ) */
      
#if 0
   context->category->node[block] = ptr;
   context->category->last[block] = ptr;
#endif
   return (ptr);
}


/* Removes nodes associated with data_name from AST completely.
 * Call ccif_refresh_contexts after return!
 * It is the responsibility of the calling routine to check that
 * the context is open for writing!
 */


void ccif_del_data_item( const char * const data_name,
			 const CONTEXT_LIST * const context)
{

   Sym *cat = context->category, *item = zzs_get(data_name), *item2;
   int block = context->block;
   int left=0, right=0, up=0, rows, cols, ovf, i;
   AST *node, *new_first, *new_last, *row_ptr, *val_ptr, *ptr;
   
   
   if (!item) 
     ccif_signal(CCIF_NOREC, "ccif_del_data_item", NULL, NULL, 0, data_name);
     
   if ( item->id <= block || !item->node[block] ) return;   /* Not in data block anyway */
   
   cat = item->category.rec;
   node = item->node[block];
   

/* Set these, if the first/last pointers in the symbol table record for the 
 * category will need updating */
   
   new_first = (node == cat->node[block]) ? node->right: NULL;
   new_last  = (node == cat->last[block])  ? node->left: NULL;

   
                                               /*************/
   if ( item->node[block]->token != Loop ) {   /* Data item */
                                               /*************/
      
      if (node->left)   node->left->right = node->right;
      if (node->right)  node->right->left = node->left;
      if (node->up)     node->up->down = node->right;
      
      node->right = ( node->left = NULL );
      ccif_ast_free(node);
   }
   
                                       /***************/
   else { 			       /* Looped data */
                                       /***************/
      LOOP_DIMENSION(node, rows, cols, ovf);
      
      if ( cols == 1 ) {	       /* Remove loop completely */
	 if (node->left)   node->left->right = node->right;
	 if (node->right)  node->right->left = node->left;
	 if (node->up)     node->up->down = node->right;

	 cat->l.loop[block] = NULL;
	 node->right = ( node->left = NULL );
	 ccif_ast_free(node);
	 
      }
      else {			       /* if ( cols == 1 ) */
	 
	/* In this case, if the loop_ node is the first or last data structure
	 * for the category in this block, don't update first/last pointers */
	 new_first = NULL;
	 new_last = NULL;
	 
	 row_ptr = node->down;
	 for (val_ptr = row_ptr->down, i = 0;
	      i < item->loop_offset[block];
	      val_ptr = val_ptr->right, i++ ) {;}
	 
	 right = val_ptr->right ? 1 : 0;
	 left  = val_ptr->left  ? 1 : 0;
	 up    = val_ptr->up    ? 1 : 0;

	 /* Update loop_offsets */
	 for (ptr = val_ptr->right;
	      ptr;
	      ptr = ptr->right ) {
	    item2 = zzs_get(ptr->a->text);
	    if (item2) (item2->loop_offset[block])--;
	 }
	   
	 do {
	    
	    if ( right ) val_ptr->right->left = val_ptr->left;
	    if ( left )  val_ptr->left->right = val_ptr->right;
	    if ( up ) {
	       val_ptr->up->down    = val_ptr->right;
	       if ( right ) val_ptr->right->up = val_ptr->up;
	    }
	       
	    /* In principle, should check node->ptr. In practice (AT THE MOMENT),
	     * only loop_ nodes have ptr's */
	    zztfree(node);

	    row_ptr = row_ptr->right;
	    
	    if ( !row_ptr ) break;
	    
	    for (val_ptr = row_ptr->down, i = 0; 
		 i < item->loop_offset[block]; 
		 val_ptr = val_ptr->right, i++ ) {;}
	 } while (1) ;
	 
	 SET_LOOP_DIMENSION(node, rows, cols-1, ovf);
      }				       /* if ( rows == 1 ) */
      
   }			/*  if ( item->node[block]->token != Loop )  */

   item->node[block] = 0;
   item->loop_offset[block] = 0;
   
   if ( new_first && new_last ) {      /* This was the last node from the category 
					* in this data block */
      cat->node[block] = NULL;
      cat->last[block] = NULL;

   }
   else {
      /* Don't trust either cat->node[block] or cat->last[block] in this branch */
      if ( new_first ) {
         /* This horrible condition says:
	  * 
	  * loop while (new_first != NULL)  and ( ( if new_first is a loop, it is not the loop which
						    belongs to our category)
						 or ( if new_first is a data item, it does not belong 
						     to our category)
						 )
	  * At the end of the loop, new_first should point to the block's new first item/loop in our
	  * category. If it is NULL, something has gone very wrong.
	  */
	 for ( ; new_first &&
	         ( (new_first->token == Loop && new_first != cat->l.loop[block]) ||
	           (new_first->token == Data_item && (zzs_get(new_first->a->text))->category.rec != cat)
		 ) ;
	      new_first = new_first->right)
	  ;
	    
	 if ( new_first )
	   cat->node[block] = new_first;
	 else 
	   ccif_signal(CCIF_BADCAT, "ccif_del_data_item", NULL, NULL, 0, zzs_recname(cat));
       }				       /* first "if ( new_first )" */

      if (new_last) {
	 /* Corresponding condition for last in category */
	 for ( ; new_last &&
	         ( (new_last->token == Loop && new_last != cat->l.loop[block]) ||
	           (new_last->token == Data_item && (zzs_get(new_last->a->text))->category.rec != cat)
		 ) ;
	      new_last = new_last->left )
	   ;
	 
	 if ( new_last )
	   cat->last[block] = new_last;
	 else 
	   ccif_signal(CCIF_BADCAT, "ccif_del_data_item", NULL, NULL, 0, zzs_recname(cat));
      }				       /*  first "if (new_last)" */
   }				       /*  if ( new_first && new_last ) */
   
}



/* Ensures that all data items in the category are within a loop
 * structure. If a loop structure does not exist for the
 * category already, the sequence of data items are converted
 * to a loop with one row.
 *
 * This routine returns a pointer to the loop structure.
 * 
 * In this case, the calling routine MUST call 
 * ccif_refresh_contexts(), to make sure that the
 * contexts are updated, before doing any operations on the 
 * loop!
 */
 


AST * ccif_data_items_to_loop ( CONTEXT_LIST * const context ) {
   
   Sym *cat = context->category, *item;
   int j, block = context->block, token, loop_add, was_last;
   char name[MAXCLLEN+1];
   AST *loop_root = context->category->l.loop[block],
       *cursor;

   static AST *null_data_item;
   
   static Attrib loop_comment, loop_attr, a_ptr;


   if ( ( cat->node[block] == loop_root ) && 
       (cat->last[block] == loop_root ) ) return (loop_root); /* All items are already in a loop */


   if ( !cat->key.rec ) 
     ccif_signal(CCIF_NOKEY, "ccif_data_items_to_loop", NULL, NULL, 0,  zzs_recname(cat) );

   if ( !null_data_item ) 
      null_data_item = ast_node(Data_item, NULL);

   
   if ( ! cat->l.loop[block] ) {
      zzcr_attr(&loop_comment, Comment, "# STAR loop structure created from data items by CCIF");
      loop_comment->col = 1;     /* Style */

      /* Create attributes for loop */
      zzcr_attr(&loop_attr, Loop, "loop_");
      loop_attr->col = 1;
      
      loop_attr->prev = loop_comment;
      loop_comment->next = loop_attr;
      
      /* Create embryonic loop structure - with one blank row
       * (This is different to when you create a new category) */
      loop_root = ast_make(ast_node (Loop, loop_attr),
			   ast_node (Row, NULL),
			   ast_node (Row, NULL),
			   NULL
			   );

      CCIF_CALLOC(1, ccif_loop_dim, loop_root->ptr, "ccif_data_items_to_loop", (0))
	SET_LOOP_DIMENSION(loop_root, 1, 0, 0);

      loop_root->down->ptr        = (void *) loop_root;
      loop_root->down->right->ptr = (void *) loop_root;
      context->category->l.loop[block] = loop_root;
      
      
   }
   else
     loop_comment = NULL;

   
   cursor = cat->node[block];
   
/* Make sure that all items go into loop */
   loop_add = context->loop_add;
   context->loop_add = 1;
   
   do {
      was_last = (cursor == cat->last[block]);

      if ( (cursor->token != Data_item) || ( (zzs_get(cursor->a->text))->category.rec != cat ) ) {
	 cursor = cursor->right;
	 continue;
      }
      
      a_ptr = cursor->down->a;	       /* Preserve value's attrib */
      cursor->down->a = NULL;
      (a_ptr->nnodes)--;
      strcpy(name,cursor->a->text);
      token = cursor->down->token;

      cursor = cursor->right;	       /* This AST will be zapped by the next statement */
      ccif_del_data_item(name, context);
      ccif_new_data_item(name, token, a_ptr, context);
      
   } while ( ! was_last );

   ccif_refresh_contexts(*context);
   
   /* Restore loop_add default */
   context->loop_add = loop_add;
   
   return (loop_root);
}
    

void ccif_add_row(const int ncontext) {
   
   int j, rows, cols, ovf;
   Attrib a;
   Sym *item;
   char attrib_buf[MAXCLLEN+1];
   CONTEXT_LIST context = ccif_context_list(ncontext);
   int block = context.block;
   AST *node, dummy_node, *item_node,
     *loop_root = context.category->l.loop[block];
   
/*   loop_root = (loop_root->token == Loop) ? loop_root : NULL; */
   
   if ( !loop_root ) ccif_signal( (CCIF_NOTLOOPED & ~CCIF_M_SEVERITY | CCIF_K_SEVERE), 
		       "ccif_add_row", NULL, NULL, 0, 
		       zzs_recname(context.category), context.block);

   
   for ( j = 0, node = &dummy_node, node->right = NULL, item_node = loop_root->down->down; 
	 j < context.ncols; 
	 j++, item_node = item_node->right ) {
     /*       zzcr_attr(&a, Non_quoted_text_string, "?"); */

     /*      zzcr_attr(&a, Non_quoted_text_string, "."); */

      item = zzs_get(item_node->a->text);

#ifdef UNKNOWN_EQ
      sprintf(attrib_buf, "%*c", item->width, '.');  
#else
      sprintf(attrib_buf, "%*c", item->width, '?');  
#endif

      zzcr_attr(&a, Non_quoted_text_string, attrib_buf);

      ast_append(node, ast_node(Non_quoted_text_string, a) );
      node = ast_tail(node);
   }

   context.packet[0].loop_item = ast_tail(loop_root->down);

   ast_append(context.packet[0].loop_item, ast_node(Row, NULL));
   context.packet[0].loop_item = context.packet[0].loop_item->right;
   
   if ( dummy_node.right )
     ast_make ( context.packet[0].loop_item, dummy_node.right, NULL );
   context.packet[0].loop_item->ptr = (void *) loop_root;
   
   
   LOOP_DIMENSION(loop_root, rows, cols, ovf)
   rows++;
   SET_LOOP_DIMENSION(loop_root, rows, cols, ovf)

   /* If this was first row, and there is at least one column, we need to
    * attach this loop to the AST*/
   if ( rows == 1 && cols > 0 ) {
      AST *data_block = ccif_data_heading_lookup(block);
      if ( !data_block->down )   /* Empty data block */
	ast_make(data_block, loop_root, NULL);
      else
	ast_append(data_block->down, loop_root);
   }
   
/*   ccif_advance_context(ncontext); */
   ccif_add_to_sorts(context.packet[0].loop_item, context.category, block);
   ccif_refresh_contexts(context);
}
      
   
/* Includes ast_free from astlib, but uses zztfree to free
   node contents and nodes (zztfree includes zzd_ast if defined */

void ccif_ast_free(SORAST * t) {

   AST *u, *v;

#ifdef DEBUG
   static int nreq;
   if ( t && t->token==Star_file ) nreq = 0;
#endif

   if ( t == NULL ) return;
#ifdef DEBUG
   nreq++;
   if ( !(nreq % 500) ) printf("%d calls to ccif_ast_free this file. \n", nreq);
#endif

#if 0    
   ccif_ast_free( t->ast_down );
   ccif_ast_free( t->ast_right );

   if ( t->ptr ) { /* Auxilary data to be freed.
		    * Action depends on type of node */
      
      switch ( t->token ) {
       case ( Loop ):
	 free(t->ptr);
      }
   }

   zztfree(t);

#else

   for ( u = t; u ; ) {
      v = u->ast_right;
      if ( u->ast_down ) ccif_ast_free (u->ast_down);
      

   
   if ( u->ptr ) { /* Auxilary data to be freed.
		    * Action depends on type of node */
      
      switch ( u->token ) {
       case ( Loop ):
	 free(u->ptr);
      }
   }

   zztfree(u);

   u = v;
   }

#endif

}

/* Includes zzdup_ast from ast.c, but updates the number of nodes
   field in the attribute.
 * Also, copies rows, cols from loop nodes, and loop pointers from
 * Row nodes
 */

AST *
#ifdef __STDC__
ccif_ast_dup(AST *t)
#else
ccif_ast_dup(t)
AST *t;
#endif
{
   AST *u;
   static AST *loop_root;
   int rows, cols, ovf;

   if ( t == NULL ) return NULL;
   u = zzastnew();
   *u = *t;			       /* copy contents */
   
   /* Copy contents of general node pointer */
   if ( t->ptr ) {
      switch ( t->token ) {
       case ( Loop ):

	 /* Don't copy information about sort order to new tree. Need to do this again
	  * from the application level, if required. */

	 CCIF_CALLOC(1, ccif_loop_dim, u->ptr, "ccif_ast_dup", (0))
	   LOOP_DIMENSION(t, rows, cols, ovf)
	   SET_LOOP_DIMENSION(u, rows, cols, ovf)
	 loop_root = u;
	 break;
	 
       case ( Row):
	 u->ptr = (void *) loop_root;
      }
   }
   
   
   if ( t->a ) (t->a->nnodes)++;
   ADD_TO_MAP_REC(t);
#ifdef zzAST_DOUBLE
   u->up = NULL;		       /* set by calling invocation */
   u->left = NULL;		       /* set by calling invocation */
#endif
   u->right = ccif_ast_dup(t->right);
   u->down = ccif_ast_dup(t->down);
#ifdef zzAST_DOUBLE
   if ( u->right!=NULL ) u->right->left = u;
   if ( u->down!=NULL ) u->down->up = u;
#endif
   if ( loop_root ) loop_root = NULL;
   return u;
}



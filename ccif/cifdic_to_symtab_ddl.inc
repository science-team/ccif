                                                              /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* DDL categories, data names and fucntions which are used to construct the symbol table from the
 * dictionary. This is 'DDL knowledge'.
 *
 * If, in the future, many DDL items and categories are used, it may be better to construct
 * a DDL symbol table directly from the DDL proper, to interpret the dictionary. Only the
 * part of the symbol table which is derived from the dictionary itself would need to be dumped
 * to disk, in that case.
 *
 * Put category names first. Keep the aliasing item last, so that as much information as possible
 * can be copied from the item->true_name record.
 */


static void set_item_type ( Sym * item );
static void add_alias_name (Sym *  item, char * data_field, AST *  val) ;
static void add_parent_link (  Sym * item,  char * data_field, AST * val ) ;
static void add_type_code ( Sym *  item, char * data_field, AST *  val ) ;
static void add_default_value( Sym *  item, char * data_field, AST *  val) ;
static char * unknown_default_value( Sym * const item ) ;
static void add_mandatory_code ( Sym* item, char * data_field, AST * val ) ;
static void add_category_pointer( Sym *item, char * data_field, AST *val) ;
static char * set_cat_ptr_implicitly ( Sym * const item ) ; 
static char * do_nothing ( Sym *const item ) ;
static void do_nothing4 (  Sym *item, char * data_field, AST *val) ;
static void add_condition_code ( Sym * item, char * data_field, AST * val);
static void add_function_code ( Sym * item, char * data_field, AST * val);
static void add_related_name ( Sym * item, char * data_field, AST * val);
static void add_category_key_name ( Sym * cat, char * data_field, AST * val);


static char * item_name_implicit(const AST * const save_frame) ;

/*
This array of structures contains two types of records:

(1) A record from which the symbol table entry to be updated is to be found. This
will usually be the key item for the DDL category concerned (i.e. _item.name or one
of its children).

(2) ... followed by a number of records which specify the DDL data names which
are used to get at the information which is to be put into the symbol table entry.

There are two different interpretations of the items in each record, to reflect this.
("item" in this context can refer to dictionary categories, as well as dictionary
items).

As items are added to this list, the structure of a symbol table entry (in sym.h)
may need to be changed, and also the add_alias function, so that records for 
aliases contain all the necessary information from the true item.
*/

static struct _dict_item_props {
   char *name;     /* (1)  Name of item which contains name of symbol table record to be updated.
                           Usually key item in category.
                       (2) Name of DDL item which contains information to be added to symbol table.
                   */
   enum item_mandatory_code mandatory_code;  /* (1) Mandatory code for DDL CATEGORY (item itself is always implicit)
                                                (2) Mandatory code for DDL item
                                             */
   void (*specified)();                      /* (1) Always NULL - test for this to distinguish between
                                                    (1) and (2)
                                                (2) Void function to add information into symbol table.
                                             */
   char * (*unspecified)();                  /* (1) Function to return pointer to implictly derived item name.
                                                (2) Function to update symbol table record when value of DDL item
                                                    is not specified. (Remember to return (char *) NULL). The
                                                    action to be taken depends on mandatory_code:
                                                 If mandatory_code is implicit, function to add item implicitly
                                                 If mandatory_code is no, function to set item to '?'
                                                 If mandatory_code is yes, NULL
                                              */
                                                    
} save_frame_items[] = {

{"_item.name",                    no,         NULL,                    item_name_implicit},
{"_item.category_id",             implicit,   add_category_pointer,    set_cat_ptr_implicitly},
{"_item.mandatory_code",          yes,        add_mandatory_code,      NULL}, 
{"_item_linked.child_name",       undefined,  NULL,                    item_name_implicit},
{"_item_linked.parent_name",      undefined,  add_parent_link,         do_nothing},
{"_item_type.name",               no,         NULL,                    item_name_implicit},
{"_item_type.code",               yes,        add_type_code,           NULL},
{"_item_default.name",            no,         NULL,                    item_name_implicit},
{"_item_default.value",           no,         add_default_value,       unknown_default_value},
{"_item_type_conditions.name",    no,         NULL,                    item_name_implicit},
{"_item_type_conditions.code",    yes,        add_condition_code,      NULL},
{"_item_related.name",            no,         NULL,                    item_name_implicit},
{"_item_related.related_name",    yes,        add_related_name,        NULL},
{"_item_related.function_code",   yes,        add_function_code,       NULL},
{"_item_aliases.name",            no,         NULL,                    item_name_implicit},
{"_item_aliases.alias_name",      yes,        add_alias_name,          NULL},
{"_item_aliases.dictionary",      yes,        do_nothing4,             NULL},
{"_item_aliases.version",         yes,        do_nothing4,             NULL},

/* _category.id comes twice, because it is a key item for the DDL category CATEGORY, and its value also
 points at the dictionary category (if that isn't too confusing to be useful....) */
{"_category.id",                  no,         NULL,                    item_name_implicit},
{"_category.id",                  yes,        add_category_pointer,    NULL},
{"_category.implicit_key",        implicit,   do_nothing4,             do_nothing},  /* for now */
{"_category.mandatory_code",      yes,        add_mandatory_code,      NULL},
{"_category_key.id",              no,         NULL,                    item_name_implicit},
{"_category_key.name",            yes,        add_category_key_name,   NULL},
{NULL,                            undefined,  NULL,                    NULL}
};


static int search_for_data_block ( const char * const datablock );
static void * add_dict_entries (void *, const AST * const );
static void * end_dict_entry ( const void *, const int);
static void * add_dict_title ( void * const, const AST *);
static void * add_dict_version ( void * const, const AST *);
static void * add_dict_db ( void * const, const AST *);
static void * dict_db_implicit (void * const ptr);

static void * add_type_entries ( void *, const AST * const);
static void * end_type_entry ( void * , const int);
static void * add_code_to_type_list ( void * const, const AST *);
static void * add_pcode_to_type_list (void * const, const AST *);
static void * add_constr_to_type_list ( void * const, const AST *);
static void * no_construct ( void * const );
static void * do_nothing_2 ( void * const, const AST *);
static void * do_nothing_3 ( void * const);

static struct _dictionary_props {

   char *name;
   enum item_mandatory_code mandatory_code;
   void  *(*specified)(), *(*unspecified)();  /* use these for start and end functions, for categories:
                                                (*specified)() allocates a block of memory (return void *)
                                                (*unspecified)() advances the pointer for each row */
   SList **category_list;
   
} dictionary_items[] = {

{"DICTIONARY",                    yes,        add_dict_entries,         end_dict_entry,   ccif_dictionary},              
{"_dictionary.title",             yes,        add_dict_title,           NULL,             NULL},
{"_dictionary.version",           yes,        add_dict_version,         NULL,             NULL},
{"_dictionary.datablock_id",      implicit,   add_dict_db,              dict_db_implicit, NULL},
{"ITEM_TYPE_LIST",                no,         add_type_entries,         end_type_entry,   ccif_item_type_list},
{"_item_type_list.code",          yes,        add_code_to_type_list,    NULL,             NULL},
{"_item_type_list.primitive_code",yes,        add_pcode_to_type_list,   NULL,             NULL},
{"_item_type_list.construct",     no,         add_constr_to_type_list,  no_construct,     NULL},
{"_item_type_list.detail",        no,         do_nothing_2,             do_nothing_3,     NULL},
{NULL,                            undefined,  NULL,                     NULL,             NULL}
};


/*
 * A n t l r  S e t s / E r r o r  F i l e  H e a d e r
 *
 * Generated from: star_map.g
 *
 * Terence Parr, Russell Quong, Will Cohen, and Hank Dietz: 1989-1999
 * Parr Research Corporation
 * with Purdue University Electrical Engineering
 * With AHPCRC, University of Minnesota
 * ANTLR Version 1.33MR22
 */

#define ANTLR_VERSION	13322
#include "ccif_defines.h"
#include "pcctscfg.h"
#include "pccts_stdio.h"

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

#include "library.h"
#include "import_export.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

/* Allow disabling of mapping with -D_USE_MMAP=0, but not enabling
if we really don't have it */
#if !defined (_USE_MMAP) && defined (HAVE_MMAP)
#define _USE_MMAP 1
#endif

#if  !defined (HAVE_MMAP)
#define _USE_MMAP 0
#endif

/* In practice, every supported system has the newer mechanism... */
#include <stdarg.h>

#ifdef HAVE_REGEX_H
#include <regex.h>
#else
#include <rxposix.h>   /* from rxdispencer */
#include <rxgnucomp.h>
#endif

#define zzAST_DOUBLE /* At the moment, we only set the up/left pointers
* for AST's of writeable CIF's */

#include "ccif_malloc.h"
#include "rb.h"

#include "charstru.h"

#include <errno.h>
#ifndef errno      /* Some implementations might have already declared this */
extern int errno;  /* as a macro. For those which haven't, a second, consistent */
#endif             /* extern declaration won't cause any problems */

/* NEVER use assert for "normal" ccif stuff. Use the mechanism
in condition.c/condition.list instead !!! */
#if defined (HAVE_ASSERT_H) && (defined(DEBUG) || defined(SQL_OUT) || defined (PTRS_OUT))
#include <assert.h>
#endif

/* Need this for errno stuff on ESV */
#ifdef ESV
/* #include <bsd/sys/types.h> */

#endif

#if 0
/* Allow use of malloc(3X) as opposed to the default malloc(3C)
* Link with -lmalloc */
#ifdef sgi
#include <malloc.h>
#endif

#endif /* 0 */

#ifdef VMS

#include rms
#include starlet
#include secdef
#include ssdef
#include stsdef
#include psldef
#include lnmdef
#include dvidef
#include descrip
#include stat
#include fibdef
#include iodef
#include atrdef
#include "fatdef.h"
#include file
#include unixio
#include unixlib

#define O_MODE 0   /* Default protection on open/create */

#else

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define O_MODE 0666   /* rw-rw-rw protection on open/create, subject to umask(2) */
#endif


/* Really need these now, since other files need the AST typedef.
Use ptr to point at other AST data required by context. Sigh, PCCTS could do with
'smart' nodes....  */

#define AST_FIELDS Attrib a; int token; void *ptr;

typedef enum _rwstat  { u=0, r=1, w=2 } ccif_rwstat;

#include "ast.h"		       /* Need typedef of AST for sym.h */

#ifndef CASTBase_h                     /* Allow sorcerer files to use identical AST */
typedef AST SORAST;
#define CASTBase_h
#endif

/* Why do we need these here? */
#include "sym.h"
#include "context.h"
#include "value_manip.h"


#include "astlib.h"
#include "smart_node.h"
#include "sort.h"


#include "condition.h"
#include "exit_handler.h"
__import__ int ccif_exit_status;  /* Defined and set in condition.c */

#include "data_block_list.h"
#include "f_interface.h"


extern char * ccif_file_name(const int unit);
extern char * ccif_file_io_name(const int unit);
extern ccif_rwstat ccif_block_status(const int block);
extern char * ccif_file_lookup (const int block);
extern AST * ccif_data_heading_lookup (const int block);
extern char * ccif_strdup(const char *);

#ifdef VMS   /* Stuff from ccif_vms.c */
extern void ccif_exit_vms_error(char *, int, int);
extern char * ccif_logname2filename(const char * const);
extern int truncate ( const char *, long int);
extern int unlink ( const char *);
extern int ccif_get_nameblock(const char * const filename, struct NAM *retnam);
extern int ccif_setup_output_file(const char * const filename);
#ifdef _MSC_VER
extern int wintruncate ( const char *, long int );
#define ccif_logname2filename(s) getenv(s)
#endif
#else
#define ccif_logname2filename(s) getenv(s)
#endif


/* Set/unset operation of fillAttr() */
extern void set_lex_act(const int func);

extern AST *zzmk_ast(AST *node, Attrib a, int t);

  
#define zzSET_SIZE 4
#include "antlr.h"
#include "ast.h"
#include "tokens.h"
#include "dlgdef.h"
#include "err.h"

ANTLRChar *zztokens[21]={
	/* 00 */	"Invalid",
	/* 01 */	"End_of_file",
	/* 02 */	"Whitespace",
	/* 03 */	"Data_name",
	/* 04 */	"Save_heading",
	/* 05 */	"Data_heading",
	/* 06 */	"Save",
	/* 07 */	"Global_heading",
	/* 08 */	"Loop",
	/* 09 */	"Stop",
	/* 10 */	"Non_quoted_text_string",
	/* 11 */	"Single_quoted_text_string",
	/* 12 */	"Double_quoted_text_string",
	/* 13 */	"Line_of_text",
	/* 14 */	"Semi_colon",
	/* 15 */	"Comment",
	/* 16 */	"Data_item",
	/* 17 */	"Star_file",
	/* 18 */	"Row",
	/* 19 */	"Char_token",
	/* 20 */	"Data_block_tokens"
};
SetWordType Char_token_set[4] = {0x0,0x1c,0x0,0x0};
SetWordType setwd1[21] = {0x0,0xd5,0x0,0xdd,0xd5,0xd5,0xc5,
	0xd5,0xdd,0x5,0x27,0x27,0x27,0x0,0x25,
	0x1,0x0,0x0,0x0,0x0,0x0};
SetWordType setwd2[21] = {0x0,0xba,0x0,0x5,0x0,0x5a,0x0,
	0x5a,0x5,0x0,0x0,0x0,0x0,0x0,0x0,
	0x0,0x0,0x0,0x0,0x0,0x0};
SetWordType setwd3[21] = {0x0,0x5,0x0,0x0,0x0,0x0,0x0,
	0x0,0x0,0x0,0x2,0x2,0x2,0x0,0x0,
	0x0,0x0,0x0,0x0,0x0,0x0};
SetWordType Data_block_tokens_set[4] = {0x38,0x1,0x0,0x0};

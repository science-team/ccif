                                                             /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* add_to_table:

Generic function to add dictionary information into symbol table.

char *data_field is the name of the data item whose value is to
be added into symbol table.

char *data_name is the name of the data item which specifies which
symbol table entry is to be modified. It must be in the same category
as data_field. If data_name cannot be found, it is determined 
implicitly from the name of the enclosing save frame definition,
given by Sym *save_frame.

See this example for more enlightenment (?):

There are two contexts in which a property of a dictionary data item
can be specified in a save frame in the dictionary. The first is as 
a simple data item, e.g.:

   save__atom_site.aniso_B[1][1]
      ....
       _item_default.value     0.0
      ....
      save_
      
The name of the data item whose default value is to be 0.0, is derived
implicitly from the block code of the save frame cell.

The DDL defines a data name _item_default.name, which is a pointer to _item.name,
and a complete specification of _item_default.value would include the data item:

     _item_default.name   '_atom_site.aniso_B[1][1]'

[This (hitherto unseen) case is dealt with explicitly in this function.] However, the 
DDL defines _item.mandatory_code for _item_default.name as 'implicit', so where it is
not explicitly specified in this way, it is derived implicitly.

The second context is where data_item occurs with data_name in a loop (this happens
where the characteristics of a child item are defined in the save frame of the
parent item). For example:

save__atom_site.id
     ....
     loop_
    _item.name
    _item.category_id
    _item.mandatory_code
               '_atom_site.id'                 atom_site            yes
               '_atom_site_anisotrop.id'       atom_site_anisotrop  yes
               '_geom_angle.atom_site_id_1'    geom_angle           yes
               '_geom_angle.atom_site_id_2'    geom_angle           yes
               ....
     ....
     save_

In this case, to add the mandatory codes, say, the values of _item.name and 
_item.mandatory_code are processed, one loop packet at a time.

Many cases can be dealt with in this way. Code which is specific to
particular data items is confined to the user-supplied functions
dictionary_items[i].add_func

*/


static int search_for_data_block ( const char * const datablock ) {

  Dictionary *dict_ptr;
  SList *cursor = ccif_dictionary[1]; /* Data block scope */

  while ( dict_ptr = slist_iterate(ccif_dictionary[1], &cursor)) {
    if ( !zzs_keycmp(datablock+5, zzs_symname(dict_ptr->datablock_id) ) ) 
      return dict_ptr->block;
  }
  
  return (-1);

}

static void add_to_table(int * const i)
{
   int ncontext, status, j, iname;
/*   static int iname;  */
   AST *name, *field;
   char errmsg[161];
   size_t len;
   const Sym *item, *dict_value;

   iname = *i;

   ncontext = ccif_new_context ( (zzs_get(save_frame_items[*i].name))->category.rec, &status, 0, 1 );
   if ( status == CAT_NOT_PRESENT ) {          /* DDL category not present in save frame at all.
                                                 Fixme - check for mandatory categories sometime */
      for ( (*i)++; save_frame_items[*i].specified; (*i)++ );  /* Go to next DDL category */
      ccif_free_context(&ncontext);
      return;
   }
   
   status = KEEP_CONTEXT;
   
   /* Loop over rows of this DDL category */
   while (1) {

      name = ccif_get_value ( save_frame_items[iname].name, &ncontext, &status, &item );

      switch ( status ) {
       case (END_OF_CONTEXT): return;  
       case (SINGLE_VALUE):
       case (LOOP_VALUE):
	 item = get_sym_record (name);
 	 break;
       case (NO_VALUE):
       case (LOOP_UNKNOWN):
	 item = zzs_get( (*save_frame_items[iname].unspecified)() );
	 break;
       default:
	 ccif_signal(DICT_BADDEF, "add_to_table", NULL, NULL, 0, current_save_frame->a->text);
       }

      if (!item) {
	if ( name && name->a && name->a->text )
	  ccif_signal(DICT_ENTRYMISS, "add_to_table", NULL, NULL, 0, name->a->text);
	else
	  ccif_signal(DICT_BADDEF, "add_to_table", NULL, NULL, 0, current_save_frame->a->text);
      }

      /* Loop over non-key items within category i */
      for ( j = iname + 1; save_frame_items[j].specified; j++ ) {
         status = KEEP_CONTEXT;
	 field = ccif_get_value (save_frame_items[j].name, &ncontext, &status, &dict_value);
	 if (!field)  {
	    if (save_frame_items[j].mandatory_code == yes) 
	      ccif_signal(DICT_NOMANDITEM, "add_to_table", NULL, NULL, 0,
			  save_frame_items[j].name, current_save_frame->a->text);
	    else 
	      (*save_frame_items[j].unspecified)(item);
	 }
	 else
	    (*save_frame_items[j].specified)(item, save_frame_items[j].name, field);
      }
      *i = j;   /* Set this, ready in case status is END_OF_CONTEXT at the top of the loop */
      status = ADVANCE_CONTEXT;
   }
}
   
   
/* Takes a data name from name->a->text, removes quoting and leading/trailing 
* spaces, and returns record from symbol table. Creates new record if 
* none exists. */ 

static Sym * get_sym_record(const AST * const name) { 
   char c[MAXCLLEN+1];
   const char *s; 
   int len; 
   static Sym *p;   /* avoid garbage collection */

   ccif_wo_quotes (name, &s, &len); 
   strncpy (c, s, len);
   c[len] = '\0';
   p = zzs_get(c);
   if ( !p ) {
     ccif_signal(DICT_CREATEREC, "get_sym_record", NULL, NULL, 0, c);
     p = zzs_newadd(c);
   }
   return p;
}

/* Sets a child's item type to that of its ultimate parent */
static void set_item_type ( Sym * item ) {

  Sym *q;
  int defset=0, type_code_save[2], old_basic_type, i ;

  ItemTypeList *type_ptr;
  SList *cursor;

  q = item->true_name.rec ? item->true_name.rec : item;

  if ( ! q->parent.rec ) return;

  for ( q = q->parent.rec; q->parent.rec; q = q->parent.rec );

  /* If a default value has already been specified for this, don't loose it,
     but check that previous item type is compatible with the new one */

  type_code_save[0] = item->type_code[0];
  type_code_save[1] = item->type_code[1];
  if ( item->type_code[0] > 0 && item->type_code[1] > 0 ) {
    defset = 1;
  }

  item->type_code[0] = - abs(q->type_code[0]);
  item->type_code[1] = - abs(q->type_code[1]);

  /* Check that previous type is compatible with the new one, if a default was given. */
  if ( defset ) {

    i = 0;
    cursor = ccif_item_type_list[ - item->type_code[0] ] ;
    do {
      type_ptr = slist_iterate( ccif_item_type_list[ type_code_save[0] ], &cursor );
    } while ( i++ < type_code_save[1] ) ;
    
    old_basic_type = type_ptr->basic_type;

    i = 0;
    cursor = ccif_item_type_list[ abs(q->type_code[0]) ] ;
    do {
      type_ptr = slist_iterate( ccif_item_type_list[ abs(q->type_code[0]) ], &cursor );
    } while ( i++ < abs(q->type_code[1]) ) ;

    if ( type_ptr->basic_type == old_basic_type ) {
      item->type_code[0] = abs(q->type_code[0]);
      item->type_code[1] = abs(q->type_code[1]);
    }
    else {
      ccif_signal(DICT_DIFFBTYPE, "set_item_type", NULL, NULL, 0, zzs_recname(item));
      memset( &(item->defval), 0, sizeof (union _defval));
    }

#ifdef UPDATE_WARN
    if ( type_code_save[0] && type_code_save[1] && 
	 ( abs(type_code_save[0]) != abs(item->type_code[0]) ||  
	   abs(type_code_save[1]) != abs(item->type_code[1]) ) )
     ccif_signal(DICT_UPDATEP, "set_item_type", NULL, NULL, 0, "_item_type.code", zzs_recname(item));
#endif
      
  }
}

static void add_default_value( Sym * item, char * data_field, AST * val) {

   ItemTypeList *type_ptr;
   SList *cursor;
   int i,j;

#ifdef UPDATE_WARN
   if ( item->type_code[0] > 0 && item->type_code[1] > 0 )     /* Already set */
     ccif_signal(DICT_UPDATE, "add_default_value", NULL, NULL, 0, data_field, zzs_recname(item), val->a->line);
#endif

   if ( !item->type_code[0] && !item->type_code[1] )  {
     ccif_signal(DICT_NODEFTYPE, "add_default_value", NULL, NULL, 0, 
         val->a->text, zzs_recname(item) );
      return;
   }

   item->type_code[0] = abs(item->type_code[0]);
   item->type_code[1] = abs(item->type_code[1]);
      /* (item->type_code's > 0) for valid default value */


/*  Get ITEM_TYPE_LIST details for this item, by iterating over the appropriate list item->type_code[1]
    times.
    This is done much more elegantly from CCIF applications, when the symbol table has been loaded
    from disk!
*/
   i = 0;
   cursor = ccif_item_type_list[ item->type_code[0] ] ;
   do {
      type_ptr = slist_iterate( ccif_item_type_list[ item->type_code[0] ], &cursor );
   } while ( i++ < item->type_code[1] ) ;

   switch ( type_ptr->basic_type ) {
   
      case (0):
         ccif_signal(DICT_NODEFBTYPE, "add_default_value", NULL, NULL, 0, 
            val->a->text, zzs_recname(item) );
         break;
      case (1):
         item->defval.dtext = zzs_strdup(val->a->text);
         break;
      case (2):
         item->defval.dint = atoi(val->a->text);
         break;
      case (3):
         item->defval.dfloat = atof(val->a->text);
         break;
   }
}


/* Unsets default value */
static char * unknown_default_value( Sym * const item ) {
   memset(&(item->defval),  '\0', sizeof (union _defval) );
   item->type_code[0] =  -abs(item->type_code[0]);
   item->type_code[1] =  -abs(item->type_code[1]);
   return ( (char *) NULL );
}
   

static void add_mandatory_code ( Sym* item, char * data_field, AST * val ) {
   char c[MAXCLLEN+1];
   const char *s;
   int len;
   enum item_mandatory_code mandatory_code = undefined;


   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';
   
   if ( !zzs_keycmp(c, "yes") )      mandatory_code = yes;
   if ( !zzs_keycmp(c, "no" ) )      mandatory_code = no; 
   if ( !zzs_keycmp(c, "implicit") ) mandatory_code = implicit;

#ifdef UPDATE_WARN   
   if ( item->mandatory_code != undefined  && item->mandatory_code != mandatory_code ) 
     ccif_signal(DICT_UPDATE, "add_mandatory_code", NULL, NULL, 0, data_field, zzs_recname(item), val->a->line);
#endif
   item->mandatory_code = mandatory_code;
   if ( mandatory_code == yes || mandatory_code == implicit ) {  /* Add to list of mandatory items */
      item->mandatory_scope.rec = item->category.rec->mandatory_scope.rec;
      item->category.rec->mandatory_scope.rec = item;
   }
}


static void add_type_code ( Sym * item, char * data_field, AST * val ) {

   char c[MAXCLLEN+1];
   const char *s;
   int len;
   int scope, i;
   SList *cursor;
   ItemTypeList *type_ptr;
   int new_type_code[2] = {0,0};
   

   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';

   for ( scope = 0, cursor = ccif_item_type_list[scope]; scope < 3; scope++ ) {  /* Look for item type info, from inner 
                                                                               to outer scopes */

   /* STAR does not allow a save_frame cell to occur in a STAR file except within a global or data block. So,
      block_seq_no will never be 0 for a save_frame - hence we don't need to worry about a spurious match with
      the (null) first element of ccif_item_type_list[0] */
                     
      for ( i = 0, cursor = ccif_item_type_list[scope]; 
            (type_ptr = slist_iterate(ccif_item_type_list[scope], &cursor) ) && type_ptr->block != block_scope_no[scope];
            i++ );
      if ( !type_ptr ) continue;
      do {
         if ( !zzs_keycmp(c, zzs_symname(type_ptr->code) ) ) {
            new_type_code[0] = scope; 
            new_type_code[1] = i;
            break;
         }
         i++; 
      } while ( (type_ptr = slist_iterate(ccif_item_type_list[scope], &cursor) ) && type_ptr->block == block_scope_no[scope] ) ;
      if ( new_type_code[0] || new_type_code[1] ) break; /* Break out of scope loop if type found */

   }  /* next scope outwards */


   if ( ! new_type_code[0] && ! new_type_code[1]  /* Could not find type_code */
	&& ! item->parent.rec )                   /* No parent to get type code from */
     {
       ccif_signal(DICT_NOTYPE, "add_type_code", NULL, NULL, 0, c, zzs_recname(item) );
       return;
     }
   
   if ( ! item->type_code[0] || ! item->type_code[1] ) { /* Not already set */
     item->type_code[0] = -new_type_code[0]; /* Negative, to indicate default not set */
     item->type_code[1] = -new_type_code[1];
   }
   else 
     {
       if ( ( abs(item->type_code[0]) != new_type_code[0] ||   /* Different type from before */
	      abs(item->type_code[1]) != new_type_code[1] ) )
	 {
	   item->type_code[0] = -new_type_code[0]; /* Negative, to indicate default not set */
	   item->type_code[1] = -new_type_code[1];
	   /* Remove default value */
	   memset( &(item->defval), 0, sizeof (union _defval) ); 
#ifdef UPDATE_WARN
	   ccif_signal(DICT_UPDATE, "add_type_code", NULL, NULL, 0, data_field, zzs_recname(item), val->a->line);
#endif
	 }
     }

}

static void add_parent_link ( Sym * item, char * data_field, AST * val ){

/* item is _item_linked.child_name, val is the node which contains the
*  value of _item_linked.parent_name
*/
   char c[MAXCLLEN+1];
   const char *s;
   int len;
   Sym *q;

#ifdef UPDATE_WARN
   if ( item->parent.rec )     /* Already set */
     ccif_signal(DICT_UPDATE, "add_parent_link", NULL, NULL, 0, data_field, zzs_recname(item), val->a->line);
#endif
   
   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';

    q = zzs_get(c);
    if ( !q ) {
      ccif_signal(DICT_CREATEREC, "add_parent_link", NULL, NULL, 0, c);
      q = zzs_newadd(c);
    }
    item->parent.rec = q;
}

static void add_condition_code ( Sym * item, char * data_field, AST * val) {
   char c[MAXCLLEN+1];
   const char *s;
   int len;
   Sym *q;
   int i,n;


   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';
   
   if ( !zzs_keycmp(c,"esd") ) {


/*  Get ITEM_TYPE_LIST details for this item, by iterating over the appropriate list item->type_code[1]
    times.
    This is done much more elegantly from CCIF applications, when the symbol table has been loaded
    from disk!
*/

      SList *type_list = ccif_item_type_list[ abs(item->type_code[0]) ], *cursor = type_list;
      int item_type = abs(item->type_code[1]), status;
      ItemTypeList *type_ptr;
      regmatch_t pmatch[NMATCH];
      
      i = 0;
      do {
         type_ptr = slist_iterate( type_list, &cursor );
      } while ( i++ < item_type ) ;

      if ( !(type_ptr->construct) ) 
	ccif_signal(DICT_NOESDREGEXP, "add_condition_code", NULL, NULL, 0, zzs_recname(item));

      
      status = regexec( type_ptr->preg, "1(2)", NMATCH, pmatch, 0); /* The value of i for which
                                                                          pmatch[i].rm_so is 1 and
                                                                          pmatch[i].rm_eo is 4, is the
                                                                          number of the esd subexpression */
      
      
      if ( status == REG_NOMATCH || ( !status && (pmatch[0].rm_so != 0 || pmatch[0].rm_eo != 4) ) ) 
	ccif_signal(DICT_BADESDEXP, "add_condition_code", NULL, NULL, 0, 
		    zzs_symname(type_ptr->code), zzs_recname(item) );
      else {
         if ( status ) {
            char re_msg[450];
            regerror( status, type_ptr->preg, re_msg, sizeof re_msg);
	    ccif_signal(RX_REGEXECERR, "add_condition_code", NULL, NULL, 0, re_msg);
         }
      }
      
   /* Now find which part of the construct contains the esd */
      for (i=1; i < NMATCH; i++) {   /* Start with pmatch[1] (pmatch[0] matches whole expression) */
         if ( pmatch[i].rm_so == 1 && pmatch[i].rm_eo == 4 ) {
            item->esd = i;
            return;
         }
      }
       
      ccif_signal(DICT_NSUBEXP, "add_condition_code", NULL, NULL, 0, zzs_symname(type_ptr->code) );
           
   } /* if ( !zzs_keycmp(c,"esd") )  */
   
   if ( !zzs_keycmp(c,"seq") ) 
     ccif_signal(DICT_NOSEQ, "add_condition_code", NULL, NULL, 0, zzs_recname(item) );

}

static void add_related_name (Sym * item, char *data_field, AST *val) {
   char c[MAXCLLEN+1];
   const char *s;
   int len;
   
   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';

   related_name = zzs_get(c);
   if ( !related_name ) {
     ccif_signal(DICT_CREATEREC, "add_related_name", NULL, NULL, 0, c);
     related_name = zzs_newadd(c);
   }
}

static void add_function_code (Sym * item, char *data_field, AST *val) {
   char c[MAXCLLEN+1];
   const char *s;
   int len;
   
   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';

   /* Fixme - deal with _item_related.function_code's other than associated_esd sometime! */
   if ( !zzs_keycmp(c, "associated_esd") )
      item->associated_esd.rec = related_name;  /* add_related_name guarantees that this is OK */
}


static void add_alias_name (Sym * item, char * data_field, AST * val) {
   char c[MAXCLLEN+1];
   const char *s;
   int len;
   Sym *q;
   
   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';

   if ( !(q = zzs_get(c)) ) 
     q = zzs_newadd(c);
#ifdef UPDATE_WARN
   else {
     if ( q->true_name.rec != item ) {
       ccif_signal(DICT_ALIASUPDATE, NULL, NULL, NULL, 0, c, val->a->line);
     }
   }
#endif

   q->true_name.rec = item;
   q->category.rec = item->category.rec; 
   q->esd = item->esd;
   q->associated_esd.rec = item->associated_esd.rec;
   q->no_in_cat = item->no_in_cat;
   q->mandatory_code = item->mandatory_code;
   q->type_code[0] = item->type_code[0];
   q->type_code[1] = item->type_code[1];
   q->defval = item->defval;
}


static void add_category_pointer( Sym *item, char * data_field, AST *val) {
/* item is _item.name (or _category.id, if save-frame defines a dictionary category),
 * val is the node which contains the value of _item.category_id (or _category.id again)
*/

   char c[MAXCLLEN+1];
   const char *s;
   Sym *q;
   int len;

#ifdef DICT_WARN   
   if ( item->category )     /* Already set */
     ccif_signal(DICT_UPDATE, "add_category_pointer", NULL, NULL, 0, data_field, zzs_recname(item), val->a->line);
#endif

   ccif_wo_quotes (val, &s, &len);
   strncpy(c, s, len);
   c[len] = '\0';

    q = zzs_get(c);
    if ( !q ) 
      ccif_signal(DICT_CREATEREC, NULL, NULL, NULL, 0, c);

    item->category.rec = q;
    /* if ( item != q ) item->no_in_cat = q->no_in_cat++ ; */ /* runs from 0 to n-1 */

}

static char * set_cat_ptr_implicitly( Sym * const item ) {

   char c[MAXCLLEN+1];
   Sym *cat;

#ifdef DICT_WARN   
   if ( item->category )    /* Already set */
     ccif_signal(DICT_IMPUPDATE, NULL, NULL, NULL, 0, 
		 "_item.category_id", zzs_recname(item), current_save_frame->a->line);
#endif
   if ( item->true_name.rec ){         /* item is an alias */
      strncpy (c, zzs_recname(item->true_name.rec) + 1, MAXCLLEN);
      *(strchr(c,'.')) = '\0';
      cat = zzs_get(c);
      item->category.rec = cat;
      if ( ! item->true_name.rec->category.rec )       /* Make sure that true name is */
         set_cat_ptr_implicitly(item->true_name.rec);  /* set up properly first */
                               
      item->no_in_cat = item->true_name.rec->no_in_cat;
   }
   else {
      strncpy (c, zzs_recname( item ) + 1, MAXCLLEN); 
      *(strchr(c,'.')) = '\0'; 
      cat = zzs_get(c);                                   
      item->category.rec = cat;
      /*      if ( item != cat && !item->cat.rec ) item->no_in_cat = cat->no_in_cat++ ; */ /* runs from 0 to n-1 */

   }
   return ( (char *) NULL );
}

static char * item_name_implicit(const AST * const save_frame) {
   return ( &(current_save_frame->a->text[5]) );
} 

static char * do_nothing ( Sym *const item ) {
   return ( (char *) NULL );
} 

static void do_nothing4 (  Sym *item, char * data_field, AST *val) {
}


/* Routines to add information about dictionary and its blocks.
   At the moment, this is similar to add_to_table, but it might diverge
   in the future. */


static void add_dict_info (int * const i, const AST * const block_heading, const int block_level)   
                                                                   /* block_level: 0,1,2 for saveframes, datablocks,
                                                                      global blocks, respectively */
{
   int ncontext, status, j, rows, cols;
   AST *name, *field;
   char errmsg[161];
   size_t len;
   const Sym *ddl_cat, *dict_value;
   static void *info_ptr, *dummy_ptr;
   static int iname;
   

   iname = *i;
   ncontext = ccif_new_context ( (ddl_cat = zzs_get(dictionary_items[*i].name))->category.rec, &status, block_level, 1 );
   if ( status == CAT_NOT_PRESENT ) {          /* DDL category not present in current block at all.
                                                  For mandatory categories, check that there is something 
                                                  around in outer/previous scopes before exiting: */
                                                  
      if ( block_heading->token != Save_heading  && dictionary_items[*i].mandatory_code == yes ) { /* Not necessary to check in 
                                                                                                     save_frames, since this is 
                                                                                                     taken care of by checking in
                                                                                                     each data block */

         if ( !dictionary_items[*i].category_list[2] ) 
	   ccif_signal(DICT_NOMANDCAT, "add_dict_info", NULL, NULL, 0, dictionary_items[*i].name, block_heading->a->text);
      }
  
      ccif_free_context(&ncontext);
      for ( (*i)++; !dictionary_items[*i].category_list && dictionary_items[*i].name; (*i)++ );  /* Go to next DDL category */
      return;
   }


   info_ptr = (*dictionary_items[iname].specified) (NULL, ddl_cat->node[block_level]);  /* Allocate memory to store info */


   /* Loop over rows of this DDL category */
   while (1) {
      /* Loop over items within category i */
      for ( j = iname + 1; 
            !dictionary_items[j].category_list && dictionary_items[j].name; 
            j++ ) {
         status = KEEP_CONTEXT; 
	 field = ccif_get_value (dictionary_items[j].name, &ncontext, &status, &dict_value);
	 if (!field)  {
	    if (dictionary_items[j].mandatory_code == yes) 
	      ccif_signal(DICT_NOMANDITEM, "add_dict_info", NULL, NULL, 0, dictionary_items[j].name, block_heading->a->text);
	    else 
	      (*dictionary_items[j].unspecified) (info_ptr);
	 }
	 else {
	    dummy_ptr = (*dictionary_items[j].specified) (info_ptr, field);
	  }
      }
      info_ptr = (*dictionary_items[iname].unspecified) (info_ptr, block_level); /* post-process row */
      *i = j;   /* Set this, ready in case status is END_OF_CONTEXT at the top of the loop */
/*      status = ADVANCE_CONTEXT; */
       if ( ! ccif_advance_context( ncontext) ) {
          ccif_free_context ( &ncontext ); 
          return;
       }
   }
}
   

static void * add_dict_entries (void * dummy_ptr, const AST * const node ) 
{
   int rows = 1, cols = 1, ovf = 0;
   void *ptr;
   

   if ( node->token == Loop ) {
      LOOP_DIMENSION(node, rows, cols, ovf)
   }
   
   CCIF_CALLOC(rows, Dictionary, ptr, "add_dict_entries", node->a->line)
      
   return ( ptr );
}

static void * end_dict_entry( const void * ptr, const int block_level)
{
   Dictionary *dict_ptr = (Dictionary *) ptr;
   dict_ptr->block = block_scope_no[block_level];
   slist_add(&ccif_dictionary[block_level], dict_ptr);
   return (void *) ++dict_ptr;
}

static void * add_dict_title ( void * const ptr, const AST * node ) 
{
   Dictionary *dict_ptr = (Dictionary *) ptr;
   size_t tmp;



   /* Need a pointer to char for zzs_strdup, and to add trailing null, and a
      pointer to const char for the call to ccif_wo_quotes.
      Doing

         ccif_wo_quotes (node, (const char **) &title, &len);

      causes problems under lcc (and maybe other compilers).
   */

   char *title;
   const char *ctitle;
   int len;

   
   ccif_wo_quotes ( node, &ctitle, &len );
   title = (char *) ctitle;
   title[len] = '\0';
   dict_ptr->title =  zzs_strdup(title);


   return NULL;
}

static void * add_dict_version ( void * const ptr, const AST * node )
{
   Dictionary *dict_ptr = (Dictionary *) ptr;

   /* see comment in add_dict_title, above */
   char *version;
   const char *cversion;
   int len;
   
   ccif_wo_quotes ( node, &cversion, &len );
   version = (char *) cversion;
   version[len] = '\0';
   dict_ptr->version = zzs_strdup(version);
   return NULL;
}

static void * add_dict_db ( void * const ptr, const AST * node )
{
   Dictionary *dict_ptr = (Dictionary *) ptr;

   /* see comment in add_dict_title, above */
   const char * cdatablock_id;
   char *datablock_id;
   int len;
   
   ccif_wo_quotes ( node, &cdatablock_id, &len );
   datablock_id = (char *) cdatablock_id;
   datablock_id[len] = '\0';
   dict_ptr->datablock_id = zzs_strdup(datablock_id);
   return NULL;
}

static void * dict_db_implicit (void * const ptr)
{
   Dictionary *dict_ptr = (Dictionary *) ptr;
   dict_ptr->datablock_id = zzs_strdup( &(current_data_block->a->text[5]) );
   return NULL;
}  

static void * add_type_entries ( void * dummy_ptr, const AST * const node ) 
{
   int rows = 1, cols = 1, ovf = 0;
   void *ptr;

   if ( node->token == Loop ) {
     LOOP_DIMENSION(node, rows, cols, ovf) ;
   }
   CCIF_CALLOC(rows, ItemTypeList, ptr, "add_type_entries", node->a->line)

   return ( ptr );
}

static void * add_code_to_type_list ( void * const ptr, const AST * node )  /* !!!fixme!!! must make sure that code and pcode
                                                                               are stored as all lower case */
{
   ItemTypeList *item_ptr = (ItemTypeList *) ptr;

   /* see comment in add_dict_title, above */
   const char * ccode;
   char * code;
   int len;
   
   ccif_wo_quotes ( node, &ccode, &len );
   code = (char *) ccode;
   code[len] = '\0';
   item_ptr->code = zzs_strdup(code);
   return NULL;
}


static void * add_pcode_to_type_list ( void * const ptr, const AST * node )
{
   ItemTypeList *item_ptr = (ItemTypeList *) ptr;

   /* see comment in add_dict_title, above */
   const char * cprimitive_code;
   char * primitive_code;
   int len;
   
   ccif_wo_quotes ( node, &cprimitive_code, &len );
   primitive_code = (char *) cprimitive_code;
   primitive_code[len] = '\0';
   item_ptr->primitive_code = zzs_strdup(primitive_code);
   return NULL;
}

static void * add_constr_to_type_list ( void * const ptr, const AST * node )
{
   ItemTypeList *item_ptr = (ItemTypeList *) ptr;

   /* see comment in add_dict_title, above */
   const char * cconstruct;
   char * construct;
   int len;
   
   ccif_wo_quotes ( node, &cconstruct, &len );
   construct = (char *) cconstruct;
   construct[len] = '\0';
   item_ptr->construct = zzs_strcdup(construct);

   return NULL;
}

static void * no_construct ( void * const ptr )
{
   ( (ItemTypeList *) ptr )->construct = 0;
   return NULL;
}

static void * do_nothing_2 ( void * const ptr, const AST * node ) {
   return ( (void *) NULL );
}

static void * do_nothing_3 ( void * const ptr) {
   return ( (void *) NULL );
}



static void * end_type_entry ( void * ptr, const int block_level )
{
   ItemTypeList *type_ptr = (ItemTypeList *) ptr;
   int status;
   regmatch_t regs[NMATCH];
   type_ptr->block = block_scope_no[block_level];

/* N.B. The contents of type_ptr->preg are not going to be dumped with the symbol table (the structure
   probably contains pointers to malloc'd memory, or at least cannot be assumed not to),
   so regcomp needs to be run after zzs_undump(). However, the compiled expression
   should be available to other routines in cifdic_to_symtab, so we don't use a local variable. */

   if (type_ptr->construct) {
      CCIF_CALLOC(1, regex_t, type_ptr->preg, "end_type_entry", (0))
      status = regcomp( type_ptr->preg, zzs_symname(type_ptr->construct), REG_EXTENDED );
      if ( status ) {
	 char re_msg[450];
	 regerror( status, type_ptr->preg, re_msg, sizeof re_msg);
	 ccif_signal(RX_REGCOMPERR, "end_type_entry", NULL, NULL, 0, status, re_msg, zzs_symname(type_ptr->construct));
      }
   }
   

   if ( !zzs_keycmp("numb", zzs_symname(type_ptr->primitive_code) ) ) {
      type_ptr->basic_type = 3;  /* float - least restrictive numerical type */
      type_ptr->single_line = 1; /* numeric types cannot span lines */
      if ( !type_ptr->construct ) 
	ccif_signal(DICT_NONUMEXP, "end_type_entry", NULL, NULL, 0, type_ptr->code);
      else 
         if ( regexec( type_ptr->preg, ".0", NMATCH, regs, 0) == REG_NOMATCH ||
              regs[0].rm_so != 0 ||
              regs[0].rm_eo != 2 ) 
                 type_ptr->basic_type = 2; /* integer type */
   }
   else {
      type_ptr->basic_type = 1;  /* text */
      if ( !type_ptr->construct ) {
	 ccif_signal(DICT_NOCHAREXP, "end_type_entry", NULL, NULL, 0, type_ptr->code);
         type_ptr->single_line = 0;
      }
      else
         type_ptr->single_line = regexec( type_ptr->preg, "\n", NMATCH, regs, 0) || 
                                     ( regs[0].rm_so == regs[0].rm_eo ) ;
   }
       
      
   slist_add(&ccif_item_type_list[block_level], ptr);
   return (void *) ++type_ptr;
   
}
         

static void add_category_key_name ( Sym * cat, char * data_field, AST * val) {

   Sym *item = get_sym_record(val);
   
   if ( cat->category.rec != cat ) {
     ccif_signal(DICT_BADKEYCAT, "add_category_key_name", NULL, NULL, 0, 
		 zzs_recname(item), zzs_recname(cat) );
   }
   
   if ( ! cat->key.rec )
     item->key.rec = cat;                /* Ensure that list is circular */
   else
     item->key.rec = cat->key.rec;       /* Add to beginning of list */
   cat->key.rec = item;
   
}

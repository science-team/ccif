/* condition.h --- CCIF condition handler header. */
/* Generated automatically by "condition.awk" */

#ifndef CONDITION_H
#define CONDITION_H


/* This file must typedef uint32 as a 32-bit unsigned integer value */
#include "uint32.h"

/* This file must define the macros __import__, __export__ and __noshare__ */
#include "import_export.h"

/* Length of message buffer for callback functions */
#define CALLBACK_LEN 512


/* Breakdown of condition value (not using control bits, at the moment */

/* Severity bits */
#define CCIF_M_SEVERITY 0x07  /* Severity mask */
#define CCIF_K_ERROR 2
#define CCIF_K_SUCCESS 1
#define CCIF_K_INFORM 3
#define CCIF_K_SEVERE 4
#define CCIF_K_WARN 0

/* Message bits */
#define CCIF_V_MSG_NO 3   /* Position of message number */
#define CCIF_S_MSG_NO 13  /* Size of message number */
#define CCIF_M_MSG_NO ( 0x1fff << CCIF_V_MSG_NO ) /* Message number mask */

/* Facility bits */
#define CCIF_V_FAC_NO 16  /* Position of facility number */
#define CCIF_S_FAC_NO 12  /* Size of facility number */
#define CCIF_M_FAC_NO ( 0x0fff << CCIF_V_FAC_NO ) /* Facility number mask */

/* Condition code symbols: */

/* Facility CCIF */
__import__ const uint32 CCIF_NOTNUM;
__import__ const uint32 CCIF_ISINT;
__import__ const uint32 CCIF_ISREAL;
__import__ const uint32 CCIF_ISNUM;
__import__ const uint32 CCIF_ISTEXT;
__import__ const uint32 CCIF_MULTILINE;
__import__ const uint32 CCIF_CHARISTEXT;
__import__ const uint32 CCIF_NOTMLINE;
__import__ const uint32 CCIF_NOTREAL;
__import__ const uint32 CCIF_NOTREALESD;
__import__ const uint32 CCIF_NOTINT;
__import__ const uint32 CCIF_NOTCHAR;
__import__ const uint32 CCIF_BADESD;
__import__ const uint32 CCIF_NOLOGNAME;
__import__ const uint32 CCIF_OPENING;
__import__ const uint32 CCIF_OPENED;
__import__ const uint32 CCIF_OPEN;
__import__ const uint32 CCIF_DICTLOADED;
__import__ const uint32 CCIF_NEWBLOCK;
__import__ const uint32 CCIF_REINIT;
__import__ const uint32 CCIF_NOITEMIP;
__import__ const uint32 CCIF_NOITEM;
__import__ const uint32 CCIF_UNKWNCMP;
__import__ const uint32 CCIF_NOMEM;
__import__ const uint32 CCIF_SORTEMPTYCAT;
__import__ const uint32 CCIF_ADDSORTCAT;
__import__ const uint32 CCIF_1LINESORT;
__import__ const uint32 CCIF_NOKEYSORT;
__import__ const uint32 CCIF_SORTED;
__import__ const uint32 CCIF_BADADDSORT;
__import__ const uint32 CCIF_NOFREESORT;
__import__ const uint32 CCIF_BADINDEX;
__import__ const uint32 CCIF_IDXRANGE;
__import__ const uint32 CCIF_NOTROW;
__import__ const uint32 CCIF_CXTRANGE;
__import__ const uint32 CCIF_BADCONTEXT;
__import__ const uint32 CCIF_WRONGSORTCXT;
__import__ const uint32 CCIF_NOROWFIND;
__import__ const uint32 CCIF_NOTLOOPED;
__import__ const uint32 CCIF_NEEDROW;
__import__ const uint32 CCIF_PRINTROW;
__import__ const uint32 CCIF_BADRBNODE;
__import__ const uint32 CCIF_LINEBAD;
__import__ const uint32 CCIF_CIFNOTOPEN;
__import__ const uint32 CCIF_SAMECXT;
__import__ const uint32 CCIF_ROCXT;
__import__ const uint32 CCIF_ROCXTADD;
__import__ const uint32 CCIF_TONONLOOPED;
__import__ const uint32 CCIF_DIFFCAT;
__import__ const uint32 CCIF_FROMNONLOOPED;
__import__ const uint32 CCIF_NONUNIQUEBLK;
__import__ const uint32 CCIF_MAXCIFS;
__import__ const uint32 CCIF_SAMEIO;
__import__ const uint32 CCIF_VMSSAMEIO;
__import__ const uint32 CCIF_UNXSAMEIO;
__import__ const uint32 CCIF_READONLY;
__import__ const uint32 CCIF_1LOOP;
__import__ const uint32 CCIF_CXTEMPTY;
__import__ const uint32 CCIF_NOTINLOOP;
__import__ const uint32 CCIF_BADAST;
__import__ const uint32 CCIF_NOAPPEND;
__import__ const uint32 CCIF_BADGET;
__import__ const uint32 CCIF_WRONGCXT;
__import__ const uint32 CCIF_NOTINDICT;
__import__ const uint32 CCIF_CATNOTCOMPLETE;
__import__ const uint32 CCIF_FOPEN;
__import__ const uint32 CCIF_NOSUCHFILE;
__import__ const uint32 CCIF_BADLOOP;
__import__ const uint32 CCIF_PARTLOOP;
__import__ const uint32 CCIF_NOFREECONTEXTS;
__import__ const uint32 CCIF_NULLSYMPTR;
__import__ const uint32 CCIF_NOUSEMMAP;
__import__ const uint32 CCIF_NOREC;
__import__ const uint32 CCIF_BADCAT;
__import__ const uint32 CCIF_NOKEY;
__import__ const uint32 CCIF_DBNAMELEN;
__import__ const uint32 CCIF_DNNOTUNIQ;
__import__ const uint32 CCIF_STRTABVAL;
__import__ const uint32 CCIF_STRTABLEN;
__import__ const uint32 CCIF_BADFMT;
__import__ const uint32 CCIF_BADFMTFLAGS;
__import__ const uint32 CCIF_BADFMTCHAR;
__import__ const uint32 CCIF_BADESDCHAR;
__import__ const uint32 CCIF_VALTOOLONG;
__import__ const uint32 CCIF_LINETOOLONG;
__import__ const uint32 CCIF_SEMI;
__import__ const uint32 CCIF_BADBLOCKNAME;
__import__ const uint32 CCIF_BADOUTPUT;
__import__ const uint32 CCIF_MAPPEDFILES;
__import__ const uint32 CCIF_SYSERR;

/* Facility DICT */
__import__ const uint32 DICT_BADDEF;
__import__ const uint32 DICT_ENTRYMISS;
__import__ const uint32 DICT_NOMANDITEM;
__import__ const uint32 DICT_NOMANDCAT;
__import__ const uint32 DICT_NOESDREGEXP;
__import__ const uint32 DICT_BADESDEXP;
__import__ const uint32 DICT_NSUBEXP;
__import__ const uint32 DICT_NOSEQ;
__import__ const uint32 DICT_NONUMEXP;
__import__ const uint32 DICT_NOCHAREXP;
__import__ const uint32 DICT_BADKEYCAT;
__import__ const uint32 DICT_CREATEREC;
__import__ const uint32 DICT_UPDATE;
__import__ const uint32 DICT_UPDATEP;
__import__ const uint32 DICT_ALIASUPDATE;
__import__ const uint32 DICT_IMPUPDATE;
__import__ const uint32 DICT_NOTYPE;
__import__ const uint32 DICT_NODEFTYPE;
__import__ const uint32 DICT_NODEFBTYPE;
__import__ const uint32 DICT_DIFFBTYPE;
__import__ const uint32 DICT_CANTADD;
__import__ const uint32 DICT_SAVEFRAME;
__import__ const uint32 DICT_NOCAT;

/* Facility ANTLR */
__import__ const uint32 ANTLR_BADLOOP;
__import__ const uint32 ANTLR_BADPCKT;
__import__ const uint32 ANTLR_MISTOK;
__import__ const uint32 ANTLR_NVALT;
__import__ const uint32 ANTLR_BADDATA;
__import__ const uint32 ANTLR_UNKPSIG;
__import__ const uint32 ANTLR_BADITEM;
__import__ const uint32 ANTLR_BADSTR;
__import__ const uint32 ANTLR_EMPTYLOOP;
__import__ const uint32 ANTLR_GLOBAL;
__import__ const uint32 ANTLR_SAVE;
__import__ const uint32 ANTLR_NULLAST;
__import__ const uint32 ANTLR_STKOVFLOW;

/* Facility DLG */
__import__ const uint32 DLG_LEXERR;
__import__ const uint32 DLG_NOLASTTOK;

/* Facility SOR */
__import__ const uint32 SOR_NOTINRANGE;
__import__ const uint32 SOR_NULL;
__import__ const uint32 SOR_NOTWILD;
__import__ const uint32 SOR_WRONGTOK;
__import__ const uint32 SOR_NULL2;
__import__ const uint32 SOR_NOVIALTNULL;
__import__ const uint32 SOR_NOVIALT;
__import__ const uint32 SOR_PANIC;

/* Facility REDBLACK */
__import__ const uint32 REDBLACK_NOSORTORDER;
__import__ const uint32 REDBLACK_NONHEAD;
__import__ const uint32 REDBLACK_NODELINT;
__import__ const uint32 REDBLACK_NODELHEAD;
__import__ const uint32 REDBLACK_SIBBLACK;
__import__ const uint32 REDBLACK_SIBNOTINT;
__import__ const uint32 REDBLACK_23c;
__import__ const uint32 REDBLACK_NONEXT;

/* Facility ASTLIB */
__import__ const uint32 ASTLIB_NOMEM;

/* Facility RX */
__import__ const uint32 RX_REGEXECERR;
__import__ const uint32 RX_REGCOMPERR;
__import__ const uint32 RX_REGCOMPERR2;

/* Facility VMS */
__import__ const uint32 VMS_SYSMSG;
__import__ const uint32 VMS_CRTL;
__import__ const uint32 VMS_TRNLNM;

/* Facility RMS */
__import__ const uint32 RMS_NOTSEQ;
__import__ const uint32 RMS_NOCRINFO;
__import__ const uint32 RMS_RECTOOLONG;
__import__ const uint32 RMS_NORECTYPE;

/* Facility EXIT */
__import__ const uint32 EXIT_FAIL;


/* Prototype for condition handler */
extern void ccif_signal(const int code, const char * const pre,
            void (*callback)(), const void * ptr, int idx, ...); 
extern void ccif_condition_init(void);
int ccif_warnmsg_switch ( const char * varname, const int on );

#endif /* CONDITION_H */


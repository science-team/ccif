                                                                                /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/


/* Declarations of routines which use/change information in contexts,
 * i.e. those which change the contents of data blocks */

extern void ccif_new_data_item( char * const new_data_name, 
                                const int token,
                                const Attrib a,
                                const CONTEXT_LIST * const context );
extern AST * ccif_new_category ( CONTEXT_LIST * const context, int loop);
extern AST * ccif_data_items_to_loop (CONTEXT_LIST * const context);
extern void ccif_ast_free(SORAST * t);
extern SORAST * ccif_ast_dup(SORAST * t);
extern void ccif_add_row(const int ncontext);

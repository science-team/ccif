/* 
 * $Source$
 * $Revision$
 * $Date$
 * $Author$
 */

#include <stdio.h>    /* Basic includes and definitions */
#include <stdlib.h>   /* For malloc */
#include "list.h"
#include "library.h"
#include "exit_handler.h"
#include "ccif_malloc.h"

#define boolean int
#define TRUE 1
#define FALSE 0


/*---------------------------------------------------------------------*
 * PROCEDURES FOR MANIPULATING DOUBLY LINKED LISTS 
 * Each list contains a sentinal node, so that     
 * the first item in list l is l->flink.  If l is  
 * empty, then l->flink = l->blink = l.            
 * The sentinal contains extra information so that these operations
 * can work on lists of any size and type.
 * Memory management is done explicitly to avoid the slowness of
 * malloc and free.  The node size and the free list are contained
 * in the sentinal node.
 *---------------------------------------------------------------------*/

/* Namespace modified to make it more sensible to include this stuff
 * in CCIF.    Peter Keller */

typedef struct int_list {  /* Information held in the sentinal node */
  struct int_list *flink;
  struct int_list *blink;
  int size;
  rb_List free_list;
} *Int_list;

void rb_insert_list(rb_List item, rb_List list)	/* Inserts to the end of a list */
{
  rb_List last_node;

  last_node = list->blink;

  list->blink = item;
  last_node->flink = item;
  item->blink = last_node;
  item->flink = list;
}

void rb_delete_item_list(rb_List item)		/* Deletes an arbitrary iterm */
{
  item->flink->blink = item->blink;
  item->blink->flink = item->flink;
}

rb_List rb_make_list(int size)	/* Creates a new list */
{
  Int_list l;

  CCIF_MALLOC(1, struct int_list, l, "rb_make_list", (0) )
  l->flink = l;
  l->blink = l;
  l->size = size;
  CCIF_MALLOC(1, struct rb_list, l->free_list, "rb_make_list", (0) )
  l->free_list->flink = l->free_list;
  return (rb_List) l;
}
  
rb_List rb_get_node_list(rb_List list)   /* Allocates a node to be inserted into the list */
{
  Int_list l;
  rb_List to_return;

  l = (Int_list) list;
  if (l->free_list->flink == l->free_list) {
    CCIF_MALLOC(l->size, struct rb_list, to_return, "rb_get_node_list", (0))
    return (to_return);
  } else {
    to_return = l->free_list;
    l->free_list = to_return->flink;
    return to_return;
  }
}

void rb_free_node_list(rb_List node, rb_List list)    /* Deallocates a node from the list */
{
  Int_list l;
  
  l = (Int_list) list;
  node->flink = l->free_list;
  l->free_list = node;
}

                                                                 /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/

/* Appalling misnomer - if I ever get around to providing a C
   interface, it will get sorted out.... */

/* Public routines from f_interface.c */

extern ccif_rwstat ccif_block_status ( const int block );
extern char * ccif_file_lookup ( const int block );
extern AST * ccif_data_heading_lookup( const int block );
extern char * ccif_file_name (const int unit);
extern char * ccif_file_io_name (const int unit);
extern void ccif_change_line_limit(const int unit, const int limit);
extern const char * ccif_logname_lookup(const int block);
extern int ccif_no_of_blocks ( const int unit);
extern CIF_FILE_LIST ccif_file_list_entry(const int unit);
extern void ccif_add_blocks_to_lookup(const CIF_FILE_LIST * const file_list_entry);
extern void ccif_del_blocks_from_lookup(const CIF_FILE_LIST * const file_list_entry);

                                                              /* -*-C-*- */
/* $Id$ */

/********************************************************************
* Copyright (c) 1995 - 2004, EMBL, Peter Keller
* 
* CCIF: a library to access and output data conforming to the
* CIF (Crystallographic Information File) and mmCIF (Macromolecular
* CIF) specifications, and other related specifications.
* 
* This library is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 3, modified in accordance with the provisions of the 
* license to address the requirements of UK law.
*
* You should have received a copy of the modified GNU Lesser General 
* Public License along with this library.  If not, copies may be 
* downloaded from http://www.ccp4.ac.uk/ccp4license.php
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
********************************************************************/


static int cr_attribs = 1;


#if defined VMS || _MSC_VER
#ifdef PTR_FROM_TEXT
#undef PTR_FROM_TEXT
#endif
#define PTR_FROM_TEXT "%p"
#endif

void
#ifdef __STDC__
zzcr_attr(Attrib *a,int token,char *text)
#else
zzcr_attr(a,token,text)
Attrib *a;
int token;
char *text;
#endif
{

/* Initial values for a malloc'd Attrib */
   static const struct attrib attr_init =  {-1, 0, 1, 0, 0, "." , NULL, NULL, NULL, constant};


   /* Using trivial rules for checking tokens. Don't create any attributes */
   if ( !cr_attribs ) {
      (*a) = NULL;
      return;
   }
   

/* The method of filling in Attrib **a depends  on whether it has  been
   called from the lexer (i.e. memory has been malloc'd  in fillAttr, and
   (*a)->text already points to the text),  or it has been called from a
   $[...] constructor (i.e. memory needs to  be malloc'd for the Attrib, and
   also for the text itself).
   
   In the first case, fillAttr replaces the lex text with '\001' followed by
   the value of the pointer to the text (with %p), and assigns the other 
   lexical information before zzcr_attr has been called
*/

   
   if ( text[0] != '\001' ) {             /* called from $[...] constructor */
      CCIF_CALLOC(1, struct attrib, *a, "zzcr_attr", (0))

      **a = attr_init;

      CCIF_MALLOC( strlen(text)+1, char, (*a)->text, "zzcr_attr", (0) )
      strcpy( (*a)->text, text );
      (*a)->length = strlen(text);
      (*a)->status = output;

      /* Now, allow for tabs in the column/length count. Irrelevant for semi-colon strings,
       * and doesn't arise for unquoted text strings */
      if ( token == Single_quoted_text_string || token == Double_quoted_text_string ) {
	 char *ptr;
	 for ( ptr = strchr((*a)->text, '\t'); 
	      ptr; 
	      ptr = strchr(ptr+1, '\t') )
	   (*a)->length += 7;
      }
   }
      
      
   else {       /* called from lexer, so most of attrib will be already
                   filled in, including pointer to text. */
      sscanf(text+1,PTR_FROM_TEXT, a);
   }


}
	 


static void Whitespacemode(int defmode)
/* Lexical action - uses the one character of lookahead provided by dlg to set next lexical class.
   Call as a lexaction for any token which ends in whitespace.
   May need to call zzskip() as well - that depends on circumstances.
   defmode is the mode to switch to, if the next character does not indicate that one of
   the special tokens is coming up.
*/
{
/*       Set default mode now. Note that zzmode just sets the status - it doesn't
         request any more characters, so it can be called several times before the
         next character is fetched.   */
         zzmode(defmode);

/*       determine next lexclass      */
         switch (zzchar) {
            case ('\''): zzmode(S_QUOTE_STRING);  break;
            case ('\"'): zzmode(D_QUOTE_STRING);  break;
            case ('#' ): zzmode(defmode==START ? START : COMMENT);        
         }

         switch ( *zzendexpr ) {
	    case (' '):
	       break;
	    case ('\t'):
	       zzendcol += 7;
	       break;
	    case ('\n'):
            case ('\r'):  /* Should allow MAC text to be used */

/*       Check for a semi-colon-bounded text string about to come up */
               if (zzchar == ';') 
                  zzmode(SEMI_STRING_START); 
	    
	    case ('\x0c'):
	       zzendcol = 0;
	    
	    case ('\x0b'):
	       zzline++;
	 }
}



static void lex_check(const int trim_ws) {
   if ( trim_ws && ( strchr("\377 \f\v\n\r\t" , *zzendexpr) ) ) *zzendexpr = '\0';
}

static void zzerr_nop(const char * s) {
}

static void fillAttr(const int trim_ws)
/*   Allocates memory for an attribute, and assigns those fields which contain lexical (as opposed to
*    syntactical or grammatical) information. Replaces the string passed to zzcr_attr() with a pointer
*    to this allocated memory (as "\001%p"), so that zzcr_attr can find it again.
*    If file was mapped, points the text field of the attribute to the original location of the token 
*    in memory (i.e. avoids storing the contents of the file twice over).
*    Otherwise, allocates memory to store the string, and copies it in.
*
*    trim_ws == TRUE => trim any final whitespace character from this token.
*/
{

   static Attrib a;    /* Avoid garbage collection */
   char * ptr;
   static int last_tok = 0;       /* Token type from last time this routine was called. Used
				   * to cope with this situation, where we are using mapping:
				   *    ;
				   *       a bit of text
				   *    ;next_token
				   * 
				   * A kludge, but this was a late-breaking aspect of
				   * the STAR syntax.
				   * The method of dealing with this, relies on the fact that zzchar
				   * is ahead of the current token at any point, i.e. tokenisation of
				   * the input means that after lexing the final Semi-colon element of the 
				   * text_string rule, zzchar already contains the following character,
				   * and *zzbegexpr will be correct.
				   */
					

   char pos[ZZLEXBUFSIZE];
   static Attrib last=NULL;

#if _USE_MMAP == 1
   __import__ MAP_FILE_RECORD * ccif_current_mapfile;
#endif
   
   CCIF_CALLOC(1, struct attrib, a, "fillAttr", zzline)

   a->col     = zzbegcol;
   a->line    = zzline;
   a->length  = strlen(zzbegexpr);
   a->err     = NULL;
   a->nnodes = 0;
   
/* Set up links */
   a->prev = last;
   if (last) last->next = a;
   a->next = NULL;
   last = a;


#if _USE_MMAP == 1  /* Point a->text to start of string in mapped file */

   if ( ccif_current_mapfile ) {                          /* Using mapping on this file */
       
      a->text  = zzstr_in - a->length - 1 ;
      if ( last_tok != Semi_colon || 
            *(a->text - 1) != ';' ) {             /* Condition for not having a text string run into
						       the following token.*/
         a->status = mapped;
         a->map_file = ccif_current_mapfile;

/*   If we are less than three bytes from the end of file, need to take special care, since the
     position of the start of the token cannot be determined accurately from zzstr_in.
     a->text may be up to two characters before the start of the token, so use memcmp to locate
     the string.
     '\377' may be inserted by DLG into zzlextext when the end-of-file is reached - it is not
     in the original file. Or, strlen(zzendexpr) may be one character too long at the end of file.
     So, a->length has to be decremented where this has happened.
*/
         if ( ! CCIF_WITHIN_MAPPED_FILE(zzstr_in)
              ||  *zzstr_in == '\0'  ) {
            if ( *zzbegexpr == '\377' ) {
	       a->length = 0;
	       a->text = zzstr_in;
	    }
	    else {
	       char *ptr1=a->text; 
               
               if ( *zzendexpr == '\377' ) a->length--; /* EOF character sometimes inserted by DLG
                                                           would be included in comparison? */

              /* This loop hunts for the exact location of zzbegexpr in zzstr_in, because
                 it is sometimes off by a few characters for the last token in the file
              */
	       do {
                  /* Correct for possibility that zzendexpr points to a spurious character
                     off the end of the file.
		     N.B. we use memcmp rather than strncmp for this, because some
		     implementations of strncmp (e.g. Tru64 Unix) require the first two
		     arguments to strncmp to be null-terminated, even though the comparison algorithm
		     doesn't require it. When the end of the file is exactly on a block boundary,
		     mmap behaves differently from when it isn't, and the EOF character from
		     DLG may not be present */
                  if ( ! CCIF_WITHIN_MAPPED_FILE(&ptr1[a->length - 1]) ) a->length--;

	          if ( !memcmp(ptr1,zzlextext,a->length) ) break;
	          ptr1++; 
	       } while ( ptr1 < zzstr_in);
	       if ( ptr1 >= zzstr_in ) 
		 ccif_signal(DLG_NOLASTTOK,"fillAttr", NULL, NULL, 0, a->line);
	       a->text = ptr1;
	    }  /* if ( *zzbegexpr == '\377' ) */
         }  /* if ( *zzstr_in == '\0'  ) */

      } /* if ( last_tok != Semi_colon || *(a->text - 1) != ';' ) */
   } /*   if ( ccif_current_mapfile ) */
   else {			       /* allocate memory for string as if not mapped */

#endif /* _USE_MMAP == 1 */

      CCIF_CALLOC(a->length + 1, char, a->text, "fillAttr", a->line)
      strcpy(a->text, zzbegexpr);
      a->status = input;

#if _USE_MMAP == 1  /* Point a->text to start of string in mapped file */
    }
#endif /* _USE_MMAP */

         
/*   Set the byte immediately following the token to NULL, so that when the text field of the 
*    Attrib is pointed at it, it can be used as a normal pointer to a string. This avoids 
*    allocating memory piecemeal to store the text of the input file, when it is already in 
*    memory once as a result of being mapped.
*    This means that newlines, in general, will be lost, but since line and column information is
*    stored in the Attrib, this is not serious.
*    *(zzstr_in - 1) has already been copied to zzchar by ZZGETC_STR, so can safely set it (or
*    previous character) to NULL. 
*
*    The file was mapped with PROT_READ|PROT_WRITE, MAP_PRIVATE [Unix] or SEC$M_CRF|SEC$M_WRT 
*    [VMS; copy on reference and read/write], and originally opened read-only, so the original
*    file is unaltered.
*
*/

   if ( NLA != Semi_colon ) { /* These are dealt with by the parser */
      
      if ( trim_ws && ( strchr("\377 \f\v\n\r\t" , a->text[a->length-1]) 
                        || ! CCIF_WITHIN_MAPPED_FILE(&(a->text[a->length - 1])) ) ) { /* These tokens may contain a trailing */
         a->length--;                                          /* whitespace character, which is not
	                                                        * really part of the token proper. \377
								* corresponds to the '@' character in DLG */
         Whitespacemode( zzauto == START ? START : NORMAL );
      }
      /* Under certain circumstances, the very last token in the file can be length 0 */
      if ( a->length > 0 ) a->text[a->length] = '\0';
      
      /* Now, allow for tabs in the column/length count. Irrelevant for semi-colon strings,
       * so it is put here */
      for ( ptr = strchr(zzbegexpr, '\t');
	   ptr; 
	   ptr = strchr(ptr+1, '\t') )
         a->length += 7;

      
   }

   last_tok = NLA;
   sprintf (pos,"%c%p",'\001',a);
   zzreplstr(pos);
}


/* Sometimes, we don't want fillAttr and zzerrstd to be called, or zzcr_attr to create attributes.
 * This can happen when we are using trivial, single-token rules to check that a token generated 
 * by an application program is a valid STAR token (i.e. we are using an ANTLR rule as an interface 
 * to the lexical analyser).
 * 
 * When we want this, do
 *     set_lex_act(0);
 * and
 *     set_lex_act(1);
 * to restore fillAttr.
 */

static void (*lex_act)() = fillAttr;

void set_lex_act(const int func) {
   if ( func ) {
      lex_act = fillAttr;
      zzerr = zzerrstd;
      cr_attribs = 1;
   }
   else {
      lex_act = lex_check;
      zzerr = zzerr_nop;
      cr_attribs = 0;
   }
}


/* Copy of zzerrstd from dlgauto.h - uses CCIF signal handler */
void
#ifdef __USE_PROTOS
ccif_zzerrstd(const char *s)
#else
ccif_zzerrstd(s)
char *s;
#endif
{
  ccif_signal(DLG_LEXERR, NULL, NULL, NULL, 0, ((s == NULL) ? "Lexical error" : s), zzline,zzlextext);
}



c                                                   -*-Fortran-*-
c     $Id$
      program cifget

c     *****************************************************************
c
c     Copyright (c) 1995 - 2004, EMBL, Peter Keller
c     
c     CCIF: a library to access and output data conforming to the
c     CIF (Crystallographic Information File) and mmCIF (Macromolecular
c     CIF) specifications, and other related specifications.
c     
c     This library is free software: you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public License
c     version 3, modified in accordance with the provisions of the 
c     license to address the requirements of UK law.

c     You should have received a copy of the modified GNU Lesser General 
c     Public License along with this library.  If not, copies may be 
c     downloaded from http://www.ccp4.ac.uk/ccp4license.php

c     This program is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU Lesser General Public License for more details.
c
c     *****************************************************************
      
c     Program to get the value of a named data item from a CIF file.
c
c     Logical names required:
c        MMCIFDIC     binary version of mmCIF prepared during the CCIF build
c        CIFIN       Input CIF      


c     This syntax for include files seems to work on every system that
c     I've tried it on.
      include 'cifget.inc'

c     Some versions of f77 under linux have an intrinsic
c     abort procedure
      external abort
      
      integer i, blk_id, nblock, blknum
      character*(cfllen) symtab, val*733, esdval, blknam, catnam, 
     & ccode, pcode, mmcifdic, cifin
      
      call ccpfyp

      call ccif_malloc_trace()
      
      call getenv('MMCIFDIC',mmcifdic)
      call getenv('CIFIN',cifin)
      if ( mmcifdic .eq. ' ' .or. ichar(mmcifdic(:1)) .eq. 0 .or.
     &     cifin .eq. ' ' .or. ichar(cifin(:1)) .eq. 0 )
     &     call abort('Need to specify dictionary and input file!')

c_______The symbol table contains dictionary information on each data item,
c       which is accessed with a hashed lookup on the data name.
      call ccif_init('MMCIFDIC')
      
      print '('' Loading CIF file....'',$)'
      call ccif_load_cif('CIFIN', nblock, istat)
      print '('' Done ['',i3,'' block(s)]'')', nblock
      if ( istat .ne. 0 ) print*, 'Loaded with syntax errors'

c_______Which block do we want data from?

      call getenv('BLOCK',blknam)

      if ( blknam .eq. ' '
     &     .or. (ichar(blknam(:1))) .eq. 0 ) then
c     This shouldn't be necessary, since standard Fortran doesn't
c     allow null strings, but some implementations are rather 'C'-like
         
         print '('' Data block name or number: '',$)'
         read (*,'(a)') blknam
      endif


      call ccpupc(blknam)
      if ( blknam(:5) .eq. 'DATA_' ) then
         call ccif_block_by_name('CIFIN', blknam, blk_id, istat, ' ')

         if ( istat.le.0 )
     &        call abort('Cannot find block '
     &        // blknam(:index(blknam,' ')) // ' in CIFIN!')

      else
         read ( blknam, '(i3)', err=999 ) blknum
         call ccif_block_by_index('CIFIN',blknum, blknam, 
     &        blk_id, istat)
         if ( istat .eq. -1 ) then
            call abort( 'CIFIN not open!')
         elseif ( istat .eq. 0 ) then
            call abort( 'Not that many data blocks in CIFIN!')
         endif
      endif


c_______Find data name.
         
      call getenv('ITEM',itmnam)
      
      if ( itmnam .eq. ' '
     &     .or. (ichar(itmnam(:1))) .eq. 0 ) then
         print '('' Data item to retrieve: '',$)'
         read (*,'(a)') itmnam
      endif

c_______Find out type of data item (real, integer, etc.)
      call ccif_item_type(itmnam, ccode, pcode, btype, sline)
      if (btype.eq.-1)
     1 call abort(itmnam//' not found in dictionary!')
      if (btype.eq.0) 
     1 call abort('Type of '//itmnam//' not defined in dictionary!')
 
      call ccif_setup_context(itmnam, catnam, blk_id, ncntxt, istat,' ')
      if ( istat.eq.cat_not_present )
     1  print*,' Category '//catnam(:index(catnam,' '))//
     2  ' not present in data block'


      istat = keep_context
      i = 0

c________If requested item is found in a loop structure, the program will loop
c________back to this point after setting istat to advance_context.      

c________Based on type of item (which is returned by ccif_item_type above)
c        call appropriate routine to get its value.
 10   if ( btype.eq.3 ) then
         call ccif_get_real(itmnam, val, rval, ncntxt, istat)
       else if (btype.eq.4) then
         call ccif_get_real_esd(itmnam, val, rval, esdval, esd, 
     & ncntxt, istat, istate)
       else if ( btype.eq.2 ) then
         call ccif_get_int(itmnam, val, ival, ncntxt, istat)
       else if ( btype.eq.1 ) then
         if ( sline.ne.0 ) then
           call ccif_get_char(itmnam, val, cval, lenval, ncntxt, istat)
         else
           itmpos = 0
           call ccif_get_text(itmnam, itmpos, nline, tex,
     &       ntval, ncntxt, istat)
           
C________If item was a single line token, itmpos will be -1.
C        number of characters in the data. Assign the values
C        needed for printval to output it as a character type.
           if (itmpos.eq.-1) then
             lenval = nline
             cval = tex
           endif

         end if
      end if
      

c_________Depending on the value of istat returned by ccif_get_*,
c         take appropriate action.
c_________This is the nearest thing that standard Fortran has to a
c         switch statement. You may have another favourite control
c         structure.
      goto (100, 200, 100, 400, 500, 200, 100, 300), istat

c_________ no_value (istat is 0)
      call abort(itmnam//' not specified in this data block!')


c_________ loop_value or loop_null or loop_unknown (istat is 1, 3 or 7)
 100   i = i + 1
       print '('' Value in loop packet '',i5,'':'',$)',i
       call printval

c_________ loop back to ccif_get_* routines. Value will be retrieved from
c          next loop packet.
       istat = advance_context
       goto 10


c_________ single_value or single_null (istat is 2 or 6)
 200   print'(1x,a, '' occurs once in this data block: value is '',$)',
     &   itmnam(:index(itmnam,' '))
       call printval
       call ccperr(0,'Normal termination')

c__________ end_of_context (istat is 8)
 300   call ccperr(0, 'No more occurences of '//itmnam)

c___________ single_dict (istat is 4)
 400   print
     &   '(1x,a,'' not in this data block; default value '//
     &    '(from dictionary): '',$)', itmnam(:index(itmnam,' '))
       call printval
       call ccperr(0,'Normal termination')

c__________ loop_dict (istat is 5)
 500   i = i + 1
       print '(1x,a,i5,a,$)',
     &   'Value in loop packet ',i,
     &   ': (unknown); default from dictionary: '
       call printval
       istat = advance_context
       goto 10


 999   call abort( 'Must specify data block either as a number ' //
     &      'starting at 1, or a string starting with "data_"')
       
      end       
       

      subroutine printval
c________Subroutine to print out a value returned by the ccif_get_* routines.

      include 'cifget.inc'

      integer i

c_______Deal with null and undefined cases first:      
      if (istat.eq.single_null .or. istat.eq.loop_null) then
        print*, ' (null)'
      else if (istat.eq.loop_unknown) then
        print*, ' (unknown)'

c_______only reached for loop_value or single_value (istat 2 or 3)
      else if (btype.eq.3) then 
         print*, rval
      else if (btype.eq.4) then
         if ( (istate.ge.2 .and. istate.le.5) .or. istate.eq.9) then
            print*,rval, 'esd: ', esd
         else
            print*,rval
         endif
      
      else if (btype.eq.2) then 
         print*,ival
       else if (btype.eq.1) then
         
C________If itmpos is -1, then item was a single-line token type,
C        even though _item.type_code would have allowed it to
C        be a semi-colon bounded text string.
          if ( sline.ne.0 .or.itmpos.eq.-1 ) then
             print '(''"'',a,''"'')', cval(:lenval)
          else

          itmpos=0
          
c_______Special treatment for text. itmpos is the position where the last
c       ccif_get_text got to (retained by the calling program) - if it is
c       non-zero, there is text remaining in the string, and it is re-input
c       to ccif_get_text to request text from that point.
c       The text buffer (variable tex) must be defined to be a multiple of
c       80 in length, so that it can contain an integral number of padded
c       lines.
c       ntval is the number of lines which tex can contain, so we have:
c       character*80 tval(ntval), tex*(80*ntval)
c       equivalence (tval, tex)
c       nline is the number of lines actually returned by the invocation of
c       ccif_get_text. (0 .le. nline  .and.  nline.le.ntval) after calling
c       ccif_get_text.
           print*
40         print '(1x,a)',(tval(i),i=1,nline)
           if (itmpos.eq.0) return
           istat = keep_context
           call ccif_get_text(itmnam, itmpos, nline, tex,
     &       ntval, ncntxt, istat)
           goto 40
         endif
      endif
      end


      subroutine abort(msg)
      character*(*) msg
      print*
      print*,'Usage: logical names required: '
      print*,'   MMCIFDIC: Binary mmCIF dictionary'
      print*,'   CIFIN:    Input CIF'
      print*,'Can also specify: '
      print*,'   BLOCK:    datablock required (either a number starting'
      print*,'             at 1 or a name starting with "data_")'
      print*,'   ITEM:     Name of required mmCIF item'
      print*
      print*,'These can be set in the environment, or given on the ' //
     &     'command line, e.g.:'
      print*,'   cifget MMCIFDIC /path/to/lib CIFIN mydata.cif ' //
     &     'BLOCK 1'
      print*
      call ccperr(1,msg)
      end
 

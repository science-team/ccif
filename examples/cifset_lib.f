c                                                        -*-Fortran-*-
c     $Id$

c     *****************************************************************
c
c     Copyright (c) 1995 - 2004, EMBL, Peter Keller
c     
c     CCIF: a library to access and output data conforming to the
c     CIF (Crystallographic Information File) and mmCIF (Macromolecular
c     CIF) specifications, and other related specifications.
c     
c     This library is free software: you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public License
c     version 3, modified in accordance with the provisions of the 
c     license to address the requirements of UK law.

c     You should have received a copy of the modified GNU Lesser General 
c     Public License along with this library.  If not, copies may be 
c     downloaded from http://www.ccp4.ac.uk/ccp4license.php

c     This program is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU Lesser General Public License for more details.
c
c     *****************************************************************

      subroutine getorm(iun, rf, ro, cell, spgrp, nspgrp, z, lflag)
C     =============================================================
C
C Read Brookhaven file open on stream iun, search for CRYST1 & SCALE lines
C Return when first ATOM line found
C
C Input:
C     iun           unit number for reading
C
C Output:
C     rf(4,4)       orthogonal to fractional matrix
C     ro(4,4)       fractional to orthogonal matrix
C     cell(6)       unit cell (only if present on file)
C     spgrp         Space group name
C     nspgrp        Space group number
C     z             No. of chains in unit cell.
C     lflag         = 0 if neither cell nor matrix found
C                   .lt. 0 if error
C                   = 1 cell only
C                   = 2 Scale only (no cell returned)
C                   = 3 both found
C
C
C
      integer iun, lflag, nspgrp, z
      real rf(4,4), ro(4,4), cell(6)
      character*(*) spgrp
C
      character line*80
      integer nsc, i, j
      real vol, rall(3,3,6)
C
      character*1 c123(3)
      data c123 /'1','2','3'/

c     CIF stuff
c     If (cifin), then iun is the block id which is being processed.


      character*80 CellItems(7)
      data CellItems
     &  /'_cell.length_a',
     &  '_cell.length_b',
     &  '_cell.length_c',
     &  '_cell.angle_alpha',
     &  '_cell.angle_beta',
     &  '_cell.angle_gamma',
     &  '_cell.Z_PDB' /


      character*80 Trans2FracItem(4,4)
      data ((Trans2FracItem(i,j), j=1,3), i=1,3),
     &  (Trans2FracItem(i,4), i=1,3)
     &  
     &  /'_atom_sites.fract_transf_matrix[1][1]',
     &  '_atom_sites.fract_transf_matrix[1][2]',
     &  '_atom_sites.fract_transf_matrix[1][3]',
     &  '_atom_sites.fract_transf_matrix[2][1]',
     &  '_atom_sites.fract_transf_matrix[2][2]',
     &  '_atom_sites.fract_transf_matrix[2][3]',
     &  '_atom_sites.fract_transf_matrix[3][1]',
     &  '_atom_sites.fract_transf_matrix[3][2]',
     &  '_atom_sites.fract_transf_matrix[3][3]',
     &  '_atom_sites.fract_transf_vector[1]',
     &  '_atom_sites.fract_transf_vector[2]',
     &  '_atom_sites.fract_transf_vector[3]' /



c ... Local variables
      integer context, status
      character*80 token, catname

c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)


c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/

C
      nsc = 0
      lflag = 0

      z = 0
      spgrp = ' '

      if ( cifin ) then

c      CIF branch
c      ----------

c______ First, mmCIF category CELL:
        call ccif_setup_context(CellItems(1), catname, iun, context,
     &    status, 'ro')
        if ( status.eq.0 ) goto 500

c________Loop over a, b, c, alpha, beta, gamma
        do 400 i = 1,6
          status = keep_context
          call ccif_get_real(CellItems(i), token, cell(i), context,
     &      status)
          status = status/2

          if ( status .ne. value .and. status .ne. dict ) then
c________Cell specification is incomplete
            lflag = -1
            return
          endif

 400    continue

c________Get Z (z only changed from 0 if value or dict, so don't need
c        to check status
        call ccif_get_int(CellItems(7), token, z, context, status)

        lflag = lflag + 1
        call ccif_release_context(context)
        
c_______ Next, mmCIF category ATOM_SITES, for fractionalisation matrix

 500    call ccif_setup_context(Trans2FracItem(1,1), catname, iun,
     &    context, status, 'ro')

        if ( status.ne.2 ) goto 100

        do 510 i = 1,3
          do 520 j = 1,4
            status = keep_context
            call ccif_get_real(Trans2FracItem(i,j), token, rf(i,j),
     &        context, status)
            status = status/2

            if ( status .ne. value .and. status .ne. dict ) then
c________ matrix is incomplete
              lflag = -1
              return
            endif

 520      continue
 510    continue

        lflag = lflag + 2
        call ccif_release_context(context)

        call getspg(iun, spgrp, nspgrp)

      else

c      PDB format branch
c      -----------------
        
 10     read (iun, '(a)') line
        if (line(1:4) .eq. 'ATOM' .or. line(1:6) .eq. 'HETATM') then
          go to 100
        elseif (line(1:6) .eq. 'CRYST1') then
C CRYST1 line, set cell
          read (line, 6001) cell
 6001     format(6x,3f9.3,3f7.2)
          spgrp = line(56:66)
          read (line(67:70), '(i4)', err=15) z
15        lflag = lflag + 1
        elseif (line(1:5) .eq. 'SCALE') then
C SCALE  line, set rf matrix
          nsc = nsc+1
          if (line(6:6) .ne. c123(nsc)) then
            lflag = -1
            return
          endif
          read (line, 6002) (rf(nsc,j), j=1,4)
 6002     format(6x,4x,3f10.5,5x,f10.5)
          if (nsc .eq. 3) then
            do 20, j = 1,3
              rf(4,j) = 0.0
 20         continue
            rf(4,4) = 1.0
            lflag = lflag+2
          endif
        endif
        go to 10
        
      endif


c      Both PDB and CIF formats
c      ------------------------
C
C-------
C All header lines read, process matrices
C  If cell was read, make matrix from cell
 100  if (mod(lflag,2) .eq. 1) then
         call rbfro1(cell,vol,rall)
         do 210, j=1,4
            do 211, i=1,4
               ro(i,j)=0.0
               if (i.lt.4.and. j.lt.4) ro(i,j)=rall(i,j,1)
 211        continue
 210     continue
         ro(4,4)=1.0
C     Invert
         call rbrinv(ro,rf)
C
      elseif (lflag .gt. 0) then
         call rbrinv(rf, ro)
      endif
C
      if ( .not. cifin ) rewind (unit=iun)
      return
      end
C


c============================================
      subroutine getspg(block, spgrp, nspgrp)
c============================================


c____Get space group info. from block of CIF. (For PDB files, is
c    read easily from CRYST1 card).
      
c ... Parameters
      integer block, nspgrp
      character*(*) spgrp

c ... Local variables
      integer context, status, lspgrp
      character*80 catnam, token


c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)


      character*80 SpgNumItem, SpgNamHMItem, SpgNamHallItem
      data SpgNumItem, SpgNamHMItem, SpgNamHallItem
     &  /'_symmetry.Int_Tables_number',
     &  '_symmetry.space_group_name_H-M',
     &  '_symmetry.space_group_name_Hall' /


      spgrp = ' '
      nspgrp = 0


      call ccif_setup_context(SpgNumItem, catnam, block, Context,
     &  Status, 'ro')

      if ( status.eq.0 ) return

      
c_______Try for space group number
      status = keep_context
      call ccif_get_int(SpgNumItem, token, nspgrp, context, status)

c_______and now HM notation        
      Status = keep_context
      call ccif_get_char(SpgNamHMItem, token, spgrp, lspgrp,
     &  context, status)
          

c_______Don't bother with Hall notation (yet)

      end



c==========================================================
      subroutine mkhdr(iunout, cell, spgnam, spgnum, z, rf)
c==========================================================

c ... Parameters
      integer iunout, z, spgnum
      real cell(6), rf(4,4)
      character*(*) spgnam

c ... Local variables
      integer i,j


c     CIF stuff
      character*80 CellItems(7)
      data CellItems
     &  /'_cell.length_a',
     &  '_cell.length_b',
     &  '_cell.length_c',
     &  '_cell.angle_alpha',
     &  '_cell.angle_beta',
     &  '_cell.angle_gamma',
     &  '_cell.Z_PDB' /


      character*80 Trans2FracItem(4,4)
      data ((Trans2FracItem(i,j), j=1,3), i=1,3),
     &  (Trans2FracItem(i,4), i=1,3)
     &  
     &  /'_atom_sites.fract_transf_matrix[1][1]',
     &  '_atom_sites.fract_transf_matrix[1][2]',
     &  '_atom_sites.fract_transf_matrix[1][3]',
     &  '_atom_sites.fract_transf_matrix[2][1]',
     &  '_atom_sites.fract_transf_matrix[2][2]',
     &  '_atom_sites.fract_transf_matrix[2][3]',
     &  '_atom_sites.fract_transf_matrix[3][1]',
     &  '_atom_sites.fract_transf_matrix[3][2]',
     &  '_atom_sites.fract_transf_matrix[3][3]',
     &  '_atom_sites.fract_transf_vector[1]',
     &  '_atom_sites.fract_transf_vector[2]',
     &  '_atom_sites.fract_transf_vector[3]' /

      character*80 SpgNamItem, SpgNumItem
      data SpgNamItem, SpgNumItem
     &  /'_symmetry.space_group_name_H-M',
     &  '_symmetry.Int_Tables_number' /


      character*80 CellKey, AtomSitesKey, SymmKey
      data CellKey, AtomSitesKey, SymmKey
     &  /'_cell.entry_id',
     &  '_atom_sites.entry_id',
     &  '_symmetry.entry_id' /

c ... Local variables
      integer context, status
      character*80 token, catnam

c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)


c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/
      


      if ( cifout ) then

c______write out spacegroup name and number first
        if ( spgnam .ne. ' ' .or. spgnum .ne. 0 ) then
          call ccif_setup_context(SymmKey, catnam, iunout, context,
     &      status, 'rw')

          status = keep_context
          call ccif_put_char(SymmKey, entry, context, status)

          if ( spgnam .ne. ' ' ) then
            status = keep_context
            call ccif_put_char(SpgNamItem, spgnam, context, status)
          endif

          if ( spgnum .ne. 0 ) then
            status = keep_context
            call ccif_put_int(SpgNumItem, spgnum, context, status)
          endif
          
          call ccif_release_context(context)
        endif

c______Now, the cell
        call ccif_setup_context(CellKey, catnam, iunout, context,
     &    status, 'rw')
        status = keep_context
        call ccif_put_char(CellKey, entry, context, status)
        do 100 i = 1,6
          status = keep_context
          call ccif_put_real(CellItems(i), cell(i), context, status)
 100    continue

        if ( z .ne. 0 ) then
          status = keep_context
          call ccif_put_int(CellItems(7), z, context, status)
        endif
        
        call ccif_release_context(context)

c______and finally, the matrix        
        call ccif_setup_context(AtomSitesKey, catnam, iunout, context,
     &    status, 'rw')
        status = keep_context
        call ccif_put_char(AtomSitesKey, entry, context, status)

        do 110 i = 1,3
          do 120 j = 1,3
            status = keep_context
            call ccif_put_real(Trans2FracItem(i,j), rf(i,j),
     &        context, status)
 120      continue
 110    continue

c______don't need to output translational parts - they are
c      0 by default, in the dictionary

        call ccif_release_context(context)
        
      else

c      PDB format branch
c      -----------------

        if ( z .eq. 0 ) then
          write (iunout,6000) cell,spgnam
        else
          write (iunout, 6002) cell, spgnam, z
        endif
        write (iunout,6001) (i,(rf(i,j),j=1,3),i=1,3)

 6000   format('CRYST1',3f9.3,3f7.2,1x,a11)
 6002   format('CRYST1',3f9.3,3f7.2,1x,a11,i3)
 6001   format(2('SCALE',i1,4x,3f10.5,5x,'   0.00000'/),
     &    ('SCALE',i1,4x,3f10.5,5x,'   0.00000'))

      endif
      
      end


c=======================================================================
      subroutine rdcoor(iun, iunout, iser, iserch, liser, atnam, attyp,
     &  restyp, idch, resno, iresno, insert, x, q, b, segid, header,
     &  lstcrd)
c=======================================================================

      
c ... Parameters

      integer iun, iunout, iser, liser, iresno
      character*(*) atnam, restyp, idch, resno, insert, segid, iserch,
     &  attyp
      real x(3), q, b

c       If header is .true., output header records to iunout
      logical header, lstcrd

c ___ iserch for PDB files is iser, converted to a string.
c     For CIF's, it is _atom_site.id, which is not necessarily a number.
c     Where it consists of digits only, it will be written into iser
c     as an integer, otherwise, iser will be returned as -1.


c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/

      integer cxin, cxout
      common /cifint/ cxin, cxout
      save /cifint/

c ... CIF stuff

      character*80 AtomSiteItems(5), AtomLabelItems(5),
     &  AtomAuthItems(4), AtomCoordItems(3), AtomBisoItem, AtomOccItem
      data AtomSiteItems, AtomLabelItems, AtomAuthItems, AtomCoordItems,
     &  AtomBisoItem, AtomOccItem /
     &  '_atom_site.id',
     &  '_atom_site.group_PDB',
     &  '_atom_site.type_symbol',
     &  '_atom_site.entity_id',
     &  '_atom_site.entity_seq_num',
     &  
     &  '_atom_site.label_atom_id',
     &  '_atom_site.label_comp_id',
     &  '_atom_site.label_asym_id',
     &  '_atom_site.label_seq_id',
     &  '_atom_site.label_alt_id',
     &  
     &  '_atom_site.auth_atom_id',
     &  '_atom_site.auth_comp_id',
     &  '_atom_site.auth_asym_id',
     &  '_atom_site.auth_seq_id',
     &  
     &  '_atom_site.cartn_x',
     &  '_atom_site.cartn_y',
     &  '_atom_site.cartn_z',
     &  '_atom_site.B_iso_or_equiv',
     &  '_atom_site.occupancy'/
      

c ... Element data (a la RBROOK, more-or-less)

      CHARACTER*2 IATM(100),IHATM(10), atlist*200, hlist*20
      equivalence(iatm, atlist)
      equivalence(ihatm, hlist)

      DATA IATM/' H','He','Li','Be',' B',' C',' N',' O',' F','Ne',
     *          'Na','Mg','Al','Si',' P',' S','Cl','Ar',' K','Ca',
     *          'Sc','Ti',' V','Cr','Mn','Fe','Co','Ni','Cu','Zn',
     *          'Ga','Ge','As','Se','Br','Kr','Rb','Sr',' Y','Zr',
     *          'Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn',
     *          'Sb','Te',' I','Xe','Cs','Ba','La','Ce','Pr','Nd',
     *          'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb',
     *          'Lu','Hf','Ta',' W','Re','Os','Ir','Pt','Au','Hg',
     *          'Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th',
     *          'Pa',' U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm'/
      DATA IHATM/'0h','1h','2h','3h','4h','5h','6h','7h','8h','9h'/


c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row, end_of_context
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3, end_of_context=8)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)

      


c ... Local variables
      character*80 atline, token, catnam, tempc
      integer latnam, lrestyp, lresno, lsegid, status, lattyp,
     &  i, j, ichrge

      lstcrd = .false.

      if (cifin) then

c      CIF branch
c      ----------

        if ( cxin .eq. -1 ) then
          call ccif_setup_context(AtomSiteItems(1), catnam, iun,
     &      cxin, status, 'ro')
          if ( status .eq. 0 ) then
            cxin = -1
            lstcrd = .true.
            return
          endif
          status = keep_context
        else
          status = advance_context
        endif


c ___ Get _atom_site.id (corresponds to PDB atom serial number, except
c     that it may be non-numeric)
        
 40     call ccif_get_char(AtomSiteItems(1), token, atline, liser,
     &    cxin, status)

        if ( status .eq. end_of_context ) then
          cxin = -1
          lstcrd = .true.
          return
        endif
        
        if (status/2 .ne. value .and. status/2 .ne. dict) then
          print*,' *** Atom site is missing mandatory _atom_site.id'//
     &      ' item! ***'
          iserch = ' '
          goto 59
        endif
        
c ___ Convert to integer, if possible.
        if (liser.le.5) then
          iserch = atline(:liser)
          iser = 0
          do 50 i = 1,liser
            read(iserch,'(i1)', err=59) j
            iser = iser*10 + j
 50       continue
        else
          iserch = atline(:len(iserch))
        endif

        goto 70

 59     iser = -1


c ___ Get atom type
 70     status = keep_context
        call ccif_get_char(AtomSiteItems(3), token, atline, lattyp,
     &    cxin, status)
        status = status/2
        if ( status.ne.value .and. status.ne.dict ) then
          print'(4a)',' *** Mandatory item _atom_site.type_symbol ',
     &      'missing from atom ''', iserch(:liser), ''' ***'
          print*,' *** Skipping ***'
          status = advance_context
          goto 40
        else

c ___ There is a slight problem here with atom types, in that
c     in the pdb format, atom types will come out right-justified
c     in 4 characters, but in CIF's, they will never have
c     a leading space (unless quoted). This is a consequence of
c     STAR.


c ___ Here we attempt to decide if the atom type would have had a leading
c     space, if it had come out of a PDB file, and fix it up, if yes.
c     CIF is flexible enough to cope with the leading space on output
c     without causing any problems
          
          tempc = atline( : min(len(attyp),lattyp) )
          call ckatyp(tempc, ichrge)
          if ( ichrge.eq.2 .or. lattyp.eq.1) then
            attyp = ' '//tempc
          else
            attyp = tempc
          endif
          
          if ( ichrge.eq.3 ) then
            call ccpupc(attyp(1:1))
            call ccplwc(attyp(2:2))
          endif
        endif


c
c---- Omit hydrogens if required
        if (lhydro .and. ichrge.eq.2 .and. attyp(:1).eq.'H' ) then
          status = advance_context
          goto 40
        endif


c ___  Get coordinates
        
        do 100 i=1,3
          status = keep_context
          call ccif_get_real(AtomCoordItems(i), token, x(i),
     &      cxin, status)

          status = status/2
          if (status.ne.value .and. status.ne.dict) then
            print'(3a)',' *** Coordinate(s) missing from atom ''',
     &        iserch(:liser), ''' - skipping ***'
            status = advance_context
            goto 40
          endif

 100    continue


c
c---- Omit dummys if required
        if (lnodum .and. abs(x(1)) .gt. xdummy) then
          status = advance_context
          go to 40
        endif


c ___ Get Biso. Fixme! Really need to look for U's, and aniso B/U as well.
c     The relationships between these are not yet defined fully in mmCIF,
c     so I'll wait on events for now.
        status = keep_context
        call ccif_get_real(AtomBisoItem, token, b, cxin, status)
        status = status/2
        if (status.ne.value .and. status.ne.dict) then
          print'(3a)',' *** Biso not determinable for atom ''',
     &      iserch(:liser), ''' ***'
          b = 0.0
        endif

c ___ Get Occupancy
        status = keep_context
        call ccif_get_real(AtomOccItem, token, q, cxin, status)
        status = status/2
        if ( status.ne.value .and. status.ne.dict ) then
c ___     With mmCIF, should never end up here, since the default occupancy
c         is 1.0.
          print'(3a)',' *** Occupancy not determinable for atom ''',
     &      iserch(:liser), ''' ***'
          print*,'Please consider supplying a default value in '//
     &      'your CIF dictionary.'
        endif
        
c ___ Get atom name
        status = keep_context
        call ccif_get_char(AtomLabelItems(1), token, atline, latnam,
     &    cxin, status)
        status = status/2
        if ( status.ne.value .and. status.ne.dict ) then
          print'(3a)',' *** Cannot determine atom name for atom ''',
     &      iserch(:liser), ''' ***'
          atnam = ' '
        else
          atnam = atline( : min(len(atnam),latnam) )
        endif 

c ___ Get residue type
        status = keep_context
        call ccif_get_char(AtomLabelItems(2), token, atline, lrestyp,
     &    cxin, status)
        status = status/2
        if ( status.ne.value .and. status.ne.dict ) then
          print'(3a)',' *** Cannot determine residue type of atom ''',
     &      iserch(:liser), ''' ***'
          restyp = ' '
        else
          restyp = atline( : min(len(restyp),lrestyp) )
        endif

c ___ Get chain id (use segid, and leave idch blank
        status = keep_context
        call ccif_get_char(AtomLabelItems(3), token, atline, lsegid,
     &    cxin, status)
        status = status/2
        idch = ' '
        if ( status.ne.value .and. status.ne.dict ) then
          segid = ' '
        else
          segid = atline( : min(len(segid),lsegid) )
        endif
          
c ___ Get residue number (extracting insertion character, if present)
        status = keep_context

        call ccif_get_int(AtomLabelItems(4), token, iresno,
     &    cxin, status)

        status = status/2
        if ( status.ne.value .and. status.ne.dict ) iresno = -1

c        call ccif_get_char(AtomLabelItems(4), token, atline, lresno,
c     &    cxin, status)


        status = keep_context
        call ccif_get_char(AtomAuthItems(4), token, atline, lresno,
     &    cxin, status)

        status = status/2
        if ( status.ne.value .and. status.ne.dict )
     &    write(resno, '(i4)') iresno

        lresno = min(len(resno),lresno)
        resno = atline(:lresno)
        call ccpupc( resno(lresno:lresno) )
        if ( lge(resno(lresno:lresno),'A') .and.
     &    lle(resno(lresno:lresno),'Z') ) then
           insert = resno(lresno:lresno)
           resno(lresno:lresno) = ' '
        endif

      else
        
c      PDB format branch
c      -----------------

400     read (iun,'(a)',end=500) atline

c____The logic of this test has been reversed from when it was
c    in the main program, so that this subroutine can deal with
c    records other than ATOM and HETATM internally

        if (atline(1:4) .ne. 'ATOM' .and. atline(1:4) .ne. 'HETA') then

          if (atline(1:3) .eq. 'TER') then
C---- Always copy TER line
            call wrline(iunout, atline)
          elseif (header) then
C---- Copy non-ATOM lines from input to output, unless EXCLUDE HEADERS set
C     except for CRYST1 & SCALE lines if they have been replaced
            if (lnewhd) then
              if (atline(1:5) .ne. 'SCALE' .and.
     &          atline(1:5) .ne. 'CRYST')
     &          call wrline(iunout, atline)
            else
              call wrline(iunout, atline)
            endif
          endif
          goto 400
        else
        

          if (lxplor) then
C Special read option for Xplor output, residue numbers .ge. 1000 are in
C     different column
            if (atline(27:27) .eq. ' ') then
              read (atline,fmt=6102)
     &          iser,atnam,restyp,resno,x,q,b,segid
 6102         format(6x,i5,1x,a4,a4,2x,a4,1x,3x,3f8.3,2f6.2,6x,a)
            else
              read (atline,fmt=6103)
     &          iser,atnam,restyp,resno,x,q,b,segid
 6103         format(6x,i5,1x,a4,a4,3x,a4,3x,3f8.3,2f6.2,6x,a)
            endif
            idch = segid(1:1)
c  Make chain ID uppercase
            call ccpupc(idch)
            insert = ' '
          else
c Normal input
            read (atline,fmt=6101)
     &        iser,atnam,restyp,idch,resno,insert,x,q,b
 6101       format(6x,i5,1x,a4,a4,1x,a1,a4,a1,3x,3f8.3,2f6.2)


c ___ Determine atom type - this is part of the new (February 1996)
c     specification, but it is too useful not to make use of it right away


            if ( atline(77:77).eq.' ' ) then
              call ccpupc(atline(78:78))
            else
              call ccpupc(atline(77:77))
              call ccplwc(atline(78:78))
            endif
            
c ___ Using correct upper/lower case for elements, ensures that this
c     test will always be correct.
            if ( index(atlist, atline(77:78)) .ne. 0 ) then
              attyp = atline(77:80)
            else
              attyp = atnam(:2)
              if ( attyp(1:1).eq.' ' ) then
                call ccpupc(attyp(2:2))
              else
                call ccpupc(attyp(1:1))
                call ccplwc(attyp(2:2))
              endif
              if ( index(atlist,attyp(:2)) .eq. 0 ) then
                if ( index(hlist, attyp(:2)) .eq. 0 ) then
                  print'(a, i5, a)',
     &              ' *** Can''t find element of atom ''', iser,
     &              ''' ***'
                  print*,
     &              ' Put symbol (right-justified) in columns 77-78'
                  attyp = 'XXXX'
                else
                  attyp = ' H'
                endif
              endif
            endif
            
              
            segid = idch
          endif

c
c---- Omit hydrogens if required
          if (lhydro) then
            if ( attyp .eq. ' H' ) goto 400
          endif
c
c---- Omit dummys if required
          if (lnodum) then
            if (abs(x(1)) .gt. xdummy) then
              go to 400
            endif
          endif

        endif

        iserch = ' '
        iserch(1:5) = atline(7:12)
        liser = 5
        iresno = -1

      endif


      return


500   lstcrd = .true.
      
      end


c=================================================================
      subroutine wrcoor(iunout, natom, atnam, restyp, idch, resno,
     &  iresno, insert, x, q, b, segid, attyp, cpyrow)
c=================================================================


c ___ NOTE: The best way to use this routine effectively for CIF 
c     output, is to specify the residue identifier in resno and
c     insert. iresno will be overridden as appropriate, to ensure
c     consistency where atoms are out of sequence

c ... Parameters
      integer iunout, natom, iresno
      character*(*) atnam, restyp, idch, resno*4, insert*1, segid, attyp
      real x(3), q, b
      logical cpyrow

c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/

      integer cxin, cxout
      common /cifint/ cxin, cxout
      save /cifint/


c ... CIF stuff

      character*80 AtomSiteItems(5), AtomLabelItems(5),
     &  AtomAuthItems(4), AtomCoordItems(3), AtomBisoItem, AtomOccItem
      data AtomSiteItems, AtomLabelItems, AtomAuthItems, AtomCoordItems,
     &  AtomBisoItem, AtomOccItem /
     &  '_atom_site.id',
     &  '_atom_site.group_PDB',
     &  '_atom_site.type_symbol',
     &  '_atom_site.entity_id',
     &  '_atom_site.entity_seq_num',
     &  
     &  '_atom_site.label_atom_id',
     &  '_atom_site.label_comp_id',
     &  '_atom_site.label_asym_id',
     &  '_atom_site.label_seq_id',
     &  '_atom_site.label_alt_id',
     &
     &  '_atom_site.auth_atom_id',
     &  '_atom_site.auth_comp_id',
     &  '_atom_site.auth_asym_id',
     &  '_atom_site.auth_seq_id',
     &  
     &  '_atom_site.cartn_x',
     &  '_atom_site.cartn_y',
     &  '_atom_site.cartn_z',
     &  '_atom_site.B_iso_or_equiv',
     &  '_atom_site.occupancy'/

c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row, end_of_context
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3, end_of_context=8)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)



c ... Local variables
      integer status, i, ikey(2), sindex, scxt, idstat, seqid

c ... iresloc is a local value of iresno, to avoid passing a modified
c     value if iresno back to the calling program.
c     maxresno is the highest residue number used so far
      integer iresloc, maxresno

      character*80 catnam, iserch, idout, cval, ckeybuf*160
      real fkey(2)

      save sindex, scxt, maxresno


      if ( attyp.eq.' ' ) attyp = 'XX'

      if ( cifout ) then

c      CIF branch
c      ----------

        if ( cxout .eq. -1 ) then
          call ccif_setup_context(AtomSiteItems(1), catnam, iunout,
     &      cxout, status, 'loop')
          if ( status .eq. 0 ) then

c ___  Set up sort and local context, to use as lookup table between
c      _atom_site.label_seq_id and _atom_site.auth_seq_id:
             
             call ccif_setup_context(AtomSiteItems(1), catnam, iunout,
     &            scxt, status, 'rw')
             call ccif_setup_sort(AtomSiteItems(1), iunout, sindex,
     &            status)
             call ccif_add_to_sort(AtomLabelItems(3), sindex, status)
             call ccif_add_to_sort(AtomAuthItems(4), sindex, status)
             call ccif_do_sort(sindex, status)
             call ccif_sort_context(sindex, scxt, status)

             maxresno = 0
             
          else
            call ccperr(1,
     &       '*** ATOM_SITE category already exists in output file ***')
          endif
        endif



        status = append_row
        iresloc = iresno


c ___ Work out residue identifier (a la PDB) for use with
c      _atom_site.auth_seq_id

c ___  This is a fall-back - it is far better for the calling
c      routine to specify resno, and let this 
c      subroutine override iresno if appropriate

        if ( resno .eq. ' ' ) then
           if ( iresno.gt.0 ) then
              write(resno, '(i4)') iresno
c ___ idout will eventually be written out as _atom_site.auth_seq_id
              idout = '''' // resno // insert // ''''
           else
              resno = '.'
              idout = '.'
              insert = ' '
           endif
        else
c ___ idout will eventually be written out as _atom_site.auth_seq_id
           idout = '''' // resno // insert // ''''
        endif


c =====
c ___ Search for any atom already in output CIF, which has the given
c     chain id and author-specified residue id.
c =====

        ckeybuf = idch
        if (idch .eq. ' ') ckeybuf = '.'
c ___ This ties in with the way that the token is written out.
c     The residue  is basically (i4,a1), single quoted.
        ckeybuf(81:) = idout(2:6)
        
        call ccif_search_context(sindex, scxt, 2, ikey, fkey, ckeybuf,
     &       80, idstat)

        if ( idstat .eq. 1 ) then

c ___ Residue with this idout exists in output file, so re-use
c      _atom_site.label_seq_id if it exists

           idstat = keep_context
           call ccif_get_int(AtomLabelItems(4), cval, seqid, 
     &          scxt, idstat)
           idstat = idstat/2

c ___ If there already is a _atom_site.label_seq_id corresponding to
c     our putative _atom_site.auth_seq_id, override the value of
c     iresno which has been passed in to this routine.

           if (idstat.eq.value .or. idstat.eq.dict) then
              iresno = seqid
           else
              call ccperr(1,
     &             ' *** Problem with _atom_site.label_seq_id!')
           endif
        else
c ___      First atom in a new residue          
           maxresno = maxresno + 1
           iresno = maxresno
        endif

        if ( cpyrow .and. cifin .and. cxin.ne.-1 ) then
           call ccif_copy_row(cxin, cxout, status)
           status = keep_context
        endif
        
c ___ Write out atom serial number
        write(iserch,'(i5.5)') natom
c     Status is already set to appropriate value
        call ccif_put_char(AtomSiteItems(1), iserch, cxout, status)

c ___ Write out atom type
        status = keep_context
        call ccif_put_char(AtomSiteItems(3), attyp, cxout, status)

c ___ Write out atom name
        status = keep_context
        call ccif_put_char(AtomLabelItems(1), atnam, cxout, status)

c ___ Write out residue type
        status = keep_context
        call ccif_put_char(AtomLabelItems(2), restyp, cxout, status)

c ___ Write out chain id
        status = keep_context
        if ( segid .ne. ' ' ) then
          call ccif_get_char(AtomLabelItems(3), segid, cxout, status)
        elseif ( idch .ne. ' ' ) then
          call ccif_put_char(AtomLabelItems(3), idch, cxout, status)
        else
          call ccif_put_char(AtomLabelItems(3), '.', cxout, status)
        endif

c ___ Write out residue numbers (The correspondence between 
c     _atom_site.label_seq_id and _atom_site.auth_seq_id has already
c     been checked above.

        status = keep_context
        if (iresno.ge.1) then
           call ccif_put_int(AtomLabelItems(4), iresno, cxout, status)
        else
           call ccif_put_char(AtomLabelItems(4), '.', cxout, status)
        endif

        status = keep_context
        call ccif_put_char(AtomAuthItems(4), idout, cxout, 
     &       status)


c ____ Write out coordinates
        do 100 i = 1,3
          status = keep_context
          call ccif_put_real(AtomCoordItems(i), x(i), cxout, status)
 100    continue

c ___ Write out Biso
        status = keep_context
        call ccif_put_real(AtomBisoItem, b, cxout, status)

c ___ Write out occupancy
        status = keep_context
        call ccif_put_real(AtomOccItem, q, cxout, status)


      else
        
c      PDB format branch
c      -----------------

c ___ Make sure that residue number is right-justified in field
        i = index(resno(:4),' ') - 1
        if (i.eq.-1) i = 4
        
        write (iunout,fmt=6111)
     &    natom,atnam(:4),restyp(:4),idch(:1),resno(:i),insert(:1),
     &    x,q,b,segid(:4),attyp(:4)
 6111   format('ATOM  ',i5,1x,a4,a4,1x,a1,a4,a1,3x,3f8.3,2f6.2,6X,2A)

      endif
      
      end



c====================================
      subroutine wrline(iunout, line)
c====================================

c____Takes a PDB-format line (other than ATOM or HETATM), and decides
c    how to output it      

c ... Parameters
      integer iunout
      character*(*) line

c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/

c ... Local variables
      real cell(6), row(3), u
      character spgrp*11, catnam*80
      integer i, j, z, context, status, n


c ... CIF stuff

      character*80 CellItems(7)
      data CellItems
     &  /'_cell.length_a',
     &  '_cell.length_b',
     &  '_cell.length_c',
     &  '_cell.angle_alpha',
     &  '_cell.angle_beta',
     &  '_cell.angle_gamma',
     &  '_cell.Z_PDB' /

      character*80 SpgNumItem, SpgNamHMItem, SpgNamHallItem
      data SpgNumItem, SpgNamHMItem, SpgNamHallItem
     &  /'_symmetry.Int_Tables_number',
     &  '_symmetry.space_group_name_H-M',
     &  '_symmetry.space_group_name_Hall' /


      character*80 Trans2FracItem(4,4)
      data ((Trans2FracItem(i,j), j=1,3), i=1,3),
     &  (Trans2FracItem(i,4), i=1,3)
     &  
     &  /'_atom_sites.fract_transf_matrix[1][1]',
     &  '_atom_sites.fract_transf_matrix[1][2]',
     &  '_atom_sites.fract_transf_matrix[1][3]',
     &  '_atom_sites.fract_transf_matrix[2][1]',
     &  '_atom_sites.fract_transf_matrix[2][2]',
     &  '_atom_sites.fract_transf_matrix[2][3]',
     &  '_atom_sites.fract_transf_matrix[3][1]',
     &  '_atom_sites.fract_transf_matrix[3][2]',
     &  '_atom_sites.fract_transf_matrix[3][3]',
     &  '_atom_sites.fract_transf_vector[1]',
     &  '_atom_sites.fract_transf_vector[2]',
     &  '_atom_sites.fract_transf_vector[3]' /



      character*80 CellKey, AtomSitesKey, SymmKey
      data CellKey, AtomSitesKey, SymmKey
     &  /'_cell.entry_id',
     &  '_atom_sites.entry_id',
     &  '_symmetry.entry_id' /

c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row, end_of_context
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3, end_of_context=8)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)

      

      if ( cifout ) then

c      CIF branch
c      ----------

c ___ CRYST1 card -> CIF        
        if ( line(:6) .eq. 'CRYST1' ) then
          read(line, '(6x,3f9.3,3f7.2)', err=29 ) cell


c ___    Write out cell constants
          call ccif_setup_context(CellKey, catnam, iunout, context,
     &      status, 'rw')
          status = keep_context
          call ccif_put_char(CellKey, entry, context, status)

          do 20 i = 1,6
            status = keep_context
            call ccif_put_real(CellItems(i), cell(i), context, status)
 20       continue
          


c ___    Write out Z
          read (line(67:70), '(i4)', err=25) z
          status = keep_context
          call ccif_put_int(CellItems(7), z, context, status)

 25       call ccif_release_context(context)

c ___    Write out space group name
          spgrp = line(56:66)
          if (spgrp.eq.' ') spgrp = '?'
          call ccif_setup_context(SymmKey, catnam, iunout, context,
     &      status, 'rw')
          status = keep_context
          call ccif_put_char(SymmKey, entry, context, status)
          status = keep_context
          call ccif_put_char(SpgNamHMItem, spgrp, context, status)
          call ccif_release_context(context)
          
          return


 29       print*,
     &      ' *** Badly-formed CRYST1 card: not converting to CIF ***'
          call ccif_release_context(context)
          

c ___ SCALEn cards -> CIF
        elseif ( line(:5) .eq. 'SCALE' ) then

          read (line(6:6), '(i1)',err=39) n
          if (n .gt. 3) goto 39

          read (line(11:), '(3f10.6, 5x, f10.5)', err=39) row, u

          call ccif_setup_context(AtomSitesKey, catnam, iunout,
     &      context, status, 'rw')
          if ( status.eq.0 ) then
            status = keep_context
            call ccif_put_char(AtomSitesKey, entry, context, status)
          endif

          do 30 i = 1,3
            status = keep_context
            call ccif_put_real(Trans2FracItem(n,i), row(i),
     &        context, status)
 30       continue

          status = keep_context
          call ccif_put_real(Trans2FracItem(n,4), u, context, status)

          call ccif_release_context(context)
          
          return
          
 39       print'(3a)', ' *** Badly-formed ',line(:6),
     &      ' card: not converting to CIF ***'
        endif
        
      else

c      PDB format branch
c      -----------------
        
        write (iunout,'(a)') line
      endif
      
      end


c===========================
      subroutine rwdcrd(iun)
c===========================

c ... Parameters
      integer iun

c ... Local variables
      character*80 catnam
      integer status

c ... Commons
      integer cxin, cxout
      common /cifint/ cxin, cxout
      save /cifint/

      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry

      save /iostat/

      if ( cifin ) then
        call ccif_release_context(cxin)
        call ccif_setup_context('ATOM_SITE', catnam, iun, cxin,
     &    status, 'ro')
      else
        rewind (unit=iun)
      endif
      end

c=====================================
      subroutine wrtrmk(iunrmk, field)
c=====================================

c     Write REMARK records to a scratch file, for later output.

c ... Parameters
      integer iunrmk
      character*(*) field

c ... local variables
      integer length

c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/

      if ( .not. rmkopn ) then
        call ccpdpn(iunrmk, 'TEMP1', 'SCRATCH', 'F', 0, 0)
        rmkopn = .true.
      endif

      length = min(72, len(field))
      write( iunrmk, '(''REMARK  '',a)' ) field(:length)
      end


c======================================
      subroutine dmprmk(iunrmk, iunout)
c======================================

c      Write out REMARK records from scratch file iunrmk, and close.

c ... Parameters
      integer iunrmk, iunout

c ... Local variables
      character*80 line, catnam
      integer context, status, i
      
c ... Commons
      logical lnewhd, lxplor, lhydro, lnodum, rmkopn, cifin, cifout
      real xdummy
      character*80 entry
      common /iostat/ lnewhd, lxplor, lhydro, lnodum, rmkopn, xdummy,
     &  cifin, cifout, entry
      save /iostat/


c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)

      
      rewind(iunrmk)

      if ( cifout ) then
        call ccif_setup_context('DATABASE_PDB_REMARK', catnam, iunout,
     &    context, status, 'rw')
        
        if ( status.eq.0 ) then
          status = keep_context
          call ccif_put_char('_database_PDB_remark.id', '0', context,
     &      status)
        endif
        
10      read(iunrmk,'(a)',end=19) line
        status = keep_context
        call ccif_put_text('_database_PDB_remark.text', 1, line, 1,
     &    context, status, ' ')
        goto 10

 19     call ccif_release_context(context)

      else

20      read(iunrmk,'(a)',end=99) line
        write(iunout, '(a)' ) line
        goto 20

      endif

      
 99   close(iunrmk)
      rmkopn = .false.
      end



c===================================================
      subroutine ccif_new(lognam, db, entry, block)
c===================================================

c      Trivial wrapper for CCIF's ccif_new_cif routine.
c      Creates a CIF with one data block
      
c ...  Parameters
      character*(*) lognam, db, entry
      integer block

c ...  local variables
      integer status, context
      character*80 catname, errmsg*160

c ... CCIF mnemonics
      integer start_new_context, keep_context, advance_context,
     & append_row
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3)
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)

c ... Commons
      integer cxin, cxout
      common /cifint/ cxin, cxout
      save /cifint/

      logical cifdic
      common /ccif/ cifdic
      save /ccif/

      if ( .not. cifdic ) then
        call ccif_init('CCIFDIC')
        cifdic = .true.
      endif

      if ( db .eq. ' ' )
     &  call ccperr(1, '*** Must specify name for new data block! ***')

      call ccif_new_cif(' ', lognam, block)
      call ccif_block_by_name(lognam, db, block, status, 'new')

      if ( status .ne. 2 ) then
c ___ ESV again....
         write (errmsg,
     &        '(''*** Cannot create data block "'',a,''" ***'')' ) db
         call ccperr(1,errmsg)
      endif

c ___ So that wrcoor can tell that the file has just been opened
      cxout = -1

c______Write _entry.id
      call ccif_setup_context('ENTRY', catname, block, context,
     &  status, 'rw')
      if ( status .ne. 0 ) call ccperr(1,
     &  '*** Something seriously wrong with output CIF! ***')

      status = keep_context
      if ( entry .eq. ' ' ) entry = db(6:)
      call ccif_put_char('_entry.id', entry, context, status)
      call ccif_release_context(context)
      
      end


c============================================
      subroutine ccif_load(lognam, db, block)
c============================================

c      Trivial wrapper for CCIF's ccif_load_cif routine.
c      Opens a CIF, and returns the block id of a named data block.
c      If the file has only one data block, its block id is returned,
c      regardless of the value of db.

c ...  Parameters
      character*(*) lognam, db
      integer block

c ...  local variables
      integer nblks, status
      character errmsg*160

c ... Commons
      integer cxin, cxout
      common /cifint/ cxin, cxout
      save /cifint/

      logical cifdic
      common /ccif/ cifdic
      save /ccif/

      if ( .not. cifdic ) then
        call ccif_init('CCIFDIC')
        cifdic = .true.
      endif
      
      call ccif_load_cif(lognam, nblks, status)
      if (nblks .ne. 1) then
        if (db .eq. ' ') then
c ___ ESV again...
           write (errmsg, 
     &      '(''*** Must specify name of data block in '',a,'' ***'')' )
     &      lognam
           call ccperr(1,errmsg)
        endif

        if ( status .ne. 0 ) print *, 'mmCIF loaded with syntax errors'

        
        call ccif_block_by_name(lognam, db, block, status)
        
        if ( status .ne. 1 ) then
c ___ ESV again...
           write (errmsg, '(''*** Cannot open block "'',a,''" ***'')') 
     &          db
           call ccperr(1,errmsg)
        endif
      else
        call ccif_block_by_index(lognam,1, db, block, status)
        if ( status .ne. 1 )
     &    call ccperr(1,
     &    '*** Cannot open first data block in XYZIN ***')
      endif

c ___ So that rdcoor can tell that the file has just been opened
      cxin = -1
      
      end


c     ============================
      subroutine ciffmt(unit,file)
c     ============================

c     Subroutine to read formats for CIF items, and set the output
c     formats

c ... Parameters
      character*(*) file
      integer unit

c ... Local variables
      character line*132, key*4, cvalue(10)*4
      integer ibeg(10), iend(10), ityp(10), idec(10), 
     & n, ifail, i, status
      real fvalue(10)
      logical lend
     
      ifail = 1
      call ccpdpn(unit, file, 'READONLY', 'F', 0, ifail)
      if ( ifail.eq.-1 ) then
        print'(3a)',' *** Cannot open CIF formats file ',file,' ***'
        print*,' Will use default formats for output'
        return
      endif

      i = 0

 10   read(unit, '(a)', end=99) line
      n = -6
      i = i + 1
      call parse(line, ibeg, iend, ityp, fvalue, cvalue, idec, n)
      if (n.eq.0) goto 10
      if (n.ne.5) then
        print'(a,i4,a)',' *** Error in CIF formats file at line ',i,
     &    ' ***'
        goto 10
      endif

      if ( ibeg(5).eq.iend(5) .and. line(ibeg(5):iend(5)).eq.'.' ) 
     & line(ibeg(5):iend(5)) = ' '

      call ccif_output_fmt(line(ibeg(1):iend(1)),
     & line(ibeg(5):iend(5)) , 
     & int(fvalue(3)+0.5), int(fvalue(4)+0.5),
     & line(ibeg(2):iend(2)), status)

      goto 10

 99   close(unit)
      end
      
      
      



c     ================================
      subroutine ckatyp(attyp, ichrge)
c     ================================
      
c_____subroutine to parse an element/charge symbol from a CIF, and
c     return the position at which the charge compnent starts.
c     Not necessary for the new (Feb. 1996) PDB format, since this
c     is fixed format.

c ... Parameters
      character*(*) attyp
      integer ichrge


c ... Local variables
      integer i

      ichrge = 0
      do 10 i = 1, len(attyp)
        if ( lge(attyp(i:i),'0') .and. lle(attyp(i:i),'9') ) then
          ichrge = i
          return
        endif
 10   continue
      end

           

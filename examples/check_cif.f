c                                                 -*- Fortran -*-
c     $Id$

c     *****************************************************************
c
c     Copyright (c) 1995 - 2004, EMBL, Peter Keller
c     
c     CCIF: a library to access and output data conforming to the
c     CIF (Crystallographic Information File) and mmCIF (Macromolecular
c     CIF) specifications, and other related specifications.
c     
c     This library is free software: you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public License
c     version 3, modified in accordance with the provisions of the 
c     license to address the requirements of UK law.

c     You should have received a copy of the modified GNU Lesser General 
c     Public License along with this library.  If not, copies may be 
c     downloaded from http://www.ccp4.ac.uk/ccp4license.php

c     This program is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU Lesser General Public License for more details.
c
c     *****************************************************************

c     Quick test program to open a mmCIF, check its syntax
c     and if required attempt to open a context on a category.
      character*100 filename

c     mmCIF stuff declarations
      integer nblocks, blockid, status, lblockname, icntxt
      character*80 blockname, catnam, cvalue
      character tmpval*80, mmcif_catgy*80, ccif_err*80
      integer tmplen,i,istat,l, estatus

      logical nocatok

c     external iargc
      integer iargc

      if ( iargc() .lt. 1 ) then
         print*, 'Usage: <prog> <file> [ [ -s ] <category> ... ]'
         print*, '   -s means that when a category is not present'
         print*, '   in the mmCIF, this does not count as an error'
         stop
      end if
      call getarg(1, filename)


      write(6,*)  '----- Checking file ' // filename

c     Initialise/load mmCIF dictionary
      call ccif_init('MMCIFDIC')
c     Stop on failure
      nblocks = 0

c     Turn off message about undefined items if it has not been set in
c     the environment
      call getenv('CCIF_NOITEMIP',ccif_err)
      if ( ccif_err .eq. ' ' ) call ccif_set_msg('CCIF_NOITEMIP',0,i)

      call ccif_load_cif(filename,nblocks,istat)
      if ( nblocks .gt. 1 ) then
         write(6,*)  nblocks, ' blocks in mmCIF!'
         stop
      endif 

      if (istat .gt. 0 ) then 
         write(6,*)  'mmCIF opened with STAR syntax errors'
         call flush(6)
         call exit(1)
      endif

      if ( istat .lt. 0 ) then
         write(6,*)  'Cannot open mmCIF: status ', istat
         call flush(6)
         call exit(1)
      endif 

      call ccif_block_by_index(filename, 1, blockname, blockid, status)
      lblockname = index(blockname,' ') - 1
      if ( status .ne. 1 ) then
         write(6,*)  'Cannot find datablock!'
         call flush(6)
         call exit(1)
      endif

      if ( lblockname .le. 0 ) lblockname = 80
      l = index(filename, ' ' ) -1

      write(6,*)  'Found datablock "',blockname(:lblockname),'" in ',
     &     filename(:l), '; id: ', blockid


      nocatok = .false.
      estatus = 0

      do 100 i=2,iargc()
         call getarg(i,mmcif_catgy)
         l = index(mmcif_catgy, ' ') - 1

c     If this is the '-s' argument, set nocatok to true and continue
         if ( i .eq. 2 .and. mmcif_catgy(:l) .eq. '-s' ) then
            nocatok = .true.
            goto 100
         endif

         
c     Set up CIF category
         call ccif_setup_context(mmcif_catgy, catnam, blockid, 
     &        icntxt, status, 'ro')
         if ( status .eq. -1 ) then
            write(6,*) 'Cannot set up context for ', mmcif_catgy
            write(6,*)  'Status: ', status
            estatus = 1
         elseif ( status .eq. 0 ) then
            write(6,*) 'Category ', mmcif_catgy(:l), ' not in CIF'
            if ( .not. nocatok ) estatus = 1
         else
            write(6, '(a,i4,2a)') 'Context', icntxt, ' set up on ', 
     &           mmcif_catgy(:l)
            call ccif_release_context(icntxt)
         endif

 100     continue

         call exit(estatus)

      end




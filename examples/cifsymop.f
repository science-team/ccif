      program cifsymop
c      $Id$

c     *****************************************************************
c
c     Copyright (c) 1995 - 2004, EMBL, Peter Keller
c     
c     CCIF: a library to access and output data conforming to the
c     CIF (Crystallographic Information File) and mmCIF (Macromolecular
c     CIF) specifications, and other related specifications.
c     
c     This library is free software: you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public License
c     version 3, modified in accordance with the provisions of the 
c     license to address the requirements of UK law.

c     You should have received a copy of the modified GNU Lesser General 
c     Public License along with this library.  If not, copies may be 
c     downloaded from http://www.ccp4.ac.uk/ccp4license.php

c     This program is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU Lesser General Public License for more details.
c
c     *****************************************************************

c      More formal example of a CCIF application. Looks for symmetry
c      information (first as space group number or name, then as
c      International Tables-style equivalent position notation).
c      Asks user which operation to apply.

c      If coordinates are in orthogonal cartesian Angstrom frame, looks
c      for orthogonalisation information (first as unit cell, then as
c      an explicit matrix), and calculates matrix for user-selected
c      symmetry operation in this coordinate frame. This program does not
c      make use of the DATABASE_PDB_MATRIX information, whose use seems to be
c      deprecated (see _category.description for this category in mmCIF)

c      In a real situation, the major sections of the preamble would appear
c      in subroutines, rather than at the same level as the main program,
c      but unrolling the indirection helps to illustrate the order in
c      which things are done.

c      Calls to CCIF and CCP4 routines are marked with
c      +'s and *'s respectively

c     Logical names required:
c        CCIFDIC     binary version of mmCIF prepared during the CCIF build
c        CIFIN       Input CIF      
c        CIFOUT      Output CIF

c     Optional logical names:
c        ASFMT       Format information for ATOM_SITE category output.
c                    An example of suitable input is in the accompanying
c                    file atom_site_fmt.lib . This kind of thing would
c                    go into $CLIBD eventually .


      INTEGER MaxSymmetry
      PARAMETER (MaxSymmetry=96)

      include 'cifget.inc'

      integer i,j


c______Set up names of mmCIF items which will be needed.
c      Note that we never need to specify the category
c      names explicitly.

      character*80 SpgNumItem, SpgNamHMItem, SpgNamHallItem
      data SpgNumItem, SpgNamHMItem, SpgNamHallItem
     &  /'_symmetry.Int_Tables_number',
     &  '_symmetry.space_group_name_H-M',
     &  '_symmetry.space_group_name_Hall' /

      character*80 SymEqPosItem
      data SymEqPosItem /'_symmetry_equiv.pos_as_xyz'/
c     This is used to try to find something like:
c
c        # Example for P21:
c        loop_
c         _symmetry_equiv.id
c         _symmetry_equiv.pos_as_xyz
c         1   'x,   y,       z'   2    '-x,  y+1/2,  -z'


c______Consistent with the CELL array from $CLIBS/rwbrook.f
      character*80 CellItems(6)
      data CellItems
     &  /'_cell.length_a',
     &  '_cell.length_b',
     &  '_cell.length_c',
     &  '_cell.angle_alpha',
     &  '_cell.angle_beta',
     &  '_cell.angle_gamma' /


c________The layout of these matrices is consistent with the
c        the arrays in the Orthog common block ($CLIBS/rwbrook.f)
      
      character*80 Trans2OrthoItem(4,4), Trans2FracItem(4,4)
      data ((Trans2OrthoItem(i,j), j=1,3), i=1,3),
     &  (Trans2OrthoItem(i,4), i=1,3),
     &  ((Trans2FracItem(i,j), j=1,3), i=1,3),
     &  (Trans2FracItem(i,4), i=1,3)
     &  
     &  /'_atom_sites.cartn_transf_matrix[1][1]',
     &  '_atom_sites.cartn_transf_matrix[1][2]',
     &  '_atom_sites.cartn_transf_matrix[1][3]',
     &  '_atom_sites.cartn_transf_matrix[2][1]',
     &  '_atom_sites.cartn_transf_matrix[2][2]',
     &  '_atom_sites.cartn_transf_matrix[2][3]',
     &  '_atom_sites.cartn_transf_matrix[3][1]',
     &  '_atom_sites.cartn_transf_matrix[3][2]',
     &  '_atom_sites.cartn_transf_matrix[3][3]',
     &  '_atom_sites.cartn_transf_vector[1]',
     &  '_atom_sites.cartn_transf_vector[2]',
     &  '_atom_sites.cartn_transf_vector[3]',
     &  '_atom_sites.frac_transf_matrix[1][1]',
     &  '_atom_sites.frac_transf_matrix[1][2]',
     &  '_atom_sites.frac_transf_matrix[1][3]',
     &  '_atom_sites.frac_transf_matrix[2][1]',
     &  '_atom_sites.frac_transf_matrix[2][2]',
     &  '_atom_sites.frac_transf_matrix[2][3]',
     &  '_atom_sites.frac_transf_matrix[3][1]',
     &  '_atom_sites.frac_transf_matrix[3][2]',
     &  '_atom_sites.frac_transf_matrix[3][3]',
     &  '_atom_sites.frac_transf_vector[1]',
     &  '_atom_sites.frac_transf_vector[2]',
     &  '_atom_sites.frac_transf_vector[3]' /


      character*80 CoordItem(2,3)
      data ((CoordItem(i,j), j=1,3), i=1,2)
     &  /'_atom_site.cartn_x',
     &  '_atom_site.cartn_y',
     &  '_atom_site.cartn_z',
     &  '_atom_site.fract_x',
     &  '_atom_site.fract_y',
     &  '_atom_site.fract_z'/


c_______Hooks into RWBROOK:

      real Cell, RR, Vol, CellAS
      common /RbrkZZ/ Cell(6),RR(3,3,6),Vol,CellAS(6)
      real Trans2Ortho, Trans2Frac
      integer Ncode, Brkfl
      common /Orthog/ Trans2Ortho(4,4), Trans2Frac(4,4), Ncode, Brkfl
      logical ifcrys, ifscal, ifend, matrix
      integer ityp
      common /RbrkXX/ ifcrys, ifscal, ifend, ityp, matrix
      save /RbrkZZ/, /Orthog/, /RbrkXX/

c_______Various internal variables.
      integer TransContext, SymContext, AtomContext, NBlocks,
     &  Block, NSym, Isym, SpgNum, coordSet, Status, esdStatus,
     &  NSymP, LSymEqPos, Natom, lspgnam
      real X(4), EsdX(4), XX(4), EsdXX(4), SymOps(4,4,MaxSymmetry),
     &  SymMat(4,4), WorkMat(4,4)
      character*80 token, tokenEsd, BlockName, SPgrp, SpgNamHM,
     &  SpgNamHall, CatName, PGName, SymEqPos, Name, Fmt
      logical putEsd



c        =============      
c________Startup stuff
c        =============      

c          ******
      call ccpfyp
c          ******


c          +++++++++++++++++
      call ccif_init('CCIFDIC')
c          +++++++++++++++++
      

c________Set up output formats for coordiates.

      call ciffmt(13,'ASFMT')

c          +++++++++++++
      call ccif_load_cif('CIFIN', NBlocks, status)
c          +++++++++++++

c            ******
      if (Nblocks .le. 0)
     &  call ccperr(1,' No data blocks in ''CIFIN''!')
c            ******
      if ( status .ne. 0 ) print *, 'Loaded with syntax errors'

      
c          ++++++++++++
      call ccif_new_cif('CIFIN', 'CIFOUT', NBlocks)
c          ++++++++++++


c_____If we have more than one block, ask user which one we want to operate on
      if (NBlocks.eq.1) then
c            +++++++++++++++++++        
        call ccif_block_by_index('CIFOUT', 1, BlockName, Block, Status)
c            +++++++++++++++++++
        print '('' Using data block '',a)',
     &    BlockName(:index(BlockName,' '))
      else
        print '('' Data block name: '',$)'
        read (*,'(a)') BlockName
c            ++++++++++++++++++
        call ccif_block_by_name('CIFOUT', BlockName, Block, Status, ' ')
c            ++++++++++++++++++
      endif

      if ( Status .le. 0 )
c            ******
     &  call ccperr(1, 'Cannot open block as requested')
c            ******

c_____CCIF variant of RBINIT. See below
      call ccif_rbinit


c      ==================
c______Find symmetry info
c      ==================

c          ++++++++++++++++++
      call ccif_setup_context(SpgNumItem, CatName, Block, SymContext,
     &  Status, 'ro')
c          ++++++++++++++++++

      goto (1010, 1020) Status
      
C       SYMMETRY category not present - try looking for equivalent positions
      goto 1100

C     Does this make sense in CIF terms? Who knows?
 1010 print*,' Space group info present as looped data.'
      print*,' Using first row'

 1020 Status = keep_context
      SpgNum = 0
      SpgNamHM = ' '
      SpgNamHall = ' '


c-------Getting symetry information from SYMMETRY category
c       ==================================================

c_______Try for space group number first
c          ++++++++++++
      call ccif_get_int(SpgNumItem, token, SpgNum, SymContext, Status)
c          ++++++++++++

c_______need single_value or loop_value      
      if ( Status/2 .ne. value ) then

        Status = keep_context

c            +++++++++++++
        call ccif_get_char(SpgNamHMItem, token, Spgrp,
     &    LSpgNam, SymContext, Status)
c            +++++++++++++
      
        if ( Status/2 .eq. value ) then
C           Remove embedded spaces from space group name
          j = 0
          do 1030 i = 1, LSpgNam
            if (Spgrp(i:i) .ne. ' ') then
              j = j+ 1
              SpgNamHM(j:j) = Spgrp(i:i)
            endif
 1030       continue
              
        else
          Status = keep_context
c              +++++++++++++
          call ccif_get_char(SpgNamHallItem, token, SpgNamHall,
     &      LSpgNam, SymContext, Status)
c              +++++++++++++

C             No space group info present - look for sym ops instead
          if ( Status/2 .ne. value ) then
c                ++++++++++++++++++++
            call ccif_release_context(SymContext)
c                ++++++++++++++++++++
            goto 1100
          endif

C             Haven't implemented the Hall notation in this prog.
c              ******
          call ccperr(1,' Don''t understand Hall space group notation')
c              ******
        endif
      endif
      
c______At this point, we have either the space group number in SpgNum,
c      or the HM name in SpgNamHM. So....

c          ******
      call msymlb(13, SpgNum, SpgNamHM, PGName, NSymP, NSym, SymOps)
c          ******

c          ++++++++++++++++++++
      call ccif_release_context(SymContext)
c          ++++++++++++++++++++

      LSpgNam = index(SpgNamHM,' ')
      print*
      print*,' Symop information derived from SYMMETRY category:'
      print '(''  Space group '',a,'', number'',i4)',
     &  SpgNamHM(:LSpgNam), SpgNum
      print*

c_______Go to part for cell/matrix data.
      goto 2000



c______Get symmetry information using Int. Tables x,y,z notation from
c      SYMMETRY_EQUIV category.
c      ==============================================================

c          ++++++++++++++++++
 1100 call ccif_setup_context(SymEqPosItem, CatName, Block,
     &  SymContext, Status, 'ro')
c          ++++++++++++++++++

c                           ******
      if (Status.eq.0) call ccperr(1, 'No information on symmetry '//
     &  'operations - sorry!')
c                           ******

      Status = keep_context
      NSym = 0
      

c          +++++++++++++
 1110 call ccif_get_char(SymEqPosItem, token, SymEqPos, LSymEqPos,
     &  SymContext, Status)
c          +++++++++++++

      if ( Status .ne. end_of_context ) then

        if ( Status/2 .eq. value ) then
          NSym = NSym + 1

c              ******
          call symfr2(SymEqPos, 1, NSym, SymOps)
c              ******

        endif
        Status = advance_context
        goto 1110
      endif
      
c          ++++++++++++++++++++
 1199 call ccif_release_context(SymContext)
c          ++++++++++++++++++++

      print*
      print*,' Symmetry information derived from equivalent positions'
      print*,' given in the SYMMETRY_EQUIV category'
      print*

      

c         ============================
c_________Do we need cell/matrix info?
c         ============================

c          ++++++++++++++++++
 2000 call ccif_setup_context(CoordItem(1,1), CatName, Block,
     &  AtomContext, Status, ' ')
c          ++++++++++++++++++
      
      if ( Status .eq. 0 )
c            ******
     &  call ccperr(1, 'No ATOM_SITE category in ''' //
     &  BlockName(:index(BlockName,' ')) // '''!')
c            ******

c          +++++++++++++++++
      call ccif_get_real_esd(CoordItem(1,1), token, X(1),
     &  tokenEsd, EsdX(1), AtomContext, Status, EsdStatus)
c          +++++++++++++++++

c        coordSet: 1 => coords present as orthogonal; 2 => present as fract.
c        ------------------------------------------------------------------
      
      if (Status/2 .eq. value) then
        coordSet = 1
      else
        Status = keep_context

c            +++++++++++++++++
        call ccif_get_real_esd(CoordItem(2,1), token, X(1),
     &    tokenEsd, EsdX(1), AtomContext, Status, EsdStatus)
c            +++++++++++++++++

c                                     ******
        if (Status/2 .ne. value) call ccperr(1,
     &    'No coordiates in ATOM_SITE category!')
c                                     ******
        coordSet = 2
      endif


c______If coordinates are fractional, don't need to bother with
c      working out the orthogonal->fractional transformation.
c      N.B. The CCIF context AtomContext is left open on the first row,
c      for use further on.
      if ( coordSet .eq. 2 ) then
        print*
        print*,' The coordinates in CIFIN are fractional. Not looking'
        print*,' for orthogonal <-> fractional transformation'
        print*
        goto 3000
      endif


c______Looking for unit cell (mmCIF category CELL)
c      ===========================================

c          ++++++++++++++++++
      call ccif_setup_context(CellItems(1), CatName, Block,
     &  TransContext, Status, 'ro')
c          ++++++++++++++++++

      goto (2010, 2020) Status
      
c______CELL category not present - look for transformation matrix
c      in ATOM_SITES category instead
      goto 2500

C     Does this make sense in CIF terms? Who knows?
 2010 print*,' Cell info present as looped data.'
      print*,' Using first row'

 2020 continue
      do 2030 i=1,6
        Status = keep_context
c            +++++++++++++        
        call ccif_get_real(CellItems(i), token, Cell(i),
     &    TransContext, Status)
c            +++++++++++++        
        Status = Status/2


c_______Note that this allows for the angles not being present in
c       the CIF, when the default values of 90.0 will be retrieved
c       from the dictionary.
        
c              ******        
        if ( Status.eq.unknown .or. Status.eq.null)
     &    call ccperr(1,' Unit cell information incomplete!')
c              ******        

 2030 continue

c_______The next few lines come from RBROOK in $CLIBS/rwbrook.f

c          ******
      call rbfror
c          ******

      Ncode = 1
      do 2040 i = 1,3
        do 2050 j = 1,3
          Trans2Ortho(i,j) = rr(i,j,Ncode)
 2050   continue
 2040 continue

      Trans2Ortho(4,4) = 1.0
c          ******
      call rbrinv(Trans2Ortho, Trans2Frac)
c          ******

      ifcrys = .true.
      matrix = .true.
      
      print*
      print*,' Orthogonal <-> fractional transformations derived from'
      print*,' the CELL category. Cell constants are:'
      print'(1x,6f8.3)',(Cell(i),i=1,6)
      print*

      goto 2999



c______Looking for transformation matrix (mmCIF category ATOM_SITES)
c      ================================================================


c          ++++++++++++++++++
 2500 call ccif_setup_context(Trans2OrthoItem(1,1), CatName, Block,
     &  TransContext, Status, 'ro')
c          ++++++++++++++++++

      goto (2510, 2520) Status

c_______No ATOM_SITES category - barf.
c          ******
      call ccperr(1, ' No cell or transformation matrix given!')
c          ******

      
c______Sophisticated use of items like _atom_site.entry_id and
c      _atom_sites.entry_id would, in principle, allow mixing of different
c      types of coordinates within one data block. I'm assuming that this is
c      not going to be done - it would take a certain amount of work in the
c      internals of CCIF to allow this to be implemented neatly, I think.

C     Does this make sense in CIF terms? Who knows?
 2510 print*,' Transformation matrix info present as looped data.'
      print*,' Using first matrix.'

c_______Looking for orthogonal->fractional transformation matrix


c       N.B. The rotational part of the transformation matrices
c       must be specified completely, but the components of the
c       translation vectors each have a default value of 0.0 .
c       Checking for the status value in the way given, copes
c       with this seamlessly
      
 2520 do 2530 i = 1,3
        do 2540 j = 1,4
          Status = keep_context

c              +++++++++++++
          call ccif_get_real(Trans2FracItem(i,j), token,
     &      Trans2Frac(i,j), TransContext, Status)
c              +++++++++++++
          Status = Status/2

          if ( Status.eq.unknown .or. Status.eq.null ) then
c                ******
            call ccperr(1,' The transformation matrices are '//
     &      'incomplete or missing, and there is no cell information!')
c                ******
          endif
          
 2540   continue
 2530 continue


      Trans2Frac(4,4) = 1.0
c          ******
      call rbrinv(Trans2Frac, Trans2Ortho)
c          ******

c______Not really SCALEi cards, but we are using the ATOM_SITES
c      category more-or-less equivalently (apart from the volume
c      consistency check, at the moment)

      ifscal = .true.
      matrix = .true.

      print*
      print*,' Orthogonal -> fractional transformation matrices given'
      print*,' explicitly in the ATOM_SITES category'
      print*

      
c______Finish up coordinate conversion stuff.

c          ++++++++++++++++++++
 2999 call ccif_release_context(TransContext)
c          ++++++++++++++++++++



c         ===================================================      
c_________Now, we find out which symmetry operation to apply.
c         ===================================================      
      

c_________Output from symtrn is not the best for interactive Q/A,
c         but it's OK for this demo.

c          ******
3000  call symtrn(NSym, SymOps)
c          ******
      print*,' Which of these (real space) '//
     &  'symmetry operations should be applied?'
      print '('' Enter a number from 1 to'',i3,'': '',$)',Nsym
      read (*,*,err=3000) Isym
      if ( Isym.lt.1 .or. Isym.gt.Nsym ) goto 3000


      if ( coordSet .eq. 2 ) then
c       Fractional: copy symmetry ops into matrix/vector
        do 3010 i=1,3
          SymMat(i,4) = SymOps(i,4,Isym)
          do 3020 j=1,3
            SymMat(i,j) = SymOps(i,j,Isym)
 3020     continue
 3010   continue

        SymMat(4,4) = 1.0
        
      else
c       Orthogonal: Calculate symmetry operator matrix in orthogonal coordiate frame.

c________ Are there really no routines to do operations on 
c         4x4 matrices in libccp4.a already, or am I missing something
c         obvious?
c         The source for the matrix routines is further on.
c         <Sigh> Whatever happened to Fortran90, in which this kind of
c         manipulation is supposedly trivial? This *is* 1996, after all!

        call mmmul(WorkMat, Trans2Ortho, SymOps(1,1,Isym) )
        call mmmul(SymMat, WorkMat, Trans2Frac )

      endif
      
        

c        ================================================================
c________... and finally, we actually start doing something with atoms...      
c        ================================================================


      Status = keep_context
      NAtom = 0

c_______Apply translational part of operator to coordinates....
      XX(4) = 1.0
c      .... but not to esd's on coordinates
      EsdXX(4) = 0.0

 3100 Natom = Natom + 1
      putEsd = .false.

c_______Loop over x, y, z
      do 3200 i = 1,3
c            +++++++++++++++++
        call ccif_get_real_esd(CoordItem(coordSet,i), token, XX(i),
     &    tokenEsd, EsdXX(i), AtomContext, Status, esdStatus)
c            +++++++++++++++++
        if (Status/2 .ne. value) goto 3299
        putEsd = putEsd .or. (esdStatus/2 .eq. value) .or.
     &    (esdStatus .eq. parenthesis_esd)
        Status = keep_context
 3200 continue

c________if (putEsd), at least one of the coordinates for this atom
c        had an esd associated with it. We need to transform the
c        esd vector as well, and output all three components for this
c        atom. For the case where there are no esd's in the input file
c        at all, none will be output, which avoids inflating the output
c        file unnecessarily.
c        If this is unclear, try adding a parenthesised esd to one
c        coordinate of one atom, and note the difference which it makes
c        to the output.

c_______Generate new coords.
c        The contents of SymMat are appropriate to the type of
c        coordinates (orthogonal/fractional). A plug-in replacement
c        for RBROOK/WBROOK would have to convert each set of
c        coordinates to orthogonal, if appropriate, before returning.
c        Providing the common block /Orthog/, and calling rbfror,
c        means that this could be done with the cvfrac routine as-is.


      call mvmul(X, SymMat, XX)

      if (putEsd) then

c______Only apply rotational part to esd's. Take abs. val. just
c      before outputting them.

        call mvmul(EsdX, SymMat, EsdXX)

        do 3250 i = 1,3
          Status = keep_context
          EsdX(i) = abs(EsdX(i))
c               +++++++++++++++++
          call ccif_put_real_esd(CoordItem(coordSet,i), X(i),
     &    EsdX(i), AtomContext, Status, ' ')
c               +++++++++++++++++
 3250   continue

      else
        do 3260 i = 1,3
          Status = keep_context
c              +++++++++++++
          call ccif_put_real(CoordItem(coordSet,i), X(i),
     &      AtomContext, Status)
c              +++++++++++++
 3260   continue

      endif
      

c_______Loop around to next atom
      Status = advance_context
      goto 3100

 3299 if ( Status .eq. end_of_context ) then
        print*
        print '(1x,i5,'' loop packets processed'')', Natom-1
        print*
c            ++++++++++++++
        call ccif_print_cif('CIFOUT')
c            ++++++++++++++
c            ******
        call ccperr(0,' Normal Termination')
c            ******
      endif
      print
     &  '('' ***Something wrong with '',i5,''th atom - skipping'')',
     &  Natom
      Status = advance_context
      goto 3100
      
      end
      

c     Note - on certain compilers, these routines will run MUCH quicker, if
c     the DO loops are unrolled, and the multiplications written out
c     explicitly (As it is done in CVFRAC in $CLIBS/rwbrook.f)

c_____Where a,b and c are 4x4 matrices, calculates
c     a = bc 
      subroutine mmmul(a, b, c)
      real a(4,4), b(4,4), c(4,4)
      integer i,j,k

      do 10 i = 1,4
        do 20 j = 1,4
          a(i,j) = 0.0
          do 30 k = 1,4
            a(i,j) = a(i,j) + b(i,k)*c(k,j)
 30       continue
 20     continue
 10   continue
      end
      

c_____Where b is a 4x4 matrix, and a and c are 4-element vectors,
c     calculates a=bc
      subroutine mvmul(a, b, c)
      real a(4), b(4,4), c(4)
      integer i,j

      do 10 i = 1,4
        a(i) = 0.0
        do 20 j = 1,4
          a(i) = a(i) + b(i,j)*c(j)
 20     continue
 10   continue
      end



c     ============================
      subroutine ciffmt(unit,file)
c     ============================

c     Subroutine to read formats for CIF items, and set the output
c     formats

c ... Parameters
      character*(*) file
      integer unit

c ... Local variables
      character line*132, key*4, cvalue(10)*4
      integer ibeg(10), iend(10), ityp(10), idec(10), 
     & n, ifail, i, status
      real fvalue(10)
      logical lend
     
      ifail = 1
      call ccpdpn(unit, file, 'READONLY', 'F', 0, ifail)
      if ( ifail.eq.-1 ) then
        print'(3a)',' *** Cannot open CIF formats file ',file,' ***'
        print*,' Will use default formats for output'
        return
      endif

      i = 0

 10   read(unit, '(a)', end=99) line
      n = -6
      i = i + 1
      call parse(line, ibeg, iend, ityp, fvalue, cvalue, idec, n)
      if (n.eq.0) goto 10
      if (n.ne.5) then
        print'(a,i4,a)',' *** Error in CIF formats file at line ',i,
     &    ' ***'
        goto 10
      endif

      if ( ibeg(5).eq.iend(5) .and. line(ibeg(5):iend(5)).eq.'.' ) 
     & line(ibeg(5):iend(5)) = ' '

      call ccif_output_fmt(line(ibeg(1):iend(1)),
     & line(ibeg(5):iend(5)) , 
     & int(fvalue(3)+0.5), int(fvalue(4)+0.5),
     & line(ibeg(2):iend(2)), status)

      goto 10

 99   close(unit)
      end



c______Hack of RBINIT from RWBROOK. We can't call RBINIT as is, because
c      of the REWIND. Eventually, change it so that we can call
c      it as RBINIT(-1), and do
c
c         if (iun .gt. 0) rewind(iun)
c

      SUBROUTINE CCIF_RBINIT
C     ======================

C This subroutine is used to initialise values in the
C      common /rbrkxx/ before calling RBROOK
C
C
c______Hmmm, I compile using options which require every variable
c      to be declared. Personally, I think that everyone should
c      try it....
      real cell, cellas, rr, vol, ro, rf
      integer ityp, ncode, ibrkfl, i, j

      LOGICAL IFCRYS,IFSCAL,IFEND,MATRIX
      COMMON /RBRKXX/IFCRYS,IFSCAL,IFEND,ITYP,MATRIX
      CHARACTER*1 BROOK
      COMMON /RBRKYY/BROOK(80)
      COMMON /RBRKZZ/CELL(6),RR(3,3,6),VOL,CELLAS(6)
      COMMON /ORTHOG/RO(4,4),RF(4,4),NCODE,IBRKFL
      SAVE /RBRKXX/,/RBRKYY/,/RBRKZZ/,/ORTHOG/
C
C---- Set flags and default matrix
C
      IFCRYS=.FALSE.
      IFSCAL=.FALSE.
      IFEND=.FALSE.
      MATRIX=.FALSE.
      NCODE=0
      ITYP=0
      IBRKFL=0
C
C
      DO 20 I=1,3
      DO 21 J=I+1,4
      RO(I,J)=0.0
      RO(J,I)=0.0
      RF(I,J)=0.0
      RF(J,I)=0.0
21    CONTINUE
      RO(I,I)=1.0
      RF(I,I)=1.0
20    CONTINUE
C
C
      RO(4,4)=1.0
      RF(4,4)=1.0
      RETURN
      END
      

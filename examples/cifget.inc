c                                                        -*-Fortran-*-
c       $Id$

c     *****************************************************************
c
c     Copyright (c) 1995 - 2004, EMBL, Peter Keller
c     
c     CCIF: a library to access and output data conforming to the
c     CIF (Crystallographic Information File) and mmCIF (Macromolecular
c     CIF) specifications, and other related specifications.
c     
c     This library is free software: you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public License
c     version 3, modified in accordance with the provisions of the 
c     license to address the requirements of UK law.

c     You should have received a copy of the modified GNU Lesser General 
c     Public License along with this library.  If not, copies may be 
c     downloaded from http://www.ccp4.ac.uk/ccp4license.php

c     This program is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU Lesser General Public License for more details.
c
c     *****************************************************************

c_________Repeated declarations start here

c  Various mnemonic declarations:
      
c_______Input status values for data-retrieval routines (see value_manip.h)
      integer start_new_context, keep_context, advance_context,
     & append_row, advance_hold
      parameter (start_new_context=0, keep_context=1,
     &  advance_context=2, append_row=3, advance_hold=4)

c_______Returned values for setting up contexts (see value_manip.h)
      integer cat_not_present, loop_context, item_context
      parameter (cat_not_present = 0, loop_context = 1,
     &  item_context = 2)

c_______Returned status values from data-retrieval routines  (see value_manip.h)
      integer no_value, loop_value, single_value, end_of_context, 
     & single_dict, loop_dict, single_null, loop_null, loop_unknown, 
     & parenthesis_esd
      parameter (no_value=0, loop_unknown=1, single_value=2, 
     &  loop_value=3, single_dict=4, loop_dict=5, single_null=6,
     &  loop_null=7, end_of_context=8, parenthesis_esd=9)

c_______These are used to check the returned value of Status/2 from
c      the CCIF_GET_* routines. Do this when you don't need to
c      determine explicitly whether the value came from a
c      loop_ or not.
      integer unknown, value, dict, null
      parameter(unknown=0, value=1, dict=2, null=3)
    
    
      external ccperr
      
      integer cfllen
      parameter(cfllen=80)
 
      real rval, esd
      integer ival, ntval, itmpos, btype, sline, texlen, nline,
     & ncntxt, istat, istate, lenval
      parameter (ntval = 20, texlen = ntval*cfllen)
      character*(cfllen) cval, tval(ntval), itmnam, tex*(texlen)
      equivalence (tval,tex)
      common /cifn/ ival, rval, esd, btype, sline, itmpos, nline, 
     & ncntxt, istat, istate, lenval
      common /cifc/ cval, tval, itmnam

      save
      
c________Repeated declarations end here

c                                                   -*-Fortran-*-
c     $Id$

c     *****************************************************************
c
c     Copyright (c) 1995 - 2004, EMBL, Peter Keller
c     
c     CCIF: a library to access and output data conforming to the
c     CIF (Crystallographic Information File) and mmCIF (Macromolecular
c     CIF) specifications, and other related specifications.
c     
c     This library is free software: you can redistribute it and/or
c     modify it under the terms of the GNU Lesser General Public License
c     version 3, modified in accordance with the provisions of the 
c     license to address the requirements of UK law.

c     You should have received a copy of the modified GNU Lesser General 
c     Public License along with this library.  If not, copies may be 
c     downloaded from http://www.ccp4.ac.uk/ccp4license.php

c     This program is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU Lesser General Public License for more details.
c
c     *****************************************************************

      program cifsearch
      
c     Program to get coordinates of atoms in a residue specified by:
c        _atom_site.label_asym_id  (chain)
c        _atom_site.label_seq_id   (residue)
c
c     Logical names required:
c        CCIFDIC     binary version of mmCIF prepared during the CCIF build
c        CIFIN       Input CIF      


c     This syntax for include files seems to work on every system that
c     I've tried it on.
      include 'cifget.inc'
      
      integer i, blk_id, nblock, sindex, status, resno, lval
      character*(cfllen) symtab, val*733, esdval, blknam, catnam, 
     & ccode, pcode
      
      integer nkey, ckeylen
      parameter (nkey=5,ckeylen=80)
      integer ikey(nkey)
      real fkey(nkey)
      character ckey(nkey)*(ckeylen), ckeybuf*(ckeylen*nkey)
      equivalence (ckey, ckeybuf)

      real x,y,z,b,occ

c     call ccpfyp

      call ccif_malloc_trace()
      
c_______The symbol table contains dictionary information on each data item,
c       which is accessed with a hashed lookup on the data name.
      call ccif_init('CCIFDIC')
      
c ___ Read in CIF
      print '('' Loading CIF file....'',$)'
      call ccif_load_cif('CIFIN', nblock, istat)
      print '('' Done ['',i3,'' block(s)]'')', nblock
      if ( istat .ne. 0 ) print *, 'Loaded with syntax errors'

c ___ Sort on chain id, residue number and atom id.
      call ccif_setup_sort('ATOM_SITE', blk_id, sindex, status)
      call ccif_add_to_sort('_atom_site.label_asym_id', sindex, status)
      call ccif_add_to_sort('_atom_site.label_seq_id', sindex, status)
      call ccif_add_to_sort('_atom_site.label_atom_id', sindex, status)
      call ccif_do_sort(sindex, status)
      
c ___ Prepare to read atom data 
      call ccif_setup_context('ATOM_SITE', catnam, blk_id, 
     &      ncntxt, istat,' ')
      if ( istat.eq.cat_not_present )
     1  print*,' Category '//catnam(:index(catnam,' '))//
     2  ' not present in data block'

c ___ Specify that atoms should be traversed in order of the
c     sort set up above.
      status = 1
      call ccif_sort_context(ncntxt, sindex, status)
      istat = keep_context

c ___ Which chain/reside?
      print '(1x,a,$)', 'Chain id [case sensitive]: '
      read(*,'(a)') ckey(1)

      ckey(2) = ' '
      print '(1x,a,$)', 'Residue id [integer]: '
      read(*,*) ikey(2)

c_______Force first atom in residue on search
      ckey(3) = '.'

c ___ Find first atom in residue
      call ccif_search_context(sindex, ncntxt, 3, ikey, fkey,
     &     ckeybuf, ckeylen, status)

      istat = keep_context
      call ccif_get_int('_atom_site.label_seq_id', 
     &     val, resno, ncntxt, istat)
      if ( resno.ne.ikey(2)) then
         print*,' Residue not found!'
         goto 100
      endif


c ___ Get residue type
      istat = keep_context
      call ccif_get_char('_atom_site.label_comp_id',
     &     val, cval, lval, ncntxt, istat)

      print '('' Residue type is: '',a/)', cval(:lval)

      istat = keep_context

c ===
c === Loop over atoms in residues:
c ===

c ___ If current atom is in next residue, exit loop
 20   call ccif_get_int('_atom_site.label_seq_id', 
     &     val, resno, ncntxt, istat)
      if ( resno.ne.ikey(2) .or. istat.eq.end_of_context) then
         print*,' No more atoms in this residue'
         goto 100
      endif

c ___ Get details of current atom
      istat = keep_context
      call ccif_get_char('_atom_site.label_atom_id',
     &     val, cval, lval, ncntxt, istat)
      istat = keep_context
      call ccif_get_real('_atom_site.cartn_x', 
     &     val, x, ncntxt, istat)
      istat = keep_context
      call ccif_get_real('_atom_site.cartn_y', 
     &     val, y, ncntxt, istat)
      istat = keep_context
      call ccif_get_real('_atom_site.cartn_z', 
     &     val, z, ncntxt, istat)
      istat = keep_context
      call ccif_get_real('_atom_site.occupancy', 
     &     val, occ, ncntxt, istat)
      istat = keep_context
      call ccif_get_real('_atom_site.b_iso_or_equiv', 
     &     val, b, ncntxt, istat)
      
      print '('' Atom "'', a, ''":'')', cval(:lval)
      print '('' Coordinates: '', 3f12.3)', x,y,z
      print '('' Occupancy: '',f6.2, '' B(iso): '',f12.3/)', occ, b
      
c ___ Loop to next atom.
      istat = advance_context
      goto 20

c ___ Clean up and exit.
 100  status = 2
      call ccif_release_sort(sindex, status)
      call ccif_release_context(ncntxt)

      status = 2
      call ccif_close_cif('CIFIN', status)

c     call ccperr(0, 'Normal termination')

      end





